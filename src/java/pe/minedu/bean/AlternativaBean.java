/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

/**
 *
 * @author Carlos
 */
public class AlternativaBean {
    
     private int id_alternativa;
     private int id_pregunta;
     private String descripcion;
     private String isRespuesta;
     private String descRespuesta;
     private String isCorrecto;
     
      public AlternativaBean() {
    }
   
    public int getId_alternativa() {
        return id_alternativa;
    }
    
    public void setId_alternativa(int id_alternativa) {
        this.id_alternativa = id_alternativa;
    }

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

  

    public String getIsRespuesta() {
        return isRespuesta;
    }

    public void setIsRespuesta(String isRespuesta) {
        this.isRespuesta = isRespuesta;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the descRespuesta
     */
    public String getDescRespuesta() {
        return descRespuesta;
    }

    /**
     * @param descRespuesta the descRespuesta to set
     */
    public void setDescRespuesta(String descRespuesta) {
        this.descRespuesta = descRespuesta;
    }

    /**
     * @return the isCorrecto
     */
    public String getIsCorrecto() {
        return isCorrecto;
    }

    /**
     * @param isCorrecto the isCorrecto to set
     */
    public void setIsCorrecto(String isCorrecto) {
        this.isCorrecto = isCorrecto;
    }

}
