
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;

public class MonitoreoClaseEstudianteBean implements Serializable {
    
    private int idEstudiante;
    private String codigoEstudiante;
    private String nombres;
    private Date fechaLogeo;
    private String ip;
    private boolean bloqueado;

    public boolean getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public void setCodigoEstudiante(String codigoEstudiante) {
        this.codigoEstudiante = codigoEstudiante;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Date getFechaLogeo() {
        return fechaLogeo;
    }

    public void setFechaLogeo(Date fechaLogeo) {
        this.fechaLogeo = fechaLogeo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "MonitoreoClaseEstudianteBean{" + "idEstudiante=" + idEstudiante + ", codigoEstudiante=" + codigoEstudiante + ", nombres=" + nombres + ", fechaLogeo=" + fechaLogeo + ", ip=" + ip + '}';
    }
    
}
