
package pe.minedu.bean;

import java.io.Serializable;

public class ControlEstudianteBean implements Serializable{
    private int idEstudiante; 
    private String nombre; 
    private String apellidoPaterno; 
    private String apellidoMaterno; 
    private String codEstudiante;
    private String usuario; 
    private String password; 
    private String ultimoDiaLog; 
    private String ultimoDiaIp; 
    private String estado; 
    private String genero; 
    private int idSeccion;

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCodEstudiante() {
        return codEstudiante;
    }

    public void setCodEstudiante(String codEstudiante) {
        this.codEstudiante = codEstudiante;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUltimoDiaLog() {
        return ultimoDiaLog;
    }

    public void setUltimoDiaLog(String ultimoDiaLog) {
        this.ultimoDiaLog = ultimoDiaLog;
    }

    public String getUltimoDiaIp() {
        return ultimoDiaIp;
    }

    public void setUltimoDiaIp(String ultimoDiaIp) {
        this.ultimoDiaIp = ultimoDiaIp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(int idSeccion) {
        this.idSeccion = idSeccion;
    }
    
}
