
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class CursoBean implements Serializable {
    
    private Integer idCurso;
    private String codigoCurso;
    private String nombreCurso;
    private String curricula;
    private String areaCurricular;
    private String estado;
    private List<CursoDocenteBean> cursoDocenteList;
    private GradoBean grado;
    private List<SesionBean> sesionList;
    private String codigoGrado;
    
    public CursoBean() {
    }

    public CursoBean(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public CursoBean(Integer idCurso, String codigoCurso, String nombreCurso) {
        this.idCurso = idCurso;
        this.codigoCurso = codigoCurso;
        this.nombreCurso = nombreCurso;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getCurricula() {
        return curricula;
    }

    public void setCurricula(String curricula) {
        this.curricula = curricula;
    }

    public String getAreaCurricular() {
        return areaCurricular;
    }

    public void setAreaCurricular(String areaCurricular) {
        this.areaCurricular = areaCurricular;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<CursoDocenteBean> getCursoDocenteList() {
        return cursoDocenteList;
    }

    public void setCursoDocenteList(List<CursoDocenteBean> cursoDocenteList) {
        this.cursoDocenteList = cursoDocenteList;
    }

    public GradoBean getGrado() {
        return grado;
    }

    public void setGrado(GradoBean grado) {
        this.grado = grado;
    }

    public List<SesionBean> getSesionList() {
        return sesionList;
    }

    public void setSesionList(List<SesionBean> sesionList) {
        this.sesionList = sesionList;
    }

    public String getCodigoGrado() {
        return codigoGrado;
    }

    public void setCodigoGrado(String codigoGrado) {
        this.codigoGrado = codigoGrado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCurso != null ? idCurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CursoBean)) {
            return false;
        }
        CursoBean other = (CursoBean) object;
        if ((this.idCurso == null && other.idCurso != null) || (this.idCurso != null && !this.idCurso.equals(other.idCurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Curso[ idCurso=" + idCurso + " ]";
    }
    
}
