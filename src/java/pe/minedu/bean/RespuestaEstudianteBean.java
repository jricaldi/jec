/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

/**
 *
 * @author Carlos
 */
public class RespuestaEstudianteBean {
    private int id_ficha_estudiante;
    private int id_pregunta;
    private int id_alternativa;
    private String descRespuesta;
    private String isCorrecto;
    
    public RespuestaEstudianteBean(){
        
    }

    /**
     * @return the id_pregunta
     */
    public int getId_pregunta() {
        return id_pregunta;
    }

    /**
     * @param id_pregunta the id_pregunta to set
     */
    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    /**
     * @return the id_alternativa
     */
    public int getId_alternativa() {
        return id_alternativa;
    }

    /**
     * @param id_alternativa the id_alternativa to set
     */
    public void setId_alternativa(int id_alternativa) {
        this.id_alternativa = id_alternativa;
    }

    /**
     * @return the descRespuesta
     */
    public String getDescRespuesta() {
        return descRespuesta;
    }

    /**
     * @param descRespuesta the descRespuesta to set
     */
    public void setDescRespuesta(String descRespuesta) {
        this.descRespuesta = descRespuesta;
    }

    /**
     * @return the isCorrecto
     */
    public String getIsCorrecto() {
        return isCorrecto;
    }

    /**
     * @param isCorrecto the isCorrecto to set
     */
    public void setIsCorrecto(String isCorrecto) {
        this.isCorrecto = isCorrecto;
    }

    /**
     * @return the id_ficha_estudiante
     */
    public int getId_ficha_estudiante() {
        return id_ficha_estudiante;
    }

    /**
     * @param id_ficha_estudiante the id_ficha_estudiante to set
     */
    public void setId_ficha_estudiante(int id_ficha_estudiante) {
        this.id_ficha_estudiante = id_ficha_estudiante;
    }
}
