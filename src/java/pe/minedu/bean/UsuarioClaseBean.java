/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;
public class UsuarioClaseBean implements Serializable {
    
    private Integer idUsuario;
    private String ip;
    private Date fecha;
    private UsuarioBean usuario;
    private ClaseBean clase;
    private Integer idClase;
    private String idSesion;

    public UsuarioClaseBean() {
    }

    public UsuarioClaseBean(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public UsuarioBean getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioBean usuario) {
        this.usuario = usuario;
    }

    public ClaseBean getClase() {
        return clase;
    }

    public void setClase(ClaseBean clase) {
        this.clase = clase;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioClaseBean)) {
            return false;
        }
        UsuarioClaseBean other = (UsuarioClaseBean) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.UsuarioClase[ idUsuario=" + idUsuario + " ]";
    }

    /**
     * @return the idSesion
     */
    public String getIdSesion() {
        return idSesion;
    }

    /**
     * @param idSesion the idSesion to set
     */
    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

  
    
}
