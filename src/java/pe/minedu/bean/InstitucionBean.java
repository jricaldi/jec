
package pe.minedu.bean;

import java.io.Serializable;

public class InstitucionBean implements Serializable {
    
    private Integer idInstitucion;
    private String codigoInstitucion;
    private String razonSocial;
    private String sede;
    private String departamento;
    private String provincia;
    private String distrito;
    private String direccionRegional;
    private String ugel;
    private String direccion;
    private String areaGeografica;
    private String director;
    private String ipServidor1;
    private String ipServidor2;

    public InstitucionBean() {
    }

    public InstitucionBean(Integer idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public InstitucionBean(Integer idInstitucion, String codigoInstitucion, String razonSocial, String sede, String departamento, String provincia, String distrito, String direccionRegional, String ugel, String direccion, String director, String ipServidor1) {
        this.idInstitucion = idInstitucion;
        this.codigoInstitucion = codigoInstitucion;
        this.razonSocial = razonSocial;
        this.sede = sede;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.direccionRegional = direccionRegional;
        this.ugel = ugel;
        this.direccion = direccion;
        this.director = director;
        this.ipServidor1 = ipServidor1;
    }

    public Integer getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(Integer idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public String getCodigoInstitucion() {
        return codigoInstitucion;
    }

    public void setCodigoInstitucion(String codigoInstitucion) {
        this.codigoInstitucion = codigoInstitucion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(String direccionRegional) {
        this.direccionRegional = direccionRegional;
    }

    public String getUgel() {
        return ugel;
    }

    public void setUgel(String ugel) {
        this.ugel = ugel;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getAreaGeografica() {
        return areaGeografica;
    }

    public void setAreaGeografica(String areaGeografica) {
        this.areaGeografica = areaGeografica;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getIpServidor1() {
        return ipServidor1;
    }

    public void setIpServidor1(String ipServidor1) {
        this.ipServidor1 = ipServidor1;
    }

    public String getIpServidor2() {
        return ipServidor2;
    }

    public void setIpServidor2(String ipServidor2) {
        this.ipServidor2 = ipServidor2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInstitucion != null ? idInstitucion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstitucionBean)) {
            return false;
        }
        InstitucionBean other = (InstitucionBean) object;
        if ((this.idInstitucion == null && other.idInstitucion != null) || (this.idInstitucion != null && !this.idInstitucion.equals(other.idInstitucion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Institucion[ idInstitucion=" + idInstitucion + " ]";
    }
    
}
