
package pe.minedu.bean;

import java.io.Serializable;

public class ClaseEstudiantePKBean implements Serializable {
    private Integer idEstudiante;
    private Integer idClase;

    public ClaseEstudiantePKBean() {
    }

    public ClaseEstudiantePKBean(Integer idEstudiante, Integer idClase) {
        this.idEstudiante = idEstudiante;
        this.idClase = idClase;
    }

    public Integer getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Integer idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idEstudiante;
        hash += (int) idClase;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseEstudiantePKBean)) {
            return false;
        }
        ClaseEstudiantePKBean other = (ClaseEstudiantePKBean) object;
        if (this.idEstudiante != other.idEstudiante) {
            return false;
        }
        if (this.idClase != other.idClase) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.ClaseEstudiantePK[ idEstudiante=" + idEstudiante + ", idClase=" + idClase + " ]";
    }
    
}
