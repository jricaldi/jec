/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

/**
 *
 * @author Carlos
 */
public class FichaDownloadBean {
    
    
    private String nombreFicha;
    private String cuerpoFicha;
    
    public FichaDownloadBean(String nombreFicha, String cuerpoFicha) {
     super();
     this.nombreFicha = nombreFicha;
     this.cuerpoFicha = cuerpoFicha;
     }
    /**
     * @return the nombreFicha
     */
    public String getNombreFicha() {
        return nombreFicha;
    }

    /**
     * @param nombreFicha the nombreFicha to set
     */
    public void setNombreFicha(String nombreFicha) {
        this.nombreFicha = nombreFicha;
    }

    /**
     * @return the cuerpoFicha
     */
    public String getCuerpoFicha() {
        return cuerpoFicha;
    }

    /**
     * @param cuerpoFicha the cuerpoFicha to set
     */
    public void setCuerpoFicha(String cuerpoFicha) {
        this.cuerpoFicha = cuerpoFicha;
    }
}
