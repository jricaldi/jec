
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class GradoBean implements Serializable {
    
    private String codigoGrado;
    private Integer idGrado;
    private String nivelPrograma;
    private Integer periodoEscolar;
    private String atencion;
    private List<SeccionBean> seccionList;
    private List<CursoBean> cursoList;
    private String descChoice;
    
    public GradoBean() {
    }

    public GradoBean(String codigoGrado) {
        this.codigoGrado = codigoGrado;
    }

    public GradoBean(String codigoGrado, Integer idGrado, String nivelPrograma) {
        this.codigoGrado = codigoGrado;
        this.idGrado = idGrado;
        this.nivelPrograma = nivelPrograma;
    }

    public String getCodigoGrado() {
        return codigoGrado;
    }

    public void setCodigoGrado(String codigoGrado) {
        this.codigoGrado = codigoGrado;
    }

    public Integer getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(Integer idGrado) {
        this.idGrado = idGrado;
    }

    public String getNivelPrograma() {
        return nivelPrograma;
    }

    public void setNivelPrograma(String nivelPrograma) {
        this.nivelPrograma = nivelPrograma;
    }

    public Integer getPeriodoEscolar() {
        return periodoEscolar;
    }

    public void setPeriodoEscolar(Integer periodoEscolar) {
        this.periodoEscolar = periodoEscolar;
    }

    public String getAtencion() {
        return atencion;
    }

    public void setAtencion(String atencion) {
        this.atencion = atencion;
    }

    public List<SeccionBean> getSeccionList() {
        return seccionList;
    }

    public void setSeccionList(List<SeccionBean> seccionList) {
        this.seccionList = seccionList;
    }

    public List<CursoBean> getCursoList() {
        return cursoList;
    }

    public void setCursoList(List<CursoBean> cursoList) {
        this.cursoList = cursoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoGrado != null ? codigoGrado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradoBean)) {
            return false;
        }
        GradoBean other = (GradoBean) object;
        if ((this.codigoGrado == null && other.codigoGrado != null) || (this.codigoGrado != null && !this.codigoGrado.equals(other.codigoGrado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Grado[ codigoGrado=" + codigoGrado + " ]";
    }

    /**
     * @return the descChoice
     */
    public String getDescChoice() {
        return descChoice;
    }

    /**
     * @param descChoice the descChoice to set
     */
    public void setDescChoice(String descChoice) {
        this.descChoice = descChoice;
    }
    
}
