
package pe.minedu.bean;

import java.io.Serializable;

public class CursoDocentePKBean implements Serializable {
    
    private Integer idCurso;
    private Integer idDocente;

    public CursoDocentePKBean() {
    }

    public CursoDocentePKBean(Integer idCurso, Integer idDocente) {
        this.idCurso = idCurso;
        this.idDocente = idDocente;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Integer getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(Integer idDocente) {
        this.idDocente = idDocente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCurso;
        hash += (int) idDocente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CursoDocentePKBean)) {
            return false;
        }
        CursoDocentePKBean other = (CursoDocentePKBean) object;
        if (this.idCurso != other.idCurso) {
            return false;
        }
        if (this.idDocente != other.idDocente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.CursoDocentePK[ idCurso=" + idCurso + ", idDocente=" + idDocente + " ]";
    }
    
}
