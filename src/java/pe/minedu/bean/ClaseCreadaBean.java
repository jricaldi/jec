
package pe.minedu.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaseCreadaBean implements Serializable {
    
    private ClaseBean claseBean;
    private List<ClaseRecursoBean> claseRecursoBeans;

    public ClaseCreadaBean(ClaseBean claseBean, List<ClaseRecursoBean> claseRecursoBeans) {
        this.claseBean = claseBean;
        this.claseRecursoBeans = claseRecursoBeans;
    }

    public ClaseCreadaBean() {
        this.claseBean = new ClaseBean();
        this.claseRecursoBeans = new ArrayList<ClaseRecursoBean>();
    }
    
    public ClaseBean getClaseBean() {
        return claseBean;
    }

    public void setClaseBean(ClaseBean claseBean) {
        this.claseBean = claseBean;
    }

    public List<ClaseRecursoBean> getClaseRecursoBeans() {
        return claseRecursoBeans;
    }

    public void setClaseRecursoBeans(List<ClaseRecursoBean> claseRecursoBeans) {
        this.claseRecursoBeans = claseRecursoBeans;
    }

    @Override
    public String toString() {
        return "ClaseCreadaBean{" + "claseBean=" + claseBean + ", claseRecursoBeans=" + claseRecursoBeans.size() + '}';
    }
    
}
