
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class UsuarioBean implements Serializable {
    
    private Integer idUsuario;
    private String usuario;
    private String password;
    private String perfil;
    private Integer idDocente;
    private Integer idEstudiante;
    private String estado;
    private String ultimoDiaLog;
    private String ultimoDiaIp;
    private List<LogActividadesBean> logActividadesList;
    private UsuarioClaseBean usuarioClase;

    public UsuarioBean() {
    }

    public UsuarioBean(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public UsuarioBean(Integer idUsuario, String usuario, String password, String perfil) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.password = password;
        this.perfil = perfil;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Integer getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(Integer idDocente) {
        this.idDocente = idDocente;
    }

    public Integer getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Integer idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUltimoDiaLog() {
        return ultimoDiaLog;
    }

    public void setUltimoDiaLog(String ultimoDiaLog) {
        this.ultimoDiaLog = ultimoDiaLog;
    }

    public String getUltimoDiaIp() {
        return ultimoDiaIp;
    }

    public void setUltimoDiaIp(String ultimoDiaIp) {
        this.ultimoDiaIp = ultimoDiaIp;
    }

    public List<LogActividadesBean> getLogActividadesList() {
        return logActividadesList;
    }

    public void setLogActividadesList(List<LogActividadesBean> logActividadesList) {
        this.logActividadesList = logActividadesList;
    }

    public UsuarioClaseBean getUsuarioClase() {
        return usuarioClase;
    }

    public void setUsuarioClase(UsuarioClaseBean usuarioClase) {
        this.usuarioClase = usuarioClase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioBean)) {
            return false;
        }
        UsuarioBean other = (UsuarioBean) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Usuario[ idUsuario=" + idUsuario + " ]";
    }
    
}
