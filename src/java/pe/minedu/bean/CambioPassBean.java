
package pe.minedu.bean;

import java.io.Serializable;

public class CambioPassBean implements Serializable{
    private String perfil;
    private String password;
    private String nuevoPassword;
    private int idDocente;
    private int idEstudiante;
    private int idUsuario;
    private String usuario;

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNuevoPassword() {
        return nuevoPassword;
    }

    public void setNuevoPassword(String nuevoPassword) {
        this.nuevoPassword = nuevoPassword;
    }

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return "CambioPassBean{" + "perfil=" + perfil + ", password=" + password + ", nuevoPassword=" + nuevoPassword + ", idDocente=" + idDocente + ", idEstudiante=" + idEstudiante + ", idUsuario=" + idUsuario + ", usuario=" + usuario + '}';
    }
    
}
