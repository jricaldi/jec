
package pe.minedu.bean;

public class CursoDeDocenteBean {
    private int idCurso;
    private String nombreCurso;
    private String codigoGrado;
    private int idGrado;
    private String nivelPrograma;

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getCodigoGrado() {
        return codigoGrado;
    }

    public void setCodigoGrado(String codigoGrado) {
        this.codigoGrado = codigoGrado;
    }

    public int getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(int idGrado) {
        this.idGrado = idGrado;
    }

    public String getNivelPrograma() {
        return nivelPrograma;
    }

    public void setNivelPrograma(String nivelPrograma) {
        this.nivelPrograma = nivelPrograma;
    }

    @Override
    public String toString() {
        return "CursosDeDocenteBean{" + "idCurso=" + idCurso + ", nombreCurso=" + nombreCurso + ", codigoGrado=" + codigoGrado + ", idGrado=" + idGrado + ", nivelPrograma=" + nivelPrograma + '}';
    }
    
}
