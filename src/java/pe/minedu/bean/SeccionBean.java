
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class SeccionBean implements Serializable {
    
    private Integer idSeccion;
    private String nombreSeccion;
    private GradoBean grado;
    private List<EstudianteBean> estudianteList;
    private List<ClaseBean> claseList;
    private String codigoGrado;

    public SeccionBean() {
    }

    public SeccionBean(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public Integer getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getNombreSeccion() {
        return nombreSeccion;
    }

    public void setNombreSeccion(String nombreSeccion) {
        this.nombreSeccion = nombreSeccion;
    }

    public GradoBean getGrado() {
        return grado;
    }

    public void setGrado(GradoBean grado) {
        this.grado = grado;
    }

    public List<EstudianteBean> getEstudianteList() {
        return estudianteList;
    }

    public void setEstudianteList(List<EstudianteBean> estudianteList) {
        this.estudianteList = estudianteList;
    }

    public List<ClaseBean> getClaseList() {
        return claseList;
    }

    public void setClaseList(List<ClaseBean> claseList) {
        this.claseList = claseList;
    }

    public String getCodigoGrado() {
        return codigoGrado;
    }

    public void setCodigoGrado(String codigoGrado) {
        this.codigoGrado = codigoGrado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSeccion != null ? idSeccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeccionBean)) {
            return false;
        }
        SeccionBean other = (SeccionBean) object;
        if ((this.idSeccion == null && other.idSeccion != null) || (this.idSeccion != null && !this.idSeccion.equals(other.idSeccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Seccion[ idSeccion=" + idSeccion + " ]";
    }
    
}
