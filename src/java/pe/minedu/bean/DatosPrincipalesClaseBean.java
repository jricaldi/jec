
package pe.minedu.bean;

import java.io.Serializable;

public class DatosPrincipalesClaseBean implements Serializable{
    private String nombreDocente;
    private String nombreCurso;
    private String grado;
    private String seccion;

    public String getNombreDocente() {
        return nombreDocente;
    }

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    @Override
    public String toString() {
        return "DatosPrincipalesClaseBean{" + "nombreDocente=" + nombreDocente + ", nombreCurso=" + nombreCurso + ", grado=" + grado + ", seccion=" + seccion + '}';
    }
    
    
}
