
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ClaseBean implements Serializable {
    private Integer idClase;
    private Date horaInicio;
    private Date horaFin;
    private String estado;
    private List<ClaseRecursoBean> claseRecursoList;
    private List<LogActividadesBean> logActividadesList;
    private List<UsuarioClaseBean> usuarioClaseList;
    private List<ClaseEstudianteBean> claseEstudianteList;
    private SesionBean sesion;
    private SeccionBean seccion;
    private CursoDocenteBean cursoDocente;
    private String idSesion;
    private Integer idSeccion;
    private Integer idCurso;
    private Integer idDocente;
    

    public ClaseBean() {
    }

    public ClaseBean(Integer idClase) {
        this.idClase = idClase;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<ClaseRecursoBean> getClaseRecursoList() {
        return claseRecursoList;
    }

    public void setClaseRecursoList(List<ClaseRecursoBean> claseRecursoList) {
        this.claseRecursoList = claseRecursoList;
    }

    public List<LogActividadesBean> getLogActividadesList() {
        return logActividadesList;
    }

    public void setLogActividadesList(List<LogActividadesBean> logActividadesList) {
        this.logActividadesList = logActividadesList;
    }

    public List<UsuarioClaseBean> getUsuarioClaseList() {
        return usuarioClaseList;
    }

    public void setUsuarioClaseList(List<UsuarioClaseBean> usuarioClaseList) {
        this.usuarioClaseList = usuarioClaseList;
    }

    public List<ClaseEstudianteBean> getClaseEstudianteList() {
        return claseEstudianteList;
    }

    public void setClaseEstudianteList(List<ClaseEstudianteBean> claseEstudianteList) {
        this.claseEstudianteList = claseEstudianteList;
    }

    public SesionBean getSesion() {
        return sesion;
    }

    public void setSesion(SesionBean sesion) {
        this.sesion = sesion;
    }

    public SeccionBean getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionBean seccion) {
        this.seccion = seccion;
    }

    public CursoDocenteBean getCursoDocente() {
        return cursoDocente;
    }

    public void setCursoDocente(CursoDocenteBean cursoDocente) {
        this.cursoDocente = cursoDocente;
    }

    public String getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    public Integer getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Integer idSeccion) {
        this.idSeccion = idSeccion;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Integer getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(Integer idDocente) {
        this.idDocente = idDocente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClase != null ? idClase.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseBean)) {
            return false;
        }
        ClaseBean other = (ClaseBean) object;
        if ((this.idClase == null && other.idClase != null) || (this.idClase != null && !this.idClase.equals(other.idClase))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Clase[ idClase=" + idClase + " ]";
    }
    
}
