
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;

public class LogErroresBean implements Serializable {
    
    private Integer idLogErrores;
    private Date fecha;
    private String ip;
    private String modulo;
    private String mensaje;

    public LogErroresBean() {
    }

    public LogErroresBean(Integer idLogErrores) {
        this.idLogErrores = idLogErrores;
    }

    public Integer getIdLogErrores() {
        return idLogErrores;
    }

    public void setIdLogErrores(Integer idLogErrores) {
        this.idLogErrores = idLogErrores;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogErrores != null ? idLogErrores.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogErroresBean)) {
            return false;
        }
        LogErroresBean other = (LogErroresBean) object;
        if ((this.idLogErrores == null && other.idLogErrores != null) || (this.idLogErrores != null && !this.idLogErrores.equals(other.idLogErrores))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.LogErrores[ idLogErrores=" + idLogErrores + " ]";
    }
    
}
