/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

import java.util.List;

/**
 *
 * @author Carlos
 */
public class PreguntaBean {
    
     private int id_ficha;
     private String descripcion;
     private int id_pregunta;
     private List<AlternativaBean> alternativaBean;
     private int tipo_pregunta;
     
      public PreguntaBean() {
    }

    public int getId_ficha() {
        return id_ficha;
    }
   
    public void setId_ficha(int id_ficha) {
        this.id_ficha = id_ficha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the id_pregunta
     */
    public int getId_pregunta() {
        return id_pregunta;
    }

    /**
     * @param id_pregunta the id_pregunta to set
     */
    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    /**
     * @return the alternativaBean
     */
    public List<AlternativaBean> getAlternativaBean() {
        return alternativaBean;
    }

    /**
     * @param alternativaBean the alternativaBean to set
     */
    public void setAlternativaBean(List<AlternativaBean> alternativaBean) {
        this.alternativaBean = alternativaBean;
    }

    /**
     * @return the tipo_pregunta
     */
    public int getTipo_pregunta() {
        return tipo_pregunta;
    }

    /**
     * @param tipo_pregunta the tipo_pregunta to set
     */
    public void setTipo_pregunta(int tipo_pregunta) {
        this.tipo_pregunta = tipo_pregunta;
    }

 
   
}
