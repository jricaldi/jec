
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class AplicacionBean implements Serializable {
    
    private Integer idAplicacion;
    private String tipoAplicacion;
    private String nombreAplicacion;
    private String documentoAbrir;
    private String extensiones;
    private String estado;
    private List<ClaseRecursoBean> claseRecursoList;
    private List<SesionRecursoBean> sesionRecursoList;

    public AplicacionBean() {
    }

    public AplicacionBean(Integer idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public Integer getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Integer idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getTipoAplicacion() {
        return tipoAplicacion;
    }

    public void setTipoAplicacion(String tipoAplicacion) {
        this.tipoAplicacion = tipoAplicacion;
    }

    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

    public void setNombreAplicacion(String nombreAplicacion) {
        this.nombreAplicacion = nombreAplicacion;
    }

    public String getDocumentoAbrir() {
        return documentoAbrir;
    }

    public void setDocumentoAbrir(String documentoAbrir) {
        this.documentoAbrir = documentoAbrir;
    }

    public String getExtensiones() {
        return extensiones;
    }

    public void setExtensiones(String extensiones) {
        this.extensiones = extensiones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<ClaseRecursoBean> getClaseRecursoList() {
        return claseRecursoList;
    }

    public void setClaseRecursoList(List<ClaseRecursoBean> claseRecursoList) {
        this.claseRecursoList = claseRecursoList;
    }

    public List<SesionRecursoBean> getSesionRecursoList() {
        return sesionRecursoList;
    }

    public void setSesionRecursoList(List<SesionRecursoBean> sesionRecursoList) {
        this.sesionRecursoList = sesionRecursoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAplicacion != null ? idAplicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AplicacionBean)) {
            return false;
        }
        AplicacionBean other = (AplicacionBean) object;
        if ((this.idAplicacion == null && other.idAplicacion != null) || (this.idAplicacion != null && !this.idAplicacion.equals(other.idAplicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Aplicacion[ idAplicacion=" + idAplicacion + " ]";
    }
    
}
