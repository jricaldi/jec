
package pe.minedu.bean;

import java.io.Serializable;
import java.util.List;

public class CursoDocenteBean implements Serializable {
    
    protected CursoDocentePKBean cursoDocentePK;
    private DocenteBean docente;
    private CursoBean curso;
    private List<ClaseBean> claseList;
    private Integer idCurso;
    private Integer idDocente;

    public CursoDocenteBean() {
    }

    public CursoDocenteBean(CursoDocentePKBean cursoDocentePK) {
        this.cursoDocentePK = cursoDocentePK;
    }

    public CursoDocenteBean(int idCurso, int idDocente) {
        this.cursoDocentePK = new CursoDocentePKBean(idCurso, idDocente);
    }

    public CursoDocentePKBean getCursoDocentePK() {
        return cursoDocentePK;
    }

    public void setCursoDocentePK(CursoDocentePKBean cursoDocentePK) {
        this.cursoDocentePK = cursoDocentePK;
    }

    public DocenteBean getDocente() {
        return docente;
    }

    public void setDocente(DocenteBean docente) {
        this.docente = docente;
    }

    public CursoBean getCurso() {
        return curso;
    }

    public void setCurso(CursoBean curso) {
        this.curso = curso;
    }

    public List<ClaseBean> getClaseList() {
        return claseList;
    }

    public void setClaseList(List<ClaseBean> claseList) {
        this.claseList = claseList;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Integer getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(Integer idDocente) {
        this.idDocente = idDocente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cursoDocentePK != null ? cursoDocentePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CursoDocenteBean)) {
            return false;
        }
        CursoDocenteBean other = (CursoDocenteBean) object;
        if ((this.cursoDocentePK == null && other.cursoDocentePK != null) || (this.cursoDocentePK != null && !this.cursoDocentePK.equals(other.cursoDocentePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.CursoDocente[ cursoDocentePK=" + cursoDocentePK + " ]";
    }
    
}
