/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

import java.util.List;

/**
 *
 * @author Carlos
 */
public class FichaBean {
    
     private int id_ficha;
     private String descripcion;
     private String terminado;
     private String nota;
     private String estado;
     private String id_clase;
     private List<PreguntaBean> listaPregunas;
     private String id_sesion;
     
      public FichaBean() {
    }

    public int getId_ficha() {
        return id_ficha;
    }
   
    public void setId_ficha(int id_ficha) {
        this.id_ficha = id_ficha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the terminado
     */
    public String getTerminado() {
        return terminado;
    }

    /**
     * @param terminado the terminado to set
     */
    public void setTerminado(String terminado) {
        this.terminado = terminado;
    }

    /**
     * @return the nota
     */
    public String getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(String nota) {
        this.nota = nota;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the listaPregunas
     */
    public List<PreguntaBean> getListaPregunas() {
        return listaPregunas;
    }

    /**
     * @param listaPregunas the listaPregunas to set
     */
    public void setListaPregunas(List<PreguntaBean> listaPregunas) {
        this.listaPregunas = listaPregunas;
    }

    /**
     * @return the id_clase
     */
    public String getId_clase() {
        return id_clase;
    }

    /**
     * @param id_clase the id_clase to set
     */
    public void setId_clase(String id_clase) {
        this.id_clase = id_clase;
    }

    /**
     * @return the id_sesion
     */
    public String getId_sesion() {
        return id_sesion;
    }

    /**
     * @param id_sesion the id_sesion to set
     */
    public void setId_sesion(String id_sesion) {
        this.id_sesion = id_sesion;
    }

  
}
