/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

/**
 *
 * @author GustavoFelix
 */
public class ClaseActivaBean {
    
    private int idClase;
    private String nombreClase;

    public ClaseActivaBean() {
    }

    public int getIdClase() {
        return idClase;
    }

    public void setIdClase(int idClase) {
        this.idClase = idClase;
    }

    public String getNombreClase() {
        return nombreClase;
    }

    public void setNombreClase(String nombreClase) {
        this.nombreClase = nombreClase;
    }

    @Override
    public String toString() {
        return "ClaseActivaBean{" + "idClase=" + idClase + ", nombreClase=" + nombreClase + '}';
    }
    
}
