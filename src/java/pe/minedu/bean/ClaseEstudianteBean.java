
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;

public class ClaseEstudianteBean implements Serializable {
    
    protected ClaseEstudiantePKBean claseEstudiantePK;
    private Date fechaClase;
    private String estado;
    private EstudianteBean estudiante;
    private ClaseBean clase;
    private Integer idEstudiante;
    private Integer idClase;
    

    public ClaseEstudianteBean() {
    }

    public ClaseEstudianteBean(ClaseEstudiantePKBean claseEstudiantePK) {
        this.claseEstudiantePK = claseEstudiantePK;
    }

    public ClaseEstudianteBean(int idEstudiante, int idClase) {
        this.claseEstudiantePK = new ClaseEstudiantePKBean(idEstudiante, idClase);
    }

    public ClaseEstudiantePKBean getClaseEstudiantePK() {
        return claseEstudiantePK;
    }

    public void setClaseEstudiantePK(ClaseEstudiantePKBean claseEstudiantePK) {
        this.claseEstudiantePK = claseEstudiantePK;
    }

    public Date getFechaClase() {
        return fechaClase;
    }

    public void setFechaClase(Date fechaClase) {
        this.fechaClase = fechaClase;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public EstudianteBean getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(EstudianteBean estudiante) {
        this.estudiante = estudiante;
    }

    public ClaseBean getClase() {
        return clase;
    }

    public void setClase(ClaseBean clase) {
        this.clase = clase;
    }

    public Integer getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Integer idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claseEstudiantePK != null ? claseEstudiantePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseEstudianteBean)) {
            return false;
        }
        ClaseEstudianteBean other = (ClaseEstudianteBean) object;
        if ((this.claseEstudiantePK == null && other.claseEstudiantePK != null) || (this.claseEstudiantePK != null && !this.claseEstudiantePK.equals(other.claseEstudiantePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.ClaseEstudiante[ claseEstudiantePK=" + claseEstudiantePK + " ]";
    }
    
}
