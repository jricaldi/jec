/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.bean;

import java.util.List;

/**
 *
 * @author Carlos
 */
public class FichaEstudianteBean {
    
     private int id_ficha;     
     private int id_estudiante;
     private int id_clase;
     private String nota_final;     
     private String isTerminado;
     private int id_ficha_estudiante;
     
      public FichaEstudianteBean() {
    }

    public int getId_ficha() {
        return id_ficha;
    }
   
    public void setId_ficha(int id_ficha) {
        this.id_ficha = id_ficha;
    }

    /**
     * @return the id_estudiante
     */
    public int getId_estudiante() {
        return id_estudiante;
    }

    /**
     * @param id_estudiante the id_estudiante to set
     */
    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    /**
     * @return the nota_final
     */
    public String getNota_final() {
        return nota_final;
    }

    /**
     * @param nota_final the nota_final to set
     */
    public void setNota_final(String nota_final) {
        this.nota_final = nota_final;
    }

    /**
     * @return the id_clase
     */
    public int getId_clase() {
        return id_clase;
    }

    /**
     * @param id_clase the id_clase to set
     */
    public void setId_clase(int id_clase) {
        this.id_clase = id_clase;
    }

    /**
     * @return the isTerminado
     */
    public String getIsTerminado() {
        return isTerminado;
    }

    /**
     * @param isTerminado the isTerminado to set
     */
    public void setIsTerminado(String isTerminado) {
        this.isTerminado = isTerminado;
    }

    /**
     * @return the id_ficha_estudiante
     */
    public int getId_ficha_estudiante() {
        return id_ficha_estudiante;
    }

    /**
     * @param id_ficha_estudiante the id_ficha_estudiante to set
     */
    public void setId_ficha_estudiante(int id_ficha_estudiante) {
        this.id_ficha_estudiante = id_ficha_estudiante;
    }

   
}
