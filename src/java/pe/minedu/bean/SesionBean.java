
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SesionBean implements Serializable {
    
    private String idSesion;
    private String codigoSesion;
    private Date fechaReg;
    private String estado;
    private List<SesionRecursoBean> sesionRecursoList;
    private CursoBean curso;
    private List<ClaseBean> claseList;
    private Integer idCurso;

    public SesionBean() {
    }

    public SesionBean(String idSesion) {
        this.idSesion = idSesion;
    }

    public String getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    public String getCodigoSesion() {
        return codigoSesion;
    }

    public void setCodigoSesion(String codigoSesion) {
        this.codigoSesion = codigoSesion;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<SesionRecursoBean> getSesionRecursoList() {
        return sesionRecursoList;
    }

    public void setSesionRecursoList(List<SesionRecursoBean> sesionRecursoList) {
        this.sesionRecursoList = sesionRecursoList;
    }

    public CursoBean getCurso() {
        return curso;
    }

    public void setCurso(CursoBean curso) {
        this.curso = curso;
    }

    public List<ClaseBean> getClaseList() {
        return claseList;
    }

    public void setClaseList(List<ClaseBean> claseList) {
        this.claseList = claseList;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSesion != null ? idSesion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SesionBean)) {
            return false;
        }
        SesionBean other = (SesionBean) object;
        if ((this.idSesion == null && other.idSesion != null) || (this.idSesion != null && !this.idSesion.equals(other.idSesion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.Sesion[ idSesion=" + idSesion + " ]";
    }
    
}
