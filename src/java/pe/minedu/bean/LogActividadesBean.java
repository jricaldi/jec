
package pe.minedu.bean;

import java.io.Serializable;

public class LogActividadesBean implements Serializable {
    
    private Integer idLogActividades;
    private String fecha;
    private String actividad;
    private String modulo;
    private String ip;
    private ClaseBean clase;
    private UsuarioBean usuario;
    private Integer idClase;
    private Integer idUsuario;
    

    public LogActividadesBean() {
    }

    public LogActividadesBean(Integer idLogActividades) {
        this.idLogActividades = idLogActividades;
    }

    public Integer getIdLogActividades() {
        return idLogActividades;
    }

    public void setIdLogActividades(Integer idLogActividades) {
        this.idLogActividades = idLogActividades;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ClaseBean getClase() {
        return clase;
    }

    public void setClase(ClaseBean clase) {
        this.clase = clase;
    }

    public UsuarioBean getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioBean usuario) {
        this.usuario = usuario;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogActividades != null ? idLogActividades.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogActividadesBean)) {
            return false;
        }
        LogActividadesBean other = (LogActividadesBean) object;
        if ((this.idLogActividades == null && other.idLogActividades != null) || (this.idLogActividades != null && !this.idLogActividades.equals(other.idLogActividades))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.LogActividades[ idLogActividades=" + idLogActividades + " ]";
    }
    
}
