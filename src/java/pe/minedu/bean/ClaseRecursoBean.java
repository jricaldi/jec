
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;

public class ClaseRecursoBean implements Serializable {
    
    private Integer idClaseRecurso;
    private String tipoRecurso;
    private String titulo;
    private String categoria;
    private String descripcion;
    private Date fechaReg;
    private String ruta;
    private AplicacionBean aplicacion;
    private ClaseBean clase;
    private Integer idAplicacion;
    private Integer idClase;
    private String nombreAplicacion;

    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

    public void setNombreAplicacion(String nombreAplicacion) {
        this.nombreAplicacion = nombreAplicacion;
    }

    public ClaseRecursoBean() {
    }

    public ClaseRecursoBean(Integer idClaseRecurso) {
        this.idClaseRecurso = idClaseRecurso;
    }

    public Integer getIdClaseRecurso() {
        return idClaseRecurso;
    }

    public void setIdClaseRecurso(Integer idClaseRecurso) {
        this.idClaseRecurso = idClaseRecurso;
    }

    public String getTipoRecurso() {
        return tipoRecurso;
    }

    public void setTipoRecurso(String tipoRecurso) {
        this.tipoRecurso = tipoRecurso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public AplicacionBean getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(AplicacionBean aplicacion) {
        this.aplicacion = aplicacion;
    }

    public ClaseBean getClase() {
        return clase;
    }

    public void setClase(ClaseBean clase) {
        this.clase = clase;
    }

    public Integer getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Integer idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClaseRecurso != null ? idClaseRecurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseRecursoBean)) {
            return false;
        }
        ClaseRecursoBean other = (ClaseRecursoBean) object;
        if ((this.idClaseRecurso == null && other.idClaseRecurso != null) || (this.idClaseRecurso != null && !this.idClaseRecurso.equals(other.idClaseRecurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.ClaseRecurso[ idClaseRecurso=" + idClaseRecurso + " ]";
    }
    
}
