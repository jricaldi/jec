
package pe.minedu.bean;

import java.io.Serializable;
import java.util.Date;

public class SesionRecursoBean implements Serializable {
    
    private Integer idSesionRecurso;
    private String titulo;
    private String categoria;
    private String descripcion;
    private Date fechaReg;
    private String ruta;
    private String estado;
    private AplicacionBean aplicacion;
    private SesionBean sesion;
    private Integer idAplicacion;
    private String idSesion;
    private String verEstudiante;

    public SesionRecursoBean() {
    }

    public SesionRecursoBean(Integer idSesionRecurso) {
        this.idSesionRecurso = idSesionRecurso;
    }

    public SesionRecursoBean(Integer idSesionRecurso, Date fechaReg) {
        this.idSesionRecurso = idSesionRecurso;
        this.fechaReg = fechaReg;
    }

    public Integer getIdSesionRecurso() {
        return idSesionRecurso;
    }

    public void setIdSesionRecurso(Integer idSesionRecurso) {
        this.idSesionRecurso = idSesionRecurso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public AplicacionBean getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(AplicacionBean aplicacion) {
        this.aplicacion = aplicacion;
    }

    public SesionBean getSesion() {
        return sesion;
    }

    public void setSesion(SesionBean sesion) {
        this.sesion = sesion;
    }

    public Integer getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Integer idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    public String getVerEstudiante() {
        return verEstudiante;
    }

    public void setVerEstudiante(String verEstudiante) {
        this.verEstudiante = verEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSesionRecurso != null ? idSesionRecurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SesionRecursoBean)) {
            return false;
        }
        SesionRecursoBean other = (SesionRecursoBean) object;
        if ((this.idSesionRecurso == null && other.idSesionRecurso != null) || (this.idSesionRecurso != null && !this.idSesionRecurso.equals(other.idSesionRecurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.minedu.bean.SesionRecurso[ idSesionRecurso=" + idSesionRecurso + " ]";
    }
    
}
