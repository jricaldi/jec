package pe.minedu.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import pe.minedu.util.Constante;

public enum AccesoDB {
    INSTANCE;

    private static Connection cn = null;

    public static Connection getConnection() throws Exception {
        if (cn == null) {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = Constante.JEC_DATABASE_CONECTION;
            cn = DriverManager.getConnection(url, Constante.JEC_DATABASE_USER, Constante.JEC_DATABASSE_PASSWORD);
        }
        return cn;
    }
}
