/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import pe.minedu.util.Constante;

/**
 *
 * @author Eccopacondori
 */
public class PoolConnection {

    private static Connection connection = null;
    private static Integer numberConn = 0;
    private static final Integer limitConn = 2;

    public static Connection getConnection() throws Exception {
        if (connection == null) {
            connection=getDataSource().getConnection();
            numberConn++;
            if(numberConn>limitConn)System.out.println("ALERTA: Limite de conexion exedido, "+numberConn+" de "+limitConn);
        }else{
            
        }
        return connection;
    }

    private static DataSource getDataSource() {
        PoolProperties p = new PoolProperties();
        p.setUrl(Constante.JEC_DATABASE_CONECTION);
        p.setDriverClassName("com.mysql.jdbc.Driver");
        p.setUsername(Constante.JEC_DATABASE_USER);
        p.setPassword(Constante.JEC_DATABASSE_PASSWORD);
        //p.setJmxEnabled(true);
        //p.setTestWhileIdle(false);
        //p.setTestOnBorrow(true);
        //p.setValidationQuery("SELECT 1");
        //p.setTestOnReturn(false);
        //p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        //p.setMaxWait(10000);
        //p.setRemoveAbandonedTimeout(60);
        //p.setMinEvictableIdleTimeMillis(30000);
        //p.setMinIdle(10);
        //p.setLogAbandoned(true);
        //p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource dataSource = new DataSource();
        dataSource.setPoolProperties(p);
        return dataSource;
    }
}
