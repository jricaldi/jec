
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pe.minedu.bean.CursoDeDocenteBean;
import pe.minedu.jdbc.AccesoDB;

public class CursoDocenteDao {
    
    ResultSet resultado;
    Statement sentencia;
    /*
    public String agregarDocentesAsignadosACursos(List<CursoDocenteBean> lista){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO curso_docente "
                    + " (id_curso, id_docente) "
                    + " VALUES(?,?)";
            for(CursoDocenteBean cursoDocenteBean : lista){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, cursoDocenteBean.getIdCurso());
                preparedStatement.setInt(2, cursoDocenteBean.getIdDocente());
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error CursoDocenteDao.agregarDocentesAsignadosACursos " + e.getMessage();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(CursoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error CursoDocenteDao.agregarDocentesAsignadosACursos RollingBack " + ex.getMessage();
            }
        }
        return mensaje;
    }
    */
    public List<CursoDeDocenteBean> listCursosDeDocentexIdDocente(int idDocente){
        Connection con=null;
        List<CursoDeDocenteBean> cursosDocente = new ArrayList<>();
        CursoDeDocenteBean cursoDeDocenteBean = null;
        PreparedStatement ps = null;
        ResultSet rs=null;
        try {

            String sql = "SELECT c.id_curso, c.nombre_curso, g.codigo_grado, g.id_grado, g.nivel_programa " +
                "FROM view_curso_docente cd " +
                "INNER JOIN curso c ON cd.id_curso = c.id_curso " +
                "INNER JOIN grado g ON c.codigo_grado = g.codigo_grado " +
                "WHERE cd.id_docente = ?";
            
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idDocente);
            rs=ps.executeQuery();
            if(rs.next()){
                cursoDeDocenteBean = new CursoDeDocenteBean();
                cursoDeDocenteBean.setCodigoGrado(rs.getString("codigo_grado"));
                cursoDeDocenteBean.setIdCurso(rs.getInt("id_curso"));
                cursoDeDocenteBean.setIdGrado(rs.getInt("id_grado"));
                cursoDeDocenteBean.setNivelPrograma(rs.getString("nivel_programa"));
                cursoDeDocenteBean.setNombreCurso(rs.getString("nombre_curso"));
                cursosDocente.add(cursoDeDocenteBean);
            }
        } catch (Exception e) {
            System.out.println("Error CursoDocenteDao.listCursosDeDocentexIdDocente " + e.getMessage());
                e.printStackTrace();
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return cursosDocente;
    }
    
     public List<CursoDeDocenteBean> listaGradosByDocente(int idDocente){
        Connection con=null;
        List<CursoDeDocenteBean> cursosDocente = new ArrayList<>();
        CursoDeDocenteBean cursoDeDocenteBean = null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            
            
            String sql = "SELECT distinct g.codigo_grado, g.id_grado, g.nivel_programa " +
                         "FROM view_curso_docente cd " +
                         "INNER JOIN curso c ON cd.id_curso = c.id_curso " +
                         "INNER JOIN grado g ON c.codigo_grado = g.codigo_grado " +
                         "WHERE cd.id_docente = ?";
            
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idDocente);
            rs=ps.executeQuery();
            while(rs.next()){
                cursoDeDocenteBean = new CursoDeDocenteBean();
                cursoDeDocenteBean.setCodigoGrado(rs.getString("codigo_grado"));
                cursoDeDocenteBean.setIdGrado(rs.getInt("id_grado"));
                cursoDeDocenteBean.setNivelPrograma(rs.getString("nivel_programa"));
                cursosDocente.add(cursoDeDocenteBean);
            }
        } catch (Exception e) {
            System.out.println("Error CursoDocenteDao.listCursosDeDocentexIdDocente " + e.getMessage());
                e.printStackTrace();
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return cursosDocente;
    }
     public List<CursoDeDocenteBean> listaCursosByDocenteYGrado(int idDocente, String codGrado){
        Connection con=null;
        List<CursoDeDocenteBean> cursosDocente = new ArrayList<>();
        CursoDeDocenteBean cursoDeDocenteBean = null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {

            String sql = "SELECT c.nombre_curso, c.id_curso " +
                        " FROM view_curso_docente cd " +
                        " INNER JOIN curso c ON cd.id_curso = c.id_curso " +
                        " INNER JOIN grado g ON c.codigo_grado = g.codigo_grado " +
                        " WHERE cd.id_docente = ? " +
                        " AND g.codigo_grado = ?";
            
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idDocente);
            ps.setString(2, codGrado);
            System.out.println(sql+"-"+idDocente+"-"+codGrado);
            rs=ps.executeQuery();
            while(rs.next()){
                cursoDeDocenteBean = new CursoDeDocenteBean();
                cursoDeDocenteBean.setNombreCurso(rs.getString("nombre_curso"));
                cursoDeDocenteBean.setIdCurso(rs.getInt("id_curso"));
                cursosDocente.add(cursoDeDocenteBean);
            }
        } catch (Exception e) {
            System.out.println("Error CursoDocenteDao.listCursosDeDocentexIdDocente " + e.getMessage());
                e.printStackTrace();
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return cursosDocente;
    }
     
    /*
    public boolean existeRelacionDocenteCurso(CursoDocenteBean cursoDocenteBean){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int contar = -1;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT COUNT(*) contar FROM curso_docente WHERE id_curso = ? AND id_docente = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cursoDocenteBean.getIdCurso());
            preparedStatement.setInt(2, cursoDocenteBean.getIdDocente());
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                contar = resultado.getInt("contar");
            }
            return contar != 0;
        } catch (Exception e) {
            return false;
        }
    }
    */
}
