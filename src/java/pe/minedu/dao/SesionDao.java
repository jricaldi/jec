
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.DateUtil;

public class SesionDao {
    
    ResultSet resultado;
    Statement sentencia;
    
    public SesionBean agregarSesionBean(SesionBean sesionBean){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO sesion "
                    + " (id_sesion, id_curso, codigo_sesion, fecha_reg, estado) "
                    + " VALUES(?,?,?,?,?)";
            System.out.println("sesion " + sesionBean.getIdSesion());
            System.out.println("sesion_len " + sesionBean.getIdSesion().length());
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, sesionBean.getIdSesion());
            preparedStatement.setInt(2, sesionBean.getIdCurso());
            preparedStatement.setString(3, sesionBean.getCodigoSesion());
            preparedStatement.setDate(4, DateUtil.utilDateToSQLDate(sesionBean.getFechaReg()));
            preparedStatement.setString(5, sesionBean.getEstado());
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            sesionBean = null;
            if(connection != null) try {
                connection.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(CursoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        } 
        return sesionBean;
    }
    
    public SesionBean obtSesionxIdSesion(String idSesion){
        SesionBean sesionBean = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT "
                    + " id_sesion, id_curso, codigo_sesion, fecha_reg, estado "
                    + " FROM sesion "
                    + " WHERE id_sesion = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, idSesion);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                sesionBean = new SesionBean();
                sesionBean.setIdSesion(resultado.getString("id_sesion"));
                sesionBean.setIdCurso(resultado.getInt("id_curso"));
                sesionBean.setCodigoSesion(resultado.getString("codigo_sesion"));
                sesionBean.setEstado(resultado.getString("estado"));
                sesionBean.setFechaReg(DateUtil.utilDateToSQLDate(resultado.getDate("fecha_reg")));
            }
        } catch (Exception e) {
            sesionBean = null;
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }  
        return sesionBean;
    }
    
    public List<SesionBean> listSesionesxCurso(int idCurso){
        Connection con=null;
        List<SesionBean> sesionesxCurso = new ArrayList<SesionBean>();
        SesionBean sesionBean = null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            
            String sql = "SELECT id_sesion,id_curso,codigo_sesion,fecha_reg,estado " +
                "FROM sesion s " +
                "WHERE s.id_curso = ? ";
            
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idCurso);
            rs=ps.executeQuery();
            if(rs.next()){
                sesionBean = new SesionBean();
                sesionBean.setIdSesion(rs.getString("id_sesion"));
                sesionBean.setIdCurso(rs.getInt("id_curso"));
                sesionBean.setCodigoSesion(rs.getString("codigo_sesion"));
                sesionBean.setEstado(rs.getString("estado"));
                sesionesxCurso.add(sesionBean);
            }
        } catch (Exception e) {
            Logger.getLogger(SesionDao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }  
        
        return sesionesxCurso;
    }
    
      public List<CursoBean> getCursosByDocente(int idDocente) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<CursoBean> listaCursos = new ArrayList<CursoBean>();
        CursoBean curso = null;
        try {
            //cn = PoolConnection.getConnection();
            //cn.setReadOnly(true);
            cn = AccesoDB.getConnection();
            String sql = "SELECT    c.id_curso, c.nombre_curso "
                        + "FROM     curso c, view_curso_docente cd "
                        + "WHERE    c.id_curso    = cd.id_curso "
                        + "AND      cd.id_docente = ? "
                        + "ORDER BY c.nombre_curso ASC";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idDocente);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                curso = new CursoBean();
                curso.setIdCurso(resultado.getInt("id_curso"));
                curso.setNombreCurso(resultado.getString("nombre_curso"));              
                listaCursos.add(curso);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }  
        
        return listaCursos;
    }
    
     public List<GradoBean> getgGradosByCurso(int idCurso) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<GradoBean> listaGrados = new ArrayList<GradoBean>();
        GradoBean grado = null;
        try {
            //cn = PoolConnection.getConnection();
            //cn.setReadOnly(true);
            cn = AccesoDB.getConnection();
            String sql = "SELECT    g.codigo_grado, g.id_grado, g.nivel_programa "
                        + "FROM     grado g, curso c "
                        + "WHERE    g.codigo_grado  =   c.codigo_grado "
                        + "AND      c.id_curso      =   ? "
                        + "ORDER BY g.codigo_grado ASC";;
            ps = cn.prepareCall(sql);
            ps.setInt(1, idCurso);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                grado = new GradoBean();
                grado.setCodigoGrado(resultado.getString("codigo_grado"));
                grado.setDescChoice(resultado.getString("id_grado")+" "+resultado.getString("nivel_programa"));              
                listaGrados.add(grado);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }  
        
        return listaGrados;
    }
    
     public List<SeccionBean> getSeccionesByGrado(String idGrado) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<SeccionBean> listaSeccion = new ArrayList<SeccionBean>();
        SeccionBean seccion = null;
        try {
            //cn = PoolConnection.getConnection();
            //cn.setReadOnly(true);
            cn = AccesoDB.getConnection();
            String sql = "SELECT    s.id_seccion, s.nombre_seccion "
                        + "FROM     seccion s, grado g "
                        + "WHERE    s.codigo_grado  = g.codigo_grado "
                        + "AND      g.codigo_grado = ? "
                        + "ORDER BY s.nombre_seccion";
            ps = cn.prepareCall(sql);
            ps.setString(1, idGrado);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                seccion = new SeccionBean();
                seccion.setIdSeccion(resultado.getInt("id_seccion"));
                seccion.setNombreSeccion(resultado.getString("nombre_seccion"));              
                listaSeccion.add(seccion);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }  
        
        return listaSeccion;
    }
     public List<SesionBean> getSesionByCurso(int idCurso) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<SesionBean> listaSesion = new ArrayList<SesionBean>();
        SesionBean sesion = null;
        try {
            //cn = PoolConnection.getConnection();
            //cn.setReadOnly(true);
            cn = AccesoDB.getConnection();
            String sql = "SELECT     s.id_sesion, s.codigo_sesion "
                         + "FROM     sesion s, curso c "
                         + "WHERE    s.id_curso = c.id_curso "
                         + "AND      c.id_curso = ? "
                         + "ORDER BY s.codigo_sesion";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idCurso);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                sesion = new SesionBean();
                sesion.setIdSesion(resultado.getString("id_sesion"));
                sesion.setCodigoSesion(resultado.getString("codigo_sesion"));              
                listaSesion.add(sesion);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }  
        
        return listaSesion;
    }
     
       public void cerrarClase(String idClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "UPDATE clase set estado = 0 where id_clase = ?";
            ps = cn.prepareCall(sql);            
            ps.setString(1, idClase);            
            ps.executeUpdate();          
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }                
    }
     
}
