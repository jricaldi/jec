
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.CursoBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
public class CursoDao {
    
    ResultSet resultado;
    Statement sentencia;
    
    public String agregarCursos(List<CursoBean> lista){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO curso "
                    + " (id_curso, codigo_grado, codigo_curso, nombre_curso, "
                    + " curricula, area_curricular, estado ) "
                    + " VALUES(?,?,?,?,?,?,?)";
            for(CursoBean cursoBean : lista){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, obtIdTablaCurso());
                preparedStatement.setString(2, cursoBean.getCodigoGrado());
                preparedStatement.setString(3, cursoBean.getCodigoCurso());
                preparedStatement.setString(4, cursoBean.getNombreCurso());
                preparedStatement.setString(5, cursoBean.getCurricula());
                preparedStatement.setString(6, cursoBean.getAreaCurricular());
                preparedStatement.setString(7, cursoBean.getEstado());
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error CursoDao.agregarCursos " + e.getMessage();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(CursoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error CursoDao.agregarCursos RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public CursoBean obtCursoParaRegistroExcel(String codigoGrado, String codigoCurso){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        CursoBean cursoBean = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT id_curso, codigo_grado, codigo_curso, nombre_curso,  "
                    + " curricula, area_curricular, estado "
                    + " FROM curso "
                    + " WHERE codigo_grado = ? "
                    + " AND codigo_curso = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, codigoGrado);
            preparedStatement.setString(2, codigoCurso);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                cursoBean = new CursoBean();
                cursoBean.setIdCurso(resultado.getInt("id_curso"));
                cursoBean.setCodigoCurso(resultado.getString("codigo_curso"));
                cursoBean.setNombreCurso(resultado.getString("nombre_curso"));
                cursoBean.setCurricula(resultado.getString("curricula"));
                cursoBean.setAreaCurricular(resultado.getString("area_curricular"));
                cursoBean.setEstado(resultado.getString("estado"));
                cursoBean.setCodigoGrado(resultado.getString("codigo_grado"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return cursoBean;
    }
    
    private int obtIdTablaCurso(){
        int idClaseRecurso = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_curso) max_id "
                    + " FROM curso";
             
            ps = cn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            while(resultSet.next()){
                idClaseRecurso = resultSet.getInt("max_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
                try { if (resultSet != null) resultSet.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        idClaseRecurso++;
        return idClaseRecurso;
    }
    
}
