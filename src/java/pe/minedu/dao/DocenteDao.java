package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.DocenteBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

/**
 *
 * @author GustavoFelix
 */
public class DocenteDao implements Serializable {

    public DocenteDao() {
    }
    
    ResultSet resultado;
    Statement sentencia;

    public DocenteBean getDocente(int idDocente) {
        Connection con=null;
        DocenteBean docente=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {

            String sql = "SELECT * FROM docente WHERE id_docente=?";
            //con=PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idDocente);
            rs=ps.executeQuery();
            if(rs.next()){
                docente = new DocenteBean();
                docente.setIdDocente(rs.getInt("id_docente"));
                docente.setDni(rs.getString("dni"));
                docente.setCodDocente(rs.getString("cod_docente"));
                docente.setNombre(rs.getString("nombre"));
                docente.setApellidoPaterno(rs.getString("apellido_paterno"));
                docente.setApellidoMaterno(rs.getString("apellido_materno"));
                docente.setGenero(rs.getString("genero"));
                docente.setCorreo(rs.getString("correo"));
                docente.setTelefono(rs.getString("telefono"));
                docente.setEstado(rs.getString("estado"));
            }
            return docente;
        } catch (Exception ex) {
            Logger.getLogger(DocenteDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }
    
    public String agregarDocentes(List<DocenteBean> lista){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            
            String sql1 = "INSERT INTO docente "
                    + " (id_docente, dni, cod_docente, nombre, apellido_paterno, " 
                    + "apellido_materno, genero, correo, telefono, estado ) "
                    + " VALUES(?,?,?,?,?,?,?,?,?,?)";
            
            String sql2 = "INSERT INTO usuario "
                    + " (usuario, password, perfil, id_docente, " 
                    + "id_estudiante, estado, ultimo_dia_log, ultimo_dia_ip ) "
                    + " VALUES(?,?,?,?,?,?,?,?)";
            
            for(DocenteBean docenteBean: lista){
                int idDocente = obtIdTablaDocente();
                preparedStatement = connection.prepareStatement(sql1);
                preparedStatement.setInt(1, idDocente);
                preparedStatement.setString(2, docenteBean.getDni());
                preparedStatement.setString(3, docenteBean.getCodDocente());
                preparedStatement.setString(4, docenteBean.getNombre());
                preparedStatement.setString(5, docenteBean.getApellidoPaterno());
                preparedStatement.setString(6, docenteBean.getApellidoMaterno());
                preparedStatement.setString(7, docenteBean.getGenero());
                preparedStatement.setString(8, docenteBean.getCorreo());
                preparedStatement.setString(9, docenteBean.getTelefono());
                preparedStatement.setString(10, docenteBean.getEstado());
                preparedStatement.executeUpdate();
                
                preparedStatement = connection.prepareStatement(sql2);
                preparedStatement.setString(1, docenteBean.getUsuarioBean().getUsuario());
                preparedStatement.setString(2, docenteBean.getUsuarioBean().getPassword());
                preparedStatement.setString(3, docenteBean.getUsuarioBean().getPerfil());
                preparedStatement.setInt(4, idDocente);
                preparedStatement.setInt(5, docenteBean.getUsuarioBean().getIdEstudiante());
                preparedStatement.setString(6, docenteBean.getUsuarioBean().getEstado());
                preparedStatement.setString(7, docenteBean.getUsuarioBean().getUltimoDiaLog());
                preparedStatement.setString(8, docenteBean.getUsuarioBean().getUltimoDiaIp());
                preparedStatement.executeUpdate();
            }
            
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            e.printStackTrace();
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error DocenteDao.registrarDocentes " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error DocenteDao.registrarDocentes RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }

    public DocenteBean obtDocenteBeanxDni(String dni){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        DocenteBean docente = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT id_docente, dni, cod_docente, nombre, apellido_paterno, " 
                    + " apellido_materno, genero, correo, telefono, estado  FROM docente "
                    + " WHERE dni = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, dni);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                docente = new DocenteBean();
                docente.setIdDocente(resultado.getInt("id_docente"));
                docente.setDni(resultado.getString("dni"));
                docente.setCodDocente(resultado.getString("cod_docente"));
                docente.setNombre(resultado.getString("nombre"));
                docente.setApellidoPaterno(resultado.getString("apellido_paterno"));
                docente.setApellidoMaterno(resultado.getString("apellido_materno"));
                docente.setGenero(resultado.getString("genero"));
                docente.setCorreo(resultado.getString("correo"));
                docente.setTelefono(resultado.getString("telefono"));
                docente.setEstado(resultado.getString("estado"));
            }
        } catch (Exception e) {
            docente = null;
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return docente;
    }
    
    private int obtIdTablaDocente(){
        int idClaseRecurso = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_docente) max_id "
                    + " FROM docente";
             
            ps = cn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            while(resultSet.next()){
                idClaseRecurso = resultSet.getInt("max_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
                try { if (resultSet != null) resultSet.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        idClaseRecurso++;
        return idClaseRecurso;
    }
    
}
