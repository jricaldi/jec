/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.AlternativaBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.FichaBean;
import pe.minedu.bean.FichaEstudianteBean;
import pe.minedu.bean.PreguntaBean;
import pe.minedu.bean.RespuestaEstudianteBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

/**
 *
 * @author GustavoFelix
 */
public class CuestionarioDao implements Serializable {
    
    ResultSet resultado;
    Statement sentencia;
    
    public CuestionarioDao() {
    }
    /**
     * @method getCursosByDocente
     * @param idDocente
     * @return Lista de Cursos Asociados al docente
     */

   public List<FichaBean> getFichasByClase(int idClase, int idEstudiante) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<FichaBean> listaFichas = new ArrayList<FichaBean>();
        FichaBean ficha = null;
        try {           
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            System.out.println("idClase:"+idClase+"-idEstudiante:"+idEstudiante);
            String sql = "SELECT    c.id_ficha, d.descripcion, "
                        + "(SELECT isTerminado from ficha_estudiante where id_ficha = c.id_ficha AND id_estudiante = ? and id_clase = ? ) as terminado, "
                        + "(SELECT nota_final from ficha_estudiante where id_ficha = c.id_ficha AND id_estudiante = ? and id_clase = ? ) as nota "
                        + "FROM     ficha_clase c, clase_estudiante ce , ficha d "
                        + "WHERE    c.id_clase = ? "
                        + "AND      c.estado = '1' "
                        + "AND      c.id_clase = ce.id_clase " 
                        + "AND      d.id_ficha = c.id_ficha " 
                        + "AND      ce.id_estudiante = ? "
                        + "ORDER BY d.descripcion ASC";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idEstudiante);
            ps.setInt(2, idClase);
            ps.setInt(3, idEstudiante);
            ps.setInt(4, idClase);
            ps.setInt(5, idClase);
            ps.setInt(6, idEstudiante);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                ficha = new FichaBean();
                ficha.setId_ficha(resultado.getInt("id_ficha"));
                ficha.setDescripcion(resultado.getString("descripcion"));   
                ficha.setTerminado(resultado.getString("terminado"));
                ficha.setNota(resultado.getString("nota"));
                listaFichas.add(ficha);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return listaFichas;
    }
    
    public List<PreguntaBean> getPreguntasByFicha(int idFicha) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<PreguntaBean> listaPreguntas = new ArrayList<PreguntaBean>();
        PreguntaBean pregunta = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "SELECT p.id_pregunta, p.id_ficha, p.descripcion, tipo_pregunta "
                        + "FROM  pregunta p "
                        + "WHERE p.id_ficha = ? ";
            System.out.println("QUERY::::::::::::"+sql +"///idFicha="+idFicha);
            ps = cn.prepareCall(sql);
            ps.setInt(1, idFicha);            
            resultado = ps.executeQuery();
            while (resultado.next()) {
                System.out.println("::::WHILE:::::::");
                pregunta = new PreguntaBean();
                pregunta.setId_ficha(resultado.getInt("id_ficha"));
                pregunta.setDescripcion(resultado.getString("descripcion"));   
                pregunta.setId_pregunta(resultado.getInt("id_pregunta"));
                pregunta.setTipo_pregunta(resultado.getInt("tipo_pregunta"));
                pregunta.setAlternativaBean(getAlternativasByPregunta(resultado.getInt("id_pregunta")));
                listaPreguntas.add(pregunta);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
       // System.out.println("listaPreguntas:"+listaPreguntas.size());
        return listaPreguntas;
    }
    
    public List<AlternativaBean> getAlternativasByPregunta(int idPregunta) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet resultado2 = null;
    
        List<AlternativaBean> listaAlternativas = new ArrayList<AlternativaBean>();
        AlternativaBean alternativa = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "SELECT id_alternativa, id_pregunta, descripcion, is_respuesta "
                        + "FROM alternativa "
                        + "WHERE id_pregunta = ? ";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idPregunta);         
            resultado2 = ps.executeQuery();           
            while (resultado2.next()) {
                alternativa = new AlternativaBean();
                alternativa.setId_alternativa(resultado2.getInt("id_alternativa"));
                alternativa.setDescripcion(resultado2.getString("descripcion"));   
                alternativa.setId_pregunta(resultado2.getInt("id_pregunta"));
                alternativa.setIsRespuesta(resultado2.getString("is_respuesta"));
                listaAlternativas.add(alternativa);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return listaAlternativas;
    }
    
    public void registrarAlumno(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            //String sql = "INSERT INTO estudiante(nombre, apellido_paterno, apellido_materno, cod_estudiante, usuario, password, genero, id_seccion) values(?,?,?,?,?,?,?,?)";
            String sql = "INSERT INTO ficha_estudiante(id_ficha, id_estudiante, nota_final, isterminado, id_clase) values(?,?,?,?,?)";
            
            ps = cn.prepareCall(sql);
            ps.setInt(1, fichaEstudianteBean.getId_ficha());
            ps.setInt(2, fichaEstudianteBean.getId_estudiante());
            ps.setString(3, fichaEstudianteBean.getNota_final());
            ps.setString(4, fichaEstudianteBean.getIsTerminado());
            ps.setInt(5, fichaEstudianteBean.getId_clase());
            ps.executeUpdate();
            
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                for (RespuestaEstudianteBean bean : listaRespuestas ){                    
                    String sql2 = "INSERT INTO estudiante_respuesta(id_ficha_estudiante, id_alternativa, id_pregunta, descRespuesta) values(?,?,?,?)";
                    ps = cn.prepareCall(sql2);
                    ps.setInt(1, generatedKeys.getInt(1));
                    ps.setInt(2, bean.getId_alternativa());
                    ps.setInt(3, bean.getId_pregunta());   
                    ps.setString(4, bean.getDescRespuesta()); 
                    ps.executeUpdate();
                    
                }
            }
                       
           
            
            cn.commit();
        } catch (Exception e) {
            if (cn != null) {
                cn.rollback();
            }
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }
    
    public List<FichaBean> getFichasByClaseByDocente(int idClase, String idSesion) throws Exception {
        //System.out.println("getFichasByClaseByDocente**idClase"+idClase+" - idSesion:"+idSesion);
        Connection cn = null;
        PreparedStatement ps = null;
        List<FichaBean> listaFichas = new ArrayList<FichaBean>();
        FichaBean ficha = null;
        try {
            cn = AccesoDB.getConnection();       
            String sql ="SELECT f.id_ficha, f.descripcion, " +
                                "(SELECT fc.estado " + 
                                "FROM ficha_clase fc " +
                                "WHERE fc.id_ficha = f.id_ficha " +
                                "AND fc.id_clase = ?) " +
                        "AS estado " +
                        "FROM ficha f " +
                        "WHERE f.id_sesion = ? " + 
                        "AND f.estado = 1 " +
                        "AND (f.id_clase = ? or f.id_clase IS NULL)";
         
            ps = cn.prepareCall(sql);
            ps.setInt(1, idClase);
            ps.setString(2, idSesion);  
            ps.setString(3, idClase+"");
            resultado = ps.executeQuery();
            while (resultado.next()) {
                ficha = new FichaBean();
                ficha.setId_ficha(resultado.getInt("id_ficha"));
                ficha.setDescripcion(resultado.getString("descripcion"));
                ficha.setEstado(resultado.getString("estado"));
                listaFichas.add(ficha);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return listaFichas;
    }
    
     public List<EstudianteBean> getParticipantesByFicha(int idFicha, int idClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<EstudianteBean> listaEstudiantes = new ArrayList<EstudianteBean>();
        EstudianteBean estudiante = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "SELECT e.id_estudiante, e.apellido_paterno, e.apellido_materno, e.nombre, " +
                         "(select nota_final from ficha_estudiante where id_estudiante = e.id_estudiante and id_ficha = ? and id_clase = ?) as nota_final, "+
                         "(SELECT isTerminado from ficha_estudiante where id_ficha = ? AND id_estudiante = e.id_estudiante and id_clase = ? ) as terminado  " +
                         "FROM estudiante e, clase_estudiante ce " +
                         "WHERE e.id_estudiante = ce.id_estudiante " +
                         "AND ce.id_clase = ? "+
                         "ORDER BY e.apellido_paterno ASC";
            ps = cn.prepareCall(sql);
            System.out.println("DAO:"+idFicha+"-"+idClase);
            ps.setInt(1, idFicha); 
            ps.setInt(2, idClase);            
            ps.setInt(3, idFicha); 
            ps.setInt(4, idClase);            
            ps.setInt(5, idClase);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                estudiante = new EstudianteBean();
                estudiante.setApellidoPaterno(resultado.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(resultado.getString("apellido_materno"));
                estudiante.setNombre(resultado.getString("nombre"));
                estudiante.setIdEstudiante(resultado.getInt("id_estudiante"));
                estudiante.setNotaFinal(resultado.getString("nota_final"));
                estudiante.setNombreCompleto(resultado.getString("apellido_paterno")+" "+resultado.getString("apellido_materno")+" "+resultado.getString("nombre"));
                estudiante.setEstado(resultado.getString("terminado"));
                listaEstudiantes.add(estudiante);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return listaEstudiantes;
    }
     
     public void activarDesactivarFicha(String estado, int idFicha, int idClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "UPDATE ficha_clase set estado = ? where id_ficha = ? and id_clase = ?";
            ps = cn.prepareCall(sql);            
            ps.setString(1, estado); 
            ps.setInt(2, idFicha);
            ps.setInt(3, idClase);
            ps.executeUpdate();
          
        } catch (Exception e) {
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(CuestionarioDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }               
    }
     
       public String getEstadoFichaEnClase(int idFicha, int idClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        String estado="";
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql =      "SELECT estado "
                            + "FROM ficha_clase "
                            + "WHERE id_ficha = ? "
                            + "AND id_clase = ? ";
            
            ps = cn.prepareCall(sql);
            ps.setInt(1, idFicha);   
            ps.setInt(2, idClase);      
            resultado = ps.executeQuery();
            while (resultado.next()) {              
                estado = resultado.getString("estado");                 
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }     
        return estado;
    }
       
       public String registrarFichaToFichaClase(int idFicha, int idClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        String error = Constante.ERROR;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            //String sql = "INSERT INTO estudiante(nombre, apellido_paterno, apellido_materno, cod_estudiante, usuario, password, genero, id_seccion) values(?,?,?,?,?,?,?,?)";
            String sql = "INSERT INTO ficha_clase(id_ficha, id_clase, estado) values(?,?,?)";
            
            ps = cn.prepareCall(sql);
            ps.setInt(1, idFicha);
            ps.setInt(2, idClase);          
            ps.setString(3, "1");        
            ps.executeUpdate();
            cn.commit();
            error = Constante.NO_ERROR;
        } catch (Exception e) {
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(CuestionarioDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }
        return error;
    }
        public String registrarNuevaFichaClase(FichaBean beanFicha, UsuarioClaseBean usuarioClase) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        String error = Constante.ERROR;
        try {
             
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            
            String sql = "INSERT INTO ficha(id_sesion, descripcion, estado, id_clase) values(?,?,?,?)";
            
            ps = cn.prepareCall(sql);
            ps.setString(1, usuarioClase.getIdSesion());
            ps.setString(2, beanFicha.getDescripcion());
            ps.setString(3, "1");
            ps.setString(4, usuarioClase.getIdClase().toString());
            ps.executeUpdate();
            
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                for (PreguntaBean bean : beanFicha.getListaPregunas() ){
                    String sql2 = "INSERT INTO pregunta (id_ficha, descripcion, tipo_pregunta) values(?,?,?)";
                    ps2 = cn.prepareCall(sql2);
                    ps2.setInt(1, generatedKeys.getInt(1));
                    ps2.setString(2, bean.getDescripcion());  
                    ps2.setInt(3, bean.getTipo_pregunta());
                    ps2.executeUpdate();
                    if(bean.getTipo_pregunta()==2){//CERRADA
                        ResultSet generatedKeys2 = ps2.getGeneratedKeys();
                         if (generatedKeys2.next()) {
                              for (AlternativaBean beanAlte : bean.getAlternativaBean() ){                            
                                   String sql3 = "INSERT INTO alternativa (id_pregunta, descripcion, is_respuesta) values(?,?,?)";
                                    ps3 = cn.prepareCall(sql3);
                                    ps3.setInt(1, generatedKeys2.getInt(1));
                                    ps3.setString(2, beanAlte.getDescripcion());       
                                    ps3.setString(3, beanAlte.getIsRespuesta()); 
                                    ps3.executeUpdate();
                              }
                         }
                    }            
                }
            }
            cn.commit();
            error = Constante.NO_ERROR;
        } catch (Exception e) {
            if (cn != null) {
                cn.rollback();
                error = Constante.ERROR;
            }            
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
                try { if (ps2 != null) ps2.close(); } catch (Exception e) {};
                try { if (ps3 != null) ps3.close(); } catch (Exception e) {};
            }
        return error;
    }
        
    public String registrarNuevaFichaFromExcel(List<FichaBean> listaFicha) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        String error = Constante.ERROR;
        try {
             
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            for(FichaBean beanFicha : listaFicha){
                String sql = "INSERT INTO ficha(id_sesion, descripcion, estado) values(?,?,?)";

                ps = cn.prepareCall(sql);
                ps.setString(1, beanFicha.getId_sesion());
                ps.setString(2, beanFicha.getDescripcion());
                ps.setString(3, "1");           
                ps.executeUpdate();

                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    for (PreguntaBean bean : beanFicha.getListaPregunas() ){
                        String sql2 = "INSERT INTO pregunta (id_ficha, descripcion, tipo_pregunta) values(?,?,?)";
                        ps2 = cn.prepareCall(sql2);
                        ps2.setInt(1, generatedKeys.getInt(1));
                        ps2.setString(2, bean.getDescripcion());  
                        ps2.setInt(3, bean.getTipo_pregunta());
                        ps2.executeUpdate();
                        if(bean.getTipo_pregunta()==2){//CERRADA
                            ResultSet generatedKeys2 = ps2.getGeneratedKeys();
                             if (generatedKeys2.next()) {
                                  for (AlternativaBean beanAlte : bean.getAlternativaBean() ){                            
                                       String sql3 = "INSERT INTO alternativa (id_pregunta, descripcion, is_respuesta) values(?,?,?)";
                                        ps3 = cn.prepareCall(sql3);
                                        ps3.setInt(1, generatedKeys2.getInt(1));
                                        ps3.setString(2, beanAlte.getDescripcion());       
                                        ps3.setString(3, beanAlte.getIsRespuesta()); 
                                        ps3.executeUpdate();
                                  }
                             }
                        }            
                    }
                }
            }
            cn.commit();
            error = Constante.NO_ERROR;
        } catch (Exception e) {
            if (cn != null) {
                cn.rollback();
                error = Constante.ERROR;
            }            
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
                try { if (ps2 != null) ps2.close(); } catch (Exception e) {};
                try { if (ps3 != null) ps3.close(); } catch (Exception e) {};
            }
        return error;
    }
        
        public List<RespuestaEstudianteBean> getRespuestasEstudiante(String idFicha, String idEstudiante, String id_clase) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2 = null;

            List<RespuestaEstudianteBean> listaAlternativas = new ArrayList<RespuestaEstudianteBean>();
            RespuestaEstudianteBean alternativa = null;
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT er.id_ficha_estudiante, er.id_pregunta, er.id_alternativa, er.descRespuesta " +
                                "FROM   estudiante_respuesta er, ficha_estudiante fe " +
                                "WHERE  fe.id_ficha_estudiante = er.id_ficha_estudiante " +
                                "AND    fe.id_ficha = ? " +
                                "AND    fe.id_estudiante = ? " +
                                "AND    fe.id_clase = ? ";
                ps = cn.prepareCall(sql);
                ps.setString(1, idFicha);   
                ps.setString(2, idEstudiante);    
                ps.setString(3, id_clase);    
                resultado2 = ps.executeQuery();           
                while (resultado2.next()) {
                    alternativa = new RespuestaEstudianteBean();
                    alternativa.setId_ficha_estudiante(resultado2.getInt("id_ficha_estudiante"));
                    alternativa.setId_alternativa(resultado2.getInt("id_alternativa"));                
                    alternativa.setId_pregunta(resultado2.getInt("id_pregunta"));
                    alternativa.setDescRespuesta(resultado2.getString("descRespuesta"));
                    listaAlternativas.add(alternativa);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return listaAlternativas;
    } 
        
         public String getNombresAlumnoByIdEstudiante(String idEstudiante) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2 = null;

            String nombre="";
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT e.apellido_paterno, e.apellido_materno, e.nombre " +
                                "FROM   estudiante e " +
                                "WHERE  e.id_estudiante = ? ";
                ps = cn.prepareCall(sql);
                ps.setString(1, idEstudiante);  
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    nombre = resultado2.getString("apellido_paterno").toUpperCase()+" "+resultado2.getString("apellido_materno").toUpperCase()+" "+resultado2.getString("nombre").toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
            }

            return nombre;
    } 
         
      public SeccionBean getSeccionByIdClase(String idClase) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2 = null;

            String nombre="";
            SeccionBean beanSeccion = new SeccionBean();
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT  s.id_seccion, s.codigo_grado, s.nombre_seccion FROM seccion s, clase c " +
                                "WHERE   s.id_seccion = c.id_seccion " +
                                "AND     c.id_clase =  ? ";
                ps = cn.prepareCall(sql);
                ps.setString(1, idClase);  
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    beanSeccion.setCodigoGrado(resultado2.getString("codigo_grado").toUpperCase().replace("_", " "));
                    beanSeccion.setNombreSeccion(resultado2.getString("nombre_seccion").toUpperCase());                    
                    beanSeccion.setIdSeccion(resultado2.getInt("id_seccion"));                       
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return beanSeccion;
    } 
         public String getNombreFichaByIdFicha(String idFicha) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2  = null;

            String nombre="";
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT  descripcion FROM ficha " +
                                "WHERE   id_ficha = ? ";
                ps = cn.prepareCall(sql);
                ps.setString(1, idFicha);  
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    nombre = resultado2.getString("descripcion").toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return nombre;
    } 
         
         public int getIdFichaByIdFichaEstudiante(String idFichaEstuiante) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2 = null;

            int id_ficha_estudiante=0;
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT id_ficha " +
                                "FROM   ficha_estudiante " +
                                "WHERE  id_ficha_estudiante = ? ";
                ps = cn.prepareCall(sql);
                ps.setString(1, idFichaEstuiante);  
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    id_ficha_estudiante = resultado2.getInt("id_ficha");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return id_ficha_estudiante;
    } 
    
         public void updateCuestionario(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas) throws Exception{
            Connection cn = null;
            PreparedStatement ps = null;
            try {
                cn = AccesoDB.getConnection();
                cn.setAutoCommit(false);
                //String sql = "INSERT INTO estudiante(nombre, apellido_paterno, apellido_materno, cod_estudiante, usuario, password, genero, id_seccion) values(?,?,?,?,?,?,?,?)";
                String sql = "UPDATE ficha_estudiante SET nota_final = ?, isTerminado = ? where id_ficha_estudiante = ?";

                ps = cn.prepareCall(sql);                
                ps.setString(1, fichaEstudianteBean.getNota_final());
                ps.setString(2, fichaEstudianteBean.getIsTerminado());
                ps.setInt(3, fichaEstudianteBean.getId_ficha_estudiante());
                ps.executeUpdate();

             
                    for (RespuestaEstudianteBean bean : listaRespuestas ){                    
                        String sql2 = "UPDATE estudiante_respuesta SET isCorrecto = ? WHERE id_ficha_estudiante = ? AND id_pregunta = ? ";
                        ps = cn.prepareCall(sql2);
                       
                        ps.setString(1, bean.getIsCorrecto());
                        ps.setInt(2, bean.getId_ficha_estudiante());   
                        ps.setInt(3, bean.getId_pregunta()); 
                        ps.executeUpdate();

                    }


                cn.commit();
            } catch (Exception e) {
                if (cn != null) {
                    cn.rollback();
                }
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }
        }       
         
    public List<FichaBean> getFichasActivasByIdSesion(String id_sesion) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<FichaBean> listaFichas = new ArrayList<FichaBean>();
        FichaBean ficha = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "SELECT id_clase, id_ficha, descripcion "
                        + "FROM  ficha where id_sesion = ? "
                        + "AND estado = 1 ORDER BY id_clase asc";
            System.out.println("QUERY::::::::::::"+sql +"///id_sesion="+id_sesion);
            ps = cn.prepareCall(sql);
            ps.setString(1, id_sesion);            
            resultado = ps.executeQuery();
            while (resultado.next()) {
                System.out.println("::::WHILE:::::::");
                ficha = new FichaBean();
                ficha.setId_ficha(resultado.getInt("id_ficha"));
                ficha.setDescripcion(resultado.getString("descripcion")); 
                ficha.setId_clase(resultado.getString("id_clase"));
                listaFichas.add(ficha);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
       // System.out.println("listaPreguntas:"+listaPreguntas.size());
        return listaFichas;
    }
    
     public List<EstudianteBean> getEstudiantesActivosByIdSeccion(int id_seccion) throws Exception {
        Connection cn = null;
        PreparedStatement ps = null;
        List<EstudianteBean> listaEstudiantes = new ArrayList<EstudianteBean>();
        EstudianteBean estudiante = null;
        try {
            cn = AccesoDB.getConnection();
            //cn.setReadOnly(true);
            String sql = "SELECT id_estudiante, nombre, apellido_paterno, apellido_materno "
                        + "FROM estudiante "
                        + "WHERE id_seccion = ? "
                        + "AND estado = 1 " 
                        + "ORDER BY apellido_paterno ASC";
            System.out.println("QUERY::::::::::::"+sql +"///id_sesion="+id_seccion);
            ps = cn.prepareCall(sql);
            ps.setInt(1, id_seccion);            
            resultado = ps.executeQuery();
            while (resultado.next()) {
                System.out.println("::::WHILE:::::::");
                estudiante = new EstudianteBean();
                estudiante.setApellidoPaterno(resultado.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(resultado.getString("apellido_materno")); 
                estudiante.setNombre(resultado.getString("nombre"));
                estudiante.setIdEstudiante(resultado.getInt("id_estudiante"));
                listaEstudiantes.add(estudiante);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
       // System.out.println("listaPreguntas:"+listaPreguntas.size());
        return listaEstudiantes;
    }
    
     public String getNotaByEstudianteAndFicha(int id_estudiante, int id_ficha) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2  = null;

            String notaFinal="";
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =  "SELECT nota_final "
                            + "FROM ficha_estudiante "
                            + "WHERE id_estudiante = ? "
                            + "AND id_ficha = ? ";
                ps = cn.prepareCall(sql);
                ps.setInt(1, id_estudiante);  
                ps.setInt(2, id_ficha);  
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    notaFinal = resultado2.getString("nota_final");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return notaFinal;
    } 
    
     public int getIdCursoByGradoYCurso(String codigo_grado, String nombre_curso) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2  = null;

            int id_curso=0;
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT id_curso FROM curso where codigo_grado = ? and nombre_curso LIKE ?";
                ps = cn.prepareCall(sql);
                ps.setString(1, codigo_grado); 
                ps.setString(2, "%"+nombre_curso+"%"); 
                resultado2 = ps.executeQuery();
                System.out.println("ps::::"+ps.toString());
                while (resultado2.next()) {
                    id_curso = resultado2.getInt("id_curso");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return id_curso;
    } 
     
     public String getIdSessionByIdCursoAndCodSesion(int id_curso, String codigo_sesion) throws Exception {
            Connection cn = null;
            PreparedStatement ps = null;
            ResultSet resultado2  = null;

            String id_sesion = "";
          
            try {
                cn = AccesoDB.getConnection();
                //cn.setReadOnly(true);
                String sql =    "SELECT id_sesion FROM sesion WHERE id_curso = ? AND codigo_sesion = ?";
                ps = cn.prepareCall(sql);
                ps.setInt(1, id_curso); 
                ps.setString(2, codigo_sesion); 
                resultado2 = ps.executeQuery();   
                while (resultado2.next()) {
                    id_sesion = resultado2.getString("id_sesion");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error: " + "\t" + e.getMessage());
            } finally{
                try { if (resultado2 != null) resultado2.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
            }

            return id_sesion;
    } 
}
