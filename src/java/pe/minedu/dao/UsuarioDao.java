package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

/**
 *
 * @author GustavoFelix
 */
public class UsuarioDao implements Serializable {

    public UsuarioDao() {
    }

    public void creaUsuario(UsuarioBean usuario) {
        Connection con=null;
        try {
            con=AccesoDB.getConnection();
            //Falta implementar
        }catch(Exception e) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void editaUsuario(UsuarioBean usuario) {
        Connection con=null;
        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            usuario = em.merge(usuario);
//            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
//                Integer id = usuario.getId();
//                if (findUsuario(id) == null) {
//                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
//                }
            }
            throw ex;
        } finally {
            if (con != null) {
          /*      try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, ex);
                }*/
            }
        }
    }
    
    public UsuarioBean logginUsuario(String user, String password){
        Connection con=null;
        UsuarioBean usuario=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {

            String sql = "SELECT * FROM usuario WHERE usuario = ? AND password = ?";
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, password);
            rs=ps.executeQuery();
            if(rs.next()){
                usuario = new UsuarioBean();
                usuario.setIdUsuario(rs.getInt("id_usuario"));
                usuario.setUsuario(user);
                usuario.setPerfil(rs.getString("perfil"));
                usuario.setIdDocente(rs.getInt("id_docente"));
                usuario.setIdEstudiante(rs.getInt("id_estudiante"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setUltimoDiaLog(rs.getString("ultimo_dia_log"));
                usuario.setUltimoDiaIp(rs.getString("ultimo_dia_ip"));
            }
            return usuario;
        } catch (Exception ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }
    
    public DatosPersonalesUsuarioBean obtDatosPersonalesUsuario(Integer idUsuario, String perfil){
        DatosPersonalesUsuarioBean datosPersonalesUsuarioBean = null;
        Connection con=null;
        String sql = "";
        PreparedStatement ps=null;
        ResultSet rs=null;
        
        try {

            if(perfil.equalsIgnoreCase(Constante.PERFIL_DOCENTE)){
                sql = "SELECT d.cod_docente as codigo, d.dni, d.nombre, d.apellido_paterno, d.genero, d.estado " +
                "FROM usuario u " +
                "INNER JOIN docente d " +
                "ON u.id_docente = d.id_docente " +
                "WHERE u.id_usuario = ?";
            }else{
                sql = "SELECT e.cod_estudiante as codigo, e.dni, e.nombre, e.apellido_paterno, e.genero, e.estado " +
                    "FROM usuario u " +
                    "INNER JOIN estudiante e " +
                    "ON u.id_estudiante = e.id_estudiante " +
                    "WHERE u.id_usuario = ?";
            }
            
            con=AccesoDB.getConnection();
//            con.setReadOnly(true);
            ps=con.prepareStatement(sql);
            ps.setInt(1, idUsuario);
            rs=ps.executeQuery();
            if(rs.next()){
                datosPersonalesUsuarioBean = new DatosPersonalesUsuarioBean();
                datosPersonalesUsuarioBean.setCodigo(rs.getString("codigo"));
                datosPersonalesUsuarioBean.setDni(rs.getString("dni"));
                datosPersonalesUsuarioBean.setNombre(rs.getString("nombre"));
                datosPersonalesUsuarioBean.setApellido(rs.getString("apellido_paterno"));
                datosPersonalesUsuarioBean.setGenero(rs.getString("genero"));
                datosPersonalesUsuarioBean.setEstado(rs.getString("estado"));
            }
            
        } catch (Exception e) {
            
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return datosPersonalesUsuarioBean;
    }
    
    public String actualizarPassword(String nuevoPassword, String usuario, int idUsuario){
        String mensaje = "";
        Connection con=null;
        PreparedStatement ps=null;
        String sql = "";
        try {
            sql = "UPDATE usuario " 
                    + "SET password = ? " 
                    + "WHERE usuario = ? AND id_usuario = ? ";
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setString(1, nuevoPassword);
            ps.setString(2, usuario);
            ps.setInt(3, idUsuario);
            ps.executeUpdate();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            e.printStackTrace();
            mensaje = "Error " + e.getMessage();
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public UsuarioBean obtUsuarioById(int idUsuario){
        Connection con=null;
        UsuarioBean usuario=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {

            String sql = "SELECT * FROM usuario WHERE id_usuario = ?";
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idUsuario);
            rs=ps.executeQuery();
            if(rs.next()){
                usuario = new UsuarioBean();
                usuario.setIdUsuario(rs.getInt("id_usuario"));
                usuario.setUsuario(rs.getString("usuario"));
                usuario.setPassword(rs.getString("password"));
                usuario.setPerfil(rs.getString("perfil"));
                usuario.setIdDocente(rs.getInt("id_docente"));
                usuario.setIdEstudiante(rs.getInt("id_estudiante"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setUltimoDiaLog(rs.getString("ultimo_dia_log"));
                usuario.setUltimoDiaIp(rs.getString("ultimo_dia_ip"));
            }
            return usuario;
        } catch (Exception ex) {
            Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }
    
}
