
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.AplicacionBean;
import pe.minedu.jdbc.AccesoDB;

public class AplicacionDao {
    ResultSet resultado;
    Statement sentencia;
    
    public AplicacionBean insertarAplicacionBean(AplicacionBean aplicacionBean){
        Connection cn = null;
        PreparedStatement ps = null;
        int inserted_id = 0;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            String sql = "INSERT INTO aplicacion(id_aplicacion,tipo_aplicacion,nombre_aplicacion,documento_abrir,extensiones,estado) "
                    + "VALUES(?,?,?,?,?,?)";
            ps = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, obtIdTablaAplicacion());
            ps.setString(2, aplicacionBean.getTipoAplicacion());
            ps.setString(3, aplicacionBean.getNombreAplicacion());
            ps.setString(4, aplicacionBean.getDocumentoAbrir());
            ps.setString(5, aplicacionBean.getExtensiones());
            ps.setString(6, aplicacionBean.getEstado());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            if (keys.next()){
                inserted_id = keys.getInt(1);
            }
            cn.commit();
        } catch (Exception e) {
            Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, e);
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally{
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        aplicacionBean.setIdAplicacion(inserted_id);
        return aplicacionBean;
    }
    
    public void eliminarAplicacionBean(AplicacionBean aplicacionBean){
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            String sql = "DELETE FROM aplicacion "
                    + "WHERE id_aplicacion = ?";
            ps = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, aplicacionBean.getIdAplicacion());
            ps.executeUpdate();
            cn.commit();
        } catch (Exception e) {
            Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, e);
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally{
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }
    
    public List<AplicacionBean> listarAplicaciones(){
        Connection cn = null;
        PreparedStatement ps = null;
        List<AplicacionBean> aplicaciones = new ArrayList<AplicacionBean>();
        AplicacionBean aplicacionBean = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT id_aplicacion, "
                    + " tipo_aplicacion, "
                    + " nombre_aplicacion, "
                    + " documento_abrir, "
                    + " extensiones, "
                    + " estado FROM aplicacion";
             
            ps = cn.prepareStatement(sql);
            resultado = ps.executeQuery();
            while(resultado.next()){
                aplicacionBean = new AplicacionBean();
                
                aplicacionBean.setIdAplicacion(resultado.getInt("id_aplicacion"));
                aplicacionBean.setTipoAplicacion(resultado.getString("tipo_aplicacion"));
                aplicacionBean.setNombreAplicacion(resultado.getString("nombre_aplicacion"));
                aplicacionBean.setDocumentoAbrir(resultado.getString("documento_abrir"));
                aplicacionBean.setExtensiones(resultado.getString("extensiones"));
                aplicacionBean.setEstado(resultado.getString("estado"));
                
                aplicaciones.add(aplicacionBean);
            }
        } catch (Exception e) {
            
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return aplicaciones;
    }
    
    private int obtIdTablaAplicacion(){
        int idAplicacion = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_aplicacion) max_id "
                    + " FROM aplicacion";
             
            ps = cn.prepareStatement(sql);
            resultado = ps.executeQuery();
            while(resultado.next()){
                idAplicacion = resultado.getInt("max_id");
            }
        } catch (Exception e) {
            
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        idAplicacion++;
        return idAplicacion;
    }
    
    public AplicacionBean obtAplicacionxId(int id){
        Connection cn = null;
        PreparedStatement ps = null;
        AplicacionBean aplicacionBean = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT id_aplicacion, "
                    + " tipo_aplicacion, "
                    + " nombre_aplicacion, "
                    + " documento_abrir, "
                    + " extensiones, "
                    + " estado FROM aplicacion "
                    + " WHERE id_aplicacion = ?";
             
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id);
            resultado = ps.executeQuery();
            while(resultado.next()){
                aplicacionBean = new AplicacionBean();
                aplicacionBean.setIdAplicacion(resultado.getInt("id_aplicacion"));
                aplicacionBean.setTipoAplicacion(resultado.getString("tipo_aplicacion"));
                aplicacionBean.setNombreAplicacion(resultado.getString("nombre_aplicacion"));
                aplicacionBean.setDocumentoAbrir(resultado.getString("documento_abrir"));
                aplicacionBean.setExtensiones(resultado.getString("extensiones"));
                aplicacionBean.setEstado(resultado.getString("estado"));
            }
        } catch (Exception e) {
            
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return aplicacionBean;
    }
}
