
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.ClaseRecursoBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
import pe.minedu.util.DateUtil;

public class ClaseRecursoDao {
    
    ResultSet resultado;

    public List<ClaseRecursoBean> listarRecursosxClase(int idClase, String perfil) {
        Connection cn = null;
        PreparedStatement ps = null;
        List<ClaseRecursoBean> recursos = new ArrayList<ClaseRecursoBean>();
        ClaseRecursoBean claseRecursoBean;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT c.id_clase_recurso, c.id_clase, c.id_aplicacion, c.tipo_recurso, " 
                    + " c.titulo, c.categoria, c.descripcion, c.fecha_reg, c.ruta, a.nombre_aplicacion "
                    + " FROM clase_recurso c "
                    + " INNER JOIN aplicacion a "
                    + " ON c.id_aplicacion = a.id_aplicacion "
                    + " WHERE id_clase = ? ";
            
            if(perfil.equals(Constante.PERFIL_ESTUDIANTE)){
                sql += "AND ver_estudiante='1'";
            }
             
            ps = cn.prepareStatement(sql);
            ps.setInt(1, idClase);
            resultado = ps.executeQuery();
            while(resultado.next()){
                claseRecursoBean = new ClaseRecursoBean();
                claseRecursoBean.setIdClaseRecurso(resultado.getInt("id_clase_recurso"));
                claseRecursoBean.setTipoRecurso(resultado.getString("tipo_recurso"));
                claseRecursoBean.setTitulo(resultado.getString("titulo"));
                claseRecursoBean.setCategoria(resultado.getString("categoria"));
                claseRecursoBean.setDescripcion(resultado.getString("descripcion"));
                claseRecursoBean.setFechaReg(DateUtil.sqlDateToUtilDate(resultado.getDate("fecha_reg")));
                claseRecursoBean.setRuta(resultado.getString("ruta"));
                claseRecursoBean.setIdAplicacion(resultado.getInt("id_aplicacion"));
                claseRecursoBean.setIdClase(resultado.getInt("id_clase"));
                claseRecursoBean.setNombreAplicacion(resultado.getString("nombre_aplicacion"));
                recursos.add(claseRecursoBean);
            }
        } catch (Exception e) {
            System.out.println("ClaseRecursoDao.listarRecursosxClase eRRor " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return recursos;
    }

    public ClaseRecursoBean obtClaseRecursoxId(int idRecursoClase) {
        Connection cn = null;
        ClaseRecursoBean claseRecursoBean = new ClaseRecursoBean();
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT id_clase_recurso, id_clase, id_aplicacion, tipo_recurso, " 
                    + " titulo, categoria, descripcion, fecha_reg, ruta "
                    + " FROM clase_recurso "
                    + " WHERE id_clase_recurso = ?";
             
            ps = cn.prepareStatement(sql);
            ps.setInt(1, idRecursoClase);
            resultado = ps.executeQuery();
            while(resultado.next()){
                claseRecursoBean.setIdClaseRecurso(resultado.getInt("id_clase_recurso"));
                claseRecursoBean.setTipoRecurso(resultado.getString("tipo_recurso"));
                claseRecursoBean.setTitulo(resultado.getString("titulo"));
                claseRecursoBean.setCategoria(resultado.getString("categoria"));
                claseRecursoBean.setDescripcion(resultado.getString("descripcion"));
                claseRecursoBean.setFechaReg(DateUtil.sqlDateToUtilDate(resultado.getDate("fecha_reg")));
                claseRecursoBean.setRuta(resultado.getString("ruta"));
                claseRecursoBean.setIdAplicacion(resultado.getInt("id_aplicacion"));
                claseRecursoBean.setIdClase(resultado.getInt("id_clase"));
            }
        } catch (Exception e) {
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return claseRecursoBean;
    }

    public ClaseRecursoBean registrarNuevoRecurso(ClaseRecursoBean claseRecursoBean) {
        Connection cn = null;
        PreparedStatement ps = null;
        int inserted_id = 0;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            String sql = "INSERT INTO clase_recurso(id_clase, id_aplicacion, tipo_recurso, " 
                    + "titulo, categoria, descripcion, fecha_reg, ruta) "
                    + "VALUES(?,?,?,?,?,?,?,?)";
//            inserted_id = obtIdTablaClaseRecurso();
            ps = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            ps.setInt(1, inserted_id);
            ps.setInt(1, claseRecursoBean.getIdClase());
            ps.setInt(2, claseRecursoBean.getIdAplicacion());
            ps.setString(3, claseRecursoBean.getTipoRecurso());
            ps.setString(4, claseRecursoBean.getTitulo());
            ps.setString(5, claseRecursoBean.getCategoria());
            ps.setString(6, claseRecursoBean.getDescripcion());
            ps.setDate(7, DateUtil.utilDateToSQLDate(claseRecursoBean.getFechaReg()));
            ps.setString(8, claseRecursoBean.getRuta());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            if (keys.next()){
                inserted_id = keys.getInt(1);
            }
            cn.commit();
        } catch (Exception e) {
            Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, e);
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(AplicacionDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        System.out.println("Id nuevo " + inserted_id);
        claseRecursoBean.setIdClaseRecurso(inserted_id);
        return claseRecursoBean;
    }
    
    private int obtIdTablaClaseRecurso(){
        int idClaseRecurso = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_clase_recurso) max_id "
                    + " FROM clase_recurso";
             
            ps = cn.prepareStatement(sql);
            resultado = ps.executeQuery();
            while(resultado.next()){
                idClaseRecurso = resultado.getInt("max_id");
            }
        } catch (Exception e) {
            
        } finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        idClaseRecurso++;
        return idClaseRecurso;
    }
}
