
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.GradoBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

public class GradoDao {
    
    ResultSet resultado;
    Statement sentencia;
    
    public String agregarGrados(List<GradoBean> lista){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO grado "
                    + " (codigo_grado, id_grado, nivel_programa, periodo_escolar, atencion) "
                    + " VALUES(?,?,?,?,?)";
            for(GradoBean gradoBean : lista){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, gradoBean.getCodigoGrado());
                preparedStatement.setInt(2, gradoBean.getIdGrado());
                preparedStatement.setString(3, gradoBean.getNivelPrograma());
                preparedStatement.setInt(4, gradoBean.getPeriodoEscolar());
                preparedStatement.setString(5, gradoBean.getAtencion());
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            e.printStackTrace();
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error GradoDao.agregarGrados " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error GradoDao.agregarGrados RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public GradoBean obtGradoBeanxGradoNivelPeriodo(int grado, String nivelPrograma, int periodoEscolar){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        GradoBean gradoBean = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT codigo_grado, id_grado, nivel_programa, periodo_escolar, atencion FROM grado "
                    + " WHERE id_grado = ? " 
                    + "AND nivel_programa = ? " 
                    + "AND periodo_escolar = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, grado);
            preparedStatement.setString(2, nivelPrograma);
            preparedStatement.setInt(3, periodoEscolar);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                gradoBean = new GradoBean();
                gradoBean.setAtencion(resultado.getString("atencion"));
                gradoBean.setCodigoGrado(resultado.getString("codigo_grado"));
                gradoBean.setIdGrado(resultado.getInt("id_grado"));
                gradoBean.setNivelPrograma(resultado.getString("nivel_programa"));
                gradoBean.setPeriodoEscolar(resultado.getInt("periodo_escolar"));
            }
        } catch (Exception e) {
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return gradoBean;
    }
    
    public GradoBean obtGradoBeanxCodGrado(String codigoGrado){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        GradoBean gradoBean = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT codigo_grado, id_grado, nivel_programa, periodo_escolar, atencion FROM grado "
                    + " WHERE codigo_grado = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, codigoGrado);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                gradoBean = new GradoBean();
                gradoBean.setAtencion(resultado.getString("atencion"));
                gradoBean.setCodigoGrado(resultado.getString("codigo_grado"));
                gradoBean.setIdGrado(resultado.getInt("id_grado"));
                gradoBean.setNivelPrograma(resultado.getString("nivel_programa"));
                gradoBean.setPeriodoEscolar(resultado.getInt("periodo_escolar"));
            }
        } catch (Exception e) {
            gradoBean = null;
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return gradoBean;
    }
    
    public Object obt(){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT codigo_grado, id_grado, nivel_programa, periodo_escolar, atencion FROM grado "
                    + " WHERE codigo_grado = ? ";
            preparedStatement = connection.prepareStatement(sql);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return new Object();
    }
}
