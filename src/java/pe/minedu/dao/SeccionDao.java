
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.SeccionBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

public class SeccionDao {
    
    ResultSet resultado;
    Statement sentencia;
    
    public List<SeccionBean> listSeccionesXGrado(String codigoGrado){
        Connection con=null;
        List<SeccionBean> seccionesxGrado = new ArrayList<SeccionBean>();
        SeccionBean seccionBean = null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {

            String sql = "SELECT id_seccion, codigo_grado, nombre_seccion " +
                "FROM seccion s " +
                "WHERE s.codigo_grado = ? ";
            
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setString(1, codigoGrado);
            rs=ps.executeQuery();
            while(rs.next()){
                seccionBean = new SeccionBean();
                seccionBean.setIdSeccion(rs.getInt("id_seccion"));
                seccionBean.setCodigoGrado(rs.getString("codigo_grado"));
                seccionBean.setNombreSeccion(rs.getString("nombre_seccion"));
                seccionesxGrado.add(seccionBean);
            }
        } catch (Exception e) {
            Logger.getLogger(SeccionDao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
         
        return seccionesxGrado;
    }
    
    public SeccionBean obtSeccionBeanxCodigoGradoNombreSeccion(String codigoGrado, String nombreSeccion){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        SeccionBean seccionBean = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT id_seccion, codigo_grado, nombre_seccion FROM seccion "
                    + " WHERE codigo_grado = ? AND nombre_seccion = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, codigoGrado);
            preparedStatement.setString(2, nombreSeccion);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                seccionBean = new SeccionBean();
                seccionBean.setIdSeccion(resultado.getInt("id_seccion"));
                seccionBean.setCodigoGrado(resultado.getString("codigo_grado"));
                seccionBean.setNombreSeccion(resultado.getString("nombre_seccion"));
            }
        } catch (Exception e) {
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return seccionBean;
    }

    public String agregarSecciones(List<SeccionBean> secciones) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO seccion "
                    + " (codigo_grado, nombre_seccion) "
                    + " VALUES(?,?)";
            for(SeccionBean seccionBean : secciones){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, seccionBean.getCodigoGrado());
                preparedStatement.setString(2, seccionBean.getNombreSeccion());
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error SeccionDao.agregarSecciones " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error SeccionDao.agregarSecciones RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
}
