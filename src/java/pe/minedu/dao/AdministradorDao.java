
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

public class AdministradorDao {
    
    ResultSet resultado;
    
    public DatosPersonalesUsuarioBean getUsuario(String dni) {
        Connection cn = null;
        PreparedStatement ps = null;
        DatosPersonalesUsuarioBean usuario = null;
        try {
            cn = AccesoDB.getConnection();
            
            String alumno = "SELECT u.usuario, e.nombre, e.apellido_paterno, e.apellido_materno, e.dni "
                    + "FROM usuario u "
                    + "INNER JOIN estudiante e "
                    + "ON e.id_estudiante = u.id_estudiante "
                    + "WHERE e.dni = ?";
            
            String docente = "SELECT u.usuario, e.nombre, e.apellido_paterno, e.apellido_materno, e.dni "
                    + "FROM usuario u "
                    + "INNER JOIN docente e "
                    + "ON e.id_docente = u.id_docente "
                    + "WHERE e.dni = ?";

            ps = cn.prepareCall(alumno);
            ps.setString(1, dni);
            resultado = ps.executeQuery();
            if (resultado.next()) {
                String nombreCompleto = resultado.getString("nombre") + " " + resultado.getString("apellido_paterno") + " " + resultado.getString("apellido_materno");
                usuario = new DatosPersonalesUsuarioBean();
                usuario.setNombre(nombreCompleto);
                usuario.setDni(resultado.getString("dni"));
                usuario.setCodigo(resultado.getString("usuario"));
                
            }
            else{
                ps = cn.prepareCall(docente);
                ps.setString(1, dni);
                resultado = ps.executeQuery();
                if (resultado.next()) {
                    String nombreCompleto = resultado.getString("nombre") + " " + resultado.getString("apellido_paterno") + " " + resultado.getString("apellido_materno");
                    usuario = new DatosPersonalesUsuarioBean();
                    usuario.setNombre(nombreCompleto);
                    usuario.setDni(resultado.getString("dni"));
                    usuario.setCodigo(resultado.getString("usuario"));
                }
                else{
                    return null;
                }
            }

        } catch (Exception e) {
            return null;
        }finally{
            try { if (resultado != null) resultado.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        

        return usuario;
    }
    
    public String resetPassword(String password, String usuario){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "UPDATE usuario SET password=? WHERE usuario=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, password);
            preparedStatement.setString(2, usuario);
            preparedStatement.executeUpdate();
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error AdministradorDao.resetPassword " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error AdministradorDao.resetPassword RollingBack " + ex.getMessage();
            }
        }
        finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
}
