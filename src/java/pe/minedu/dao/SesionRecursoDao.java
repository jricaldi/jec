
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.SesionRecursoBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
import pe.minedu.util.DateUtil;

public class SesionRecursoDao {
    
    ResultSet resultado;
    Statement sentencia;

    public String agregarSesionRecursos(List<SesionRecursoBean> sesionRecursos) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO sesion_recurso "
                    + " (id_sesion_recurso, id_sesion, id_aplicacion, titulo, "
                    + " categoria, descripcion, fecha_reg, ruta, estado, ver_estudiante ) "
                    + " VALUES(?,?,?,?,?,?,?,?,?,?)";
            for(SesionRecursoBean sesionRecursoBean : sesionRecursos){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, obtIdTablaSesionRecurso());
                preparedStatement.setString(2, sesionRecursoBean.getIdSesion());
                preparedStatement.setInt(3, sesionRecursoBean.getIdAplicacion());
                preparedStatement.setString(4, sesionRecursoBean.getTitulo());
                preparedStatement.setString(5, sesionRecursoBean.getCategoria());
                preparedStatement.setString(6, sesionRecursoBean.getDescripcion());
                preparedStatement.setDate(7, DateUtil.utilDateToSQLDate(sesionRecursoBean.getFechaReg()));
                preparedStatement.setString(8, sesionRecursoBean.getRuta());
                preparedStatement.setString(9, sesionRecursoBean.getEstado());
                preparedStatement.setString(10, sesionRecursoBean.getVerEstudiante());
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error CursoDao.agregarCursos " + e.getMessage();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(CursoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error CursoDao.agregarCursos RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public SesionRecursoBean obtSesionRecursoxIdSesionRuta(String idSesion, String ruta){
        SesionRecursoBean sesionRecursoBean = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT "
                    + " id_sesion_recurso, id_sesion, id_aplicacion, titulo, "
                    + " categoria, descripcion, fecha_reg, ruta, estado, ver_estudiante "
                    + " FROM sesion_recurso "
                    + " WHERE id_sesion = ?"
                    + " AND ruta = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, idSesion);
            preparedStatement.setString(2, ruta);
            resultado = preparedStatement.executeQuery();
            if(resultado.next()){
                sesionRecursoBean = new SesionRecursoBean();
                sesionRecursoBean.setIdSesionRecurso(resultado.getInt("id_sesion_recurso"));
                sesionRecursoBean.setTitulo(resultado.getString("titulo"));
                sesionRecursoBean.setCategoria(resultado.getString("categoria"));
                sesionRecursoBean.setDescripcion(resultado.getString("descripcion"));
                sesionRecursoBean.setFechaReg(DateUtil.utilDateToSQLDate(resultado.getDate("fecha_reg")));
                sesionRecursoBean.setRuta(resultado.getString("ruta"));
                sesionRecursoBean.setEstado(resultado.getString("estado"));
                sesionRecursoBean.setIdAplicacion(resultado.getInt("id_aplicacion"));
                sesionRecursoBean.setIdSesion(resultado.getString("id_sesion"));
                sesionRecursoBean.setVerEstudiante(resultado.getString("ver_estudiante"));
            }
        } catch (Exception e) {
            sesionRecursoBean = null;
            e.printStackTrace();
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return sesionRecursoBean;
    }
 
    public int obtIdTablaSesionRecurso(){
        int id = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_sesion_recurso) max_id "
                    + " FROM sesion_recurso";
             
            ps = cn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            while(resultSet.next()){
                id = resultSet.getInt("max_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
                try { if (resultSet != null) resultSet.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        id++;
        return id;
    }
}
