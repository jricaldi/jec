package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.DateUtil;

/**
 *
 * @author GustavoFelix
 */
public class UsuarioClaseDao implements Serializable {

    public UsuarioClaseDao() {
    }

    public void creaUsuarioEnClase(UsuarioClaseBean usuarioClase) {
        Connection con=null;
        PreparedStatement ps=null;
        try {

            String sql="";
            con=AccesoDB.getConnection();
//            con.setReadOnly(false);
            con.setAutoCommit(true);
            /*Borra si ya fue registrado anteriormente, para mantener el ultimo IP*/
            sql="DELETE FROM usuario_clase WHERE id_usuario=? AND id_clase=?";
            ps=con.prepareStatement(sql);
            ps.setInt(1, usuarioClase.getIdUsuario());
            ps.setInt(2, usuarioClase.getIdClase());
            ps.executeUpdate();
            
            sql="INSERT INTO usuario_clase(id_usuario,id_clase,ip,fecha) VALUES(?,?,?,?)";
            ps=con.prepareStatement(sql);
            ps.setInt(1, usuarioClase.getIdUsuario());
            ps.setInt(2, usuarioClase.getIdClase());
            ps.setString(3, usuarioClase.getIp());
            ps.setDate(4, DateUtil.utilDateToSQLDate(usuarioClase.getFecha()));
            ps.executeUpdate();
            
        }catch(Exception e) {
            if(con != null) try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioClaseDao.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } finally{
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }

}
