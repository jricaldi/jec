package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;

public class MonitoreoDao {
    
    public String bloquearEquipo(int idClase, int idEstudiante){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "REPLACE INTO monitoreo "
                    + " (id_clase, id_estudiante, bloqueado) "
                    + " VALUES(?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            preparedStatement.setInt(2, idEstudiante);
            preparedStatement.setBoolean(3, true);
            preparedStatement.executeUpdate();
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error MonitoreoDao.bloquearEquipo " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error MonitoreoDao.bloquearEquipo RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
        
    public String bloquearEquipoTodos(int idClase, List<MonitoreoClaseEstudianteBean> estudiantes){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "REPLACE INTO monitoreo "
                    + " (id_clase, id_estudiante, bloqueado) "
                    + " VALUES(?,?,?)";
            for(MonitoreoClaseEstudianteBean monitoreoEstudianteBean : estudiantes){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, idClase);
                preparedStatement.setInt(2, monitoreoEstudianteBean.getIdEstudiante());
                preparedStatement.setBoolean(3, true);
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error MonitoreoDao.bloquearEquipoTodos " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error MonitoreoDao.bloquearEquipoTodos RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public String desbloquearEquipo(int idClase, int idEstudiante){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "REPLACE INTO monitoreo "
                    + " (id_clase, id_estudiante, bloqueado) "
                    + " VALUES(?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            preparedStatement.setInt(2, idEstudiante);
            preparedStatement.setBoolean(3, false);
            preparedStatement.executeUpdate();
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error MonitoreoDao.desbloquearEquipo " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error MonitoreoDao.desbloquearEquipo RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public String desbloquearEquipoTodos(int idClase, List<MonitoreoClaseEstudianteBean> estudiantes){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String mensaje = "";
        try {
            connection = AccesoDB.getConnection();
            connection.setAutoCommit(false);
            String sql = "REPLACE INTO monitoreo "
                    + " (id_clase, id_estudiante, bloqueado) "
                    + " VALUES(?,?,?)";
            for(MonitoreoClaseEstudianteBean monitoreoEstudianteBean : estudiantes){
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, idClase);
                preparedStatement.setInt(2, monitoreoEstudianteBean.getIdEstudiante());
                preparedStatement.setBoolean(3, false);
                preparedStatement.executeUpdate();
            }
            connection.commit();
            mensaje = Constante.PROCESO_CORRECTO;
        } catch (Exception e) {
            if(connection != null) try {
                connection.rollback();
                mensaje = "Error MonitoreoDao.desbloquearEquipoTodos " + e.getMessage();
            } catch (SQLException ex) {
                Logger.getLogger(GradoDao.class.getName()).log(Level.SEVERE, null, ex);
                mensaje = "Error MonitoreoDao.desbloquearEquipoTodos RollingBack " + ex.getMessage();
            }
        } finally{
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return mensaje;
    }
    
    public MonitoreoClaseEstudianteBean getEstadoBloqueo(int idClase, int idEstudiante){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        MonitoreoClaseEstudianteBean monitoreo = new MonitoreoClaseEstudianteBean();
        ResultSet resultado = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT bloqueado FROM monitoreo "
                    + " WHERE id_clase = ? AND id_estudiante = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            preparedStatement.setInt(2, idEstudiante);
            resultado = preparedStatement.executeQuery();
            
            if(resultado.next()){
                monitoreo.setBloqueado(resultado.getBoolean("bloqueado"));
            }
            else{
                monitoreo.setBloqueado(false);
            }
        } catch (Exception e) {
            monitoreo.setBloqueado(false);
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return monitoreo;
    }
    
}
