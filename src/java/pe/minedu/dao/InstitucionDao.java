
package pe.minedu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import pe.minedu.bean.InstitucionBean;
import pe.minedu.jdbc.AccesoDB;

public class InstitucionDao {
    
    ResultSet resultado;
    Statement sentencia;
    
    public InstitucionBean obtInstitucionEscolar(){
        InstitucionBean institucionBean = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT id_institucion,codigo_institucion,razon_social,sede,departamento, " 
                    + "provincia,distrito,direccion_regional,ugel,direccion,area_geografica, " 
                    + "director,ip_servidor1,ip_servidor2 FROM institucion LIMIT 1";
            preparedStatement = connection.prepareStatement(sql);
            resultado = preparedStatement.executeQuery();
            institucionBean = new InstitucionBean();
            while(resultado.next()){
                institucionBean.setIdInstitucion(resultado.getInt("id_institucion"));
                institucionBean.setCodigoInstitucion(resultado.getString("codigo_institucion"));
                institucionBean.setRazonSocial(resultado.getString("razon_social"));
                institucionBean.setSede(resultado.getString("sede"));
                institucionBean.setDepartamento(resultado.getString("departamento"));
                institucionBean.setProvincia(resultado.getString("provincia"));
                institucionBean.setDistrito(resultado.getString("distrito"));
                institucionBean.setDireccionRegional(resultado.getString("direccion_regional"));
                institucionBean.setUgel(resultado.getString("ugel"));
                institucionBean.setDireccion(resultado.getString("direccion"));
                institucionBean.setAreaGeografica(resultado.getString("area_geografica"));
                institucionBean.setDirector(resultado.getString("director"));
                institucionBean.setIpServidor1(resultado.getString("ip_servidor1"));
                institucionBean.setIpServidor2(resultado.getString("ip_servidor2"));
            }
            
        } catch (Exception e) {
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return institucionBean;
    }
    
}
