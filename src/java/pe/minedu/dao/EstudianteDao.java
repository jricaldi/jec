package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.ControlEstudianteBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.EstudianteExcelBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
import pe.minedu.util.DateUtil;

/**
 *
 * @author GustavoFelix
 */
public class EstudianteDao implements Serializable {

    ResultSet resultado;
    Statement sentencia;

    public EstudianteDao() {
    }

    public EstudianteBean getEstudiante(int idEstudiante) {
        Connection con = null;
        EstudianteBean estudiante = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            
            String sql = "SELECT * FROM estudiante WHERE id_estudiante=?";
           // con = PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEstudiante);
            rs = ps.executeQuery();
            if (rs.next()) {
                estudiante = new EstudianteBean();
                estudiante.setIdEstudiante(rs.getInt("id_estudiante"));
                estudiante.setIdSeccion(rs.getInt("id_seccion"));
                estudiante.setDni(rs.getString("dni"));
                estudiante.setCodEstudiante(rs.getString("cod_estudiante"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellidoPaterno(rs.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(rs.getString("apellido_materno"));
                estudiante.setGenero(rs.getString("genero"));
                estudiante.setEstado(rs.getString("estado"));
            }
            return estudiante;
        } catch (Exception ex) {
            Logger.getLogger(EstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }

    public List<EstudianteBean> listEstudiantesxSeccion(int idSeccion) {
        Connection con = null;
        List<EstudianteBean> estudiantes = new ArrayList<EstudianteBean>();
        EstudianteBean estudiante = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            String sql = "SELECT id_estudiante, id_seccion, dni, cod_estudiante, nombre, apellido_paterno, apellido_materno, genero, estado "
                    + "FROM estudiante "
                    + "WHERE estado = ? "
                    + "AND id_seccion = ? ";

            con = AccesoDB.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, Constante.ESTADO_ACTIVO);
            ps.setInt(2, idSeccion);
            rs = ps.executeQuery();
            if (rs.next()) {
                estudiante = new EstudianteBean();
                estudiante.setIdEstudiante(rs.getInt("id_estudiante"));
                estudiante.setIdSeccion(rs.getInt("id_seccion"));
                estudiante.setDni(rs.getString("dni"));
                estudiante.setCodEstudiante(rs.getString("cod_estudiante"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellidoPaterno(rs.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(rs.getString("apellido_materno"));
                estudiante.setGenero(rs.getString("genero"));
                estudiante.setEstado(rs.getString("estado"));
                estudiantes.add(estudiante);
            }
        } catch (Exception e) {
            Logger.getLogger(EstudianteDao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }

        return estudiantes;
    }

    public void registrarAlumnoMasivo(List<EstudianteExcelBean> estudianteExcelBeans, int idClase, String perfilEstudiante) {
        Connection cn = null;
        PreparedStatement ps = null;
        Calendar calendar = Calendar.getInstance();
        ResultSet rs = null;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);
            String sql0 = "SELECT id_seccion FROM clase WHERE id_clase = ?";
            ps = cn.prepareStatement(sql0);
            ps.setInt(1, idClase);
            rs = ps.executeQuery();
            int idSeccion = 0;
            while (rs.next()) {
                idSeccion = rs.getInt("id_seccion");
            }
            
            String sql_0 = "delete from clase_estudiante where id_estudiante = (select id_estudiante from estudiante where dni = ?)";
            String sql_1 = "delete from usuario where id_estudiante = (select id_estudiante from estudiante where dni = ?)";
            String sql_2 = "delete from estudiante where dni = ?";

            String sql = "INSERT INTO estudiante(id_seccion,dni,cod_estudiante, "
                    + "nombre,apellido_paterno,apellido_materno,genero,estado) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            
            String sql2 = "INSERT INTO usuario(usuario,password,perfil,id_docente,id_estudiante, "
                    + "estado,ultimo_dia_log,ultimo_dia_ip) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            
            String sql3 = "INSERT INTO clase_estudiante(id_estudiante,id_clase,fecha_clase,estado) " 
                    + " VALUES(?,?,?,?)";
                        
            for (EstudianteExcelBean estudianteExcelBean : estudianteExcelBeans) {
                
                //eliminamos rastros del estudiante
                //lo eliminamos de alguna clase_estudiante
                ps = cn.prepareCall(sql_0);
                ps.setString(1, estudianteExcelBean.getDni());
                ps.executeUpdate();
                //lo eliminamos de la tabla usuario
                ps = cn.prepareCall(sql_1);
                ps.setString(1, estudianteExcelBean.getDni());
                ps.executeUpdate();
                //lo eliminamos de la tabla estudiante
                ps = cn.prepareCall(sql_2);
                ps.setString(1, estudianteExcelBean.getDni());
                ps.executeUpdate();
                
                ps = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, idSeccion);
                ps.setString(2, estudianteExcelBean.getDni());
                ps.setString(3, estudianteExcelBean.getCodEstudiante());
                ps.setString(4, estudianteExcelBean.getNombre());
                ps.setString(5, estudianteExcelBean.getApellidoPaterno());
                ps.setString(6, estudianteExcelBean.getApellidoMaterno());
                ps.setString(7, estudianteExcelBean.getGenero());
                ps.setString(8, Constante.ESTADO_ACTIVO);
                ps.executeUpdate();
                ResultSet keys = ps.getGeneratedKeys();
                int idEstudiante = 0;
                if (keys.next()) {
                    idEstudiante = keys.getInt(1);
                }
//                String usuario = Constante.PREFIJO_ALUMNO + estudianteExcelBean.getCodEstudiante().trim().split(" ")[0];
//                String password = estudianteExcelBean.getCodEstudiante().trim().split(" ")[0];
                ps = cn.prepareCall(sql2);
                ps.setString(1, estudianteExcelBean.getDni());
                ps.setString(2, estudianteExcelBean.getDni());
                ps.setString(3, perfilEstudiante);
                ps.setInt(4, 0);
                ps.setInt(5, idEstudiante);
                ps.setString(6, Constante.ESTADO_ACTIVO);
                ps.setString(7, "");
                ps.setString(8, "");
                ps.executeUpdate();
                
                ps=cn.prepareStatement(sql3);
                ps.setInt(1, idEstudiante);
                ps.setInt(2, idClase);
                ps.setDate(3, DateUtil.utilDateToSQLDate(calendar.getTime()));
                ps.setString(4, Constante.ESTADO_INACTIVO);
                ps.executeUpdate();
            }

            cn.commit();
        } catch (Exception e) {
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(EstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            e.printStackTrace();
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }

    public void registrarAlumno(EstudianteExcelBean estudianteExcelBean, int idClase, String perfilEstudiante) {
        Connection cn = null;
        PreparedStatement ps = null;
        Calendar calendar = Calendar.getInstance();
        ResultSet rs = null;
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);

            String sql0 = "SELECT id_seccion FROM clase WHERE id_clase = ?";
            ps = cn.prepareStatement(sql0);
            ps.setInt(1, idClase);
            rs = ps.executeQuery();
            int idSeccion = 0;
            while (rs.next()) {
                idSeccion = rs.getInt("id_seccion");
            }
            
            String sql_0 = "delete from clase_estudiante where id_estudiante = (select id_estudiante from estudiante where dni = ?)";
            String sql_1 = "delete from usuario where id_estudiante = (select id_estudiante from estudiante where dni = ?)";
            String sql_2 = "delete from estudiante where dni = ?";

            //eliminamos rastros del estudiante
            //lo eliminamos de alguna clase_estudiante
            ps = cn.prepareCall(sql_0);
            ps.setString(1, estudianteExcelBean.getDni());
            ps.executeUpdate();
            //lo eliminamos de la tabla usuario
            ps = cn.prepareCall(sql_1);
            ps.setString(1, estudianteExcelBean.getDni());
            ps.executeUpdate();
            //lo eliminamos de la tabla estudiante
            ps = cn.prepareCall(sql_2);
            ps.setString(1, estudianteExcelBean.getDni());
            ps.executeUpdate();

            String sql = "INSERT INTO estudiante(id_seccion,dni,cod_estudiante, "
                    + "nombre,apellido_paterno,apellido_materno,genero,estado) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            ps = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, idSeccion);
            ps.setString(2, estudianteExcelBean.getDni());
            ps.setString(3, estudianteExcelBean.getCodEstudiante());
            ps.setString(4, estudianteExcelBean.getNombre());
            ps.setString(5, estudianteExcelBean.getApellidoPaterno());
            ps.setString(6, estudianteExcelBean.getApellidoMaterno());
            ps.setString(7, estudianteExcelBean.getGenero());
            ps.setString(8, Constante.ESTADO_ACTIVO);
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            int idEstudiante = 0;
            if (keys.next()) {
                idEstudiante = keys.getInt(1);
            }

            String sql2 = "INSERT INTO usuario(usuario,password,perfil,id_docente,id_estudiante, "
                    + "estado,ultimo_dia_log,ultimo_dia_ip) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
//            String usuario = Constante.PREFIJO_ALUMNO + estudianteExcelBean.getCodEstudiante().trim().split(" ")[0];
//            String password = estudianteExcelBean.getCodEstudiante().trim().split(" ")[0];
            ps = cn.prepareCall(sql2);
            ps.setString(1, estudianteExcelBean.getDni());
            ps.setString(2, estudianteExcelBean.getDni());
            ps.setString(3, perfilEstudiante);
            ps.setInt(4, 0);
            ps.setInt(5, idEstudiante);
            ps.setString(6, Constante.ESTADO_ACTIVO);
            ps.setString(7, "");
            ps.setString(8, "");
            ps.executeUpdate();
            
            String sql3 = "INSERT INTO clase_estudiante(id_estudiante,id_clase,fecha_clase,estado) " 
                    + " VALUES(?,?,?,?)";
            ps=cn.prepareStatement(sql3);
            ps.setInt(1, idEstudiante);
            ps.setInt(2, idClase);
            ps.setDate(3, DateUtil.utilDateToSQLDate(calendar.getTime()));
            ps.setString(4, Constante.ESTADO_ACTIVO);
            ps.executeUpdate();

            cn.commit();
        } catch (Exception e) {
            if (cn != null) {
                try {
                    cn.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(EstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            e.printStackTrace();
//            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }

    public List<ControlEstudianteBean> listarAlumnosXSeccion(int idClase) {
        Connection cn = null;
        PreparedStatement ps = null;
        List<ControlEstudianteBean> estudiantes = new ArrayList<ControlEstudianteBean>();
        ControlEstudianteBean estudiante = null;
        ResultSet rs = null;
        try {
            cn = AccesoDB.getConnection();
            
            String sql0 = "SELECT id_seccion FROM clase WHERE id_clase = ?";
            ps = cn.prepareStatement(sql0);
            ps.setInt(1, idClase);
            rs = ps.executeQuery();
            int idSeccion = 0;
            while (rs.next()) {
                idSeccion = rs.getInt("id_seccion");
            }
            
            //cn.setReadOnly(true);
            String sql = "SELECT "
                    + "e.id_estudiante, e.nombre, e.apellido_paterno, e.apellido_materno, e.cod_estudiante, "
                    + "u.usuario, u.password, u.ultimo_dia_log, u.ultimo_dia_ip, u.estado, e.genero, e.id_seccion "
                    + "FROM estudiante e "
                    + "INNER JOIN usuario u "
                    + "ON e.id_estudiante = u.id_estudiante "
                    + "WHERE e.id_seccion = ? ";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idSeccion);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                estudiante = new ControlEstudianteBean();
                estudiante.setIdEstudiante(resultado.getInt("id_estudiante"));
                estudiante.setNombre(resultado.getString("nombre"));
                estudiante.setApellidoPaterno(resultado.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(resultado.getString("apellido_materno"));
                estudiante.setCodEstudiante(resultado.getString("cod_estudiante"));
                estudiante.setUsuario(resultado.getString("usuario"));
                estudiante.setPassword(resultado.getString("password"));
                estudiante.setUltimoDiaLog(resultado.getString("ultimo_dia_log"));
                estudiante.setUltimoDiaIp(resultado.getString("ultimo_dia_ip"));
                estudiante.setEstado(resultado.getString("estado"));
                estudiante.setGenero(resultado.getString("genero"));
                estudiante.setIdSeccion(resultado.getInt("id_seccion"));
                estudiantes.add(estudiante);
            }

        } catch (Exception e) {
            e.printStackTrace();
//            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }

        return estudiantes;
    }
    
    public List<ControlEstudianteBean> listarAlumnosXClase(int idClase) {
        Connection cn = null;
        PreparedStatement ps = null;
        List<ControlEstudianteBean> estudiantes = new ArrayList<ControlEstudianteBean>();
        ControlEstudianteBean estudiante = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT e.id_estudiante, e.nombre, e.apellido_paterno, e.apellido_materno, e.cod_estudiante, c.estado, e.genero "
                    + " FROM clase_estudiante c"
                    + " INNER JOIN estudiante e"
                    + " ON e.id_estudiante = c.id_estudiante "
                    + " WHERE c.id_clase=?";
            ps = cn.prepareCall(sql);
            ps.setInt(1, idClase);
            resultado = ps.executeQuery();
            while (resultado.next()) {
                estudiante = new ControlEstudianteBean();
                estudiante.setIdEstudiante(resultado.getInt("id_estudiante"));
                estudiante.setNombre(resultado.getString("nombre"));
                estudiante.setApellidoPaterno(resultado.getString("apellido_paterno"));
                estudiante.setApellidoMaterno(resultado.getString("apellido_materno"));
                estudiante.setCodEstudiante(resultado.getString("cod_estudiante"));
                estudiante.setEstado(resultado.getString("estado"));
                estudiante.setGenero(resultado.getString("genero"));
                estudiantes.add(estudiante);
            }

        } catch (Exception e) {
            e.printStackTrace();
//            throw new Exception("Error: " + "\t" + e.getMessage());
        } finally{
                try { if (resultado != null) resultado.close(); } catch (Exception e) {};
                try { if (ps != null) ps.close(); } catch (Exception e) {};
        }

        return estudiantes;
    }
}
