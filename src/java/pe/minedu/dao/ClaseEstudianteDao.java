package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
import pe.minedu.util.DateUtil;

/**
 *
 * @author GustavoFelix
 */
public class ClaseEstudianteDao implements Serializable {
    
    ResultSet rs;
    
    public ClaseEstudianteDao() {
    }

    public void iniciarEstudiantesEnClase(int idClase, Date fechaClase, int idEstudiantes[]) {
        Connection con=null;
        PreparedStatement ps=null;
        try {

            String sql="";
            con=AccesoDB.getConnection();
//            con.setReadOnly(false);
            con.setAutoCommit(true);
            
            /*Borra inicios anteriores de la clase*/
            sql="DELETE FROM clase_estudiante WHERE id_clase=?";
            ps=con.prepareStatement(sql);
            ps.setInt(1, idClase);
            ps.executeUpdate();
            
            //Busca si el estudiante tiene alguna clase reciente
            sql="INSERT INTO clase_estudiante(id_clase,id_estudiante,fecha_clase,estado) VALUES(?,?,?,?)";
            for (int idEstudiante: idEstudiantes) {
                ps=con.prepareStatement(sql);
                ps.setInt(1, idClase);
                ps.setInt(2, idEstudiante);
                ps.setDate(3, DateUtil.utilDateToSQLDate(fechaClase));
                ps.setString(4, Constante.ESTADO_INACTIVO);
                ps.executeUpdate();
            }
            
        } catch (Exception e) {
            if(con != null) try {
                con.rollback();
            }catch (SQLException ex) {
                Logger.getLogger(ClaseEstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
    }
    
    public boolean activaAsistenciaDelEstudiante(int idClase, int idEstudiante) throws Exception {
        System.out.println("activaAsistenciaDelEstudiante");
        Connection con=null;
        PreparedStatement ps=null;
        boolean resultado=false;
        try {
            
            String sql="";
      //      con=PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            //con.setReadOnly(false);
            //con.setAutoCommit(true);
            
            /*Actualiza su asistencia a Activo en la clase*/
            sql="UPDATE clase_estudiante SET estado=? WHERE id_clase=? AND id_estudiante=?";
            ps=con.prepareCall(sql);
            ps.setString(1, Constante.ESTADO_ACTIVO);
            ps.setInt(2, idClase);
            ps.setInt(3, idEstudiante);
            if(ps.executeUpdate()>0){
                resultado=true;
            }
            
        } catch (Exception e) {
            if(con != null) try {
                con.rollback();
            }catch (SQLException ex) {
                Logger.getLogger(ClaseEstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return resultado;
    }
    
    public boolean desactivaAsistenciaDelEstudiante(int idClase, int idEstudiante) throws Exception {
        System.out.println("desactivaAsistenciaDelEstudiante");
        Connection con=null;
        PreparedStatement ps=null;
        boolean resultado=false;
        try {
            
            String sql="";
      //      con=PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            //con.setReadOnly(false);
            //con.setAutoCommit(true);
            
            /*Actualiza su asistencia a Activo en la clase*/
            sql="UPDATE clase_estudiante SET estado=? WHERE id_clase=? AND id_estudiante=?";
            ps=con.prepareCall(sql);
            ps.setString(1, Constante.ESTADO_INACTIVO);
            ps.setInt(2, idClase);
            ps.setInt(3, idEstudiante);
            if(ps.executeUpdate()>0){
                resultado=true;
            }
            
        } catch (Exception e) {
            if(con != null) try {
                con.rollback();
            }catch (SQLException ex) {
                Logger.getLogger(ClaseEstudianteDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return resultado;
    }
    
    public List<MonitoreoClaseEstudianteBean> listEstudiantesEnClase(int idClase){
        Connection con=null;
        List<MonitoreoClaseEstudianteBean> estudiantes = new ArrayList<MonitoreoClaseEstudianteBean>();
        MonitoreoClaseEstudianteBean estudiante = null;
        PreparedStatement ps=null;
        try {
            
            con=AccesoDB.getConnection();
            String sql = "SELECT " +
                    "e.id_estudiante, " +
                    "e.cod_estudiante, " +
                    "CONCAT(e.nombre, ' ', e.apellido_paterno, ' ', e.apellido_materno) nombres, " +
                    "uc.fecha, " +
                    "uc.ip " +
                    "FROM usuario_clase uc " +
                    "INNER JOIN usuario u " +
                    "ON uc.id_usuario = u.id_usuario " +
                    "INNER JOIN estudiante e " +
                    "ON u.id_estudiante = e.id_estudiante " +
                    "WHERE u.perfil = ? and uc.id_clase = ? ";
            ps=con.prepareStatement(sql);
            ps.setString(1, Constante.PERFIL_ESTUDIANTE);
            ps.setInt(2, idClase);
            rs = ps.executeQuery();
            while(rs.next()){
                estudiante = new MonitoreoClaseEstudianteBean();
                estudiante.setIdEstudiante(rs.getInt("id_estudiante"));
                estudiante.setCodigoEstudiante(rs.getString("cod_estudiante"));
                estudiante.setNombres(rs.getString("nombres"));
                estudiante.setFechaLogeo(DateUtil.sqlDateToUtilDate(rs.getDate("fecha")));
                estudiante.setIp(rs.getString("ip"));
                estudiantes.add(estudiante);
            }
        } catch (Exception e) {
            Logger.getLogger(ClaseEstudianteDao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return estudiantes;
    }
    
}
