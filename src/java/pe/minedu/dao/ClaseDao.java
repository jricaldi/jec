package pe.minedu.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.ClaseActivaBean;
import pe.minedu.bean.ClaseBean;
import pe.minedu.bean.ClaseCreadaBean;
import pe.minedu.bean.ClaseRecursoBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.jdbc.AccesoDB;
import pe.minedu.util.Constante;
import pe.minedu.util.DateUtil;

/**
 *
 * @author GustavoFelix
 */
public class ClaseDao implements Serializable {
    
    ResultSet rs;
    
    public ClaseDao() {
    }

    public ClaseBean buscaClaseActivoPorSeccionYEstudiante(int idSeccion, int idEstudiante) {
        System.out.println("idSeccion:"+idSeccion+"-idEstudiante:"+idEstudiante);
        Connection con=null;
        PreparedStatement ps = null;
        ClaseBean clase=null;
        try {
            
            //OJO: CREAR SP O JOB PARA CERRAR LA SESIONES ABIERTAS LUEGO DE 5 HORAS (Parametro de config.).
            
            //Busca si el estudiante tiene alguna clase reciente
            String sql = " SELECT c.* FROM clase c, clase_estudiante ce "+
                         " WHERE (c.id_clase=ce.id_clase AND ce.id_estudiante=?) AND c.estado=? AND c.id_seccion=? "+
                         " ORDER BY c.hora_inicio DESC ";
            //con=PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idEstudiante);
            ps.setString(2, Constante.ESTADO_ACTIVO);
            ps.setInt(3, idSeccion);
            rs=ps.executeQuery();
            if(rs.next()){
                // se espera unica respuesta
                clase = new ClaseBean();
                clase.setIdClase(rs.getInt("id_clase"));
                clase.setIdSeccion(rs.getInt("id_seccion"));
                clase.setIdDocente(rs.getInt("id_docente"));
                clase.setIdSesion(rs.getString("id_sesion"));
                clase.setHoraInicio(rs.getDate("hora_inicio"));
                clase.setHoraFin(rs.getDate("hora_fin"));
                clase.setEstado(rs.getString("estado"));
                
                // si hay mas resultados
                if(rs.next()){
                    // Ejecutar algun SP que cierre las clases antiguas
                }
            }
            return clase;
        } catch (Exception ex) {
            System.out.println("Error ClaseDao.buscaClaseActivoPorSeccionYEstudiante " + ex.getMessage());
            ex.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }
    
    public ClaseBean buscaClaseActivoPorDocente(int idDocente) {
        Connection con=null;
        ClaseBean clase=null;
        PreparedStatement ps=null;
        try {
            
            String sql = "SELECT * FROM clase WHERE estado=? AND id_docente=?";
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setString(1, Constante.ESTADO_ACTIVO);
            ps.setInt(2, idDocente);
            rs=ps.executeQuery();
            if(rs.next()){
                clase = new ClaseBean();
                clase.setIdClase(rs.getInt("id_clase"));
                clase.setIdSeccion(rs.getInt("id_seccion"));
                clase.setIdDocente(rs.getInt("id_docente"));
                clase.setIdSesion(rs.getString("id_sesion"));
                clase.setHoraInicio(rs.getDate("hora_inicio"));
                clase.setHoraFin(rs.getDate("hora_fin"));
                clase.setEstado(rs.getString("estado"));
            }
            return clase;
        } catch (Exception ex) {
            System.out.println("Error ClaseDao.buscaClaseActivoPorDocente " + ex.getMessage());
            ex.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return null;
    }
    
    
    public DatosPrincipalesClaseBean obtDatosPrincipalesClase(int idClase){
        Connection con=null;
        DatosPrincipalesClaseBean datosPrincipalesClaseBean = null;
        PreparedStatement ps=null;
        try {

            String sql = "SELECT CONCAT(d.nombre, ' ', d.apellido_paterno) as nombreDocente, " +
                "cur.nombre_curso, " +
                "CONCAT(g.id_grado,'° ', g.nivel_programa) as grado, " +
                "s.nombre_seccion " +
                "FROM clase c " +
                "INNER JOIN sesion ss " +
                "ON ss.id_sesion = c.id_sesion " +
                "INNER JOIN seccion s " +
                "ON c.id_seccion = s.id_seccion " +
                "INNER JOIN docente d " +
                "ON c.id_docente = d.id_docente " +
                "INNER JOIN grado g " +
                "ON s.codigo_grado = g.codigo_grado " +
                "INNER JOIN curso cur " +
                "ON ss.id_curso = cur.id_curso " +
                "WHERE id_clase = ?";
            con=AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idClase);
            rs=ps.executeQuery();
            if(rs.next()){
                datosPrincipalesClaseBean = new DatosPrincipalesClaseBean();
                datosPrincipalesClaseBean.setNombreDocente(rs.getString("nombreDocente"));
                datosPrincipalesClaseBean.setNombreCurso(rs.getString("nombre_curso"));
                datosPrincipalesClaseBean.setGrado(rs.getString("grado"));
                datosPrincipalesClaseBean.setSeccion(rs.getString("nombre_seccion"));
            }
        } catch (Exception e) {
            System.out.println("Error ClaseDao.obtDatosPrincipalesClase " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        
        return datosPrincipalesClaseBean;
    }
    
    public ClaseCreadaBean crearClase(int idSeccion, int idDocente, String idSesion){
        Connection con=null;
        PreparedStatement ps=null;
        ClaseCreadaBean claseCreadaBean = null;
        ClaseBean claseBean = null;
        List<ClaseRecursoBean> claseRecursoBeans = new ArrayList<>();
        ClaseRecursoBean claseRecursoBean = null;
        String sql = "";
        Calendar calendar = Calendar.getInstance();
        int idClase = 0;
        try {
            con=AccesoDB.getConnection();
//            con.setReadOnly(false);
            con.setAutoCommit(false);
            sql = "INSERT INTO clase(id_clase, id_seccion, id_docente, id_sesion, hora_inicio, estado) "
                    + "VALUES (?,?,?,?,?,?)";
            idClase = obtIdTablaClase();
            ps=con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, idClase);
            ps.setInt(2, idSeccion);
            ps.setInt(3, idDocente);
            ps.setString(4, idSesion);
            ps.setDate(5, DateUtil.utilDateToSQLDate(calendar.getTime()));
            ps.setString(6, Constante.ESTADO_ACTIVO);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                idClase = rs.getInt(1);
            }
            
            claseBean = new ClaseBean();
            claseBean.setEstado(Constante.ESTADO_ACTIVO);
            claseBean.setHoraInicio(calendar.getTime());
            claseBean.setIdClase(idClase);
            claseBean.setIdDocente(idDocente);
            claseBean.setIdSeccion(idSeccion);
            claseBean.setIdSesion(idSesion);
            
            sql = "INSERT INTO clase_recurso " +
                    "(id_clase,id_aplicacion,tipo_recurso,titulo,categoria,descripcion,fecha_reg,ruta,ver_estudiante) " +
                    "SELECT ?, id_aplicacion, ?, titulo, categoria, descripcion, ?, ruta, ver_estudiante " +
                    "FROM sesion_recurso sr " +
                    "WHERE sr.id_sesion = ?"; //AND sr.ver_estudiante = ?";
            
            ps=con.prepareStatement(sql);
            ps.setInt(1, idClase);
            ps.setString(2, Constante.RECURSO_MINEDU);
            ps.setDate(3, DateUtil.utilDateToSQLDate(calendar.getTime()));
            ps.setString(4, idSesion);
//            ps.setString(5, Constante.PERMITIR_VER_ESTUDIANTE);
            ps.executeUpdate();
            
            sql = "INSERT INTO clase_estudiante(id_estudiante,id_clase,fecha_clase,estado) " 
                    + "SELECT id_estudiante, ?, ?, ? FROM estudiante WHERE id_seccion = ?";
            ps=con.prepareStatement(sql);
            ps.setInt(1, idClase);
            ps.setDate(2, DateUtil.utilDateToSQLDate(calendar.getTime()));
            ps.setString(3, Constante.ESTADO_INACTIVO);
            ps.setInt(4, idSeccion);
            ps.executeUpdate();
            
            sql = "SELECT id_clase_recurso,id_clase,id_aplicacion,tipo_recurso,titulo,categoria,descripcion,fecha_reg,ruta "
                    + "FROM clase_recurso sr "
                    + "WHERE sr.id_clase = ? ";
            ps=con.prepareStatement(sql);
            ps.setInt(1, idClase);
            rs = ps.executeQuery();
            
            while(rs.next()){
                claseRecursoBean = new ClaseRecursoBean();
                
                claseRecursoBean.setCategoria(rs.getString("categoria"));
                claseRecursoBean.setDescripcion(rs.getString("descripcion"));
                claseRecursoBean.setFechaReg(DateUtil.sqlDateToUtilDate(rs.getDate("fecha_reg")));
                claseRecursoBean.setIdAplicacion(rs.getInt("id_aplicacion"));
                claseRecursoBean.setIdClaseRecurso(rs.getInt("id_clase_recurso"));
                claseRecursoBean.setIdClase(rs.getInt("id_clase"));
                claseRecursoBean.setRuta(rs.getString("ruta"));
                claseRecursoBean.setTipoRecurso(rs.getString("tipo_recurso"));
                claseRecursoBean.setTitulo(rs.getString("titulo"));
                claseRecursoBeans.add(claseRecursoBean);
            }
            con.commit();
        } catch (Exception e) {
            System.out.println("Error ClaseDao.crearClase " + e.getMessage());
            e.printStackTrace();
            if(con != null) try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ClaseDao.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Logger.getLogger(ClaseDao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        claseCreadaBean = new ClaseCreadaBean(claseBean, claseRecursoBeans);
        return claseCreadaBean;
    }
    
    public boolean cerrarClase(int idClase, int idDocente){
        Connection con=null;
        boolean result=false;
        String sql = "";
        Calendar calendar = Calendar.getInstance();
        PreparedStatement ps=null;
        try {
            rs=null;
            con=AccesoDB.getConnection();
            con.setAutoCommit(true);
            sql = "UPDATE clase SET estado=?, hora_fin=? WHERE id_clase=? AND id_docente=? AND estado=?";
            
            ps=con.prepareStatement(sql);
            ps.setString(1, Constante.ESTADO_INACTIVO);
            ps.setDate(2, DateUtil.utilDateToSQLDate(calendar.getTime()));
            ps.setInt(3, idClase);
            ps.setInt(4, idDocente);
            ps.setString(5, Constante.ESTADO_ACTIVO);
            if(ps.executeUpdate()>0){
                result=true;
                //log de cierre de clase
            }
            //con.commit();
        } catch (Exception e) {
            if(con != null) try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ClaseDao.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Error ClaseDao.cerrarClase " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        return result;
    }
    
    private int obtIdTablaClase(){
        int idClaseRecurso = 0;
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            cn = AccesoDB.getConnection();
            String sql = "SELECT max(id_clase) max_id "
                    + " FROM clase";
             
            ps = cn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            while(resultSet.next()){
                idClaseRecurso = resultSet.getInt("max_id");
            }
        } catch (Exception e) {
            System.out.println("Error ClaseDao.obtIdTablaClase " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {};
            try { if (ps != null) ps.close(); } catch (Exception e) {};
        }
        idClaseRecurso++;
        return idClaseRecurso;
    }
    
    public String obtCodigoSesionxIdClase(int idClase){
        String dato = "";
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT s.codigo_sesion FROM clase c " 
                    + "INNER JOIN sesion s "
                    + "ON c.id_sesion = s.id_sesion "
                    + "WHERE id_clase = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            rs = preparedStatement.executeQuery();
            if(rs.next()){
                dato = rs.getString("codigo_sesion");
            }
        } catch (Exception e) {
            System.out.println("Error ClaseDao.obtCodigoSesionxIdClase " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return dato;
    }
    
    public String obtCodigoGradoxIdClase(int idClase){
        String dato = "";
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT cur.codigo_grado FROM clase c " 
                    + "INNER JOIN sesion s "
                    + "ON c.id_sesion = s.id_sesion "
                    + "INNER JOIN curso cur "
                    + "ON cur.id_curso = s.id_curso "
                    + "WHERE id_clase = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            rs = preparedStatement.executeQuery();
            if(rs.next()){
                dato = rs.getString("codigo_grado");
            }
        } catch (Exception e) {
            System.out.println("Error ClaseDao.obtCodigoGradoxIdClase " + e.getMessage());
            e.printStackTrace();
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return dato;
    }
    
    public String[] obtDatosCreacionCarpetaIdClase(int idClase) throws Exception{
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String[] rutaCarpeta = new String[3];
        try {
            connection = AccesoDB.getConnection();
            String sql = "SELECT cur.codigo_grado, cur.nombre_curso, s.codigo_sesion " +
                    "FROM clase c " +
                    "INNER JOIN sesion s " +
                    "ON c.id_sesion = s.id_sesion " +
                    "INNER JOIN curso cur " +
                    "ON cur.id_curso = s.id_curso " +
                    "WHERE c.id_clase = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClase);
            rs = preparedStatement.executeQuery();
            if(rs.next()){
                rutaCarpeta[0] = rs.getString("codigo_grado");
                rutaCarpeta[1] = rs.getString("nombre_curso");
                rutaCarpeta[2] = rs.getString("codigo_sesion");
            }
        } catch (Exception e) {
            System.out.println("Error ClaseDao.obtDatosCreacionCarpetaIdClase " + e.getMessage());
            e.printStackTrace();
            throw new Exception(e);
        } finally{
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
        }
        return rutaCarpeta;
    }
    
    
    public List<ClaseActivaBean> obtClasesActivaXEstudiante(int idEstudiante) {
        System.out.println("obtClasesActivaXEstudiante idEstudiante: "+idEstudiante);
        Connection con=null;
        ClaseActivaBean claseActivaBean=null;
        List<ClaseActivaBean> list = new ArrayList<>();
        try {
            PreparedStatement ps=null;
            ResultSet rs=null;
            
            //Busca si el estudiante tiene alguna clase reciente
            String sql = " SELECT c.id_clase, concat(ifnull(cur.nombre_curso,'') , ' - ' , ifnull(doc.nombre,'') , ' ' , ifnull(doc.apellido_paterno,'') ) clase_str " +
                    "FROM clase c " +
                    "INNER JOIN clase_estudiante ce " +
                    "ON c.id_clase=ce.id_clase " +
                    "INNER JOIN sesion s " +
                    "ON s.id_sesion = c.id_sesion " +
                    "INNER JOIN curso cur " +
                    "ON cur.id_curso = s.id_curso " +
                    "INNER JOIN docente doc " +
                    "ON doc.id_docente = c.id_docente " +
                    "WHERE ce.id_estudiante=? AND c.estado=? " +
                    "ORDER BY c.hora_inicio DESC ";
            //con=PoolConnection.getConnection();
            con = AccesoDB.getConnection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idEstudiante);
            ps.setString(2, Constante.ESTADO_ACTIVO);
            rs=ps.executeQuery();
            while(rs.next()){
                claseActivaBean = new ClaseActivaBean();
                // se espera unica respuesta
                claseActivaBean.setIdClase(rs.getInt("id_clase"));
                claseActivaBean.setNombreClase(rs.getString("clase_str"));
                list.add(claseActivaBean);
            }
        } catch (Exception ex) {
            System.out.println("Error ClaseDao.obtClasesActivaXEstudiante " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            if (con != null) {
            }
        }
        return list;
    }
}
