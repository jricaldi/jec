package pe.minedu.servlet;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pe.minedu.bean.AplicacionBean;
import pe.minedu.bean.ClaseRecursoBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.bean.RemoteServerBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ListaBeanService;
import pe.minedu.service.ManejoAplicacionService;
import pe.minedu.service.ManejoRecursoService;
import pe.minedu.util.Constante;

@WebServlet(name = "RecursosServlet", urlPatterns = {"/comun/RecursosServlet", "/comun_IrARecursos", "/comun/RegistrarNuevoRecurso", "/comun/AbrirRecurso"})
public class RecursosServlet extends HttpServlet {

    private ManejoAplicacionService manejoAplicacionService;
    private ListaBeanService listaBeanService;
    private ManejoRecursoService manejoRecursoService;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getServletPath();
        if (accion.equals("/comun_IrARecursos")) {
            irARecursos(request, response);
        } else if (accion.equals("/comun/RegistrarNuevoRecurso")) {
            registrarNuevoRecurso(request, response);
        } else if (accion.equals("/comun/AbrirRecurso")) {
            abrirRecurso(request, response);
        }
    }

    private void irARecursos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "comun/recursos/mainRecursos.jsp";
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");

            manejoRecursoService = new ManejoRecursoService();
            List<ClaseRecursoBean> claseRecursoBeans = manejoRecursoService.listarRecursosxClase(usuarioClase.getIdClase(),usuario.getPerfil());
            request.setAttribute("listRecursosClase", claseRecursoBeans);

            List<AplicacionBean> listaAplicaciones = new ArrayList<AplicacionBean>();

            if (usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)) {
                manejoAplicacionService = new ManejoAplicacionService();
                listaAplicaciones = manejoAplicacionService.listarAplicaciones();
            }

            request.setAttribute("listaAplicaciones", listaAplicaciones);

        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }

    private void registrarNuevoRecurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = null;
        File file;
        FileItem fileItem;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        String filePath = "";

        //Para inputs que no son tipo file
        String fieldname;
        String fieldvalue;

        // Verificando el content type
        String contentType = request.getContentType();
        try {
            out = response.getWriter();
            manejoRecursoService = new ManejoRecursoService();
            manejoAplicacionService = new ManejoAplicacionService();
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
            DatosPrincipalesClaseBean datosPrincipalesClaseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");
            ClaseRecursoBean claseRecursoBean = new ClaseRecursoBean();
            filePath = manejoRecursoService.obtCarpetaDeArchivoNuevo(usuarioClase.getIdClase());
            if(filePath.equals("")){
                throw new Exception("Error en creacion de carpeta clase, la ruta del folder no existe");
            }
            
            if ((contentType.indexOf("multipart/form-data") >= 0)) {
                DiskFileItemFactory factory = new DiskFileItemFactory();
                // Setear el la capacidad maxima que vamos a guardar en memoria
                factory.setSizeThreshold(maxMemSize);
                // Seteamos carpeta donde guardaremos data mayor a el tamaño de la memoria (maxMemSize).
                factory.setRepository(new File(Constante.REPOSITORIO_WEB_TOMCAT));
                // Creamos un nuevo manejador de archivos
                ServletFileUpload upload = new ServletFileUpload(factory);
                // Seteamos el tamaño maximo a subirse por el aplicativo
                upload.setSizeMax(maxFileSize);
                // Parse the request to get file items.
                List fileItems = upload.parseRequest(request);
                // Process the uploaded file items
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
//                        claseRecursoBean.setRuta(request.getParameter("txtRuta"));
                    fileItem = (FileItem) i.next();
                    if (!fileItem.isFormField()) {
                        String fileName = fileItem.getName();
                        if (fileName.lastIndexOf("\\") >= 0) {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fileItem.write(file);
                        if (file.exists()) {
                            claseRecursoBean.setRuta(file.getAbsolutePath());
                        }
                    } else {
                        fieldname = fileItem.getFieldName();
                        fieldvalue = fileItem.getString();
                        if (fieldname.equals("txtTitulo")) {
//                                claseRecursoBean.setTitulo(request.getParameter("txtTitulo"));
                            claseRecursoBean.setTitulo(fieldvalue);
                        } else if (fieldname.equals("txtDescripcion")) {
//                                claseRecursoBean.setDescripcion(request.getParameter("txtDescripcion"));
                            claseRecursoBean.setDescripcion(fieldvalue);
                        } else if (fieldname.equals("cbTipoAplicacion")) {
//                                request.getParameter("cbTipoAplicacion")
                            int idAplicacion = Integer.parseInt(fieldvalue);
                            claseRecursoBean.setIdAplicacion(idAplicacion);
                        }
                    }
                }

                claseRecursoBean.setCategoria("");
                claseRecursoBean.setTipoRecurso(Constante.RECURSO_DOCENTE);
                claseRecursoBean.setIdClase(usuarioClase.getIdClase());
                claseRecursoBean.setFechaReg(Calendar.getInstance().getTime());
                manejoRecursoService.registrarNuevoRecurso(claseRecursoBean, datosPrincipalesClaseBean.getNombreCurso(), usuarioClase.getIdClase());

                List<ClaseRecursoBean> claseRecursoBeans = manejoRecursoService.listarRecursosxClase(usuarioClase.getIdClase(),usuario.getPerfil());
                request.setAttribute("listRecursosClase", claseRecursoBeans);
                List<AplicacionBean> listaAplicaciones = new ArrayList<AplicacionBean>();
                if (usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)) {
                    manejoAplicacionService = new ManejoAplicacionService();
                    listaAplicaciones = manejoAplicacionService.listarAplicaciones();
                }
                request.setAttribute("listaAplicaciones", listaAplicaciones);
                out.print("Se registro el recurso con éxito.");
            } else {
                out.print("No se ha subido ningún archivo");
            }
        } catch (Exception e) {
            if (out != null) out.print("Error RecursosServlet.registrarNuevoRecurso " + e.getMessage());
            System.out.println("Error RecursosServlet.registrarNuevoRecurso " + e.getMessage());
            e.printStackTrace();
        } finally{
            if (out != null) {
                out.close();
            }
        }
    }

    private void abrirRecurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            manejoRecursoService = new ManejoRecursoService();
            listaBeanService = new ListaBeanService();
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");

            int idClaseRecurso = Integer.parseInt(request.getParameter("idClaseRecurso"));
            String tipoBoton = request.getParameter("tipoBoton").toUpperCase();
            ClaseRecursoBean claseRecursoBean = manejoRecursoService.obtClaseRecursoxId(idClaseRecurso);

            if (usuario.getPerfil().equals(Constante.PERFIL_ESTUDIANTE)) {
                EstudianteBean estudianteBean = listaBeanService.obtEstudiantexId(usuario.getIdEstudiante());
                Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                RemoteServerBean comando = new RemoteServerBean();
                comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO);
                comando.setPerfil(Constante.PERFIL_ESTUDIANTE);
                comando.setDocumento(claseRecursoBean.getRuta());
                comando.setCodigoClase(usuarioClase.getIdClase() + "");
                comando.setCodigoUsuario(estudianteBean.getCodEstudiante() + "");
                oos.writeObject(comando);
                oos.flush();
                oos.close();
                server.close();
            } else if (usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)) {
                if(tipoBoton.equals(Constante.LOCAL)){
                    Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                    ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                    RemoteServerBean comando = new RemoteServerBean();
                    comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO);
                    comando.setPerfil(Constante.PERFIL_DOCENTE);
                    comando.setDocumento(claseRecursoBean.getRuta());
                    comando.setCodigoClase(usuarioClase.getIdClase() + "");
                    comando.setCodigoUsuario(usuario.getIdDocente() + "");
                    oos.writeObject(comando);
                    oos.flush();
                    oos.close();
                    server.close();
                }
                else{
                    List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(usuarioClase.getIdClase());
                    for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
                        Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
                        ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                        RemoteServerBean comando = new RemoteServerBean();
                        comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO);
                        comando.setPerfil(Constante.PERFIL_ESTUDIANTE);
                        comando.setDocumento(claseRecursoBean.getRuta());
                        comando.setCodigoClase(usuarioClase.getIdClase() + "");
                        comando.setCodigoUsuario(estudiante.getCodigoEstudiante());
                        oos.writeObject(comando);
                        oos.flush();
                        oos.close();
                        server.close();
                    }
                    Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                    ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                    RemoteServerBean comando = new RemoteServerBean();
                    comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO);
                    comando.setPerfil(Constante.PERFIL_DOCENTE);
                    comando.setDocumento(claseRecursoBean.getRuta());
                    comando.setCodigoClase(usuarioClase.getIdClase() + "");
                    comando.setCodigoUsuario(usuario.getIdDocente() + "");
                    oos.writeObject(comando);
                    oos.flush();
                    oos.close();
                    server.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
