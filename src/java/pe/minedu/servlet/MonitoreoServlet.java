
package pe.minedu.servlet;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.bean.RemoteServerBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ListaBeanService;
import pe.minedu.service.MonitoreoService;
import pe.minedu.service.UsuarioService;
import pe.minedu.util.Constante;

@WebServlet(name = "MonitoreoServlet", urlPatterns = {"/MonitoreoServlet", 
    "/TomarPantallazoUno", 
    "/EnviarMensajeTodos", "/EnviarMensajeUno", 
    "/BloquearTodos", "/BloquearUno",
    "/BloquearTodosOverlay", "/BloquearUnoOverlay", 
    "/DesbloquearTodos", "/DesbloquearUno",
    "/DesbloquearTodosOverlay", "/DesbloquearUnoOverlay",
    "/AbrirBrowserTodos", "/AbrirBrowserUno",
    "/GetEstadoBloqueo","/ListarPantallas"})
public class MonitoreoServlet extends HttpServlet {

    private ListaBeanService listaBeanService;
    private MonitoreoService monitoreoService;
    private UsuarioService usuarioService;
    
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getServletPath();
        if(accion.equals("/TomarPantallazoUno")){
            tomarPantallazoUno(request, response);
        }else if(accion.equals("/EnviarMensajeTodos")){
            enviarMensajeTodos(request, response);
        }else if(accion.equals("/EnviarMensajeUno")){
            enviarMensajeUno(request, response);
        }else if(accion.equals("/BloquearTodos")){
            bloquearTodos(request, response);
        }else if(accion.equals("/BloquearUno")){
            bloquearUno(request, response);
        }else if(accion.equals("/DesbloquearUno")){
            desbloquearUno(request, response);
        }else if(accion.equals("/DesbloquearTodos")){
            desbloquearTodos(request, response);
        }else if(accion.equals("/MonitoreoServlet")){
            monitoreoServlet(request, response);
        }else if(accion.equals("/AbrirBrowserTodos")){
            abrirBrowserTodos(request, response);
        }else if(accion.equals("/AbrirBrowserUno")){
            abrirBrowserUno(request, response);
        }else if(accion.equals("/GetEstadoBloqueo")){
            getEstadoBloqueo(request, response);
        }else if(accion.equals("/BloquearTodosOverlay")){
            bloquearTodosOverlay(request, response);
        }else if(accion.equals("/BloquearUnoOverlay")){
            bloquearUnoOverlay(request, response);
        }else if(accion.equals("/DesbloquearTodosOverlay")){
            desbloquearTodosOverlay(request, response);
        }else if(accion.equals("/DesbloquearUnoOverlay")){
            desbloquearUnoOverlay(request, response);
        }else if(accion.equals("/ListarPantallas")){
            listarPantallas(request, response);
        }
    }
    
    private void monitoreoServlet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/monitoreo/mainMonitoreo.jsp";
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            int idClase = usuarioClase.getIdClase();
            listaBeanService = new ListaBeanService();
            List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
            System.out.println("estudiantes.size " + estudiantes.size());
            request.setAttribute("estudiantes", estudiantes);
            request.setAttribute("idClase", usuarioClase.getIdClase());
            
            //Screenshoot de alumnos 
            int cantFilas;
            int cantColumnas = 4;
            cantFilas = estudiantes.size() / cantColumnas + (estudiantes.size()%cantColumnas==0?0:1);
            request.setAttribute("cantColumnas", cantColumnas);
            request.setAttribute("cantFilas", cantFilas);
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
	rd.forward(request, response);
    }
    
    private void tomarPantallazoUno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String userIP = request.getParameter("hfIpAlumno");
            System.out.println("tomarPantallazoUno.userIP " + userIP);
            response.setContentType("image/png");
            Socket server = new Socket(userIP, Constante.PUERTO_SOCKET);
            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
            RemoteServerBean comando = new RemoteServerBean();
            comando.setAccion(Constante.COMANDO_SCREENSHOOT);
            oos.writeObject(comando);
            oos.flush();
            
            ObjectInputStream in = new ObjectInputStream(server.getInputStream());
            Rectangle size = (Rectangle) in.readObject();
            int[] rgbData = new int[(int) (size.getWidth() * size.getHeight())];
            for (int x = 0; x < rgbData.length; x++) {
                rgbData[x] = in.readInt();
            }
            
            in.close();
            server.close();
            BufferedImage screen = new BufferedImage((int) size.getWidth(), (int) size.getHeight(), BufferedImage.TYPE_INT_ARGB);
            screen.setRGB(0, 0, (int) size.getWidth(), (int) size.getHeight(), rgbData, 0, (int) size.getWidth());
            OutputStream out = response.getOutputStream();
            
            ImageIO.write(screen, "png", out);
            out.flush();
            out.close();
            oos.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("ex " + ex.getMessage());
        }
    }
    
    private void enviarMensajeTodos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        String mensaje = request.getParameter("mensaje");
        
        for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
            Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
            RemoteServerBean comando = new RemoteServerBean();
            comando.setAccion(Constante.COMANDO_ENVIAR_MENSAJE);
            comando.setNombreDocente("Docente dice: ");  //HARDCODEADO
            comando.setMensaje(mensaje);
            oos.writeObject(comando);
            oos.flush();
            oos.close();
            server.close();
        }
    }
    
    private void enviarMensajeUno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIP = request.getParameter("hfIpAlumno");
        String mensaje = request.getParameter("mensaje");
        System.out.println("enviarMensajeUno.userIP " + userIP);
        Socket server = new Socket(userIP, Constante.PUERTO_SOCKET);
        ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
        RemoteServerBean comando = new RemoteServerBean();
        comando.setAccion(Constante.COMANDO_ENVIAR_MENSAJE);
        comando.setNombreDocente("Docente dice: ");  //HARDCODEADO
        comando.setMensaje(mensaje);
        oos.writeObject(comando);
        oos.flush();
        oos.close();
        server.close();
    }

    private void bloquearTodos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
            RemoteServerBean comando = new RemoteServerBean();
            comando.setAccion(Constante.COMANDO_BLOQUEAR_ESCRITORIO);
            Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
            oos.writeObject(comando);
            oos.flush();
            oos.close();
            server.close();
        }
    }
    
    private void bloquearTodosOverlay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        monitoreoService = new MonitoreoService();

        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        try (PrintWriter pw = response.getWriter()) {
            String mensaje = monitoreoService.bloquearEquipoTodos(idClase, estudiantes);
            pw.write(mensaje);
        }
    }
    
    private void bloquearUno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIP = request.getParameter("hfIpAlumno");
        System.out.println("bloquearUno.userIP " + userIP);
        Socket server = new Socket(userIP, Constante.PUERTO_SOCKET);
        ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
        RemoteServerBean comando = new RemoteServerBean();
        comando.setAccion(Constante.COMANDO_BLOQUEAR_ESCRITORIO);
        oos.writeObject(comando);
        oos.flush();
        oos.close();
        server.close();
    }
    
    private void bloquearUnoOverlay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String userIP = request.getParameter("hfIpAlumno");
        HttpSession session = request.getSession(); 
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
//        int idEstudiante = Integer.parseInt(session.getAttribute("idEstudiante").toString());
        int idEstudiante = Integer.parseInt(request.getParameter("idEstudiante"));
        int idClase = usuarioClase.getIdClase();
        monitoreoService = new MonitoreoService();
        try (PrintWriter pw = response.getWriter()) {
            String mensaje = monitoreoService.bloquearEquipo(idClase, idEstudiante);
            pw.write(mensaje);
        }
        
    }
    
    private void desbloquearUno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        usuarioService = new UsuarioService();
        String userIP = request.getParameter("hfIpAlumno");
        System.out.println("desbloquearUno.userIP " + userIP);
        HttpSession session = request.getSession(); 
        UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
        usuarioService.desactivaAsistenciaDelEstudiante(usuario);
        Socket server = new Socket(userIP, Constante.PUERTO_SOCKET);
        ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
        RemoteServerBean comando = new RemoteServerBean();
        comando.setAccion(Constante.COMANDO_DESBLOQUEAR_ESCRITORIO);
        oos.writeObject(comando);
        oos.flush();
        oos.close();
        server.close();
    }
    
    private void desbloquearUnoOverlay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String userIP = request.getParameter("hfIpAlumno");
        HttpSession session = request.getSession(); 
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
//        int idEstudiante = Integer.parseInt(session.getAttribute("idEstudiante").toString());
        int idEstudiante = Integer.parseInt(request.getParameter("idEstudiante"));
        
        int idClase = usuarioClase.getIdClase();
        monitoreoService = new MonitoreoService();
        try (PrintWriter pw = response.getWriter()) {
            String mensaje = monitoreoService.desbloquearEquipo(idClase, idEstudiante);
            pw.write(mensaje);
        }
    }
    
    private void desbloquearTodos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
            Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
            RemoteServerBean comando = new RemoteServerBean();
            comando.setAccion(Constante.COMANDO_DESBLOQUEAR_ESCRITORIO);
            oos.writeObject(comando);
            oos.flush();
            oos.close();
            server.close();
        }
    }
    
    private void desbloquearTodosOverlay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        monitoreoService = new MonitoreoService();

        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        try (PrintWriter pw = response.getWriter()) {
            String mensaje = monitoreoService.desbloquearEquipoTodos(idClase, estudiantes);
            pw.write(mensaje);
        }
    }
    
    private void abrirBrowserTodos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idClase = usuarioClase.getIdClase();
        listaBeanService = new ListaBeanService();
        List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
        for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
            Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
            RemoteServerBean comando = new RemoteServerBean();
            comando.setAccion(Constante.COMANDO_ABRIR_BROWSER);
            oos.writeObject(comando);
            oos.flush();
            oos.close();
            server.close();
        }
    }
    
    private void abrirBrowserUno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIP = request.getParameter("hfIpAlumno");
        Socket server = new Socket(userIP, Constante.PUERTO_SOCKET);
        ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
        RemoteServerBean comando = new RemoteServerBean();
        comando.setAccion(Constante.COMANDO_ABRIR_BROWSER);
        oos.writeObject(comando);
        oos.flush();
        oos.close();
        server.close();
    }
    
    private void getEstadoBloqueo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String userIP = request.getParameter("hfIpAlumno");
        HttpSession session = request.getSession(); 
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        int idEstudiante = Integer.parseInt(session.getAttribute("idEstudiante").toString());
        
        int idClase = usuarioClase.getIdClase();
        monitoreoService = new MonitoreoService();
        try (PrintWriter pw = response.getWriter()) {
            MonitoreoClaseEstudianteBean monitoreo = monitoreoService.getEstadoBloqueo(idClase, idEstudiante);
            String test = String.valueOf(monitoreo.getBloqueado());
            pw.write(test);
        }


    }
    
    private void listarPantallas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/monitoreo/pantallasAlumnos.jsp";
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            int idClase = usuarioClase.getIdClase();
            listaBeanService = new ListaBeanService();
            List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);
            System.out.println("estudiantes.size " + estudiantes.size());
            request.setAttribute("estudiantes", estudiantes);
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
	rd.forward(request, response);
    }
}
