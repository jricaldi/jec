/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pe.minedu.bean.ClaseCreadaBean;
import pe.minedu.bean.CursoDeDocenteBean;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ClaseService;
import pe.minedu.service.CursoDocenteService;
import pe.minedu.service.SeccionService;
import pe.minedu.service.SesionService;
import pe.minedu.service.SesionServiceImpl;
import pe.minedu.service.UsuarioService;
import pe.minedu.util.Constante;

/**
 *
 * @author edwin
 */
@WebServlet(name = "ClaseServlet", urlPatterns = {"/docente/ClaseIniciarServlet", "/docente/ClaseSelectSeccionServlet", "/docente/ClaseSelectSesionServlet", "/docente/ClasePublicarServlet", "/docente/ClaseFinalizarServlet","/docente/ClaseSelectCursoServlet"})
public class ClaseServlet extends HttpServlet {

    private CursoDocenteService cursoDocenteService;
    private ClaseService claseService;
    private SeccionService seccionService;
    private SesionService sesionService;
    private UsuarioService usuarioService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //super.service(req, resp);
        String accion = req.getServletPath();
        if(accion.equals("/docente/ClaseIniciarServlet")){
            iniciar(req, res);
        }else if(accion.equals("/docente/ClaseSelectSeccionServlet")){
            listaSecciones(req, res);
        }else if(accion.equals("/docente/ClaseSelectCursoServlet")){
            listaCursosByDocenteyGrado(req, res);
        }else if(accion.equals("/docente/ClaseSelectSesionServlet")){
            listaSesiones(req, res);
        }else if(accion.equals("/docente/ClasePublicarServlet")){
            publicar(req, res);
        }else if(accion.equals("/docente/ClaseFinalizarServlet")){
            finalizar(req, res);
        }
    }
    
    private void iniciar(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        cursoDocenteService = new CursoDocenteService();
        
        String view="clase/iniciarClase.jsp";
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE)){
            int idDocente=usuarioBean.getIdDocente();
           List<CursoDeDocenteBean> lista = cursoDocenteService.listaGradosByDocente(idDocente);
            req.setAttribute("GradosByDocente", lista);
        }
        RequestDispatcher rd = req.getRequestDispatcher(view);
        rd.forward(req, res);
    }
     private void listaCursosByDocenteyGrado(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        //claseService = new ClaseService();
        seccionService = new SeccionService();
        
        String view="clase/iniciarClaseSelect.jsp";
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        String idGrado = req.getParameter("idGrado");        
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE)){
            int idDocente=usuarioBean.getIdDocente();
            System.out.println("pe.minedu.servlet.ClaseServlet.listaSeccion()::: "+idDocente+"-"+idGrado);
            req.setAttribute("Cursos", cursoDocenteService.listaCursosByDocenteYGrado(idDocente, idGrado));
             System.out.println("pe.minedu.servlet.ClaseServlet.listaSeccion()::: "+cursoDocenteService.listaCursosByDocenteYGrado(idDocente, idGrado).size());
        }
        RequestDispatcher rd = req.getRequestDispatcher(view);
        req.setAttribute("select", "curso");
        rd.forward(req, res);
    }
    private void listaSecciones(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        //claseService = new ClaseService();
        seccionService = new SeccionService();
        
        String view="clase/iniciarClaseSelect.jsp";
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        String idGrado = req.getParameter("idGrado");
        String idCurso = req.getParameter("selectCurso");
       // String codGrado = idGradoCurso.split(":")[0];
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE)){
            int idDocente=usuarioBean.getIdDocente();
            System.out.println("pe.minedu.servlet.ClaseServlet.listaSeccion():::Grado: "+idGrado+"-Curso:"+idCurso);
            req.setAttribute("Secciones", seccionService.listSeccionesXGrado(idGrado));
        }
        RequestDispatcher rd = req.getRequestDispatcher(view);
        req.setAttribute("select", "seccion");
        rd.forward(req, res);
    }
    
    private void listaSesiones(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        //claseService = new ClaseService();
        sesionService = new SesionServiceImpl();
        
        String view="clase/iniciarClaseSelect.jsp";
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        String idCurso = req.getParameter("selectCurso");
       // String idCursoString = idGradoCurso.split(":")[2];
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE)){
            int idCursoe=Integer.parseInt(idCurso);
            System.out.println("pe.minedu.servlet.ClaseServlet.listaSesiones()"+idCurso);
            req.setAttribute("Sesiones", sesionService.getSesionesByCurso(idCursoe));
        }
        RequestDispatcher rd = req.getRequestDispatcher(view);
        req.setAttribute("select", "sesion");
        rd.forward(req, res);
    }
    
    private void publicar(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        String view="../";
        
        claseService = new ClaseService();
        int idSeccion=0, idDocente=0;
        String idSesion="0";
        String ip = req.getRemoteHost();
        
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE)){
            idSeccion = Integer.parseInt(req.getParameter("selectSeccion")==null?"0":req.getParameter("selectSeccion"));
            idDocente = usuarioBean.getIdDocente();
            idSesion = req.getParameter("selectSesion");
            
            //// REVISAR UsuarioServlet
            //*** Proceso sesion de clase #1: se crea la clase
            ClaseCreadaBean claseCreada = claseService.crearClase(idSeccion, idDocente, idSesion);
            
            if(claseCreada!=null){
                //*** Proceso sesion de clase #2: se inicia docente en IP
                usuarioService = new UsuarioService();
                UsuarioClaseBean usuarioClase = usuarioService.iniciarDocenteClaseEnIP(usuarioBean, ip);

                //*** Proceso sesion de clase #3: se recupera informacion de la clase
                claseService = new ClaseService();
                DatosPrincipalesClaseBean claseInfo = claseService.obtDatosPrincipalesClase(claseCreada.getClaseBean().getIdClase());

                //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
                HttpSession session = req.getSession();
                session.setAttribute("idClase", usuarioClase.getIdClase().toString());//OK:igual a usuarioServlet
                session.setAttribute("idSesion", usuarioClase.getIdSesion());//OK:igual a usuarioServlet
                
                session.setAttribute("claseInfo", claseInfo);//OK:igual a usuarioServlet
                session.setAttribute("usuarioClase", usuarioClase);//OK:igual a usuarioServlet
                
            }
        }
        
        
        res.sendRedirect(view);
    }
    
    private void finalizar(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        String view="../?"+(new Date().getTime());
        
        int idClase, idDocente;
        
        UsuarioBean usuarioBean = (UsuarioBean)req.getSession().getAttribute("usuario");
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean)req.getSession().getAttribute("usuarioClase");
        if(usuarioBean.getPerfil().equals(Constante.PERFIL_DOCENTE) && usuarioClase!=null){
            idClase = usuarioClase.getIdClase();
            idDocente = usuarioBean.getIdDocente();
            
            HttpSession session = req.getSession();
//            session.setAttribute("claseInfo", claseCreada);
//            session.setAttribute("usuarioClase", usuarioClase);
//            session.setAttribute("usuarioDatosClase", usuarioDatosClase);
            
            //*** Proceso sesion de clase #99: se finaliza la sesion de clase
            claseService = new ClaseService();
            if(claseService.cerrarClase(idClase, idDocente)){
                //session.removeAttribute("idClase");
                //session.removeAttribute("idSesion");
                session.setAttribute("idClase", "0");
                session.setAttribute("idSesion", "0");
                
                session.removeAttribute("claseInfo");
                session.removeAttribute("usuarioClase");
            }
        }
        
        //System.out.println("URL: " + view);
        res.sendRedirect(view);
    }

}
