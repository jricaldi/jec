package pe.minedu.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pe.minedu.bean.ControlEstudianteBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.bean.EstudianteExcelBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ControlAlumnoService;
import pe.minedu.util.Constante;

@WebServlet(name = "ControlAlumnoServlet", urlPatterns = {"/docente/ControlAlumnoServlet", "/docente_IrAControlEstudiante", "/docente_estudiantes_FileUploadServlet",
    "/docente_estudiantes_NuevoAlumno", "/docente_estudiantes_ListarAlumnos", "/docente_estudiantes_AgregarNuevoAlumno", "/docente_listarConectados"})
public class ControlAlumnoServlet extends HttpServlet {

    private ControlAlumnoService controlAlumnoService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getServletPath();
        if (path.equals("/docente_IrAControlEstudiante")) {
            irAControlEstudiante(req, resp);
        } else if (path.equals("/docente_estudiantes_FileUploadServlet")) {
            alumnosExcel(req, resp);
        } else if (path.equals("/docente_estudiantes_ListarAlumnos")) {
            listarAlumnos(req, resp);
        } else if (path.equals("/docente_estudiantes_NuevoAlumno")) {
            nuevoAlumno(req, resp);
        } else if (path.equals("/docente_estudiantes_AgregarNuevoAlumno")) {
            agregarNuevoAlumno(req, resp);
        } else if (path.equals("/docente_listarConectados")) {
            listarConectados(req, resp);
        }
    }

    private void listarAlumnos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
        DatosPrincipalesClaseBean claseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");

        try {
            controlAlumnoService = new ControlAlumnoService();
            List<ControlEstudianteBean> estudiantes = controlAlumnoService.listarAlumnosXSeccion(usuarioClase.getIdClase());
            request.setAttribute("estudiantes", estudiantes);
        } catch (Exception ex) {
            Logger.getLogger(ControlAlumnoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        String destino = "docente/estudiantes/divListarAlumnos.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }

    private void alumnosExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
        DatosPrincipalesClaseBean claseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        // process only if it is multipart content
        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                // Parse the request
                List<FileItem> multiparts = upload.parseRequest(request);
                List<EstudianteExcelBean> estudianteExcelBeans = new ArrayList<EstudianteExcelBean>();
                EstudianteExcelBean estudianteExcelBean;
                int i = 0;
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        System.out.println("Items " + item.getName());
                        String name = new File(item.getName()).getName();
                        System.out.println("name " + name);
                        XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(item.getInputStream());
                        XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
                        Iterator<Row> rowIterator = xSSFSheet.iterator();
                        while (rowIterator.hasNext()) {
                            Row row = rowIterator.next();
                            if (i != 0) {
                                estudianteExcelBean = new EstudianteExcelBean();
                                estudianteExcelBean.setNombre(row.getCell(0).getStringCellValue());
                                estudianteExcelBean.setApellidoPaterno(row.getCell(1).getStringCellValue());
                                estudianteExcelBean.setApellidoMaterno(row.getCell(2).getStringCellValue());
//                                estudianteExcelBean.setCodEstudiante(row.getCell(3).getStringCellValue());
                                estudianteExcelBean.setCodEstudiante(row.getCell(4).getStringCellValue());
                                estudianteExcelBean.setGenero(row.getCell(3).getStringCellValue());
                                row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                                estudianteExcelBean.setDni(row.getCell(4).getStringCellValue());
                                estudianteExcelBeans.add(estudianteExcelBean);
                            }
                            i++;
                        }
//                        String name = new File(item.getName()).getName();
//                        item.write(new File(UPLOAD_DIRECTORY + File.separator + name));
                    }
                }

                if (!estudianteExcelBeans.isEmpty()) {
                    controlAlumnoService = new ControlAlumnoService();
                    controlAlumnoService.registrarAlumnoMasivo(estudianteExcelBeans, usuarioClase.getIdClase(), Constante.PERFIL_ESTUDIANTE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void nuevoAlumno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/estudiantes/agregarAlumno.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }

    private void agregarNuevoAlumno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
        UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
        DatosPrincipalesClaseBean claseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");
        try {
            EstudianteExcelBean estudianteExcelBean = new EstudianteExcelBean();
            estudianteExcelBean.setNombre(request.getParameter("txtNombres"));
            estudianteExcelBean.setApellidoPaterno(request.getParameter("txtAp"));
            estudianteExcelBean.setApellidoMaterno(request.getParameter("txtAm"));
            estudianteExcelBean.setCodEstudiante(request.getParameter("txtCodigoEstudiante"));
            estudianteExcelBean.setGenero(request.getParameter("genero"));
            estudianteExcelBean.setDni(request.getParameter("txtDni"));
            controlAlumnoService = new ControlAlumnoService();
            controlAlumnoService.registrarAlumno(estudianteExcelBean, usuarioClase.getIdClase(), Constante.PERFIL_ESTUDIANTE);
        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
            ex.printStackTrace();
        }

    }

    private void irAControlEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/estudiantes/mainListaAlumnos.jsp";
        
//        try {
//            HttpSession session = request.getSession();
//            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
//            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
//            DatosPrincipalesClaseBean claseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");
//            controlAlumnoService = new ControlAlumnoService();
//            List<ControlEstudianteBean> estudiantes = controlAlumnoService.listarAlumnosXClase(usuarioClase.getIdClase());
//            request.setAttribute("estudiantes", estudiantes);
//        } catch (Exception e) {
//        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }
    
    private void listarConectados(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/estudiantes/listaConectados.jsp";
        
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
            DatosPrincipalesClaseBean claseBean = (DatosPrincipalesClaseBean) session.getAttribute("claseInfo");
            controlAlumnoService = new ControlAlumnoService();
            List<ControlEstudianteBean> estudiantes = controlAlumnoService.listarAlumnosXClase(usuarioClase.getIdClase());
            request.setAttribute("estudiantes", estudiantes);
        } catch (Exception e) {
        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }

}
