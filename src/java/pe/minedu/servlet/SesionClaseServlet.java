
package pe.minedu.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import pe.minedu.bean.EstudianteExcelBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.dao.EstudianteDao;
import pe.minedu.service.SesionServiceImpl;
import pe.minedu.util.Constante;

@WebServlet(name = "SesionClaseServlet", urlPatterns = {"/GetGrados", "/GetSeccion", "/GetSesion","/CrearClase","/GetEstadoClase","/CerrarClase"})
public class SesionClaseServlet extends HttpServlet {
    private SesionServiceImpl sessionServiceImpl;
    private List<GradoBean> listaGrados = new ArrayList<GradoBean>();
    private List<SeccionBean> listaSecciones = new ArrayList<SeccionBean>();
    private List<SesionBean> listaSesiones = new ArrayList<SesionBean>();
    private static final long serialVersionUID = 1L;    

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getServletPath();
        if(path.equals("/GetGrados")){
            getGradosByCurso(req, resp);
        }else if(path.equals("/GetSeccion")){
            getSeccionByGrado(req, resp);
        }else if(path.equals("/GetSesion")){
            getSessionByCurso(req, resp);
        }else if(path.equals("/CrearClase")){
            crearClase(req, resp);
        }else if(path.equals("/GetEstadoClase")){
            getEstadoClaseSesion(req, resp);
        }else if(path.equals("/CerrarClase")){
            cerrarClase(req, resp);
        }
    }
    private void getGradosByCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        sessionServiceImpl = new SesionServiceImpl();
        try {
            String id_curso = request.getParameter("selectMateria");
            listaGrados = sessionServiceImpl.getGradosByCurso(Integer.parseInt(id_curso));
            String option = "<select id=\"selectGrado\" name=\"selectGrado\"> \n"+
                            " <option value =\"\">Seleccionar</option>\n";
            for(GradoBean bean : listaGrados){
                option += " <option value =\""+bean.getCodigoGrado()+"\">"+bean.getDescChoice()+"</option>\n" ;
            }
                option += " </select>";
             pw.write(option);
        } catch (Exception ex) {
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
    private void getSeccionByGrado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        try {
            sessionServiceImpl = new SesionServiceImpl();
            String id_grado = request.getParameter("selectGrado");
            listaSecciones = sessionServiceImpl.getSeccionesByGrado(id_grado);
            String option = "<select id=\"selectSeccion\" name=\"selectSeccion\"> \n"+
                            " <option value =\"\">Seleccionar</option>\n";
            for(SeccionBean bean : listaSecciones){
                option += " <option value =\""+bean.getIdSeccion()+"\">"+bean.getNombreSeccion()+"</option>\n" ;
            }
                option += " </select>";
             pw.write(option);
        } catch (Exception ex) {
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
    private void getSessionByCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        try {
            sessionServiceImpl = new SesionServiceImpl();
            String id_curso = request.getParameter("selectMateria");
            listaSesiones = sessionServiceImpl.getSesionesByCurso(Integer.parseInt(id_curso));
            String option = "<select id=\"selectSesion\" name=\"selectSesion\"> \n"+
                            " <option value =\"\">Seleccionar</option>\n";
            for(SesionBean bean : listaSesiones){
                option += " <option value =\""+bean.getIdSesion()+"\">"+bean.getCodigoSesion()+"</option>\n" ;
            }
                option += " </select>";
             pw.write(option);
        } catch (Exception ex) {
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
     private void crearClase(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        String option = Constante.ERROR;
        try {
            
            HttpSession session = request.getSession();           
            String id_curso = request.getParameter("selectMateria");
            String id_grado = request.getParameter("selectGrado");
            String id_seccion = request.getParameter("selectSeccion");
            String id_sesion = request.getParameter("selectSesion");
            Integer id_docente = (Integer)session.getAttribute("idDocenteSesion");
            if(id_curso.equals("")|| id_grado.equals("")|| id_seccion.equals("") ||id_sesion.equals("")){
                  throw new Exception("error");
            }            
            
           //CODIGO HARD
            
           //AQUI USA LOS PARAMETROS id_curso id_seccion id_sesion id_docente, PARA CREAR LA CLASE
           
           //LUEGO DE CREAR LA CLASE EN BASE DE DATOS CON ESTADO 1 TRAER EL ULTIMO ID DE LA CLASE QUE ACABAS DE GENERAR Y EL ID SESION
           // PARA MANDARLOS EN SESION ESOS ATRIBUTOS LOS USO PARA EL TIMER.. IPORTANTE SETEARLOS
           
           
            session.setAttribute("idClase", "AQUI VA EL EL ID_CLASE RECIEN CREADO");
            session.setAttribute("isSesion", id_sesion);      
            option = Constante.NO_ERROR;
            pw.write(option);
        } catch (Exception ex) {
            pw.write(ex.getMessage());
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }
     
    private void cerrarClase(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            
        try {
            sessionServiceImpl = new SesionServiceImpl();
            HttpSession session = request.getSession();  
            Integer id_docente = (Integer)session.getAttribute("idDocenteSesion");
            sessionServiceImpl.cerrarClase((String)session.getAttribute("idClase"));
            session.setAttribute("idClase", "0");//DESACTIVAMOS CLASE EN SESION
            session.setAttribute("isSesion", "0");//DESACTIVAMOS CLASE EN SESION          
        } catch (Exception ex) {
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }   
     private void getEstadoClaseSesion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        try {
            HttpSession session = request.getSession();
            String idClase=session.getAttribute("idClase")==null?"0":(String)session.getAttribute("idClase");
            String estadoClaseSesion = ( ((idClase).equals("0")) ? "0" : "1");
            pw.write(estadoClaseSesion);
        } catch (Exception ex) {
            Logger.getLogger(SesionClaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
     
}
