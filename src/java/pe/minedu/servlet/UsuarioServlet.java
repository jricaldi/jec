/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pe.minedu.bean.ClaseActivaBean;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ClaseService;
import pe.minedu.service.SesionServiceImpl;
import pe.minedu.service.UsuarioService;
import pe.minedu.util.Constante;

/**
 *
 * @author edwin
 */
@WebServlet(name = "UsuarioServlet", urlPatterns = {"/comun/UsuarioLoginServlet", "/comun/UsuarioLogoutServlet", "/comun/UsuarioSelectLoginServlet"})
public class UsuarioServlet extends HttpServlet {

    private UsuarioService usuarioService;
    private ClaseService claseService;
    private SesionServiceImpl sessionServiceImpl;
    private List<CursoBean> listaCursos;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //super.service(req, resp);
        String accion = req.getServletPath();
         sessionServiceImpl = new SesionServiceImpl();
        if(accion.equals("/comun/UsuarioLoginServlet")){
            login(req, res);
        }else if(accion.equals("/comun/UsuarioLogoutServlet")){
            logout(req, res);
        }else if(accion.equals("/comun/UsuarioSelectLoginServlet")){
            logearClaseSeleccionada(req, res);
        }
    }
    
    private void login(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        usuarioService = new UsuarioService();
        
        String view="../login.jsp";
        
        String user = req.getParameter("txtUsuario");
        String pass = req.getParameter("txtPassword");
        String ip = req.getRemoteHost();
        boolean docenteSinClase=false;
        boolean variasClasesActivas = false;
        UsuarioBean usuario = usuarioService.login(user, pass);
        //System.out.println("usuario:"+usuario.getIdEstudiante());
        UsuarioClaseBean usuarioClase = null;
        if(usuario != null){
            //// REVISAR ClaseServlet, para comentarios con ***
            req.getSession().invalidate();
            HttpSession session = req.getSession();
            if(usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)){
                
                //*** Proceso sesion de clase #2: se inicia docente en IP
                usuarioClase = usuarioService.iniciarDocenteClaseEnIP(usuario, ip);
                if(usuarioClase==null){
                    docenteSinClase=true;
                }
                
                //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
                if(!docenteSinClase){
                    session.setAttribute("idClase", usuarioClase.getIdClase().toString());
                    session.setAttribute("idSesion", usuarioClase.getIdSesion());
                }else{
                    session.setAttribute("idClase","0");
                    session.setAttribute("idSesion","0");
                }
                listaCursos = sessionServiceImpl.getCursosByDocenteLogeado(usuario.getIdDocente());  
                context.setAttribute("listaCursos", listaCursos);                
              
               
            
            }else if(usuario.getPerfil().equals(Constante.PERFIL_ESTUDIANTE)){
                
                // logica para saber si tiene mas de una clase
                claseService = new ClaseService();
                List<ClaseActivaBean> listaClasesActivas = claseService.obtClasesActivaXEstudiante(usuario.getIdEstudiante());
                session.setAttribute("idEstudiante", usuario.getIdEstudiante());
                if(listaClasesActivas.size() == 1){
                    //*** Proceso sesion de clase #2: se inicia estudiante en IP
                    usuarioClase = usuarioService.iniciarEstudianteClaseEnIP(usuario, ip);
                    //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
                    session.setAttribute("idClase", usuarioClase.getIdClase().toString());
                    session.setAttribute("idSesion", usuarioClase.getIdSesion());
                }else if(listaClasesActivas.size() > 1){
                    // que habra una opcion para que seleccione la clase que quiera ingresar
                    session.setAttribute("listaClases", listaClasesActivas);
                    variasClasesActivas = true;
                    // dbe de retornar otra pagina
                }else{
                    // error
                }
                
                
               
               
            }
            if(variasClasesActivas){
                session.setAttribute("usuario", usuario);
                view="../login_multiClases.jsp"; // pichicata indica
            }else{
                if(usuario.getPerfil().equals(Constante.PERFIL_ADMINISTRADOR)){
                session.setAttribute("usuario", usuario);
                
                view="../";
                }else if(usuarioClase != null || docenteSinClase){
                    claseService = new ClaseService();

                    if(usuarioClase != null){
                        //*** Proceso sesion de clase #3: se recupera informacion de la clase
                        DatosPrincipalesClaseBean claseInfo = claseService.obtDatosPrincipalesClase(usuarioClase.getIdClase());

                        //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
                        session.setAttribute("claseInfo", claseInfo);
                        session.setAttribute("usuarioClase", usuarioClase);

                    }

                    DatosPersonalesUsuarioBean usuarioInfo = claseService.obtDatosPersonalesUsuario(usuario.getIdUsuario(), usuario.getPerfil());

                    session.setAttribute("usuario", usuario);
                    session.setAttribute("usuarioInfo", usuarioInfo);

                    view="../";
                }else{
                    view="../login.jsp?alerta=sinclase";
                }
            }
            
            
            session.setAttribute("PERFIL_ADMINISTRADOR", Constante.PERFIL_ADMINISTRADOR);
            session.setAttribute("PERFIL_DOCENTE", Constante.PERFIL_DOCENTE);
            session.setAttribute("PERFIL_ESTUDIANTE", Constante.PERFIL_ESTUDIANTE);
            session.setAttribute("CONST_JEC_IP_CHAT", Constante.JEC_IP_CHAT);
        }
        
        res.sendRedirect(view);
    }
    
    
    private void logout(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        ServletContext context = req.getSession().getServletContext();
        String view="../";
        
        req.getSession().removeAttribute("usuario");
        req.getSession().invalidate();
        
        res.sendRedirect(view);
    }
    
     private void logearClaseSeleccionada(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String view = "../";
        usuarioService = new UsuarioService();
        claseService = new ClaseService();
        UsuarioClaseBean usuarioClase = null;
        String strIdClaseSeleccionada = req.getParameter("selectClaseToLogin"); // lo que pichi debe de hacer
        int idClaseSeleccionada = Integer.parseInt(strIdClaseSeleccionada);
        HttpSession session = req.getSession();
        UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
        String ip = req.getRemoteHost();
        usuarioClase = usuarioService.iniciarClaseSeleccionadaXEstudiante(usuario, ip, idClaseSeleccionada);
        //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
        session.setAttribute("idClase", usuarioClase.getIdClase());
        session.setAttribute("idSesion", usuarioClase.getIdSesion());

        //*** Proceso sesion de clase #3: se recupera informacion de la clase
        DatosPrincipalesClaseBean claseInfo = claseService.obtDatosPrincipalesClase(usuarioClase.getIdClase());

        //*** Proceso sesion de clase #4: se guardan datos de la clase en la sesion
        session.setAttribute("claseInfo", claseInfo);
        session.setAttribute("usuarioClase", usuarioClase);

        DatosPersonalesUsuarioBean usuarioInfo = claseService.obtDatosPersonalesUsuario(usuario.getIdUsuario(), usuario.getPerfil());

        session.setAttribute("usuario", usuario);
        session.setAttribute("usuarioInfo", usuarioInfo);
        session.setAttribute("CONST_JEC_IP_CHAT", Constante.JEC_IP_CHAT);
        view = "../";

        res.sendRedirect(view);
    }
}
