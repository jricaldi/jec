
package pe.minedu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pe.minedu.bean.CambioPassBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.service.CambiarPasswordService;

@WebServlet(name = "CambioPasswordServlet", urlPatterns = {"/CambioPasswordServlet","/comun/seguridad/CambioPassword"})
public class CambioPasswordServlet extends HttpServlet {

    CambiarPasswordService cambiarPasswordService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getServletPath();
        if(accion.equals("/comun/seguridad/CambioPassword")){
            cambiarPassword(req, resp);
        }
    }
    
    private void cambiarPassword(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter = resp.getWriter();
        try {
            cambiarPasswordService = new CambiarPasswordService();
            HttpSession session = req.getSession();
            String pass1 = req.getParameter("txtPassAct");
            String pass2 = req.getParameter("txtNewPass");
            CambioPassBean cambioPassBean = new CambioPassBean();
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
            cambioPassBean.setPassword(pass1);
            cambioPassBean.setNuevoPassword(pass2);
            cambioPassBean.setPerfil(usuario.getPerfil());
            cambioPassBean.setUsuario(usuario.getUsuario());
            cambioPassBean.setIdDocente(usuario.getIdDocente());
            cambioPassBean.setIdEstudiante(usuario.getIdEstudiante());
            cambioPassBean.setIdUsuario(usuario.getIdUsuario());
            
            String mensaje = cambiarPasswordService.cambiarPassword(cambioPassBean);
            printWriter.write(mensaje);
        } catch (Exception e) {
            printWriter.write("Ocurrio un error " + e.getMessage());
        }finally{
            printWriter.close();
        }
    }

}
