
package pe.minedu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.service.AdministradorService;
import pe.minedu.util.Constante;

@WebServlet(name = "AdministradorServlet", urlPatterns = {"/AdministradorServlet", "/admin/CargarGrado", "/admin/CargarSesion"
, "/admin/CargarSeccion", "/admin/CargaDocente", "/admin/CargaCurso", "/admin/CargaDocenteCurso","/admin/CargaCuestionario"
,"/admin/GetUsuario","/admin/ResetPassword"})
public class AdministradorServlet extends HttpServlet {

    private AdministradorService administradorService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getServletPath();
        if(accion.equals("/admin/CargarGrado")){
            administardorCargarGrados(req, resp);
        }else if(accion.equals("/admin/CargarSeccion")){
            administardorCargarSeccion(req, resp);
        }else if(accion.equals("/admin/CargaDocente")){
            administardorDocente(req, resp);
        }else if(accion.equals("/admin/CargarSesion")){
            administardorCargarSesiones(req, resp);
        }else if(accion.equals("/admin/CargaCurso")){
            administardorCargarCursos(req, resp);
        }else if(accion.equals("/admin/CargaDocenteCurso")){
            administardorCargarDocenteCurso(req, resp);
        }else if(accion.equals("/admin/CargaCuestionario")){
            administardorCargarCuestionario(req, resp);
        }else if(accion.equals("/admin/GetUsuario")){
            getUsuario(req, resp);
        }else if(accion.equals("/admin/ResetPassword")){
            resetPassword(req, resp);
        }
    }

    private void administardorCargarGrados(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_GRADO, req);
            System.out.println("AdministradorServlet.administardorCargarGrados.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    private void administardorCargarCursos(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_CURSO, req);
            System.out.println("AdministradorServlet.administardorCargarCursos.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    private void administardorCargarSeccion(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_SECCION, req);
            System.out.println("AdministradorServlet.administardorCargarSeccion.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    private void administardorDocente(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_DOCENTE, req);
            System.out.println("AdministradorServlet.administardorDocente.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    private void administardorCargarCuestionario(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_CUESTIONARIO, req);
            System.out.println("AdministradorServlet.administardorCargarCuestionario.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    
    private void administardorCargarSesiones(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_SESION, req);
            System.out.println("AdministradorServlet.administardorCargarSesiones.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    
    private void administardorCargarDocenteCurso(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            String mensaje = cargarDatos(Constante.CARGA_DOCENTE_CURSO, req);
            System.out.println("AdministradorServlet.administardorCargarSesiones.mensaje " + mensaje);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(e.getMessage());
            e.printStackTrace();
        } finally{
            out.close();
        }
    }
    
    private String cargarDatos(int tipoCarga, HttpServletRequest request){
        String mensaje = "";
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        // process only if it is multipart content
        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                // Parse the request
                List<FileItem> multiparts = upload.parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        administradorService = new AdministradorService();
                        mensaje = administradorService.manejarCargaDatosGrados(item.getInputStream(), tipoCarga);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mensaje;
    }
    
    private void getUsuario(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            administradorService = new AdministradorService();
            String txtDni = req.getParameter("txtDni");
            DatosPersonalesUsuarioBean usuario = administradorService.getUsuario(txtDni);
            if(usuario == null)
                out.write(Constante.PROCESO_ERROR);
            else{
                String mensaje = "{\"nombre\" : \"" + usuario.getNombre() + "\" , \"dni\" : \"" + usuario.getDni() + "\" , \"usuario\" : \"" + usuario.getCodigo() + "\"}";
                out.write(mensaje);
            }
        } catch (Exception e) {
            out.write(Constante.PROCESO_ERROR);
        } finally{
            out.close();
        }
    }
    
    private void resetPassword(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            administradorService = new AdministradorService();
            String txtDni = req.getParameter("dataDni");
            String txtUsuario = req.getParameter("dataUsuario");
            String mensaje = administradorService.resetPassword(txtDni,txtUsuario);
            out.write(mensaje);
        } catch (Exception e) {
            out.write(Constante.PROCESO_ERROR);
        } finally{
            out.close();
        }
    }
    
}
