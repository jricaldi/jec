
package pe.minedu.servlet;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import pe.minedu.bean.AplicacionBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.bean.RemoteServerBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.service.ListaBeanService;
import pe.minedu.service.ManejoAplicacionService;
import pe.minedu.util.Constante;

@WebServlet(name = "AplicacionServlet", urlPatterns = {"/IrAAplicaciones","/AbrirAplicacion","/RegistrarNuevaAplicacion"})
public class AplicacionServlet extends HttpServlet {

    private ManejoAplicacionService manejoAplicacionService;
    private ListaBeanService listaBeanService;
    
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getServletPath();
        if(accion.equals("/IrAAplicaciones")){
            irAAplicaciones(request, response);
        }else if(accion.equals("/AbrirAplicacion")){
            abrirAplicacion(request, response);
        }else if(accion.equals("/RegistrarNuevaAplicacion")){
            registrarNuevaAplicacion(request, response);
        }
    }

    private void irAAplicaciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destino = "docente/aplicaciones/mainAplicaciones.jsp";
        try {
            manejoAplicacionService = new ManejoAplicacionService();
            List<AplicacionBean> listaAplicaciones = manejoAplicacionService.listarAplicaciones();
            ServletContext context = request.getServletContext();
            File fileWebApp = new File(context.getRealPath(context.getContextPath()));
            String filePathWeb = fileWebApp.getParent() + File.separator + "recursos" + File.separator + "img" + File.separator + "aplicaciones" + File.separator;
            String filePathBackUp = Constante.IMAGENES_APP_WEB;
            File imageInWeb = null;
            File imageInBackUp = null;
            for (AplicacionBean listaAplicacione : listaAplicaciones) {
                imageInWeb = new File(filePathWeb + listaAplicacione.getNombreAplicacion() + ".png");
                if(!imageInWeb.exists()){
                    imageInBackUp = new File(filePathBackUp + listaAplicacione.getNombreAplicacion() + ".png");
                    if(imageInBackUp.exists()){
                        FileUtils.copyFile(imageInBackUp, imageInWeb);
                    }
                }
            }
            request.setAttribute("listaAplicaciones", listaAplicaciones);
        } catch (Exception e) {
        }
        RequestDispatcher rd = request.getRequestDispatcher(destino);
	rd.forward(request, response);
    }
    
    private void abrirAplicacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int idAplicacion = Integer.parseInt(request.getParameter("idAplicacion"));
            String tipoBoton = request.getParameter("tipoBoton").toUpperCase();
            manejoAplicacionService = new ManejoAplicacionService();
            AplicacionBean aplicacionBean = manejoAplicacionService.obtAplicacionxId(idAplicacion);
            String documentoNuevo = manejoAplicacionService.generarDocumento(aplicacionBean.getNombreAplicacion(), aplicacionBean.getExtensiones().split(",")[0]);
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            UsuarioBean usuario = (UsuarioBean) session.getAttribute("usuario");
            int idClase = usuarioClase.getIdClase();
            listaBeanService = new ListaBeanService();
            List<MonitoreoClaseEstudianteBean> estudiantes = listaBeanService.listEstudiantesEnClase(idClase);

            if (usuario.getPerfil().equals(Constante.PERFIL_ESTUDIANTE)) {
                EstudianteBean estudianteBean = listaBeanService.obtEstudiantexId(usuario.getIdEstudiante());
                Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                RemoteServerBean comando = new RemoteServerBean();
                comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO_NUEVO);
                comando.setPerfil(Constante.PERFIL_ESTUDIANTE);
                comando.setDocumento(documentoNuevo);
                comando.setCodigoClase(idClase+"");
                comando.setCodigoUsuario(estudianteBean.getCodEstudiante() + "");
                oos.writeObject(comando);
                oos.flush();
                oos.close();
                server.close();
            } else if (usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)) {
                
                if(tipoBoton.equals(Constante.LOCAL)){
                    if(usuario.getIdDocente() != null){
                        if(usuario.getIdDocente() != 0){
                            Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                            RemoteServerBean comando = new RemoteServerBean();
                            comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO_NUEVO);
                            comando.setPerfil(Constante.PERFIL_DOCENTE);
                            comando.setDocumento(documentoNuevo);
                            comando.setCodigoClase(idClase+"");
                            comando.setCodigoUsuario(usuario.getIdDocente() + "");
                            oos.writeObject(comando);
                            oos.flush();
                            oos.close();
                            server.close();
                        }
                    }
                }
                else{
                    for (MonitoreoClaseEstudianteBean estudiante : estudiantes) {
                    Socket server = new Socket(estudiante.getIp(), Constante.PUERTO_SOCKET);
                    ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                    RemoteServerBean comando = new RemoteServerBean();
                    comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO_NUEVO);
                    comando.setPerfil(Constante.PERFIL_ESTUDIANTE);
                    comando.setDocumento(documentoNuevo);
                    comando.setCodigoClase(idClase+"");
                    comando.setCodigoUsuario(estudiante.getCodigoEstudiante());
                    oos.writeObject(comando);
                    oos.flush();
                    oos.close();
                    server.close();
                    }
                    if(usuario.getIdDocente() != null){
                        if(usuario.getIdDocente() != 0){
                            Socket server = new Socket(usuarioClase.getIp(), Constante.PUERTO_SOCKET);
                            ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
                            RemoteServerBean comando = new RemoteServerBean();
                            comando.setAccion(Constante.COMANDO_ABRIR_ARCHIVO_NUEVO);
                            comando.setPerfil(Constante.PERFIL_DOCENTE);
                            comando.setDocumento(documentoNuevo);
                            comando.setCodigoClase(idClase+"");
                            comando.setCodigoUsuario(usuario.getIdDocente() + "");
                            oos.writeObject(comando);
                            oos.flush();
                            oos.close();
                            server.close();
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }
    
    private void registrarNuevaAplicacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = null;
        File fileWeb = null;
        File fileBackUp = null;
        FileItem fileItem;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        ServletContext context = request.getServletContext();
        File fileWebApp = new File(context.getRealPath(context.getContextPath()));
        String filePathWeb = fileWebApp.getParent() + File.separator + "recursos" + File.separator + "img" + File.separator + "aplicaciones" + File.separator;
        String filePathBackUp = Constante.IMAGENES_APP_WEB;

        //Para inputs que no son tipo file
        String fieldname;
        String fieldvalue;

        // Verificando el content type
        String contentType = request.getContentType();
        
        try {
            out = response.getWriter();
            manejoAplicacionService = new ManejoAplicacionService();
            AplicacionBean aplicacionBean = new AplicacionBean();
            aplicacionBean.setDocumentoAbrir("");
            if ((contentType.indexOf("multipart/form-data") >= 0)) {
                DiskFileItemFactory factory = new DiskFileItemFactory();
                // Setear el la capacidad maxima que vamos a guardar en memoria
                factory.setSizeThreshold(maxMemSize);
                // Seteamos carpeta donde guardaremos data mayor a el tamaño de la memoria (maxMemSize).
                factory.setRepository(new File(Constante.REPOSITORIO_WEB_TOMCAT));
                // Creamos un nuevo manejador de archivos
                ServletFileUpload upload = new ServletFileUpload(factory);
                // Seteamos el tamaño maximo a subirse por el aplicativo
                upload.setSizeMax(maxFileSize);
                // Parse the request to get file items.
                List fileItems = upload.parseRequest(request);
                // Process the uploaded file items
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    fileItem = (FileItem) i.next();
                    if (!fileItem.isFormField()) {
                        String fileName = fileItem.getName();
                        if (fileName.lastIndexOf("\\") >= 0) {
                            fileWeb = new File(filePathWeb
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                            fileBackUp = new File(filePathBackUp
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            fileWeb = new File(filePathWeb
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                            fileBackUp = new File(filePathBackUp
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fileItem.write(fileWeb);
                        fileItem.write(fileBackUp);
                    } else {
                        fieldname = fileItem.getFieldName();
                        fieldvalue = fileItem.getString();
                        if (fieldname.equals("idEstado")) {
                            aplicacionBean.setEstado(fieldvalue);
                        } else if (fieldname.equals("extensiones")) {
                            aplicacionBean.setExtensiones(fieldvalue);
                        } else if (fieldname.equals("nombreAplicacion")) {
                            aplicacionBean.setNombreAplicacion(fieldvalue);
                        } else if (fieldname.equals("tipoAplicacion")) {
                            aplicacionBean.setTipoAplicacion(fieldvalue);
                        }
                    }
                }
                
                if(fileWeb != null){
                    if(fileWeb.exists()){
                        fileWeb.renameTo(new File(fileWeb.getParent() + File.separator + aplicacionBean.getNombreAplicacion() + ".png"));
                    }
                }
                
                if(fileBackUp != null){
                    if(fileBackUp.exists()){
                        fileBackUp.renameTo(new File(fileBackUp.getParent() + File.separator + aplicacionBean.getNombreAplicacion() + ".png"));
                    }
                }
                
                manejoAplicacionService.registrarNuevaAplicacion(aplicacionBean);
                out.print("Se registro la aplicación con éxito.");
            }else{
                out.print("No se ha subido ningún archivo");
            }
            
        } catch (Exception e) {
            if (out != null) out.print("Error AplicacionServlet.registrarNuevaAplicacion " + e.getMessage());
            System.out.println("Error AplicacionServlet.registrarNuevaAplicacion " + e.getMessage());
            e.printStackTrace();
        } finally{
            if (out != null) {
                out.close();
            }
        }
    }
}
