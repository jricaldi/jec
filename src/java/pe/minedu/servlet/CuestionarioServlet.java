package pe.minedu.servlet;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Font;

import pe.minedu.bean.AlternativaBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.FichaBean;
import pe.minedu.bean.FichaEstudianteBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.PreguntaBean;
import pe.minedu.bean.RespuestaEstudianteBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.dao.EstudianteDao;
import pe.minedu.service.CuestionarioServiceImpl;
import pe.minedu.util.Constante;

@WebServlet(name = "CuestionarioServlet", urlPatterns = {"/GetFichas","/GetFichaFull","/SaveEvalu","/GetParticipantes","/ActivarDesactivar",
                                                         "/DownloadFile","/AddFicha","/GetNumAlternativas","/AddPreguntaTemporal","/GetFichaTempo","/SaveNombreFichaTemp",
                                                         "/SaveFichaTemporal","/DownloadFichaAlmuno","/GetNameFicha","/calificarFicha","/calcularNota",
                                                         "/DownloadExcel"})
public class CuestionarioServlet extends HttpServlet {
    private CuestionarioServiceImpl cuestionarioServiceImpl;
    private List<FichaBean> listaFichas = new ArrayList<FichaBean>();    
    private List<PreguntaBean> listaPreguntas = new ArrayList<PreguntaBean>();
    private static final long serialVersionUID = 1L;    
    private String idFichaEnUso = "";
    private List<EstudianteBean> listaEstudiantes = new ArrayList<EstudianteBean>();
    private String [] letrasCuestionario = {"a)","b)","c)","d)","e)","f)","g)","h)","i)","j)","k)","l)","m)","n)"};

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getServletPath();
        if(path.equals("/GetFichas")){
            getCuestionarByClase(req, resp);
        }else if(path.equals("/GetFichaFull")){
            getFichaByIdFicha(req, resp);
        } else if(path.equals("/SaveEvalu")){
            saveRespuestas(req, resp);
        } else if(path.equals("/GetParticipantes")){
            getParticipantesCuestionario(req, resp);
        } else if(path.equals("/ActivarDesactivar")){
            activarDesactivarFicha(req, resp);
        } else if(path.equals("/DownloadFile")){
            descargarFicha(req, resp);
        } else if(path.equals("/AddFicha")){
            addFichaToClase(req, resp);
        }else if(path.equals("/GetNumAlternativas")){
            getNumAlternativas(req, resp);
        }else if(path.equals("/AddPreguntaTemporal")){
            addPreguntasTemporal(req, resp);
        } else if(path.equals("/GetFichaTempo")){
            getFichaTemporal(req, resp);
        } else if(path.equals("/SaveFichaTemporal")){
            saveFichaTemporal(req, resp);
        } else if(path.equals("/SaveNombreFichaTemp")){
            saveNombreFichaTemporal(req, resp);
        } else if(path.equals("/DownloadFichaAlmuno")){
            descargarFichaByAlumno(req, resp);
        }else if(path.equals("/GetNameFicha")){
            getNombreFicha(req, resp);
        }else if(path.equals("/calificarFicha")){
            getFichaToCalificar(req, resp);
        }else if(path.equals("/calcularNota")){
            calificarexamen(req, resp);
        }else if(path.equals("/DownloadExcel")){
            downloadExcel(req, resp);
        }
    }
     private void getCuestionarByClase(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
         PrintWriter pw = response.getWriter();
        try {            
            
            HttpSession session = request.getSession();   
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase");                
            cuestionarioServiceImpl = new CuestionarioServiceImpl();
            String salida = "";
               
            if(usuarioClase == null){
                salida = "";System.out.println("1");
            }else if(usuarioClase.getIdClase() == null){
                salida = "";System.out.println("2");
            }else{                 
                  UsuarioBean usuario = (UsuarioBean)session.getAttribute("usuario");  
                  int id_clase = (( usuarioClase.getIdClase() == null ) ? 0 : usuarioClase.getIdClase());
                  String id_sesion = (( usuarioClase.getIdSesion() == null ) ? "0" : usuarioClase.getIdSesion());
                if(usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)){
                    listaFichas = cuestionarioServiceImpl.getFichaClaseByDocente(id_clase, id_sesion);               
                    salida+="<div>"
                            + "<p> - Para crear un nuevo cuestionario, presione “nuevo cuestionario”.<br>" +
                                "\n" +
                                "- Para cambiar el estado de un cuestionario de activo a inactivo o viceversa, \n" +
                                "\n" +
                                "presione sobre el botón de estado.<br>"
                                                            + "- Para revisar el estado  de los alumnos, presione “ver”.<br>\n" +
                                "\n" +
                                "- Descargue la información de la sesión en Excel (.xls), presionando en “descargar”.<p></div>"
                            + "<div>"
                            + "<button class=\"btnJec bprimario\" id=\"btnNuevaFicha\" >Nuevo Cuestionario&nbsp;&nbsp;&nbsp;+</button> "
                            + "<a style=\"text-align: center\" href=\"../../DownloadExcel\" ><button style=\"margin-left: 25px;\" class=\"btnJec bprimario\"  >DESCARGAR TODOS&nbsp;&nbsp;&nbsp;<img src=\"../../recursos/img/icons/DESCARGAR.png\" alt=\"Descargar\"/></button></a>"
                            + "</div>"
                            + "<div>"
                           // + "<table style=\"margin-left: -11px;\">" 
                                 + "<table class=\"tabla-simple\" style=\"width: 100%\">" 
                                + "<tr>"
                                 + "<td style=\"/*width: 1000px;*/ padding: 15px;\">"
                                    +"</td>"
                                 + "<td style=\"width: 60px;text-align: center;\"><b style=\"color: #2D174C;\">ALUMNOS</b></td>"
                                 + "<td style=\"/*width: 100px;*/text-align: center; \"><b style=\"color: #2D174C;\">ESTADO</b></td>"
                                 + "<td style=\"width: 115px;text-align: center; \"><b></b></td>"
                                 + "</tr><br>";
                     if(listaFichas.size()>0){
                         for(FichaBean bean : listaFichas){                             
                            String estado = (bean.getEstado()==null) ? "Inactivo" : ((bean.getEstado().equals("0")) ? "Inactivo" : "Activo");     
                            String back_color = ((estado.toUpperCase().equals("INACTIVO")) ? " background-color: red;"  : "background-color: green;") ;
                            System.out.println("bc:"+back_color);
                            salida+= "<tr>"
                                    + "<td style=\"height: 50px; border-bottom: 1px dotted #2D174C;\" >"+bean.getDescripcion().toUpperCase()+"</td>" 
                                    + "<td style=\"text-align: center; border-bottom: 1px dotted #2D174C;\"><button style=\"padding-right: 25px; padding-left: 25px;\" class=\"btnJec bprimario\"  onclick=\"getAlumnos(this);return false;\" id=\""+bean.getId_ficha()+"\">Ver </button></td>"
                                    + "<td style=\"text-align: center; border-bottom: 1px dotted #2D174C;\"><input type=\"button\" style=\"padding-right: 15px; padding-left: 15px; "+back_color+"\" class=\"btnJec bprimario\" onclick=\"closeFicha(this);return false;\" id=\""+bean.getId_ficha()+"\" value=\""+estado+"\"</input></td>"
                                    + "<td style=\"text-align: center; border-bottom: 1px dotted #2D174C;\"><button class=\"btnJec bprimario\" style=\"padding-right: 15px; padding-left: 15px;\" onclick=\"downloadFicha(this);return false;\" id=\""+bean.getId_ficha()+"\"  >Descargar&nbsp;&nbsp;&nbsp;<img src=\"../../recursos/img/icons/DESCARGAR.png\" alt=\"Descargar\"/></button> </td>"
                                    + "</tr>";
                            System.out.println("estado:"+estado);
                         }
//                         salida+=  "</table>";
                     }
                      salida+=  "</table>"
                              + "</div>";
                              
                }else if(usuario.getPerfil().equals(Constante.PERFIL_ESTUDIANTE)){
                System.out.println("estudiante");
                    Integer id_estudiante = (Integer)session.getAttribute("idEstudiante");            
                    listaFichas = cuestionarioServiceImpl.getFichasByClase(id_clase,id_estudiante);           
                       System.out.println("listaFichas:"+listaFichas.size());        
                    if(listaFichas.size()>0){
                         salida += "<div>" +
                                "  <p>\n" +
                                "  - Haz clic en el nombre del cuestionario para iniciarlo.\n" +
                                "  <br>\n" +
                               
                                "  - Si indica que está incompleto significa que debes realizarlo.\n" +
                                "  </p>\n" +
                                "  </div>";
                        salida+="<table >"
                                + "<tr>"
                                + "<td style=\"width: 470px; text-align: center;\"><b style=\"text-align: center;\">CUESTIONARIOS</b></td>"
                                + "<td style=\"width: 150px; text-align: center; \"><b>ESTADO</b></td>"
                                + "<td style=\"width: 150px; text-align: center; \"><b>NOTA</b></td></tr>";
                        String onclick = "";   
                        String estadoFicha = "";
                        for(FichaBean bean : listaFichas){                            
                            if(bean.getTerminado()!=null){
                                onclick="";
                            }else{
                                onclick ="href=\"#\" onclick=\"getFichaEvaluacion(this);return false;\" ";
                            }
                                 salida+= "<tr>"
                                        + "<td style=\"height: 50px; border-bottom: 1px dotted #2D174C;\" ><a class=\"textStyle2\" "+onclick+" id=\""+bean.getId_ficha()+"\">"+bean.getDescripcion().toUpperCase()+"</a></td>" ;
                                        if(bean.getTerminado() == null){
                                           estadoFicha = "INCOMPLETO";
                                        }else if(bean.getTerminado().equals("1")){
                                              estadoFicha = "TERMINADO";
                                        }else if(bean.getTerminado().equals("2")){
                                              estadoFicha = "POR CALIFICAR";
                                        }
                                  salida+="<td style=\"text-align: center; border-bottom: 1px dotted #2D174C;\">"+estadoFicha+"</td>";
                                 salida+="<td style=\"text-align: center; border-bottom: 1px dotted #2D174C;\">"+ ((bean.getNota()==null) ? "--" : bean.getNota())+"</td></tr>";
                        }    
                        salida+=  "</table>";
                    }
                }
            }
            
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
     private void getFichaByIdFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            String salida = "";
            String id_ficha = request.getParameter("id_ficha");
            idFichaEnUso = id_ficha;
            listaPreguntas = cuestionarioServiceImpl.getPreguntasByFicha(Integer.parseInt(id_ficha));
            String nombreFicha = cuestionarioServiceImpl.getNombreficha(id_ficha);
            int cont = 1;
            int numLetra = 0;
            salida += "<h5 class=\"blancoNegrita\" style=\"text-align: center; \">"+nombreFicha+"</h5>"
                    + "<form id=\"formPreguntas\" method=\"POST\" action=\"\">";
            for(PreguntaBean bean : listaPreguntas){
              salida += "<div>";
              salida += "<b>"+ cont+") &nbsp;"+bean.getDescripcion()+"</b><br>";
               numLetra = 0;
               if(bean.getTipo_pregunta() == 2){
                   salida += "<p>";
                for(AlternativaBean alter : bean.getAlternativaBean()){
                    String letra = letrasCuestionario[numLetra];
                    salida += "<input style=\"margin-left: 10px;\" type=\"radio\" name=\"opc"+alter.getId_pregunta()+"\" id=\""+alter.getId_alternativa()+"\" value=\""+alter.getId_alternativa()+"\">"+letra+" "+alter.getDescripcion()+"<br>";
                    numLetra++;
                }
                  salida += "</p>";
               }
               else if(bean.getTipo_pregunta() == 1){
                  // bean.getId_pregunta();
                   salida +="<textarea placeholder=\"Max(500) Caracteres.\" maxlength=\"500\" style=\"margin-left: 20px;width: 600px;\" class=\"form-control\" maxlength=\"400\" name=\"resp"+bean.getId_pregunta()+"\" id=\"resp"+bean.getId_pregunta()+"\" required ></textarea>";
               }
               salida += "<br></div>";
               cont++;
            }
            salida += "<br><div style=\"text-align: center; \"> <input type=\"button\" class=\"btn waves-effect colorCeleste\" type=\"button\" value=\"Guardar Evaluacion\" id=\"btnSave\" /></div>"
                    + "</form>";
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
     
      private void saveRespuestas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            cuestionarioServiceImpl = new CuestionarioServiceImpl();
            List<RespuestaEstudianteBean> listaRespuestas = new ArrayList<RespuestaEstudianteBean>();
            RespuestaEstudianteBean respu = new RespuestaEstudianteBean();
            int correcto = 0;
            int fichaAbierta = 0;
            for(PreguntaBean bean : listaPreguntas){
                if(bean.getTipo_pregunta() == 2){//CERRADA                
                    String alternativaSelecta = request.getParameter("opc"+bean.getId_pregunta());              
                    for(AlternativaBean alter : bean.getAlternativaBean()){                  
                        if(alter.getId_alternativa() == Integer.parseInt(alternativaSelecta)){
                            respu = new RespuestaEstudianteBean();
                            respu.setId_pregunta(bean.getId_pregunta());
                            respu.setId_alternativa(Integer.parseInt(alternativaSelecta));
                            listaRespuestas.add(respu);
                            if(alter.getIsRespuesta().equals("1")){
                                correcto++;
                                break;
                            }
                        }
                    }
                }else if (bean.getTipo_pregunta() == 1){//ABIERTA
                       String respuesta = request.getParameter("resp"+bean.getId_pregunta());
                       respu = new RespuestaEstudianteBean();
                       respu.setId_pregunta(bean.getId_pregunta());
                       respu.setDescRespuesta(respuesta);
                       listaRespuestas.add(respu);
                       fichaAbierta++;
                       System.out.println("resp"+bean.getId_pregunta()+":"+respuesta);
                }
                
            }
           
            
            int notaFinal = ( correcto * 20 )/listaPreguntas.size();            
            HttpSession session = request.getSession();                     
            Integer id_estudiante = (Integer)session.getAttribute("idEstudiante"); 
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            FichaEstudianteBean fichaEstu = new FichaEstudianteBean();
            fichaEstu.setId_estudiante(id_estudiante);
            fichaEstu.setId_ficha(Integer.parseInt(idFichaEnUso));
            fichaEstu.setId_clase(usuarioClase.getIdClase());
            if(fichaAbierta > 0){                
                fichaEstu.setIsTerminado("2");
            }else if(fichaAbierta == 0){
                fichaEstu.setNota_final(notaFinal+"");
                fichaEstu.setIsTerminado("1");
            }       
            cuestionarioServiceImpl.saveCuestionario(fichaEstu, listaRespuestas);
            System.out.println("SAVE?:notaFinal->"+notaFinal);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }
      
    private void getParticipantesCuestionario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            String salida = "";  
            HttpSession session = request.getSession();   
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase");                
            if(usuarioClase.getIdClase()== null){
                salida = "";
            }else{
                 String id_ficha = request.getParameter("id_ficha");  
                 int id_clase = usuarioClase.getIdClase();
                 session.setAttribute("id_ficha_select", id_ficha);
                 System.out.println("id_ficha:"+id_ficha+"-id_clase:"+id_clase);
                 int nume = 1;
                 listaEstudiantes = cuestionarioServiceImpl.getParticipantesByFicha(Integer.parseInt(id_ficha), id_clase);
                 //String estadoFicha = "";
                 String botonOTexto = "";
                 salida += "<div style=\"\n" +
                                "    margin-bottom: 5px;\n" +
                                "\">\n" +
                                "  <p>\n" +
                                "  - Puede descargar cada ficha de evaluación de cada alumno  por separado.\n" +
                                "  </br>"+
                                "  - Para descargar el resumen de los resultados de las evaluaciones de todos los \n" +
                                "\n" +
                                "alumnos, haga clic en “DESCARGAR RESULTADOS”.\n" +
                                "  </p>\n" +
                                "  </div>";
                     if(listaEstudiantes.size()>0){
                         salida+="<div><button class=\"btnJec bprimario\"  id=\"btnExportExcel\" >DESCARGAR RESULTADOS&nbsp;&nbsp;&nbsp;<img src=\"../../recursos/img/icons/DESCARGAR.png\" alt=\"Descargar\"/></button></div><br>"
                                 + "<div><table  border=\"0\" style=\"margin: 0 auto;\" >"
                                 + "<tr>"
                                 + "<td style=\"width: 20px;text-align: center; \">&nbsp;</td>"
                                 + "<td style=\"width: 400px; text-align: center;\"><b>ALUMNO</b></td>"
                                 + "<td style=\"width: 200px; text-align: center;\"><b>NOTA</b></td>"
                                 + "<td style=\"width: 200px; text-align: center;\"><b>ESTADO</b></td>"
                                 + "<td style=\"width: 200px; text-align: center;\"><b></b></td>"
                                 + "</tr>";
                         for(EstudianteBean bean : listaEstudiantes){                                                     
                            salida+= "<tr>"
                                    + "<td><label style=\"margin-top: 15px; margin-bottom: 15px\" class=\"blancoNegrita\">"+nume+")</label> </td>"
                                    + "<td>"+bean.getNombreCompleto()+"</td>" 
                                    + "<td style=\"text-align: center; \">"+((bean.getNotaFinal()==null) ? "0" : bean.getNotaFinal())+"</td>";
                                     if(bean.getEstado()== null){                                          
                                            botonOTexto = "Incompleto";
                                        }else if(bean.getEstado().equals("1")){
                                            botonOTexto = "Terminado";                                     
                                        }else if(bean.getEstado().equals("2")){
                                            botonOTexto = "<button class=\"btn waves-effect colorMorado\"  onclick=\"calificarFicha(this);return false;\" id=\""+bean.getIdEstudiante()+"\" >Calificar</button>";
                                        }
                             salida+= "<td style=\"text-align: center; \">"+botonOTexto+"</td>"
                                    + "<td style=\"text-align: center; \"><button class=\"btnJec bprimario\" onclick=\"downloadFichaEstu(this);return false;\" id=\""+bean.getIdEstudiante()+"\" >Descargar&nbsp;&nbsp;&nbsp;<img src=\"../../recursos/img/icons/DESCARGAR.png\" alt=\"Descargar\"/></button></td>"
                                    + "</tr>";
                            nume++;
                         }
                         salida+=  "</table>";
                     }
                
                salida += "<br>";
              //  salida += "<input class=\"styleBotonRecur\" type=\"button\"  id=\"btnExportPDF\" value=\"Exportar PDF\" />"
              //          + "</div>";
                
            }
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
     private void activarDesactivarFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        String salida = "";
        try {
            String error = Constante.ERROR;
            HttpSession session = request.getSession();               
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase");                
            String id_ficha = request.getParameter("id_ficha");  
            int id_clase = usuarioClase.getIdClase();
            
            String estado_ficha = cuestionarioServiceImpl.getEstadoFichaEnClase(Integer.parseInt(id_ficha), id_clase);
           
            if(estado_ficha.equals("")){
                error = cuestionarioServiceImpl.registrarFichaToFichaClase(Integer.parseInt(id_ficha), id_clase);
                if(error.equals(Constante.ERROR)){
                    salida = "error";
                    throw new Exception("Error interno, comuniquese con el Administrador del Sistema");
                }else if(error.equals(Constante.NO_ERROR)){
                    salida = Constante.ESTADO_ACTIVO;//SE REGISTRO Y SE ACTIVO LA FICHA
                }
            }else{
                 if(estado_ficha.equals("1")){
                     cuestionarioServiceImpl.activarDesactivarFicha(Constante.ESTADO_INACTIVO, Integer.parseInt(id_ficha), id_clase);
                     salida = Constante.ESTADO_INACTIVO;
                }else if(estado_ficha.equals("0")){
                     cuestionarioServiceImpl.activarDesactivarFicha(Constante.ESTADO_ACTIVO, Integer.parseInt(id_ficha), id_clase);
                     salida = Constante.ESTADO_ACTIVO;
                }                 
            }
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
            pw.write(salida);
        }
	pw.close();
    }
     private void descargarFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            HttpSession session = request.getSession();   
            String salida = "";
            String id_ficha = request.getParameter("id_ficha");  
            session.setAttribute("id_ficha_select", id_ficha);
            listaPreguntas = cuestionarioServiceImpl.getPreguntasByFicha(Integer.parseInt(id_ficha));
            int cont = 1;           
            int indcAlter= 0;
             String letra ="";
            for(PreguntaBean bean : listaPreguntas){     
                indcAlter= 0;
                salida += "<form><div><br>";      
                    salida += cont+")"+bean.getDescripcion();
               for(AlternativaBean alter : bean.getAlternativaBean()){
                   letra = (letrasCuestionario[indcAlter]);
                   String checked = ((alter.getIsRespuesta().equals("1")) ? "checked" : "");
                   String texto =  ((alter.getIsRespuesta().equals("1")) ? "<font color=\"green\">"+letra+" "+alter.getDescripcion()+"</font>" : letra+" "+alter.getDescripcion()); 
                   salida += "<input type=\"radio\" "+checked+" name=\"opc"+alter.getId_pregunta()+"\" value=\""+alter.getId_alternativa()+"\">  &nbsp;"+texto;
                   indcAlter++;
               }           
               
               cont++;
                salida += "</div></form><br><br><br>";
            }
           
            System.out.println(salida);
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
     
     private void addFichaToClase(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
          
            String salida = "<form>"
                    +"<br>"
                          
                            + "<div id=\"divNombreFicha\">"
                            + "<input style=\"padding-left: 10px;\"class=\"input_nombre_cuestionario\" id=\"inputNombreFicha\" type=\"text\" required=\"true\" value=\"\" placeholder=\"Nombre del cuestionario\">"
                            + "<br><br><button class=\"btnJec bprimario\"  onclick=\"return false\" id=\"btnAddPregunta\" >Agregar Pregunta&nbsp;&nbsp;&nbsp;+</button> "
                            + "</div>"
                           
                    
                           

                           //  +"<div class=\"Division\" id=\"divNuevoTitulo\"><input class=\"styleBotonCuesti\" type=\"button\" id=\"btnNuevoTitulo\" value=\"Nombre Ficha\" /> </div>"
                           //  +"<div class=\"Division\" id=\"divSaveTitulo\"><input class=\"styleBotonCuesti\" type=\"button\" id=\"btnSaveTitulo\" value=\"Guardar\" /> </div>"
                           // + "<br><br>"
                  
                    
                            + "<br><div id=\"contPregunta\">"
                            + "<b>"+(listaPreguntasTemporales.size()+1)+")</b>&nbsp;<input class=\"input_pregunta_cuestionario\" type=\"text\" placeholder=\"&nbsp;&nbsp;&nbsp;Ingrese la pregunta..\" id=\"inputPregunta\"/>&nbsp;&nbsp;&nbsp;"
                           // + "</div>"
                          
                    + "<div class=\"input-field col s6\">"
                    + "<select id=\"selectAlter\" name=\"selectAlter\">"
                            + "<option value =\"\" disabled selected> Alternativas </option>"
                            + "<option value =\"2\"> 2</option>"
                            + "<option value =\"3\"> 3</option>"
                            + "<option value =\"4\"> 4</option>"
                            + "<option value =\"5\"> 5</option>"
                            + "<option value =\"1\"> Abierta </option>"
                            + "</select>"
                           
                           // + "<label>Numero Alternativas</label>\n" 
                    + "</div>"
                    +"</div>"
                     + "<div id=\"contAlternativas\" style=\"margin-left: 15px;display: block;\"> </div>"
                      
       //             +"<div class=\"row\">"
            //        +"<div class=\"col s2\">"              
                    +"<div  id=\"divCancelar\"><br>"
                    +"<button style=\"background-color: #ffffff;color: #2D174C;border-radius: 4px;border-color: #2D174C;\n" +
"    padding: 8px 30px 4px 30px;font-weight: bold;cursor: pointer;text-transform: uppercase;font-size: 13px;\" id=\"btnCancelar\" >Cancelar</button>"
                    + "<button class=\"btnJec bprimario\" style=\"margin-left: 30px;\" onclick=\"return false\" id=\"btnSavePregunta\" >Guardar</button>"
             //       + "</div>"
                     +"</div>";  
               //     +"<div class=\"col s2\">"
                 //   + "<div  id=\"divSavePre\"> </div>"
               //     +"</div>"
          //          + "</div>   " ;      
            salida += "<br></form>"
                    + "<form><div id=\"contFichaTempo\" ></div>"
                    + "</form>";
            
           
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
     
    private void getNumAlternativas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            int val = Integer.parseInt(request.getParameter("val")); 
            String salida = "";
            if(val == 2 || val == 3 || val == 4 || val == 5){//CERRADA
                salida += "<br>";
            for(int i = 0; i < val; i++){
                salida += "<b>"+letrasCuestionario[i]+"</b>&nbsp;<input class=\"input_pregunta_cuestionario\" name=\"nameAlternativas\" type=\"text\" placeholder=\"Ingrese la alternativa..\" /><br>"; 
            }     
            salida+= "<div class=\"input-field col s6\">"
                    + "<select id=\"selectRespCorrecta\" name=\"selectRespCorrecta\">"
                    + "<option value =\"\" disabled selected> Alternativa Correcta </option>";
                    for(int i = 0; i < val; i++){
                salida+=  "<option value =\""+i+"\">"+letrasCuestionario[i]+"</option>";
                         }
            salida+= "</select>"
                     + "</div>";
            }else if(val ==  1){//ABIERTA
              
            }
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    public List<PreguntaBean> listaPreguntasTemporales = new ArrayList<PreguntaBean>();
   
    private void addPreguntasTemporal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        String salida = Constante.ERROR;
        try {
            String pregunta = request.getParameter("pregunta");  
            String correcta = request.getParameter("correcta");
            String numAlter = request.getParameter("numAlter");
            String[] alternativas = request.getParameterValues("alternativas[]");
            boolean isAbierta = false;
             System.out.println("Linea:430");
            if(quitaEspacios(pregunta).equals("")){
                throw new Exception("Complete la Pregunta");
            }   
         
            if(isNumeric(numAlter)){
                if(Integer.parseInt(numAlter) == 1){ System.out.println("Linea:436");
                    isAbierta = true;
                }else if (Integer.parseInt(numAlter) > 5 || Integer.parseInt(numAlter) < 2){
                       throw new Exception("Solo puede crear 2 a 5 alternativas!");
                }            
            }else{
                 throw new Exception("Seleccion un numero de Alternativas"); 
            }                
             System.out.println("Linea:443");    
            if (!isAbierta){ System.out.println("Linea:444");
                for(String str : alternativas){
                    if(quitaEspacios(str).equals("")){
                        throw new Exception("Complete todas las alternativas!");                 
                    }
                }
                if (isNumeric(correcta)){             
                    if (Integer.parseInt(correcta) > 4 || Integer.parseInt(correcta) < 0){
                       throw new Exception("Accion no permitida!");
                    }
                }else{
                    throw new Exception("Seleccion la Alternativa Correcta!"); 
                }
               
                    
            }     
            System.out.println("Linea:460");
            PreguntaBean preguBean = new PreguntaBean();
            List<AlternativaBean> listaAlternativas = new ArrayList<AlternativaBean>();
            AlternativaBean alterBean = new AlternativaBean();            
            preguBean.setDescripcion(pregunta);
            if(alternativas !=null){
                for(int i = 0; i < alternativas.length; i++ ){
                    alterBean = new AlternativaBean();
                    alterBean.setDescripcion(alternativas[i]);
                    if(i == Integer.parseInt(correcta)){
                       alterBean.setIsRespuesta("1");
                    }else{
                       alterBean.setIsRespuesta("0");
                    }
                     listaAlternativas.add(alterBean);
                }            
                preguBean.setAlternativaBean(listaAlternativas);
            }
            if(Integer.parseInt(numAlter) == 1){
                preguBean.setTipo_pregunta(1);//ABIERTA
            }else{
                preguBean.setTipo_pregunta(2);//CERRADA
            }
            listaPreguntasTemporales.add(preguBean);  
            salida = Constante.NO_ERROR;
            System.out.println("salida:"+salida+"-Linea:483");
            pw.write(salida);
        } catch (Exception ex) {
            ex.printStackTrace();
            salida = ex.getMessage();
            pw.write(salida);
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);            
        }
	pw.close();
    }
    private String nombreFicha="";
     private void saveNombreFichaTemporal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         try {       
           nombreFicha = ((request.getParameter("nombreFicha") == null) ? "" : request.getParameter("nombreFicha"));
           } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }
     private void getFichaTemporal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();        
        try {
            int cont = 1;
            int numLetra = 0;
        //  String salida = "<h2 class=\"blancoNegrita\" style=\"text-align: center; \">"+nombreFicha+"</h2><form id=\"formPreguntas\" method=\"POST\" action=\"\">";
         String salida ="";   
        for(PreguntaBean bean : listaPreguntasTemporales){
              salida += "<div style=\"margin-left: 40px;\">";
              salida += "<b>"+cont+") &nbsp;"+bean.getDescripcion()+"</b><br>";
               numLetra = 0;
               if(bean.getTipo_pregunta() == 2){
                for(AlternativaBean alter : bean.getAlternativaBean()){
                    String letra = letrasCuestionario[numLetra];
                    if(alter.getIsRespuesta().equals("1")){
                        salida += "<strong style=\"color:#0D91C5\">"+letra+" "+alter.getDescripcion()+"</strong><br>";
                    }else if(alter.getIsRespuesta().equals("0")){
                        salida += ""+letra+"&nbsp;"+alter.getDescripcion()+"<br>";
                    }
                    numLetra++;
                }
               }
               
               salida += "<br></div>";
                      
               cont++;
            }
            
            
            
            
            if(listaPreguntasTemporales.size()>0 ){
                  salida += "<div style=\"text-align: center; margin-bottom: 40px;\"><input type=\"button\"  class=\"btn waves-effect colorMorado\"  id=\"btnSaveFichaTemporal\" value=\"Guardar Ficha\" /></div>";
            }
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
    private void saveFichaTemporal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        try {       
            HttpSession session = request.getSession(); 
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase");  
            FichaBean ficha = new FichaBean();
            //ficha.setDescripcion(nombreFicha);
            ficha.setDescripcion(request.getParameter("titulo"));
            ficha.setListaPregunas(listaPreguntasTemporales);
            cuestionarioServiceImpl = new CuestionarioServiceImpl();
            String salida = cuestionarioServiceImpl.saveFichaTemporal(ficha, usuarioClase);          
            listaPreguntasTemporales = new ArrayList<PreguntaBean>();
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }
            
    public String quitaEspacios(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        StringBuilder buff = new StringBuilder();
        while (tokens.hasMoreTokens()) {
            buff.append(" ").append(tokens.nextToken());
        }
        return buff.toString().trim();
     }
    private static boolean isNumeric(String cadena){
            try {
                    Integer.parseInt(cadena);
                    return true;
            } catch (NumberFormatException nfe){
                    return false;
            }
    }
    private void getNombreFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
         try {
               HttpSession session = request.getSession();
               String id_ficha = session.getAttribute("id_ficha_select").toString();
               String salida = cuestionarioServiceImpl.getNombreficha(id_ficha);
               pw.write(salida);
         } catch (Exception ex) {
              Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
          }
          pw.close();
         }
         
    private void descargarFichaByAlumno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase"); 
            String id_ficha = session.getAttribute("id_ficha_select").toString();
            //String salida = "<div style=\"width: 400px;\"><h2 style=\"text-align: center; \">"+cuestionarioServiceImpl.getNombreficha(id_ficha)+"</h2></div>";
          
            
           // System.out.println("ID_FICHA_DOWNLOAD:"+id_ficha);
            String id_clase = usuarioClase.getIdClase()+"";
            String salida ="<label>GRADO Y SECCION: "+cuestionarioServiceImpl.getSeccionByIdClase(id_clase).getCodigoGrado()+" "+cuestionarioServiceImpl.getSeccionByIdClase(id_clase).getNombreSeccion()+"</label></br>";
            String id_estudiante = request.getParameter("id_estudiante");
            salida +="<label>ALUMNO: "+cuestionarioServiceImpl.getNombreAlumno(id_estudiante)+"</label><br>";
           
            //System.out.println("ID_ESTUDIANTE_DOWNLOAD:"+id_estudiante);
            
           // System.out.println("ID_CLASE_DOWNLOAD:"+id_clase);
            listaPreguntas = cuestionarioServiceImpl.getPreguntasByFicha(Integer.parseInt(id_ficha));
            List<RespuestaEstudianteBean> listaRespuestas = cuestionarioServiceImpl.getAlternativasByAlumno(id_ficha, id_estudiante, id_clase);
            int cont = 1;           
            int indcAlter= 0;
             String letra ="";
            for(PreguntaBean bean : listaPreguntas){     
                //System.out.println("pregunta"+bean.getDescripcion()+"-"+"alternativa dle alumno:"+alternativaAlumno);
                indcAlter= 0;
                salida += "<form><div><br>";      
                    salida += cont+")"+bean.getDescripcion().toUpperCase();
                    if(bean.getTipo_pregunta() == 2){
                        int alternativaAlumno = getAlternativaByIdPreguntaEstudiante(bean.getId_pregunta(), listaRespuestas);
                        for(AlternativaBean alter : bean.getAlternativaBean()){
                            letra = (letrasCuestionario[indcAlter]);       
                            String checked = "";
                            String texto = "";
                            if(alter.getId_alternativa() == alternativaAlumno){System.out.println("igualdad");
                                checked = "checked";
                                if(alter.getIsRespuesta().equals("1")){                         
                                     texto =  ((alter.getIsRespuesta().equals("1")) ? "<font color=\"green\">"+letra+" "+alter.getDescripcion()+"</font>" : letra+" "+alter.getDescripcion()); 
                                }else{                            
                                     texto =  "<font color=\"red\">"+letra+" "+alter.getDescripcion()+"</font>"; 
                                 }
                            }else{
                                texto = letra+" "+alter.getDescripcion();
                            } 
                            System.out.println("texto:"+texto);
                            salida += "<input type=\"radio\" "+checked+" name=\"opc"+alter.getId_pregunta()+"\" value=\""+alter.getId_alternativa()+"\">  &nbsp;"+texto;
                            indcAlter++;
                        }           
                    }else if(bean.getTipo_pregunta() == 1){
                        for(RespuestaEstudianteBean repu : listaRespuestas){
                            System.out.println(repu.getId_pregunta()+"-"+repu.getDescRespuesta());
                            if(repu.getId_pregunta()==bean.getId_pregunta()){
                                 salida +="<br>"+repu.getDescRespuesta();
                                 break;
                            }
                        }                           
                    }
               
               cont++;
                salida += "</div></form><br><br><br>";
            }
           
          //  System.out.println(salida);
            pw.write(salida);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }
    
    public int getAlternativaByIdPreguntaEstudiante (int idPregunta,  List<RespuestaEstudianteBean> listaRespuestas){
         System.out.println("respuestas:"+listaRespuestas.size());
        for(RespuestaEstudianteBean resp : listaRespuestas){
            System.out.println("resp estu:"+resp.getId_pregunta()+"-"+resp.getId_alternativa());
            if(resp.getId_pregunta()==idPregunta){
                return resp.getId_alternativa();
            }
        }
        return 0;
    }
    
    private void getFichaToCalificar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        cuestionarioServiceImpl = new CuestionarioServiceImpl();
        try {
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase"); 
            String id_ficha = session.getAttribute("id_ficha_select").toString();
            //String salida = "<div style=\"width: 400px;\"><h2 style=\"text-align: center; \">"+cuestionarioServiceImpl.getNombreficha(id_ficha)+"</h2></div>";
          
            
           // System.out.println("ID_FICHA_DOWNLOAD:"+id_ficha);
            String id_clase = usuarioClase.getIdClase()+"";
            String salida ="<div id=\"contFichaCalificar\" style =\" margin-left: 40px; margin-right: 40px;\"><label>GRADO Y SECCION: "+cuestionarioServiceImpl.getSeccionByIdClase(id_clase)+"</label></br>";
            String id_estudiante = request.getParameter("id_estudiante");
            session.setAttribute("id_estudiante_to_calificar", id_estudiante);
            salida +="<label>ALUMNO: "+cuestionarioServiceImpl.getNombreAlumno(id_estudiante)+"</label><br>"
                    + "<form id=\"formPreguntas\" method=\"POST\" action=\"\">";
           
            //System.out.println("ID_ESTUDIANTE_DOWNLOAD:"+id_estudiante);
            
           // System.out.println("ID_CLASE_DOWNLOAD:"+id_clase);
            listaPreguntas = cuestionarioServiceImpl.getPreguntasByFicha(Integer.parseInt(id_ficha));
            List<RespuestaEstudianteBean> listaRespuestas = cuestionarioServiceImpl.getAlternativasByAlumno(id_ficha, id_estudiante, id_clase);
            int cont = 1;           
            int indcAlter= 0;
             String letra ="";
            for(PreguntaBean bean : listaPreguntas){     
                //System.out.println("pregunta"+bean.getDescripcion()+"-"+"alternativa dle alumno:"+alternativaAlumno);
                indcAlter= 0;
                salida += "<div><br>";      
                    salida += "<b>"+cont+")"+bean.getDescripcion().toUpperCase()+"</b>";
                    if(bean.getTipo_pregunta() == 2){
                        int alternativaAlumno = getAlternativaByIdPreguntaEstudiante(bean.getId_pregunta(), listaRespuestas);
                        for(AlternativaBean alter : bean.getAlternativaBean()){
                            letra = (letrasCuestionario[indcAlter]);       
                            String checked = "";
                            String texto = "";
                            String disabled ="";
                            if(alter.getId_alternativa() == alternativaAlumno){System.out.println("igualdad");
                                checked = "checked";
                                if(alter.getIsRespuesta().equals("1")){                         
                                     texto =  ((alter.getIsRespuesta().equals("1")) ? "<font color=\"green\">"+letra+" "+alter.getDescripcion()+"</font>" : letra+" "+alter.getDescripcion()); 
                                }else{                            
                                     texto =  "<font color=\"red\">"+letra+" "+alter.getDescripcion()+"</font>"; 
                                 }
                            }else{
                                disabled = "disabled";
                                texto = letra+" "+alter.getDescripcion();
                            } 
                            System.out.println("texto:"+texto);
                            salida += "<br><input type=\"radio\" "+checked+" name=\"opc"+alter.getId_pregunta()+"\" value=\""+alter.getId_alternativa()+"\" "+disabled+">  &nbsp;"+texto;
                            indcAlter++;
                        }           
                    }else if(bean.getTipo_pregunta() == 1){
                        for(RespuestaEstudianteBean repu : listaRespuestas){
                            System.out.println(repu.getId_pregunta()+"-"+repu.getDescRespuesta());
                            if(repu.getId_pregunta()==bean.getId_pregunta()){
                                 salida +="<br>"+repu.getDescRespuesta()+"<br>"
                                         + "<p>"
                                         + "<input type=\"radio\"  id=\"correcto\" name=\"opc"+bean.getId_pregunta()+"\" value=\"1\"><label  for=\"correcto\">Correcto</label>&nbsp;"
                                         + "<input type=\"radio\"  id=\"Incorrecto\" name=\"opc"+bean.getId_pregunta()+"\" value=\"2\"><label  for=\"Incorrecto\">Incorrecto</label>"
                                         + "</p><br>";
                                 break;
                            }
                        }                           
                    }
               
               cont++;
                salida += "<br><br></div>";
            }
           
          //  System.out.println(salida);onclick=\"getAlumnos(this);return false;\"
            pw.write(salida+"<div style=\"text-align: center; \"> <input type=\"button\" class=\"btn waves-effect waves-light\"  value=\"Guardar y Calificar\" onclick=\"saveCalificacion(this);return false;\" id=\""+id_ficha+"\" /></div></form>");
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	pw.close();
    }

        
         private void calificarexamen(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //1 CORRECTO, 2 INCORRECTO
            
            HttpSession session = request.getSession();
            UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase"); 
            String id_ficha = session.getAttribute("id_ficha_select").toString();         
            String id_clase = usuarioClase.getIdClase()+"";
            String id_estudiante = session.getAttribute("id_estudiante_to_calificar").toString();  
            List<RespuestaEstudianteBean> listaRespuestasOriginales = cuestionarioServiceImpl.getAlternativasByAlumno(id_ficha, id_estudiante, id_clase);
            int id_ficha_estudiante=0;
            int contIdFic=0;
            cuestionarioServiceImpl = new CuestionarioServiceImpl();
            List<RespuestaEstudianteBean> listaRespuestas = new ArrayList<RespuestaEstudianteBean>();
            RespuestaEstudianteBean respu = new RespuestaEstudianteBean();
            int correcto = 0;
            int fichaAbierta = 0;
            for(PreguntaBean bean : listaPreguntas){
                if(bean.getTipo_pregunta() == 2){//CERRADA                
                    String alternativaSelecta = request.getParameter("opc"+bean.getId_pregunta());              
                    for(AlternativaBean alter : bean.getAlternativaBean()){                  
                        if(alter.getId_alternativa() == Integer.parseInt(alternativaSelecta)){
                            //respu = new RespuestaEstudianteBean();
                            //respu.setId_pregunta(bean.getId_pregunta());
                           // respu.setId_alternativa(Integer.parseInt(alternativaSelecta));
                           // listaRespuestas.add(respu);
                            if(alter.getIsRespuesta().equals("1")){
                                correcto++;
                                break;
                            }
                        }
                    }
                }else if (bean.getTipo_pregunta() == 1){//ABIERTA
                       String respuesta = request.getParameter("opc"+bean.getId_pregunta());
                         respu = new RespuestaEstudianteBean();
                       for(RespuestaEstudianteBean bean2 : listaRespuestasOriginales){
                           if(bean2.getId_pregunta() == bean.getId_pregunta()){
                               respu.setId_ficha_estudiante(bean2.getId_ficha_estudiante());
                               respu.setId_pregunta(bean2.getId_pregunta());
                               respu.setIsCorrecto(respuesta);
                               if(respuesta.equals("1")){
                                   correcto++;
                               }
                               if(contIdFic == 0){
                                   id_ficha_estudiante = bean2.getId_ficha_estudiante();
                                   contIdFic++;
                               }
                               break;
                           }
                       }
                    
                       listaRespuestas.add(respu);
                    
                       System.out.println("resp"+bean.getId_pregunta()+":"+respuesta);
                }
                
            }
           
            
            int notaFinal = ( correcto * 20 )/listaPreguntas.size();            
            //HttpSession session = request.getSession();                     
            //UsuarioClaseBean usuarioClase = (UsuarioClaseBean) session.getAttribute("usuarioClase");
            FichaEstudianteBean fichaEstu = new FichaEstudianteBean();
            fichaEstu.setId_ficha_estudiante(id_ficha_estudiante);         
            fichaEstu.setNota_final(notaFinal+"");
            fichaEstu.setIsTerminado("1");

            cuestionarioServiceImpl.updateCuestionario(fichaEstu, listaRespuestas);
            System.out.println("SAVE?:notaFinal->"+notaFinal);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
	
    }
                        
    private void downloadExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        
       //PARAMETROS TO PRINT
        HttpSession session = request.getSession();
        UsuarioClaseBean usuarioClase = (UsuarioClaseBean)session.getAttribute("usuarioClase");      
        String id_clase = usuarioClase.getIdClase()+"";
        String id_sesion = usuarioClase.getIdSesion()+"";
        SeccionBean seccion = new SeccionBean();
        seccion = cuestionarioServiceImpl.getSeccionByIdClase(id_clase);
        String gradoYSeccion = seccion.getCodigoGrado() + " - "+seccion.getNombreSeccion();
        System.out.println("ID _ SECCION = "+seccion.getIdSeccion());
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename="+gradoYSeccion+"_"+id_sesion+".xls");
        HSSFWorkbook workbook = new HSSFWorkbook();
        // ...        
     
            HSSFSheet worksheet = workbook.createSheet("POI Worksheet");
            HSSFCellStyle cellStyle2 = workbook.createCellStyle();
            cellStyle2 = workbook.createCellStyle();
            Font font2 = workbook.createFont();//Create font
            font2.setBoldweight(Font.BOLDWEIGHT_BOLD);//Make font bold
            cellStyle2.setFont(font2);
            // index from 0,0... cell A1 is cell(0,0)
            HSSFRow row1 = worksheet.createRow((short) 0);

            HSSFCell cellA1 = row1.createCell((short) 0);
            cellA1.setCellValue("GRADO Y SECCION");
            cellA1.setCellStyle(cellStyle2);
            /*HSSFCellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellA1.setCellStyle(cellStyle);*/

            HSSFCell cellB1 = row1.createCell((short) 1);
            cellB1.setCellValue(gradoYSeccion);
            /*cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellB1.setCellStyle(cellStyle);*/

           /* HSSFCell cellC1 = row1.createCell((short) 2);
            cellC1.setCellValue(true);

            HSSFCell cellD1 = row1.createCell((short) 3);
            cellD1.setCellValue(new Date());
            cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat(HSSFDataFormat
                            .getBuiltinFormat("m/d/yy h:mm"));
            cellD1.setCellStyle(cellStyle);*/
            HSSFRow row2 = worksheet.createRow((short) 1);
            worksheet.autoSizeColumn(0);
            worksheet.autoSizeColumn(1);
           
            HSSFCell cellA2 = row2.createCell((short) 0);
            cellA2.setCellValue("SESION");
            cellA2.setCellStyle(cellStyle2);
             HSSFCell cellB2 = row2.createCell((short) 1);
            cellB2.setCellValue(id_sesion);
            
            HSSFRow row4 = worksheet.createRow((short) 3);
            HSSFCell cellA4 = row4.createCell((short) 0);
            cellA4.setCellValue("APELLIDOS");
             
               cellA4.setCellStyle(cellStyle2);
            HSSFCell cellB4 = row4.createCell((short) 1);
            cellB4.setCellValue("NOMBRES"); 
            cellB4.setCellStyle(cellStyle2);            
            //SI EXISTEN CUESTIONARIOS
            List<FichaBean> listaFichasBySesion = new ArrayList<FichaBean>();
            listaFichasBySesion = cuestionarioServiceImpl.getFichasActivasByIdSesion(id_sesion);
            int x = 2;
            for(FichaBean beanFicha : listaFichasBySesion){
                HSSFCell cellC4 = row4.createCell((short) x);
                if(beanFicha.getId_clase()==null){
                    cellC4.setCellValue(beanFicha.getDescripcion());
                }else{
                    cellC4.setCellValue("(CLASE) "+beanFicha.getDescripcion());
                }                
                worksheet.autoSizeColumn(x);
                HSSFCellStyle cellStyle = workbook.createCellStyle();
                cellStyle = workbook.createCellStyle();
                cellStyle.setAlignment(cellStyle.ALIGN_CENTER);
                Font font = workbook.createFont();//Create font
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);//Make font bold
                cellStyle.setFont(font);
                cellC4.setCellStyle(cellStyle);
                x++;
                
            }
            
            List<EstudianteBean> listaEstudiantesBySeccion = new ArrayList<EstudianteBean>();
            listaEstudiantesBySeccion = cuestionarioServiceImpl.getEstudiantesActivosByIdSeccion(seccion.getIdSeccion());
            String notaFinal="";
            if(listaEstudiantesBySeccion.size()>0){
               int i = 4;
                for(EstudianteBean bean : listaEstudiantesBySeccion){
                    HSSFRow row5 = worksheet.createRow((short) i);
                    HSSFCell cellA3 = row5.createCell((short) 0);
                    cellA3.setCellValue(bean.getApellidoPaterno().toUpperCase()+" "+bean.getApellidoMaterno().toUpperCase());
                    HSSFCell cellB3 = row5.createCell((short) 1);
                    cellB3.setCellValue(bean.getNombre().toUpperCase());
                    int y = 2;
                    for(FichaBean ficha : listaFichasBySesion){
                     HSSFCell cellC3 = row5.createCell((short) y);
                     notaFinal = cuestionarioServiceImpl.getNotaByEstudianteAndFicha(bean.getIdEstudiante(), ficha.getId_ficha());
                     cellC3.setCellValue((notaFinal == "" ) ? "0" : notaFinal);
                      HSSFCellStyle cellStyle = workbook.createCellStyle();
                        cellStyle = workbook.createCellStyle();
                        cellStyle.setAlignment(cellStyle.ALIGN_CENTER);
                        cellC3.setCellStyle(cellStyle);
                     y++;
                    }
                    i++;
                }
            }
                        
            
            
        //	fileOut.flush();
        //	fileOut.close();
                
        // ...
        System.out.println("hehe");
        workbook.write(response.getOutputStream()); // Write workbook to response.
        workbook.close();
    }
       
}
