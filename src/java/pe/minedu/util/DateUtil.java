/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.util;

/**
 *
 * @author edwin
 */
public class DateUtil {

    public static java.sql.Date utilDateToSQLDate(java.util.Date javaDate) {
        java.sql.Date sqlDate = null;
        if (javaDate != null) {
            sqlDate = new java.sql.Date(javaDate.getTime());
        }
        return sqlDate;
    }
    
    public static java.util.Date sqlDateToUtilDate(java.sql.Date sqlDate) {
        java.util.Date javaDate = null;
        if (sqlDate != null) {
            javaDate = new java.util.Date(sqlDate.getTime());
        }
        return sqlDate;
    }
}
