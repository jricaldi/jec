package pe.minedu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

public class FileUtil implements Serializable{

    public static String descargarArchivo(String archivo, String rutaDestino) throws Exception {
        File source = new File(archivo);
        File dest = new File(rutaDestino + File.separator + source.getName());
        if (source.exists()) {
            if (!dest.exists()) {
                InputStream is = null;
                OutputStream os = null;
                try {
                    is = new FileInputStream(source);
                    os = new FileOutputStream(dest);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = is.read(buffer)) > 0) {
                        os.write(buffer, 0, length);
                    }
                } finally {
                    if (is != null) {
                        is.close();
                    }
                    if (os != null) {
                        os.close();
                    }
                }
            }
        }
        
        return dest.getAbsolutePath();
    }

    public static void crearCarpeta(String ruta) {
        File file = new File(ruta);
        if (!file.exists()) {
            file.mkdir();
        }
    }
    
    public static void crearNuevoArchivo(String ruta) throws IOException{
        File file = new File(ruta);
        if (!file.exists()) {
            file.createNewFile();
        }
    }
    
    public static String moverArchivo(File fileAMover, String rutaDestino){
        try {
            File directorio = new File(rutaDestino);
            if(!directorio.exists()){
                return "";
            }
            File dest = new File(rutaDestino + fileAMover.getName());
            FileUtils.copyFile(fileAMover, dest);
            if(dest.exists()){
                return dest.getAbsolutePath();
            }else{
                return "";
            }
        } catch (IOException ex) {
            System.out.println("Error FileUtil.moverArchivo " + ex.getMessage());
            return "";
        }
    }
}
