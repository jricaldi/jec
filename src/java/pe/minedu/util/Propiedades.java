package pe.minedu.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Propiedades {
    public static Properties getProperties() {
        
        String catalinaHome = System.getenv("CATALINA_HOME");
        String propertyFolder = "/properties/";
        String nameFile = "jec.properties";
        InputStream properties = null;
        if(catalinaHome != null){
            String filePath= catalinaHome + propertyFolder + nameFile;
            try{
                properties = new FileInputStream(filePath);
                System.out.println("Se cargo el properties desde " + filePath);
            }catch( Exception e){
                System.out.println("No se encontro el properties en el directorio " + filePath);
                try {
                    properties = Propiedades.class.getClassLoader().getResourceAsStream(nameFile);
                    System.out.println("Se cargo el properties interno " + nameFile);
                } catch (Exception ex) {
                    System.out.println("No se encontro el properties interno ");
                }
            }
        } else {
            System.out.println("No se ha encontrado la variable de entorno CATALINA_HOME");
            try {
                properties = Propiedades.class.getClassLoader().getResourceAsStream(nameFile);
                System.out.println("Se cargo el properties interno " + nameFile);
            } catch (Exception e) {
                System.out.println("No se encontro el properties interno ");
            }

        }
        Properties config = new Properties();
        try {
            config.load(properties);
            properties.close();
             System.out.println("Se cargo satisfactoriamente el propertie al sistema");
        } catch (IOException ex) {
            System.out.println("Hubo un error al cargar el properties al sistema ");
        }
        return config;
    }
}