
package pe.minedu.util;

import java.util.Calendar;
import java.util.Properties;

public interface Constante {
    
    int PUERTO_SOCKET = 5494;
    
    String ESTADO_ACTIVO="1";
    String ESTADO_INACTIVO="0";
    String ERROR="1";
    String NO_ERROR="0";
    
    String PERFIL_DOCENTE="1";
    String PERFIL_ESTUDIANTE="2";
    String PERFIL_ADMINISTRADOR="9";
    
    String COMANDO_ABRIR_ARCHIVO = "ABRIR_ARCHIVO";
    String COMANDO_ABRIR_ARCHIVO_NUEVO = "ABRIR_ARCHIVO_NUEVO";
    String COMANDO_ENVIAR_MENSAJE = "ENVIAR_MENSAJE";
    String COMANDO_SCREENSHOOT = "SCREENSHOOT";
    String COMANDO_BLOQUEAR_ESCRITORIO = "BLOQUEAR_ESCRITORIO";
    String COMANDO_DESBLOQUEAR_ESCRITORIO = "DESBLOQUEAR_ESCRITORIO";
    String COMANDO_ABRIR_BROWSER = "COMANDO_ABRIR_BROWSER";
    
    String ESTUDIANTE_PATH = "C:\\JEC\\ESTUDIANTE";
    String DOCENTE_PATH = "C:\\JEC\\DOCENTE";
    String PREFIJO_ALUMNO = "ALU";
    String PREFIJO_DOCENTE = "DOC";
    String SERVIDOR_PATH = "\\JEC\\" + Calendar.getInstance().get(Calendar.YEAR) + "\\SESIONES\\";
    
    String RECURSO_MINEDU = "1";
    String RECURSO_DOCENTE = "2";
    
    String LOCAL = "LOCAL";
    String TODOS = "TODOS";
    
    int CARGA_GRADO = 1;
    int CARGA_CURSO = 2;
    int CARGA_SECCION = 3;
    int CARGA_DOCENTE = 4;
    int CARGA_SESION = 5;
    int CARGA_DOCENTE_CURSO = 6;
    int CARGA_CUESTIONARIO = 7;
    
    String PROCESO_CORRECTO = "OK";
    String PROCESO_ERROR = "ERROR";
    
    int ID_MS_WORD = 1;
    int ID_MS_EXCEL = 2;
    int ID_MS_POWER_POINT = 3;
    int ID_DOCUMENTO_TEXTO = 4;
    int ID_DOCUMENTO_PDF = 5;
    int ID_XMIND = 6;
    int ID_IMAGEN = 7;
    int ID_CMAPTOOLS = 8;
    int ID_PAGINA_WEB = 9;
    int ID_EXE_LEARNING = 10;
    int ID_REP_VIDEO = 11;
    int ID_REP_AUDIO = 12;
    int ID_ARDORA = 13;
    int ID_WINRAR = 14;
    int ID_SCRATCH_PROJECT =15;
    int ID_PUBLISHER = 16;
    int ID_JCLIC_PUZZLE = 17;
    int ID_GEOGEBRA = 18;
    
    String REPOSITORIO_WEB_TOMCAT = "C:\\JEC\\TOMCAT_REPOSITORY";
    String NOMBRE_JEC = "JEC";
    String NOMBRE_SESIONES = "SESIONES";
    int PERIODO_ACTUAL = Calendar.getInstance().get(Calendar.YEAR);
    String IMAGENES_APP_WEB = "C:\\JEC\\WEB_IMAGENES_APP\\";
    
    String ICON_ERROR = "<i class=\"small material-icons vertical\">error_outline</i>";
    
    String PERMITIR_VER_ESTUDIANTE = "1";
    
    /***
     * Constantes obtenidas del properties
     ***/
    Properties prop = Propiedades.getProperties();
    
    String JEC_DATABASE_CONECTION = prop.getProperty("jec.database.conection");
    String JEC_DATABASE_USER = prop.getProperty("jec.database.user");
    String JEC_DATABASSE_PASSWORD = prop.getProperty("jec.database.password");
    String JEC_IP_CHAT = prop.getProperty("jec.ip.chat");

}
