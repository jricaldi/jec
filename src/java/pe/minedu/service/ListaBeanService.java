/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.CursoDeDocenteBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.dao.ClaseEstudianteDao;
import pe.minedu.dao.CursoDocenteDao;
import pe.minedu.dao.EstudianteDao;
import pe.minedu.dao.SeccionDao;
import pe.minedu.dao.SesionDao;

/**
 *
 * @author GustavoSedano
 */
public class ListaBeanService {
    
    EstudianteDao estudianteDao;
    CursoDocenteDao cursoDocenteDao;
    SesionDao sesionDao;
    SeccionDao seccionDao;
    ClaseEstudianteDao claseEstudianteDao;
    
    public List<EstudianteBean> listEstudiantesxSeccion(int idSeccion){
        estudianteDao = new EstudianteDao();
        return estudianteDao.listEstudiantesxSeccion(idSeccion);
    }
    
    public List<CursoDeDocenteBean> listCursosDeDocentexIdDocente(int idDocente){
        cursoDocenteDao = new CursoDocenteDao();
        return cursoDocenteDao.listCursosDeDocentexIdDocente(idDocente);
    }
    
    public List<SesionBean> listSesionesxCurso(int idCurso){
        sesionDao = new SesionDao();
        return sesionDao.listSesionesxCurso(idCurso);
    }
    
    public List<SeccionBean> listSeccionesXGrado(String codigoGrado){
        seccionDao = new SeccionDao();
        return seccionDao.listSeccionesXGrado(codigoGrado);
    }
    
    public List<MonitoreoClaseEstudianteBean> listEstudiantesEnClase(int idClase){
        claseEstudianteDao = new ClaseEstudianteDao();
        return claseEstudianteDao.listEstudiantesEnClase(idClase);
    }
    
    public EstudianteBean obtEstudiantexId(int id){
        estudianteDao = new EstudianteDao();
        return estudianteDao.getEstudiante(id);
    }
}
