
package pe.minedu.service;

import java.io.File;
import java.util.List;
import pe.minedu.bean.ClaseRecursoBean;
import pe.minedu.bean.InstitucionBean;
import pe.minedu.dao.ClaseDao;
import pe.minedu.dao.ClaseRecursoDao;
import pe.minedu.dao.InstitucionDao;
import pe.minedu.util.Constante;
import pe.minedu.util.FileUtil;

public class ManejoRecursoService {
    
    private ClaseRecursoDao claseRecursoDao;
    private InstitucionDao institucionDao;
    private ClaseDao claseDao;
    
    public List<ClaseRecursoBean> listarRecursosxClase(int idClase, String perfil){
        claseRecursoDao = new ClaseRecursoDao();
        return claseRecursoDao.listarRecursosxClase(idClase, perfil);
    }
    
    public ClaseRecursoBean obtClaseRecursoxId(int idRecursoClase){
        claseRecursoDao = new ClaseRecursoDao();
        return claseRecursoDao.obtClaseRecursoxId(idRecursoClase);
    }
    public ClaseRecursoBean registrarNuevoRecurso(ClaseRecursoBean claseRecursoBean, String nombreCurso, int idClase){
        claseRecursoDao = new ClaseRecursoDao();
//        institucionDao = new InstitucionDao();
        claseDao = new ClaseDao();
//        String codigoSesion = claseDao.obtCodigoSesionxIdClase(idClase);
//        String codigoGrado = claseDao.obtCodigoGradoxIdClase(idClase);
//        File file = new File(claseRecursoBean.getRuta());
//        InstitucionBean institucionBean = institucionDao.obtInstitucionEscolar();
//        String rutaDestino = "\\\\" + institucionBean.getIpServidor1() + 
//                Constante.SERVIDOR_PATH + codigoGrado +
//                File.separator + nombreCurso +
//                File.separator + codigoSesion + File.separator;
//        if(file.exists()){
//            String rutaFinal = FileUtil.moverArchivo(file, rutaDestino);
//            if(!rutaFinal.equals("")){
//                claseRecursoBean.setRuta(rutaFinal);
//            }
//        }
        return claseRecursoDao.registrarNuevoRecurso(claseRecursoBean);
    }
    
    public String obtCarpetaDeArchivoNuevo(int idClase) throws Exception{
        institucionDao = new InstitucionDao();
        claseDao = new ClaseDao();
        String ruta = "";
        try {
            InstitucionBean institucionBean = institucionDao.obtInstitucionEscolar();
            ruta = institucionBean.getIpServidor1() + File.separator + Constante.NOMBRE_JEC;
            File folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            ruta = ruta + File.separator + Constante.PERIODO_ACTUAL;
            folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            ruta = ruta + File.separator + Constante.NOMBRE_SESIONES;
            folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            
            String[] rutasSesion = claseDao.obtDatosCreacionCarpetaIdClase(idClase);
            //codigo_grado
            ruta = ruta + File.separator + rutasSesion[0];
            folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            
            //nombre_curso
            ruta = ruta + File.separator + rutasSesion[1];
            folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            
            //codigo_sesion
            ruta = ruta + File.separator + rutasSesion[2];
            folderServidor = new File(ruta);
            if(!folderServidor.exists()){
                folderServidor.mkdir();
            }
            ruta = ruta + File.separator;
        } catch (Exception e) {
            ruta = "";
            e.printStackTrace();
            throw new Exception("Error ManejoRecursoService.obtCarpetaDeArchivoNuevo " + e.getMessage());
        }
        return ruta;
    }
    
}
