
package pe.minedu.service;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pe.minedu.bean.AlternativaBean;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.CursoDocenteBean;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.DocenteBean;
import pe.minedu.bean.FichaBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.InstitucionBean;
import pe.minedu.bean.PreguntaBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.bean.SesionRecursoBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.dao.AdministradorDao;
import pe.minedu.dao.CuestionarioDao;
import pe.minedu.dao.CursoDao;
import pe.minedu.dao.CursoDocenteDao;
import pe.minedu.dao.DocenteDao;
import pe.minedu.dao.GradoDao;
import pe.minedu.dao.InstitucionDao;
import pe.minedu.dao.SeccionDao;
import pe.minedu.dao.SesionDao;
import pe.minedu.dao.SesionRecursoDao;
import pe.minedu.util.Constante;

public class AdministradorService {

    public AdministradorService() {
        institucionDao = new InstitucionDao();
        institucionBean = institucionDao.obtInstitucionEscolar();
    }
    
    private List<GradoBean> grados;
    private List<CursoBean> cursos;
    private List<SeccionBean> secciones;
    private List<DocenteBean> docentes;
    private List<SesionBean> sesiones;
    private List<SesionRecursoBean> sesionRecursos;
    private List<CursoDocenteBean> cursoDocenteBeans;
    
    private GradoBean gradoBean;
    private CursoBean cursoBean;
    private SeccionBean seccionBean;
    private DocenteBean docenteBean;
    private UsuarioBean usuarioBean;
    private SesionBean sesionBean;
    private SesionRecursoBean sesionRecursoBean;
    private CursoDocenteBean cursoDocenteBean;
    
    private GradoDao gradoDao;
    private CursoDao cursoDao;
    private SeccionDao seccionDao;
    private DocenteDao docenteDao;
    private SesionDao sesionDao;
    private SesionRecursoDao sesionRecursoDao;
    private CursoDocenteDao cursoDocenteDao;
    private CuestionarioDao cuestionarioDao;
    
    private InstitucionBean institucionBean;
    private InstitucionDao institucionDao;
    
    private AdministradorDao administradorDao;
    
    public String manejarCargaDatosGrados(InputStream isExcel, int tipoCarga) {
        String mensaje = "";
        switch (tipoCarga) {
            case Constante.CARGA_CURSO:
                mensaje = cargarCursos(isExcel);
                break;
            case Constante.CARGA_DOCENTE:
                mensaje = cargarDocentes(isExcel);
                break;
            case Constante.CARGA_GRADO:
                mensaje = cargarGrados(isExcel);
                break;
            case Constante.CARGA_SECCION:
                mensaje = cargarSecciones(isExcel);
                break;
            case Constante.CARGA_SESION:
                mensaje = cargarSesiones(isExcel);
                break;
            case Constante.CARGA_CUESTIONARIO:
                mensaje = cargarCuestionarios(isExcel);
                break;
            case Constante.CARGA_DOCENTE_CURSO:
//                mensaje = cargarDocenteCurso(isExcel);
                mensaje = "Ya no hay carga de Docente Curso.";
                break;
        }
        return mensaje;
    }
    
    private String cargarGrados(InputStream inputStream) {
        String mensaje = "";
        try {
            grados = new ArrayList<GradoBean>();
            gradoDao = new GradoDao();
            int i = 0;
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    gradoBean = new GradoBean();
                    Double dIdGrado = row.getCell(0).getNumericCellValue();
                    gradoBean.setIdGrado(dIdGrado.intValue());
                    gradoBean.setNivelPrograma(row.getCell(1).getStringCellValue());
                    Double dPeriodoEscolar = row.getCell(2).getNumericCellValue();
                    gradoBean.setPeriodoEscolar(dPeriodoEscolar.intValue());
                    gradoBean.setAtencion(row.getCell(3).getStringCellValue());
                    String codigoGrado = gradoBean.getIdGrado() + "_" + gradoBean.getNivelPrograma();
                    gradoBean.setCodigoGrado(codigoGrado);
                    if(gradoDao.obtGradoBeanxGradoNivelPeriodo(gradoBean.getIdGrado(), gradoBean.getNivelPrograma(), gradoBean.getPeriodoEscolar()) == null){
                        grados.add(gradoBean);
                    }
                }
                i++;
            }
            mensaje = gradoDao.agregarGrados(grados);
            if(mensaje.equals(Constante.PROCESO_CORRECTO)){
                mensaje = "Se agregaron " + grados.size() + " grados del excel.";
            }
        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarGrados " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    
    private String cargarSecciones(InputStream inputStream){
        String mensaje = "";
        try {
            secciones = new ArrayList<SeccionBean>();
            seccionDao = new SeccionDao();
            gradoDao = new GradoDao();
            String nombreSeccion = "";
            int idGrado = 0;
            String nivelPrograma = "";
            String codigoGrado = "";
            int i = 0;
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    seccionBean = new SeccionBean();
                    nombreSeccion = row.getCell(0).getStringCellValue();
                    Double dIdGrado = row.getCell(1).getNumericCellValue();
                    idGrado = dIdGrado.intValue();
                    nivelPrograma = row.getCell(2).getStringCellValue();
                    codigoGrado = idGrado + "_" + nivelPrograma;
                    seccionBean.setCodigoGrado(codigoGrado);
                    seccionBean.setNombreSeccion(nombreSeccion);
                    if(gradoDao.obtGradoBeanxCodGrado(codigoGrado) != null){
                        if(seccionDao.obtSeccionBeanxCodigoGradoNombreSeccion(codigoGrado, nombreSeccion) == null){
                            secciones.add(seccionBean);
                        }
                    }
                }
                i++;
            }
            mensaje = seccionDao.agregarSecciones(secciones);
            if(mensaje.equals(Constante.PROCESO_CORRECTO)){
                mensaje = "Se agregaron " + secciones.size() + " secciones del excel.";
            }
        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarSecciones " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    private static final String ORIGINAL = "ÁáÉéÍíÓóÚúÜü";
    private static final String REPLACEMENT = "AaEeIiOoUuUu";
    public String stripAccents(String str) {
        if (str == null) {
            return null;
        }
        char[] array = str.toCharArray();
        for (int index = 0; index < array.length; index++) {
            int pos = ORIGINAL.indexOf(array[index]);
            if (pos > -1) {
                array[index] = REPLACEMENT.charAt(pos);
            }
        }
        return new String(array);
    }	
	
    
    private String cargarCuestionarios(InputStream inputStream){
        cuestionarioDao = new CuestionarioDao();
        String mensaje = "";
        String grado = "";
        String curso = "";
        int id_curso = 0;
        String id_sesion = "";
        String nombre_cuestionario = "";
        String alternativa_correcta = "";
        try {
            int i = 0;
            
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            boolean nuevoCuestionario = false;
            boolean nombreCuestionario = false;
            boolean preguntaAlternativaOK = false;
            
            List<FichaBean> listaFichas = new ArrayList<FichaBean>();
            FichaBean fichaBaen = new FichaBean();
            List<PreguntaBean> listaPreguntas = new ArrayList<PreguntaBean>();
            PreguntaBean preguntaBean = new PreguntaBean();
            List<AlternativaBean> listaAlternativas = new ArrayList<AlternativaBean>();
            AlternativaBean alternativaBean = new AlternativaBean();
            
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
              
                    
                    if( row.getCell(0) != null){
                        row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(1) != null){
                        row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(2) != null){
                        row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(3) != null){
                        row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(4) != null){
                        row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(5) != null){
                        row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
                    }
                    if( row.getCell(6) != null){
                        row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
                    }
                 
                    if( row.getCell(0) != null){
                        if(i==1){
                            grado = row.getCell(0).getStringCellValue();
                          //  System.out.println("::::::GRADO:::::");
                        }
                        if(i==2){
                          //  System.out.println("::::::CURSO:::::");
                            curso =  row.getCell(0).getStringCellValue();
                     //       System.out.println("::::::CURSO:::::"+stripAccents(curso.toUpperCase()));
                            id_curso = cuestionarioDao.getIdCursoByGradoYCurso(grado, stripAccents(curso.toUpperCase()));
                            if(id_curso == 0){//SI NO EXISTE EL CURSO EN LA BASE DE DATOS CANCELAR IMPORTACION
                               throw new Exception("ERROR(Fila "+(i+1)+"):El nombre de curso "+curso+" no es valido. por favor asegurarse de que el curso este cargado en el sistema.");
                            }                          
                        }
                      
                        if(!row.getCell(0).getStringCellValue().trim().equals("")){
                             // System.out.println("AQUI:"+row.getCell(0).getStringCellValue());
                        if(row.getCell(0).getStringCellValue().substring(0, 3).toUpperCase().equals("U00")){
                            id_sesion = "";
                            id_sesion = cuestionarioDao.getIdSessionByIdCursoAndCodSesion(id_curso, row.getCell(0).getStringCellValue());
                          //  System.out.println("::::::NUEVO CUESTIONARIO:::::SESION:"+id_sesion);
                            if(id_sesion.equals("")){//SI NO EXISTE UN ID SESION PARA EL CODIGO DE SESION Y CURSO EN LA BASE DE DATOSS
                                 throw new Exception("ERROR(Fila "+(i+1)+"):No se encontro una sesion valida para el curso: "+curso+" y el codigo de sesion : "+row.getCell(0).getStringCellValue());                         
                            }
                           
                            nombreCuestionario = true;
                            nuevoCuestionario = false;
                        }else if(nombreCuestionario){
                            if(fichaBaen.getDescripcion() != null){
                                fichaBaen.setListaPregunas(listaPreguntas);
                                listaFichas.add(fichaBaen);
                            }
                           
                            listaPreguntas = new ArrayList<PreguntaBean>();
                            nombre_cuestionario = row.getCell(0).getStringCellValue();     
                            fichaBaen = new FichaBean();
                            fichaBaen.setDescripcion(nombre_cuestionario);
                            fichaBaen.setEstado("1");
                            fichaBaen.setId_sesion(id_sesion);                            
                         //   System.out.println("::::::NOMBRE CUESTIONARIO:::::"+nombre_cuestionario);
                            nombreCuestionario = false;
                            nuevoCuestionario = true;
                        }else if(nuevoCuestionario){
                            preguntaBean = new PreguntaBean();//Nueva pregunta
                            listaAlternativas = new ArrayList<AlternativaBean>();//limpiamos la lista de alternativas
                            preguntaAlternativaOK = false;
                            preguntaBean.setTipo_pregunta(2);//PREGUNTA CERRADA
                            preguntaBean.setDescripcion(row.getCell(0).getStringCellValue());//PREGUNTA DESCRIPCION
                         //   System.out.println("::::::NUEVA PREGUNTA CUESTIONARIO:::::"+row.getCell(0).getStringCellValue());                        
                                
                            if(row.getCell(1) != null){//RESPUESTA CORRECTA                                
                                alternativa_correcta = "";
                                alternativa_correcta = row.getCell(1).getStringCellValue().toUpperCase().trim();                           
                          //      System.out.print("-"+row.getCell(1).getStringCellValue());//SIEMPRE ES LA ALTERNATIVA CORRECTA
                            }
                            if( row.getCell(2) != null){
                                alternativaBean = new AlternativaBean();
                                alternativaBean.setDescripcion(row.getCell(2).getStringCellValue());
                                if(alternativa_correcta.equals("A")){
                                    alternativaBean.setIsRespuesta("1");
                                    preguntaAlternativaOK = true;
                                }else{
                                    alternativaBean.setIsRespuesta("0");
                                }
                               listaAlternativas.add(alternativaBean);
                               // preguntaBean.getAlternativaBean().add(alternativaBean);
                          //      System.out.print("-"+row.getCell(2).getStringCellValue());//ALTERNATIVA 1
                            }
                            if( row.getCell(3) != null){
                                alternativaBean = new AlternativaBean();
                                alternativaBean.setDescripcion(row.getCell(3).getStringCellValue());
                                if(alternativa_correcta.equals("B")){
                                    alternativaBean.setIsRespuesta("1");
                                    preguntaAlternativaOK = true;
                                }else{
                                    alternativaBean.setIsRespuesta("0");
                                }
                                listaAlternativas.add(alternativaBean);
                                //preguntaBean.getAlternativaBean().add(alternativaBean);
                       //     System.out.print("-"+row.getCell(3).getStringCellValue());//ALTERNATIVA 2
                            }
                            if( row.getCell(4) != null){
                                alternativaBean = new AlternativaBean();
                                alternativaBean.setDescripcion(row.getCell(4).getStringCellValue());
                                if(alternativa_correcta.equals("C")){
                                    alternativaBean.setIsRespuesta("1");
                                    preguntaAlternativaOK = true;
                                }else{
                                    alternativaBean.setIsRespuesta("0");
                                }
                                listaAlternativas.add(alternativaBean);
                                //preguntaBean.getAlternativaBean().add(alternativaBean);
                       //     System.out.print("-"+row.getCell(4).getStringCellValue());//ALTERNATIVA 3
                            }
                            if( row.getCell(5) != null){
                                alternativaBean = new AlternativaBean();
                                alternativaBean.setDescripcion(row.getCell(5).getStringCellValue());
                                if(alternativa_correcta.equals("D")){
                                    alternativaBean.setIsRespuesta("1");
                                    preguntaAlternativaOK = true;
                                }else{
                                    alternativaBean.setIsRespuesta("0");
                                }
                                listaAlternativas.add(alternativaBean);
                                //preguntaBean.getAlternativaBean().add(alternativaBean);
                       //     System.out.print("-"+row.getCell(5).getStringCellValue());//ALTERNATIVA 4
                            }
                            if( row.getCell(6) != null){
                                alternativaBean = new AlternativaBean();
                                alternativaBean.setDescripcion(row.getCell(6).getStringCellValue());
                               if(alternativa_correcta.equals("E")){
                                    alternativaBean.setIsRespuesta("1");    
                                    preguntaAlternativaOK = true;
                               }else{
                                    alternativaBean.setIsRespuesta("0");
                                }
                               listaAlternativas.add(alternativaBean);
                               //preguntaBean.getAlternativaBean().add(alternativaBean);
                    //        System.out.print("-"+row.getCell(6).getStringCellValue());//ALTERNATIVA 5
                            }               
                            preguntaBean.setAlternativaBean(listaAlternativas);
                            if(!preguntaAlternativaOK){
                               throw new Exception("ERROR(Fila "+(i+1)+"):Las alternativas y respuesta correcta no tienen el formato correcto.");
                            }else{
                                listaPreguntas.add(preguntaBean);
                            }
                        }
                    }
                     //   System.out.println(row.getCell(0).getStringCellValue());
                    }
                
                i++;
            }
            
            System.out.println("**************************************************************");
           /* for(FichaBean ficha : listaFichas){
                 System.out.println("NOMBRE FICHA:"+ficha.getDescripcion());
                 for(PreguntaBean pregunta : ficha.getListaPregunas()){
                     System.out.println("::"+pregunta.getDescripcion());
                     for(AlternativaBean alternativa : pregunta.getAlternativaBean()){
                         System.out.println("::::"+alternativa.getDescripcion());
                     }
                 }
            } */
          
            String salida = cuestionarioDao.registrarNuevaFichaFromExcel(listaFichas);
           if(salida.equals("0")){
               mensaje = "Importacion Completa!";
           }else{
               mensaje = "ERROR : Hubo un error interno, vuelva a intentarlo si continua el problema comuniquese con el Administrador.";
           }
           
            System.out.println("**************************************************************");
          
        } catch (Exception e) {
            mensaje = e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    
    private String cargarCursos(InputStream inputStream){
        String mensaje = "";
        try {
            cursos = new ArrayList<CursoBean>();
            cursoDao = new CursoDao();
            gradoDao = new GradoDao();
            int i = 0;
            int idGrado = 0;
            String nivelPrograma = "";
            String codigoGrado = "";
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
//                    row.getCell(5).setCellType(Cell.CELL_TYPE_STRING); // area_curricular
                    row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
                    cursoBean = new CursoBean();
//                    Double dIdGrado = row.getCell(0).getNumericCellValue();
//                    idGrado = dIdGrado.intValue();
                    idGrado = Integer.parseInt(row.getCell(0).getStringCellValue().trim());
                    nivelPrograma = row.getCell(1).getStringCellValue();
                    codigoGrado = idGrado + "_" + nivelPrograma;
                    
                    cursoBean.setCodigoGrado(codigoGrado);
                    cursoBean.setCodigoCurso(row.getCell(2).getStringCellValue());
                    cursoBean.setNombreCurso(row.getCell(3).getStringCellValue());
                    cursoBean.setCurricula(row.getCell(4).getStringCellValue().trim());
                    cursoBean.setAreaCurricular("AREA");
                    cursoBean.setCurricula(row.getCell(5).getStringCellValue().trim());
                    
                    if(gradoDao.obtGradoBeanxCodGrado(codigoGrado) != null){
                        if(cursoDao.obtCursoParaRegistroExcel(cursoBean.getCodigoGrado(), cursoBean.getCodigoCurso()) == null){
                            cursos.add(cursoBean);
                        }
                    }
                }
                i++;
            }
            
            mensaje = cursoDao.agregarCursos(cursos);
            if(mensaje.equals(Constante.PROCESO_CORRECTO)){
                mensaje = "Se agregaron " + cursos.size() + " cursos del excel.";
            }

        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarCursos " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    
    private String cargarDocentes(InputStream inputStream){
        String mensaje = "";
        try {
            docentes = new ArrayList<DocenteBean>();
            docenteDao = new DocenteDao();
            int i = 0;
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    docenteBean = new DocenteBean();
                    usuarioBean = new UsuarioBean();
                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    docenteBean.setDni(row.getCell(0).getStringCellValue());
                    docenteBean.setCodDocente(row.getCell(0).getStringCellValue());
                    docenteBean.setNombre(row.getCell(1).getStringCellValue());
                    docenteBean.setApellidoPaterno(row.getCell(2).getStringCellValue());
                    docenteBean.setApellidoMaterno(row.getCell(3).getStringCellValue());
                    docenteBean.setGenero(row.getCell(4).getStringCellValue());
                    docenteBean.setCorreo(row.getCell(5).getStringCellValue());
                    row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
                    docenteBean.setTelefono(row.getCell(6).getStringCellValue());
                    row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
                    docenteBean.setEstado(row.getCell(7).getStringCellValue());
                    
                    usuarioBean.setUsuario(row.getCell(0).getStringCellValue());
//                    row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
                    usuarioBean.setPassword(row.getCell(0).getStringCellValue());
                    usuarioBean.setPerfil(Constante.PERFIL_DOCENTE);
                    usuarioBean.setIdEstudiante(0);
                    usuarioBean.setEstado(Constante.ESTADO_ACTIVO);
                    usuarioBean.setUltimoDiaLog("");
                    usuarioBean.setUltimoDiaIp("");
                    
                    docenteBean.setUsuarioBean(usuarioBean);
                    
                    if(docenteDao.obtDocenteBeanxDni(docenteBean.getDni()) == null){
                        docentes.add(docenteBean);
                    }
                }
                i++;
            }

            mensaje = docenteDao.agregarDocentes(docentes);
            if(mensaje.equals(Constante.PROCESO_CORRECTO)){
                mensaje = "Se agregaron " + docentes.size() + " docentes del excel.";
            }
        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarDocentes " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    
    private String cargarSesiones(InputStream inputStream){
        String mensaje = "";
        try {
            sesionRecursos = new ArrayList<SesionRecursoBean>();
            sesiones = new ArrayList<SesionBean>();
            sesionDao = new SesionDao();
            sesionRecursoDao = new SesionRecursoDao();
            gradoDao = new GradoDao();
            cursoDao = new CursoDao();
            String idSesion = "";
            boolean existeGrado = false, existeCurso = false, codigoDeSesionNoVacio = false;
            int i = 0;
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    sesionBean = new SesionBean();
                    cursoBean = new CursoBean();
                    gradoBean = new GradoBean();
                    
                    String verEstudiante = "";
                    String word = "";
                    String cmaptools = "";
                    String ardora = "";
                    String exeLearning = "";
                    String reproductorVideo = "";
                    String reproductoMusica = "";
                    String image = "";
                    String paginaweb = "";
                    String pdf = "";
                    String powerPoint = "";
                    String winrar = "";
                    String excel = "";
                    String xmind = "";
                    String scratchProject = "";
                    String publisher = "";
                    String jclicPuzzle = "";
                    String geogebra = "";

                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);// id_grado
                    String idGrado = row.getCell(0).getStringCellValue();
                    row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);// nivel_programa
                    String nivelPrograma = row.getCell(1).getStringCellValue();
                    row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);// codigo_curso
                    String codigoCurso = row.getCell(2).getStringCellValue();
                    row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);// codigo_sesion
                    String codigoSesion = row.getCell(3).getStringCellValue();
                    
                    try{
                        row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);// 
                        verEstudiante = row.getCell(4).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);// PROCESADOR DE TEXTO
                        word = row.getCell(5).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);// CMAPTOOL
                        cmaptools = row.getCell(6).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);// ARDORA
                        ardora = row.getCell(7).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);// EXE LEARNING
                        exeLearning = row.getCell(8).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);// REPRODUCTOR DE VIDEO
                        reproductorVideo = row.getCell(9).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);// REPRODUCTOR DE MUSICA
                        reproductoMusica = row.getCell(10).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(11).setCellType(Cell.CELL_TYPE_STRING);// IMAGE
                        image = row.getCell(11).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(12).setCellType(Cell.CELL_TYPE_STRING);// PAGINA WEB
                        paginaweb = row.getCell(12).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(13).setCellType(Cell.CELL_TYPE_STRING);// PDF
                        pdf = row.getCell(13).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(14).setCellType(Cell.CELL_TYPE_STRING);// PRESENTADOR DE DIAPOSITIVAS
                        powerPoint = row.getCell(14).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(15).setCellType(Cell.CELL_TYPE_STRING);// WINRAR
                        winrar = row.getCell(15).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(16).setCellType(Cell.CELL_TYPE_STRING);// HOJA DE CALCULO
                        excel = row.getCell(16).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(17).setCellType(Cell.CELL_TYPE_STRING);// XMIND
                        xmind = row.getCell(17).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(18).setCellType(Cell.CELL_TYPE_STRING);// SCRATCH PROJECT
                        scratchProject = row.getCell(18).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(19).setCellType(Cell.CELL_TYPE_STRING);// PUBLISHER
                        publisher = row.getCell(19).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(20).setCellType(Cell.CELL_TYPE_STRING);// JCLIC PUZZLE
                        jclicPuzzle = row.getCell(20).getStringCellValue();
                    }
                    catch(Exception e){
                    }
                    try{
                        row.getCell(21).setCellType(Cell.CELL_TYPE_STRING);// GEOGEBRA
                        geogebra = row.getCell(21).getStringCellValue();
                    }
                    catch(Exception e){
                    }

                    
                    String codigoGrado = idGrado + "_" + nivelPrograma;                   
                    idSesion = generarIdSesion(idGrado, nivelPrograma, codigoCurso, codigoSesion);
                    gradoBean = gradoDao.obtGradoBeanxCodGrado(codigoGrado);
                    existeGrado = gradoBean != null;
                    if(existeGrado){
                        cursoBean = cursoDao.obtCursoParaRegistroExcel(codigoGrado, codigoCurso);
                        existeCurso = cursoBean != null;
                    }
                    codigoDeSesionNoVacio = !codigoSesion.trim().equals("");
                    
                    if(existeCurso && codigoDeSesionNoVacio){
                        sesionBean = sesionDao.obtSesionxIdSesion(idSesion);
                        if(sesionBean == null){
                            //agregamos una nueva sesion
                            sesionBean = new SesionBean();
                            sesionBean.setIdCurso(cursoBean.getIdCurso());
                            sesionBean.setIdSesion(idSesion);
                            sesionBean.setCodigoSesion(codigoSesion);
                            sesionBean.setFechaReg(new Date());
                            sesionBean.setEstado(Constante.ESTADO_ACTIVO);
                            sesionBean = sesionDao.agregarSesionBean(sesionBean);
                        }
                        
                        if(sesionBean != null){
                            
                            //creamos las carpetas respectivas
                            String rutaFolderSesion = "\\\\" + institucionBean.getIpServidor1() 
                                    + Constante.SERVIDOR_PATH + codigoGrado 
                                    + File.separator + cursoBean.getNombreCurso() 
                                    + File.separator + codigoSesion;
                            File fFolderSesion = new File(rutaFolderSesion);
                            if(!fFolderSesion.exists()){
                                fFolderSesion.mkdirs();
                            }
                            
                            //agregamos recursos
                            if(!word.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_MS_WORD);
                                sesionRecursoBean.setTitulo(word.substring(word.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(word.substring(word.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(word);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, word) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!excel.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_MS_EXCEL);
                                sesionRecursoBean.setTitulo(excel.substring(excel.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(excel.substring(excel.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(excel);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, excel) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!powerPoint.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_MS_POWER_POINT);
                                sesionRecursoBean.setTitulo(powerPoint.substring(powerPoint.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(powerPoint.substring(powerPoint.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(powerPoint);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, powerPoint) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!pdf.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_DOCUMENTO_PDF);
                                sesionRecursoBean.setTitulo(pdf.substring(pdf.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(pdf.substring(pdf.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(pdf);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, pdf) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!xmind.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_XMIND);
                                sesionRecursoBean.setTitulo(xmind.substring(xmind.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(xmind.substring(xmind.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(xmind);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, xmind) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!image.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_IMAGEN);
                                sesionRecursoBean.setTitulo(image.substring(image.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(image.substring(image.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(image);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, image) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!cmaptools.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_CMAPTOOLS);
                                sesionRecursoBean.setTitulo(cmaptools.substring(cmaptools.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(cmaptools.substring(cmaptools.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(cmaptools);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, cmaptools) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!paginaweb.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_PAGINA_WEB);
                                sesionRecursoBean.setTitulo(paginaweb.substring(paginaweb.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(paginaweb.substring(paginaweb.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(paginaweb);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, paginaweb) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!exeLearning.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_EXE_LEARNING);
                                sesionRecursoBean.setTitulo(exeLearning.substring(exeLearning.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(exeLearning.substring(exeLearning.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(exeLearning);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, exeLearning) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!reproductorVideo.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_REP_VIDEO);
                                sesionRecursoBean.setTitulo(reproductorVideo.substring(reproductorVideo.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(reproductorVideo.substring(reproductorVideo.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(reproductorVideo);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, reproductorVideo) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            if(!reproductoMusica.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_REP_AUDIO);
                                sesionRecursoBean.setTitulo(reproductoMusica.substring(reproductoMusica.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(reproductoMusica.substring(reproductoMusica.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(reproductoMusica);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, reproductoMusica) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!ardora.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_ARDORA);
                                sesionRecursoBean.setTitulo(ardora.substring(ardora.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(ardora.substring(ardora.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(ardora);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, ardora) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!winrar.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_WINRAR);
                                sesionRecursoBean.setTitulo(winrar.substring(winrar.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(winrar.substring(winrar.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(winrar);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, winrar) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!scratchProject.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_SCRATCH_PROJECT);
                                sesionRecursoBean.setTitulo(scratchProject.substring(scratchProject.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(scratchProject.substring(scratchProject.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(scratchProject);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, scratchProject) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!publisher.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_PUBLISHER);
                                sesionRecursoBean.setTitulo(publisher.substring(publisher.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(publisher.substring(publisher.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(publisher);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, publisher) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!jclicPuzzle.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_JCLIC_PUZZLE);
                                sesionRecursoBean.setTitulo(jclicPuzzle.substring(jclicPuzzle.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(jclicPuzzle.substring(jclicPuzzle.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(jclicPuzzle);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, jclicPuzzle) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                            
                            if(!geogebra.trim().equals("")){
                                sesionRecursoBean = new SesionRecursoBean();
                                sesionRecursoBean.setIdSesion(idSesion);
                                sesionRecursoBean.setIdAplicacion(Constante.ID_GEOGEBRA);
                                sesionRecursoBean.setTitulo(geogebra.substring(geogebra.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setCategoria("");
                                sesionRecursoBean.setDescripcion(geogebra.substring(geogebra.lastIndexOf("\\") + 1));
                                sesionRecursoBean.setFechaReg(new Date());
                                sesionRecursoBean.setRuta(geogebra);
                                sesionRecursoBean.setEstado(Constante.ESTADO_ACTIVO);
                                sesionRecursoBean.setVerEstudiante(verEstudiante);
                                
                                if(sesionRecursoDao.obtSesionRecursoxIdSesionRuta(idSesion, geogebra) == null){
                                    sesionRecursos.add(sesionRecursoBean);
                                }
                            }
                        }
                    }
                }
                i++;
            }

            mensaje = sesionRecursoDao.agregarSesionRecursos(sesionRecursos); //falta implementar
        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarSesiones " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
/*
    private String cargarDocenteCurso(InputStream inputStream) {
        String mensaje = "";
        try {
            cursoDocenteBeans = new ArrayList<CursoDocenteBean>();
            gradoDao = new GradoDao();
            docenteDao = new DocenteDao();
            cursoDao = new CursoDao();
            cursoDocenteDao = new CursoDocenteDao();
            boolean existeDocente = false, existeGrado = false, existeCurso = false;            
            String idGrado = "";
            String nivelPrograma = "";
            String codigoGrado = "";
            String codigoCurso = "";
            String dni = "";
            int i = 0;
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            Iterator<Row> rowIterator = xSSFSheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i != 0) {
                    cursoDocenteBean = new CursoDocenteBean();
                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                    
                    idGrado = row.getCell(0).getStringCellValue();
                    nivelPrograma = row.getCell(1).getStringCellValue();
                    codigoGrado = idGrado + "_" + nivelPrograma;
                    codigoCurso = row.getCell(2).getStringCellValue();
                    dni = row.getCell(4).getStringCellValue();
                    docenteBean = docenteDao.obtDocenteBeanxDni(dni);
                    gradoBean = gradoDao.obtGradoBeanxCodGrado(codigoGrado);
                    cursoBean = null;
                    existeDocente = docenteBean != null;
                    existeGrado = gradoBean != null;
                    if(existeGrado){
                        cursoBean = cursoDao.obtCursoParaRegistroExcel(codigoGrado, codigoCurso);
                        existeCurso = cursoBean != null;
                    }
                    
                    if(existeDocente && existeGrado && existeCurso){
                        cursoDocenteBean.setIdCurso(cursoBean.getIdCurso());
                        cursoDocenteBean.setIdDocente(docenteBean.getIdDocente());
                        if(!cursoDocenteDao.existeRelacionDocenteCurso(cursoDocenteBean)){
                            cursoDocenteBeans.add(cursoDocenteBean);
                        }
                    }
                }
                i++;
            }
            mensaje = cursoDocenteDao.agregarDocentesAsignadosACursos(cursoDocenteBeans);
            if(mensaje.equals(Constante.PROCESO_CORRECTO)){
                mensaje = "Se agregaron " + cursoDocenteBeans.size() + " relaciones docente - curso del excel.";
            }
        } catch (Exception e) {
            mensaje = "Error AdministradorService.cargarDocenteCurso " + e.getMessage();
            e.printStackTrace();
        }
        return mensaje;
    }
    */
    private String generarIdSesion(String idGrado, String nivelPrograma,String codCurso, String codSesion){
        return idGrado + "-" + nivelPrograma.substring(0, 3) + "-" + codCurso + "-" + codSesion;
    }
    
    public DatosPersonalesUsuarioBean getUsuario(String dni){
        administradorDao = new AdministradorDao();
        return administradorDao.getUsuario(dni);
    }
            
    public String resetPassword(String txtDni, String txtUsuario){
        administradorDao = new AdministradorDao();
        return administradorDao.resetPassword(txtDni,txtUsuario);
    }
}

