/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.dao.SesionDao;

/**
 *
 * @author CarlosZavala
 */
public class SesionServiceImpl implements SesionService{   
    SesionDao sesionDao;
    
    @Override
    public List<CursoBean> getCursosByDocenteLogeado(int idDocente) { 
        sesionDao=new SesionDao();
        List<CursoBean> listaCursos = new ArrayList<CursoBean>();
        try {   
            listaCursos = sesionDao.getCursosByDocente(idDocente);
        } catch (Exception ex) {
            Logger.getLogger(SesionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaCursos;
    }

    @Override
    public List<GradoBean> getGradosByCurso(int idCurso) {
       sesionDao=new SesionDao();
        List<GradoBean> listaGrados = new ArrayList<GradoBean>();
        try {   
            listaGrados = sesionDao.getgGradosByCurso(idCurso);
        } catch (Exception ex) {
            Logger.getLogger(SesionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaGrados; 
    }

    @Override
    public List<SeccionBean> getSeccionesByGrado(String idGrado) {
    sesionDao=new SesionDao();
        List<SeccionBean> listaSecciones = new ArrayList<SeccionBean>();
        try {   
            listaSecciones = sesionDao.getSeccionesByGrado(idGrado);
        } catch (Exception ex) {
            Logger.getLogger(SesionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaSecciones;  
    }

    @Override
    public List<SesionBean> getSesionesByCurso(int idCurso) {
     sesionDao=new SesionDao();
        List<SesionBean> listaSesiones = new ArrayList<SesionBean>();
        try {   
            listaSesiones = sesionDao.getSesionByCurso(idCurso);
        } catch (Exception ex) {
            Logger.getLogger(SesionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaSesiones;  
    }

    @Override
    public void cerrarClase(String idClase) {
        sesionDao=new SesionDao();
        try {
            sesionDao.cerrarClase(idClase);
        } catch (Exception ex) {
            Logger.getLogger(SesionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
