/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.ClaseBean;
import pe.minedu.bean.DocenteBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.dao.ClaseDao;
import pe.minedu.dao.ClaseEstudianteDao;
import pe.minedu.dao.DocenteDao;
import pe.minedu.dao.EstudianteDao;
import pe.minedu.dao.UsuarioClaseDao;
import pe.minedu.dao.UsuarioDao;
import pe.minedu.util.Constante;

/**
 *
 * @author GustavoFelix
 */
public class UsuarioService {
    private ClaseDao claseDao;
    private ClaseEstudianteDao claseEstudianteDao;
    private UsuarioDao usuarioDao;
    private UsuarioClaseDao usuarioClaseDao;
    private DocenteDao docenteDao;
    private EstudianteDao estudianteDao;
    
    public UsuarioBean login(String usuario, String password){
        usuarioDao = new UsuarioDao();
        return usuarioDao.logginUsuario(usuario, password);
    }
    
    public UsuarioClaseBean iniciarEstudianteClaseEnIP(UsuarioBean usuario, String ip){
        UsuarioClaseBean usuarioClaseBean=null;
        try {
            //obtiene datos del estudiante, como seccion
            estudianteDao=new EstudianteDao();
            EstudianteBean estudiante=estudianteDao.getEstudiante(usuario.getIdEstudiante());
            
            if(estudiante!=null){
                //busca ultima clase activa por su seccion & id
                claseDao=new ClaseDao();
                ClaseBean clase=claseDao.buscaClaseActivoPorSeccionYEstudiante(estudiante.getIdSeccion(), estudiante.getIdEstudiante());
                
                if(clase!=null){
                    //actualiza asistencia a la clase seleccionada si estaa en la lista
                    claseEstudianteDao=new ClaseEstudianteDao();
                                     
                    boolean enListaDeAlumnos = claseEstudianteDao.activaAsistenciaDelEstudiante(clase.getIdClase(), estudiante.getIdEstudiante());
                    
                    if(enListaDeAlumnos){
                        usuarioClaseBean=new UsuarioClaseBean();
                        usuarioClaseBean.setIdUsuario(usuario.getIdUsuario());
                        usuarioClaseBean.setIdClase(clase.getIdClase());
                        usuarioClaseBean.setIp(ip);
                        usuarioClaseBean.setFecha(new Date());
                        usuarioClaseBean.setClase(clase);
                        usuarioClaseBean.setIdSesion(clase.getIdSesion());

                        usuarioClaseDao = new UsuarioClaseDao();
                        usuarioClaseDao.creaUsuarioEnClase(usuarioClaseBean);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "iniciarEstudianteClaseEnIP", e);
        }
        
        return usuarioClaseBean;
    }
    
    public void desactivaAsistenciaDelEstudiante(UsuarioBean usuario){
        
        try {
            //obtiene datos del estudiante, como seccion
            estudianteDao=new EstudianteDao();
            EstudianteBean estudiante=estudianteDao.getEstudiante(usuario.getIdEstudiante());
            
            if(estudiante!=null){
                //busca ultima clase activa por su seccion & id
                claseDao=new ClaseDao();
                ClaseBean clase=claseDao.buscaClaseActivoPorSeccionYEstudiante(estudiante.getIdSeccion(), estudiante.getIdEstudiante());
                
                if(clase!=null){
                    //actualiza asistencia a la clase seleccionada si estaa en la lista
                    claseEstudianteDao=new ClaseEstudianteDao();
                    claseEstudianteDao.desactivaAsistenciaDelEstudiante(clase.getIdClase(), estudiante.getIdEstudiante());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "iniciarEstudianteClaseEnIP", e);
        }
        
    }
    
    public UsuarioClaseBean iniciarDocenteClaseEnIP(UsuarioBean usuario, String ip){
        UsuarioClaseBean usuarioClaseBean=null;
        try {
            //obtiene datos del docente
            docenteDao=new DocenteDao();
            DocenteBean docente=docenteDao.getDocente(usuario.getIdDocente());
            if(docente!=null){
                //busca clase activo por su id
                claseDao=new ClaseDao();
                ClaseBean clase=claseDao.buscaClaseActivoPorDocente(docente.getIdDocente());
                //System.out.println("Profe: "+ docente.getNombre()+ ", Clase: "+clase.getHoraInicio());
                if(clase!=null){
                    usuarioClaseBean=new UsuarioClaseBean();
                    usuarioClaseBean.setIdUsuario(usuario.getIdUsuario());
                    usuarioClaseBean.setIdClase(clase.getIdClase());
                    usuarioClaseBean.setIdSesion(clase.getIdSesion());
                    usuarioClaseBean.setIp(ip);
                    usuarioClaseBean.setFecha(new Date());
                    usuarioClaseBean.setClase(clase);
                    
                    usuarioClaseDao = new UsuarioClaseDao();
                    usuarioClaseDao.creaUsuarioEnClase(usuarioClaseBean);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "iniciarDocenteClaseEnIP", e);
        }
        
        return usuarioClaseBean;
    }
    
    public UsuarioClaseBean iniciarClaseSeleccionadaXEstudiante(UsuarioBean usuario, String ip, int idClase){
        System.out.println("usuario " + usuario + " ip " + ip + " idClase " + idClase);
        UsuarioClaseBean usuarioClaseBean=null;
        try {
            //obtiene datos del estudiante, como seccion
            estudianteDao=new EstudianteDao();
            EstudianteBean estudiante=estudianteDao.getEstudiante(usuario.getIdEstudiante());
            System.out.println("estudiante " + estudiante);
            if(estudiante!=null){
                //busca ultima clase activa por su seccion & id
                claseDao=new ClaseDao();
                
                    //actualiza asistencia a la clase seleccionada si estaa en la lista
                    claseEstudianteDao=new ClaseEstudianteDao();
                                     
                    boolean enListaDeAlumnos = claseEstudianteDao.activaAsistenciaDelEstudiante(idClase, estudiante.getIdEstudiante());
                    System.out.println("enListaDeAlumnos " + enListaDeAlumnos);
                    if(enListaDeAlumnos){
                        usuarioClaseBean=new UsuarioClaseBean();
                        usuarioClaseBean.setIdUsuario(usuario.getIdUsuario());
                        usuarioClaseBean.setIdClase(idClase);
                        usuarioClaseBean.setIp(ip);
                        usuarioClaseBean.setFecha(new Date());

                        usuarioClaseDao = new UsuarioClaseDao();
                        usuarioClaseDao.creaUsuarioEnClase(usuarioClaseBean);
                    }
                
            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "iniciarEstudianteClaseEnIP", e);
        }
        
        return usuarioClaseBean;
    }
    
//    public List<UsuarioActivo> usuarioActivos(){
//        usuarioActivoDao = new UsuarioActivoDao();
//        return usuarioActivoDao.findUsuarioActivoEntities();
//    }
//    
//    public List<UsuarioActivo> alumnosActivos(String ipDocente){
//        usuarioActivoDao = new UsuarioActivoDao();
//        return usuarioActivoDao.obtenerIpAlumnos(ipDocente);
//    }
}
