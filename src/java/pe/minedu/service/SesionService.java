/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.dao.UsuarioDao;

/**
 *
 * @author CarlosZavala
 */
public interface SesionService {
   public List<CursoBean> getCursosByDocenteLogeado(int idDocente);
   public List<GradoBean> getGradosByCurso(int idCurso);
   public List<SeccionBean> getSeccionesByGrado(String idGrado);
   public List<SesionBean> getSesionesByCurso(int idCurso);
   public void cerrarClase(String idClase);
}
