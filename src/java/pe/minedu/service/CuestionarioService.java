 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.FichaBean;
import pe.minedu.bean.FichaEstudianteBean;
import pe.minedu.bean.PreguntaBean;
import pe.minedu.bean.RespuestaEstudianteBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.UsuarioClaseBean;

/**
 *
 * @author CarlosZavala
 */
public interface CuestionarioService {
   public List<FichaBean> getFichasByClase(int idClase, int idEstudiante);
   public List<PreguntaBean> getPreguntasByFicha(int idFicha);
   public void saveCuestionario(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas);
   public List<FichaBean> getFichaClaseByDocente(int idClase, String idSesion);
   public List<EstudianteBean> getParticipantesByFicha(int idFicha, int idClase);
   public void activarDesactivarFicha(String estado, int idFicha, int idClase);
   public String getEstadoFichaEnClase(int idFicha, int idClase); 
   public String registrarFichaToFichaClase(int idFicha, int idClase);
   public String saveFichaTemporal(FichaBean beanFicha, UsuarioClaseBean usuarioClase);
   public List<RespuestaEstudianteBean> getAlternativasByAlumno(String idFicha, String idEstudiante, String id_clase);
   public String getNombreAlumno(String idEstudiante);
   public SeccionBean getSeccionByIdClase(String idClase);
   public String getNombreficha(String idFicha);
   public void updateCuestionario(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas);
   public List<FichaBean> getFichasActivasByIdSesion(String id_sesion);
   public List<EstudianteBean> getEstudiantesActivosByIdSeccion(int id_seccion);
   public String getNotaByEstudianteAndFicha(int id_estudiante, int id_ficha);
  
}
