
package pe.minedu.service;

import pe.minedu.bean.CambioPassBean;
import pe.minedu.bean.UsuarioBean;
import pe.minedu.dao.UsuarioDao;
import pe.minedu.util.Constante;

public class CambiarPasswordService {
    
    UsuarioDao usuarioDao;
    
    public String cambiarPassword(CambioPassBean cambioPassBean){
        String mensaje = "";
        UsuarioBean usuarioBean;
        try {
            usuarioDao = new UsuarioDao();
            usuarioBean = usuarioDao.obtUsuarioById(cambioPassBean.getIdUsuario());
            if(usuarioBean != null){
                if(!cambioPassBean.getPassword().equals(usuarioBean.getPassword())){
                    mensaje = "La contraseña actual no es la correcta.";
                }else{
                    if(cambioPassBean.getPassword().equals(cambioPassBean.getNuevoPassword())){
                        mensaje = "La nueva contraseña no puede ser igual a la contraseña actual.";
                    }else{
                        mensaje = usuarioDao.actualizarPassword(cambioPassBean.getNuevoPassword(), cambioPassBean.getUsuario(), cambioPassBean.getIdUsuario());
//                        if(mensaje.equals(Constante.PROCESO_CORRECTO)){
//                            mensaje = "La contraseña se ha cambiado satisfactoriamente.";
//                        }
                    }
                }
            }else{
                mensaje = "El usuario no existe";
            }
        } catch (Exception e) {
            mensaje = "Ocurrio un error " + e.getMessage();
        }
        return mensaje;
    }
}
