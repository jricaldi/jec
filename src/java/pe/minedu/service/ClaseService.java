
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.ClaseActivaBean;
import pe.minedu.bean.ClaseCreadaBean;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.dao.ClaseDao;
import pe.minedu.dao.UsuarioDao;

public class ClaseService {
    
    private ClaseDao claseDao;
    private UsuarioDao usuarioDao;
    
    public DatosPrincipalesClaseBean obtDatosPrincipalesClase(int idClase){
        claseDao = new ClaseDao();
        return claseDao.obtDatosPrincipalesClase(idClase);
    }
    
    public DatosPersonalesUsuarioBean obtDatosPersonalesUsuario(Integer idUsuario, String perfil){
        usuarioDao = new UsuarioDao();
        return usuarioDao.obtDatosPersonalesUsuario(idUsuario, perfil);
    }
    
    public ClaseCreadaBean crearClase(int idSeccion, int idDocente, String idSesion){
        claseDao = new ClaseDao();
        return claseDao.crearClase(idSeccion, idDocente, idSesion);
    }
    
    public boolean cerrarClase(int idClase, int idDocente){
        claseDao = new ClaseDao();
        return claseDao.cerrarClase(idClase, idDocente);
    }
    
    public List<ClaseActivaBean> obtClasesActivaXEstudiante(int idEstudiante) {
        claseDao = new ClaseDao();
        return claseDao.obtClasesActivaXEstudiante(idEstudiante);
    }
}
