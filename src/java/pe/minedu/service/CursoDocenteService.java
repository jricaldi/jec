/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.CursoDeDocenteBean;
import pe.minedu.dao.CursoDocenteDao;

/**
 *
 * @author Edwin
 */
public class CursoDocenteService {
    private CursoDocenteDao cursoDocenteDao;
    
    public List<CursoDeDocenteBean> listCursosDeDocentexIdDocente(int idDocente){
        cursoDocenteDao = new CursoDocenteDao();
        return cursoDocenteDao.listCursosDeDocentexIdDocente(idDocente);
    }
    
    public List<CursoDeDocenteBean> listaGradosByDocente(int idDocente){
        cursoDocenteDao = new CursoDocenteDao();
        return cursoDocenteDao.listaGradosByDocente(idDocente);
    }
    
    public List<CursoDeDocenteBean> listaCursosByDocenteYGrado(int idDocente, String idGrado){
        cursoDocenteDao = new CursoDocenteDao();
        return cursoDocenteDao.listaCursosByDocenteYGrado(idDocente,idGrado);
    }
}
