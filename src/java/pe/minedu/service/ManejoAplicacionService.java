
package pe.minedu.service;

import java.util.Calendar;
import java.util.List;
import pe.minedu.bean.AplicacionBean;
import pe.minedu.dao.AplicacionDao;

public class ManejoAplicacionService {
    
    private AplicacionDao aplicacionDao;
    
    public List<AplicacionBean> listarAplicaciones(){
        aplicacionDao = new AplicacionDao();
        return aplicacionDao.listarAplicaciones();
    }
    
    public String generarDocumento(String nombreAplicacion, String extension){
        String nuevoDocNombre = Calendar.getInstance().getTimeInMillis() + "_" + nombreAplicacion + "." + extension;
        return nuevoDocNombre;
    }
    
    public AplicacionBean obtAplicacionxId(int id){
        aplicacionDao = new AplicacionDao();
        return aplicacionDao.obtAplicacionxId(id);
    }
    
    public AplicacionBean registrarNuevaAplicacion(AplicacionBean aplicacionBean){
        aplicacionDao = new AplicacionDao();
        return aplicacionDao.insertarAplicacionBean(aplicacionBean);
    }
}
