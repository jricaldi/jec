/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.minedu.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.minedu.bean.CursoBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.FichaBean;
import pe.minedu.bean.FichaEstudianteBean;
import pe.minedu.bean.GradoBean;
import pe.minedu.bean.PreguntaBean;
import pe.minedu.bean.RespuestaEstudianteBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.bean.SesionBean;
import pe.minedu.bean.UsuarioClaseBean;
import pe.minedu.dao.CuestionarioDao;
import pe.minedu.dao.SesionDao;
import pe.minedu.dao.UsuarioDao;

/**
 *
 * @author CarlosZavala
 */
public class CuestionarioServiceImpl implements CuestionarioService{   
    CuestionarioDao cuestionarioDao;
  
    @Override
    public List<FichaBean> getFichasByClase(int idClase, int idEstudiante) {
        cuestionarioDao=new CuestionarioDao();
        List<FichaBean> listaFichas = new ArrayList<FichaBean>();
        try {   
            listaFichas = cuestionarioDao.getFichasByClase(idClase,idEstudiante);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaFichas;
    }

    @Override
    public List<PreguntaBean> getPreguntasByFicha(int idFicha) {
       cuestionarioDao=new CuestionarioDao();
        List<PreguntaBean> listaPreguntas = new ArrayList<PreguntaBean>();
        try {   
            listaPreguntas = cuestionarioDao.getPreguntasByFicha(idFicha);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaPreguntas;
    }

    @Override
    public void saveCuestionario(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas) {
        try {
            cuestionarioDao=new CuestionarioDao();
            cuestionarioDao.registrarAlumno(fichaEstudianteBean, listaRespuestas); //To change body of generated methods, choose Tools | Templates.
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<FichaBean> getFichaClaseByDocente(int idClase, String idSesion) {        
        cuestionarioDao=new CuestionarioDao();
        List<FichaBean> listaFichas = new ArrayList<FichaBean>();
        try {   
            listaFichas = cuestionarioDao.getFichasByClaseByDocente(idClase,idSesion);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaFichas;
    }

    @Override
    public List<EstudianteBean> getParticipantesByFicha(int idFicha, int idClase) {
        cuestionarioDao=new CuestionarioDao();
        List<EstudianteBean> listaEstudiantes = new ArrayList<EstudianteBean>();
        try {   
            listaEstudiantes = cuestionarioDao.getParticipantesByFicha(idFicha,idClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaEstudiantes;
    }

    @Override
    public void activarDesactivarFicha(String estado, int idFicha, int idClase) {
        cuestionarioDao=new CuestionarioDao();
        try {
            cuestionarioDao.activarDesactivarFicha(estado, idFicha, idClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getEstadoFichaEnClase(int idFicha, int idClase) {
        cuestionarioDao=new CuestionarioDao();
        String salida="";
        try {
            salida = cuestionarioDao.getEstadoFichaEnClase(idFicha, idClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salida;
    }

    @Override
    public String registrarFichaToFichaClase(int idFicha, int idClase) {
        cuestionarioDao=new CuestionarioDao();
         String salida = "";
        try {
            salida = cuestionarioDao.registrarFichaToFichaClase(idFicha, idClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salida;
    }

    @Override
    public String saveFichaTemporal(FichaBean beanFicha, UsuarioClaseBean usuarioClase) {
        cuestionarioDao=new CuestionarioDao();
        String salida = "";
        try {
            salida = cuestionarioDao.registrarNuevaFichaClase(beanFicha, usuarioClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salida;
    }

    @Override
    public List<RespuestaEstudianteBean> getAlternativasByAlumno(String idFicha, String idEstudiante, String id_clase) {
        cuestionarioDao=new CuestionarioDao();
        List<RespuestaEstudianteBean> lista = new ArrayList<RespuestaEstudianteBean>();
        try {
            lista = cuestionarioDao.getRespuestasEstudiante(idFicha, idEstudiante, id_clase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista; 
    }
    
     @Override
    public String getNombreAlumno(String idEstudiante) {
        cuestionarioDao=new CuestionarioDao();
        String nombreCompleto ="";
        try {
            nombreCompleto = cuestionarioDao.getNombresAlumnoByIdEstudiante(idEstudiante);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombreCompleto; 
    }

    @Override
    public SeccionBean getSeccionByIdClase(String idClase) {
        cuestionarioDao=new CuestionarioDao();
        SeccionBean seccion = new SeccionBean();
        try {
            seccion = cuestionarioDao.getSeccionByIdClase(idClase);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return seccion; 
    }

    @Override
    public String getNombreficha(String idFicha) {
        cuestionarioDao=new CuestionarioDao();
        String nombreFicha ="";
        try {
            nombreFicha = cuestionarioDao.getNombreFichaByIdFicha(idFicha);
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombreFicha; 
    }

    @Override
    public void updateCuestionario(FichaEstudianteBean fichaEstudianteBean, List<RespuestaEstudianteBean> listaRespuestas) {
      try {
            cuestionarioDao=new CuestionarioDao();
            cuestionarioDao.updateCuestionario(fichaEstudianteBean, listaRespuestas); //To change body of generated methods, choose Tools | Templates.
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<FichaBean> getFichasActivasByIdSesion(String id_sesion)  {
         List<FichaBean> lista = new ArrayList<FichaBean>();
      try {
            cuestionarioDao=new CuestionarioDao();           
            lista = cuestionarioDao.getFichasActivasByIdSesion(id_sesion); //To change body of generated methods, choose Tools | Templates.
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
      return lista;
    }
    
     @Override
     public List<EstudianteBean> getEstudiantesActivosByIdSeccion(int id_seccion){
         List<EstudianteBean> lista = new ArrayList<EstudianteBean>();
      try {
            cuestionarioDao=new CuestionarioDao();           
            lista = cuestionarioDao.getEstudiantesActivosByIdSeccion(id_seccion); //To change body of generated methods, choose Tools | Templates.
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
      return lista;
    }
     
      @Override
      public String getNotaByEstudianteAndFicha(int id_estudiante, int id_ficha){
         String notaFinal = "";
      try {
            cuestionarioDao=new CuestionarioDao();           
            notaFinal = cuestionarioDao.getNotaByEstudianteAndFicha(id_estudiante, id_ficha); //To change body of generated methods, choose Tools | Templates.
        } catch (Exception ex) {
            Logger.getLogger(CuestionarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
      return notaFinal;
      }
}
