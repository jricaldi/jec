
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.ClaseCreadaBean;
import pe.minedu.bean.DatosPersonalesUsuarioBean;
import pe.minedu.bean.DatosPrincipalesClaseBean;
import pe.minedu.bean.SeccionBean;
import pe.minedu.dao.ClaseDao;
import pe.minedu.dao.SeccionDao;
import pe.minedu.dao.UsuarioDao;

public class SeccionService {
    
    private SeccionDao seccionDao;
    
    public List<SeccionBean> listSeccionesXGrado(String codigoGrado){
        seccionDao = new SeccionDao();
        return seccionDao.listSeccionesXGrado(codigoGrado);
    }
}
