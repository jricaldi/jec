
package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.ControlEstudianteBean;
import pe.minedu.bean.EstudianteBean;
import pe.minedu.bean.EstudianteExcelBean;
import pe.minedu.dao.EstudianteDao;

public class ControlAlumnoService {

    private EstudianteDao estudianteDao;
    
    public List<ControlEstudianteBean> listarAlumnosXSeccion(int idClase) {
        estudianteDao = new EstudianteDao();
        return estudianteDao.listarAlumnosXSeccion(idClase);
    }
    
    public List<ControlEstudianteBean> listarAlumnosXClase(int idClase) {
        estudianteDao = new EstudianteDao();
        return estudianteDao.listarAlumnosXClase(idClase);
    }

    public void registrarAlumnoMasivo(List<EstudianteExcelBean> estudianteExcelBeans, int idClase, String perfilEstudiante) {
        estudianteDao = new EstudianteDao();
        estudianteDao.registrarAlumnoMasivo(estudianteExcelBeans, idClase, perfilEstudiante);
    }

    public void registrarAlumno(EstudianteExcelBean estudianteExcelBean, int idClase, String perfilEstudiante) {
        estudianteDao = new EstudianteDao();
        estudianteDao.registrarAlumno(estudianteExcelBean, idClase, perfilEstudiante);
    }
    
}
