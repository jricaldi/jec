package pe.minedu.service;

import java.util.List;
import pe.minedu.bean.MonitoreoClaseEstudianteBean;
import pe.minedu.dao.MonitoreoDao;

public class MonitoreoService {
    private MonitoreoDao monitoreoDao;
    
    public String bloquearEquipo(int idClase,int idEstudiante){
        monitoreoDao = new MonitoreoDao();
        String mensaje = monitoreoDao.bloquearEquipo(idClase, idEstudiante);
        return mensaje;
    }
    
    public String desbloquearEquipo(int idClase,int idEstudiante){
        monitoreoDao = new MonitoreoDao();
        String mensaje = monitoreoDao.desbloquearEquipo(idClase, idEstudiante);
        return mensaje;
    }
    
    public String desbloquearEquipoTodos(int idClase, List<MonitoreoClaseEstudianteBean> estudiantes){
        monitoreoDao = new MonitoreoDao();
        String mensaje = monitoreoDao.desbloquearEquipoTodos(idClase, estudiantes);
        return mensaje;
    }
    
    public String bloquearEquipoTodos(int idClase, List<MonitoreoClaseEstudianteBean> estudiantes){
        monitoreoDao = new MonitoreoDao();
        String mensaje = monitoreoDao.bloquearEquipoTodos(idClase, estudiantes);
        return mensaje;
    }
    
    public MonitoreoClaseEstudianteBean getEstadoBloqueo(int idClase, int idEstudiante){
        monitoreoDao = new MonitoreoDao();
        MonitoreoClaseEstudianteBean monitoreo = monitoreoDao.getEstadoBloqueo(idClase, idEstudiante);
        return monitoreo;
    }
}
