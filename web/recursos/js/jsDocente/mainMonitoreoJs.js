$( document ).ready(function () {
    
    var idClase = $("#idClase").val();
    if(store("bloq-idClase") == idClase){
        $("#des-todo-btn").removeClass("hide");
    }
    else{
        $("#blo-todo-btn").removeClass("hide");
    }
    
    $("a#btnPantallas").simulate("click");
    setInterval(function(){
        $("a#btnPantallas").simulate("click");
    },6000);
    
    $(".btnEnviarMensaje").on("click",function(){
        window.parent.$("#mensaje").val("");
        var codAlumno = $("#hfIpAlumno").val();
        if(codAlumno === ""){
            window.parent.$("#modalEnviarMensaje #titulo").html("Enviar Mensaje a Todos");
        }
        else{
            window.parent.$("#modalEnviarMensaje #titulo").html("Enviar Mensaje Directo");
        }
        window.parent.$("#modalEnviarMensaje").modal();
    });
       
    window.parent.$("#lnkEnviarMensaje").on("click",function(){
        enviarMensajeJS();
    });

    function enviarMensajeJS() {
        var codAlumno = $("#hfIpAlumno").val();
        var sms = window.parent.$("#mensaje").val();
        if (codAlumno == "") {
            $.ajax({
                type: 'GET',
                url: "EnviarMensajeTodos?mensaje=" + sms,
                async: true
            }).done(function (resultado) {
            });
        } else {
            $.ajax({
                type: 'GET',
                url: "EnviarMensajeUno?hfIpAlumno=" + codAlumno + "&mensaje=" + sms,
                async: true
            }).done(function (resultado) {
            });
        }
    };

    enviarMensajeTodos = function () {
        $("#hfIpAlumno").val("");
    };

    enviarMensajeUno = function (codigo) {
        $("#hfIpAlumno").val(codigo);      
    };

    tomarPantallazo = function (ip) {
        window.open("TomarPantallazoUno?hfIpAlumno=" + ip);
    };

    abrirBrowserTodos = function () {
        $("#hfIpAlumno").val("");
        $.ajax({
            type: 'GET',
            url: "AbrirBrowserTodos",
            async: true
        }).done(function (resultado) {
        });
    };

    abrirBrowserUno = function (ip) {
        $("#hfIpAlumno").val(ip);
        $.ajax({
            type: 'GET',
            url: "AbrirBrowserUno?hfIpAlumno=" + ip,
            async: true
        }).done(function (resultado) {
        });
    };
	
    ejecutarEfecto=function(dato){
        var src = $(dato).attr('src');
        console.log("src: " + src);
        $("#imagenGrande").attr("src", src);
        $('#imagenGrande').modal();
    };
    
    bloquearEquipoTodos = function () {
        $("#blo-todo-btn").addClass("hide");
        $("#des-todo-btn").removeClass("hide");
        store("bloq-idClase", idClase);
        $.get("BloquearTodosOverlay");
    };

    desbloquearEquipoTodos = function () {
        $("#blo-todo-btn").removeClass("hide");
        $("#des-todo-btn").addClass("hide");
        $.get("DesbloquearTodosOverlay");
    };

    bloquearEquipoUno = function (codigo) {
        $("#bloq-"+codigo).addClass("hide");
        $("#des-"+codigo).removeClass("hide");
        $.get("BloquearUnoOverlay?idEstudiante=" + codigo);
    };

    desbloquearEquipoUno = function (codigo) {
        $("#bloq-"+codigo).removeClass("hide");
        $("#des-"+codigo).addClass("hide");
        $.get("DesbloquearUnoOverlay?idEstudiante=" + codigo);
    };


});

