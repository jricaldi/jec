$(document).ready(function(){
     
    var inicio= 0;
    
    setInterval(function(){ 
                
                $.ajax({
                      url: "GetEstadoClase",
                      method: "POST"
            
                }).done(function(data){
                  if(data == "1"){
                    if(inicio==0){
                        state = 1;
                        inicio= new Date();
                        var result1=LeadingZero(inicio.getUTCHours())+":"+LeadingZero(inicio.getUTCMinutes())+":"+LeadingZero(inicio.getUTCSeconds());
                        $("#crono1").html(result1);
                    }
                    var actual = new Date().getTime();   
                    var diff=new Date(actual-inicio);
                    var result=LeadingZero(diff.getUTCHours())+":"+LeadingZero(diff.getUTCMinutes())+":"+LeadingZero(diff.getUTCSeconds());
                    $("#crono").html(result);
                  }else if(data == "0"){
                        state = 0;
                        inicio=0;
                        $("#crono").html("");
                        $("#crono1").html("");
                  }
                })
        
   },1000*60);   
   
        function LeadingZero(Time) {       
		return (Time < 10) ? "0" + Time : + Time;
	}
    
    
    $("#selectMateria").change(function(){        
        var formulario = $("#form_sesion").serialize();              
        $.ajax({
           url: "GetGrados",
           method: "POST",
           data: formulario
         })
            .done(function(data) {
             $("#selectGrado").html(data);
         });
         $.ajax({
           url: "GetSesion",
           method: "POST",
           data: formulario
         })
         .done(function(data) {
             $("#selectSesion").html(data);
         });
    });
     $("#selectGrado").change(function(){
              var formulario = $("#form_sesion").serialize();
                 var request = $.ajax({
                    url: "GetSeccion",
                    method: "POST",
                    data: formulario
                  });
                  request.done(function(data) {
                      $("#selectSeccion").html(data);
                  });
    });
    
       var state = 0;
     $("#btnCrearSesion").click(function(){
     //ir a servelet y ver si existe clase     
     
     if(state == 0){
           var formulario = $("#form_sesion").serialize();
          $.ajax({
              url: "CrearClase",
              method: "POST",
              data: formulario
              }).done(function(data){
                  if(data=="error"){
                       new PNotify({                 
                        title: 'No se pudo crear la clase!',
                        text: 'Seleccione todas las opciones!',
                        type: 'info'
                    });
                  }else{
                      
                    $("#btnCrearSesion").val("Terminar Clase");
                    $('#selectMateria').prop('disabled', 'disabled');
                    $('#selectGrado').prop('disabled', 'disabled');
                    $('#selectSeccion').prop('disabled', 'disabled');
                    $('#selectSesion').prop('disabled', 'disabled');
                    state = 1;
                  }
                  new PNotify({                 
                        title: 'Operacion realizada con exito!',
                        text: 'Clase Creada',
                        type: 'info'
                    });
              })
              
     }else if(state == 1){
          $.ajax({
              url: "CerrarClase",
              method: "POST",
              data: formulario
              }).done(function(data){
                  if(data == "1"){
                      mostrarAlerta("Hubo un error al crear la clase!","error")
                  }else if(data = "0"){
                      $("#btnCrearSesion").val("Crear Clase");
                
                $('#selectMateria').val("");
                $('#selectGrado').val("");
                $('#selectSeccion').val("");
                $('#selectSesion').val("");
                
                $('#selectMateria').prop('disabled', false);
                $('#selectGrado').prop('disabled', false);
                $('#selectSeccion').prop('disabled', false);
                $('#selectSesion').prop('disabled', false);
                state = 0;
                mostrarAlerta("Clase creado con exito!","info")
                  }
                
       })
   }        
                           
    });
    
      verifyHasClase();
});

function verifyHasClase(){
    
    $.ajax({    
            url: "GetEstadoClase",
            method: "POST"
        }).done(function(data){
          if(data == "1"){
            $("#selectMateria").val("1");
            $("#btnCrearSesion").val("Terminar Clase");
            $('#selectMateria').prop('disabled', 'disabled');
            $('#selectGrado').prop('disabled', 'disabled');
            $('#selectSeccion').prop('disabled', 'disabled');
            $('#selectSesion').prop('disabled', 'disabled');
            state = 1;
          }
        })
}