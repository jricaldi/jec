abrirAplicacion=function(idAplicacion){
    $.ajax({
        type: 'POST',
        url: "AbrirAplicacion?idAplicacion=" + idAplicacion,
        async: true
    }).done(function (resultado) {
        //alert("Termino");
    });
};

abrirRegistroApp=function(){
  $("#divNuevoRegistro").show();
};

//abrirAplicacion=function(idAplicacion){
//    $.ajax({
//        type: 'POST',
//        url: "AbrirAplicacion?idAplicacion=" + idAplicacion,
//        async: true
//    }).done(function (resultado) {
//        $('#frmRegistro').trigger("reset");
//    });
//};

validarFormulario=function() {
    var na = $("#nombreAplicacion").val();
    var e = $("#extensiones").val();
    if (na == null || na == "") {
        alert("Campo Nombre de aplicacion debe de llenarse.");
        return false;
    }
    if (e == null || e == "") {
        alert("Campo Extensiones debe de llenarse.");
        return false;
    }
};

cancelarRegistro=function(){
    $("#nombreAplicacion").val("");
    $("#extensiones").val("");
    $("#divNuevoRegistro").hide();
};

$( "#frmRegistro" ).submit(function( event ) {
  cancelarRegistro();
  event.preventDefault();
});

$(document).ready(function(){  
     if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                     $('.selectpicker3').selectpicker('mobile');
                     $('.selectpicker4').selectpicker('mobile');
                    }
                    else{
                     $('.selectpicker3').selectpicker();
                     $('.selectpicker4').selectpicker();
                    }
});