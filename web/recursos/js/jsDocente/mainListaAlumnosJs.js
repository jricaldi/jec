$(document).ready(function() {

    $("a#btnListarConectados").simulate("click");

    setInterval(function(){
        $("a#btnListarConectados").simulate("click");
    },6000);
       
    $('input[type="file"]').ajaxfileupload({
    action : 'docente_estudiantes_FileUploadServlet',
    onComplete : function(filename, response) {
        $("#divOpciones").removeClass("hide")
        $("#divAgregar").addClass("hide");
        $("#divVarios").addClass("hide"); 
        $("#divLista").removeClass("hide");
        $("#msgTexto").addClass("ok");
        $("#msgTexto").html("!Se agrego correctamente la lista de estudiantes!");
        $("a#btnListarConectados").simulate("click");
  },
  onStart : function() {
        $("#msgTexto").html("");
        $("#txtFilename").val($("#file").val());
  }
  });
          
});
                
nuevoAlumno=function(){
    $("#divOpciones").addClass("hide")
    $("#divAgregar").removeClass("hide");
    $("#divVarios").addClass("hide");
    $("#divLista").addClass("hide");

  $("#nuevoAlumno").load("docente_estudiantes_NuevoAlumno");
};

variosAlumnos=function(){
    $("#divOpciones").addClass("hide")
    $("#divAgregar").addClass("hide");
    $("#divVarios").removeClass("hide"); 
    $("#divLista").addClass("hide");
};

agregarAlumno=function(){
    var formulario = $("#form").serialize();
    var request = $.ajax({
      url: "docente_estudiantes_AgregarNuevoAlumno",
      method: "POST",
      data: formulario
    });
  };

  cancelar=function(){
      $("#divOpciones").removeClass("hide")
      $("#divAgregar").addClass("hide");
      $("#divVarios").addClass("hide");;
      $("#divLista").removeClass("hide");
  };

