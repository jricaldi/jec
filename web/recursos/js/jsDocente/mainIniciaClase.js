$(document).ready(function () {
    
    "use strict";
    
    $("#selectCurso").prop('disabled', 'disabled');
    $("#selectSeccion").prop('disabled', 'disabled');
    $("#selectSesion").prop('disabled', 'disabled');
    $("#btnExecute").prop('disabled', 'disabled');
    
    
    $("#btnExecute").on("click",function(event){
        event.preventDefault();
        var idGradoCurso=$("#idGradoCurso").val();
        var selectSeccion=$("#selectSeccion").val();
        var selectSesion=$("#selectSesion").val();

        if(selectSesion!=='0' && selectSeccion!=='0' && idGradoCurso!=='0'){
            window.parent.$("#modalIniciarClase").modal();
        }else{
            return;
        }
    });
    
    $("#idGrado").change(function(){
        var formulario = $("#form_sesion").serialize();
         $.ajax({
           url: "ClaseSelectCursoServlet",
           method: "POST",
           data: formulario
         }).done(function(data) {
             $("#selectCurso").removeAttr("disabled");
             $("#selectCurso").html(data);
             $("#selectSeccion").html("<option value='0'>Seleccionar</option>");
             $("#selectSesion").html("<option value='0'>Seleccionar</option>");

         });        
    });
    
    $("#selectCurso").change(function(){
        validarCombos();
        var formulario = $("#form_sesion").serialize();
        
        if($(this).val() == 0){
            $("#selectSeccion").prop('disabled', 'disabled');
            $("#selectSesion").prop('disabled', 'disabled');
            return;
        }
        $.ajax({
           url: "ClaseSelectSeccionServlet",
           method: "POST",
           data: formulario
         })
            .done(function(data) {
             $("#selectSeccion").html(data);
             $("#selectSeccion").removeAttr("disabled");
             $("#selectSesion").html("<option value='0'>Seleccionar</option>");

         });
         
    });
    
    $("#selectSeccion").change(function(){
        validarCombos();
        var formulario = $("#form_sesion").serialize();
        if($(this).val() == 0){
            $("#selectSesion").prop('disabled', 'disabled');
            $("#selectSesion").html("<option value='0'>Seleccionar</option>");
            return;
        }
        $.ajax({
           url: "ClaseSelectSesionServlet",
           method: "POST",
           data: formulario
         })
        .done(function(data) {
            $("#selectSesion").html(data);
            $("#selectSesion").removeAttr("disabled");
            $("#selectSesion > option").each(function() {
                $(this).html(convertirTexto(this.text));
            });
         });
         
    });
    
    $("#selectSesion").change(function(){
        validarCombos();     
    });
    
    window.parent.$("#btnConfirmar").on("click",function(){
        $("#form_sesion").submit();
    });
    
    var validarCombos = function(){
        var idGrado=$("#idGrado").val();
        var idGradoCurso=$("#selectCurso").val();
        var selectSeccion=$("#selectSeccion").val();
        var selectSesion=$("#selectSesion").val();
        if(idGrado != '0' && selectSesion!='0' && selectSeccion!='0' && idGradoCurso!='0'){
            $("#btnExecute").removeClass("disabled");
            $("#btnExecute").removeAttr("disabled");
        }
        else{
            $("#btnExecute").prop('disabled', 'disabled');
            $("#btnExecute").addClass("disabled");
        }
    };
    
    var convertirTexto = function(data){
        if(data === "Seleccionar")
            return data;
        var unidad = parseInt(data.substring(1,4));
        var sesion = parseInt(data.substring(5));
        return "Unidad " + unidad + " - " + "Sesion " + sesion;
    };
    
});

agregarAlumno = function () {
    var formulario = $("#form").serialize();
    var request = $.ajax({
        url: "docente/AgregarNuevoAlumno",
        method: "POST",
        data: formulario
    });
    request.done(function (msg) {
        $("#nuevoAlumno").hide();
        location.reload();
    });
};

cancelarAgregarAlumno = function () {
    $("#nuevoAlumno").hide();
};