(function() {

    "use strict";
    $(document).on("ready",function(){     

        try {
            $("a#bienvenida").simulate("click");
        }
        catch (err){
            console.log("no se ha iniciado la clase");
        }
        
        $("#secMenu ul li a").on("click", function(){
            var id = $(this).parent().attr("id");
            var classActive = id + "Active";
            $("#secMenu ul li a").removeAttr("class");
            $(this).addClass("bMenu " + classActive);
        });
        
        
    });
    
})();