var doc;
var accion=0;

function init(){
     if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	    $('.selectpicker').selectpicker('mobile');
	}
	else{
            $('.selectpicker').selectpicker();		
	}
}

$(document).ready(function(){ 
    init();
    $.ajax({
        type: "POST",
        url: "../../GetFichas",
        async : false
        }).done(function(data){
            $("#contFichas").html(data);
        });
   //  doc = new jsPDF();

      $("#btnNuevaFicha").click(function(){
       $.ajax({
        type: "POST",
        url: "../../AddFicha",
        async : false
        }).done(function(data){
            $("#contFichas").html(data);
            $("#divSavePre").hide();
            $("#contPregunta").hide();
            $("#divSaveTitulo").hide();
            $("#divNombreFicha").hide();
            $("#divCancelar").hide();

                $("#btnAddPregunta").click(function(){
                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                        $('.selectpicker').selectpicker('mobile');
                    }
                    else{
                        $('.selectpicker').selectpicker();		
                    }
                   $("#inputPregunta").val("");
                   $("#selectAlter").val("");
                   $("#contAlternativas").hide();

                   $("#contPregunta").show();
                   $("#divAddPre").hide();
                   $("#divSavePre").show();
                   $("#divCancelar").show();
                   $("#divNuevoTitulo").hide();
                    
                   $("#contFichaTempo").hide();
                });
             $("#btnNuevoTitulo").click(function(){
                  $("#divNombreFicha").show();
                  $("#divSaveTitulo").show();
                  $("#divCancelar").show();
                  $("#divNuevoTitulo").hide();
                  $("#divAddPre").hide();
                  $("#contFichaTempo").hide();
             });
             $("#btnCancelar").click(function(){
                  $("#divAddPre").show();
                  $("#divNuevoTitulo").show();
                  $("#divCancelar").hide();
                  $("#divSaveTitulo").hide();
                  $("#divSavePre").hide();
                  $("#contPregunta").hide();
                  $("#divNombreFicha").hide();
                  $("#contFichaTempo").show();
             });
             $("#btnSaveTitulo").click(function(){
               var titulo= $("#inputNombreFicha").val();
               if(titulo==""){
                   alert("Ingrese un Nombre de Ficha Valido");
               }
             })
             $("#btnSavePregunta").click(function(){
                 var pregunta = $("#inputPregunta").val();
                 var alternativas = [];
                 $('input:text[name=nameAlternativas]').each(function(){
                    alternativas.push($(this).val());
                 });
                 var correcta = $("#selectRespCorrecta").val();
                 var numAlter = $("#selectAlter").val();

                     $.ajax({
                       type: "POST",
                       url: "../../AddPreguntaTemporal",
                       data : {"pregunta":pregunta,
                               "alternativas": alternativas,
                               "correcta":correcta,
                               "numAlter": numAlter},
                       async : false
                       }).done(function(data){
                           if(data == "0"){
                               $("#contPregunta").hide();
                               $("#divAddPre").show();
                               $("#divSavePre").hide();
                               $("#divNuevoTitulo").show();
                               $("#divCancelar").hide();
                               $("#contFichaTempo").show();
                               $.ajax({
                                    type: "POST",
                                    url: "../../GetFichaTempo",
                                    async : false
                                    }).done(function(data){
                                        $("#contFichaTempo").html(data);
                                        $("#btnSaveFichaTemporal").click(function(){
                                            $.ajax({
                                                type: "POST",
                                                url: "../../SaveFichaTemporal",
                                                async : false
                                                }).done(function(data){

                                                    if(data == "0"){
                                                         location.reload();
                                                    }else{
                                                        alert("OCURRIO UN PROBLEMA INTERNO!");
                                                    }

                                                });
                                        });
                                    });
                           }else{
                            alert(data);
                           }

                       });

             });

             $("#btnSaveTitulo").click(function(){
                var nombreFicha = $("#inputNombreFicha").val();
                   $.ajax({
                        type: "POST",
                        url: "../../SaveNombreFichaTemp",
                        data : {"nombreFicha" : nombreFicha},
                        async : false
                        }).done(function(){
                            $("#divAddPre").show();
                            $("#divNuevoTitulo").show();
                            $("#divCancelar").hide();
                            $("#divSaveTitulo").hide();
                            $("#divSavePre").hide();
                            $("#contPregunta").hide();
                            $("#divNombreFicha").hide();
                            $("#contFichaTempo").show();
                            $.ajax({
                                type: "POST",
                                url: "../../GetFichaTempo",
                                data : {"nombreFicha" : nombreFicha},
                                async : false
                                }).done(function(data){
                                    $("#contFichaTempo").html(data);
                                    $("#btnSaveFichaTemporal").click(function(){
                                        $.ajax({
                                            type: "POST",
                                            url: "../../SaveFichaTemporal",
                                            async : false
                                            }).done(function(data){

                                                if(data == "0"){
                                                     location.reload();
                                                }else{
                                                    alert("OCURRIO UN PROBLEMA INTERNO!");
                                                }

                                            });
                                    });

                                })
                            })

             });

            $("#selectAlter").change(function(){
                var val = $("#selectAlter").val();
                $.ajax({
                   url: "../../GetNumAlternativas",
                   method: "POST",
                   data: {"val":val}
                 })
                    .done(function(data) {
                     $("#contAlternativas").html(data);
                     $("#contAlternativas").show();
                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                     $('.selectpicker1').selectpicker('mobile');
                    }
                    else{
                     $('.selectpicker1').selectpicker();		
                    }
                 });
             });



        });
        //TRAE PREGUNTAS GUARDADAS TEMPORALMENTE
         $.ajax({
                type: "POST",
                url: "../../GetFichaTempo",
                async : false
                }).done(function(data){
                    $("#contFichaTempo").html(data);
                });

      });
});
var specialElementHandlers = {
                    '#editor': function (element, renderer) {
                        return true;
    }};

 function getFichaEvaluacion(componente){
    var value = $(componente).attr("id");
    $.ajax({
            url: "../../GetFichaFull",
            method: "POST",
            data : {"id_ficha":value}
            }).done(function(data){
                $("#contFichas").html(data);
                 saveEvaluacion();
            });

    }

    function downloadFicha(componente){
       var value = $(componente).attr("id");
       $.ajax({
            url: "../../DownloadFile",
            method: "POST",
            data : {"id_ficha":value}
            }).done(function(data){
                $("#contFichaDownload").html(data);
                 $.ajax({
                        url: "../../GetNameFicha",
                        method: "POST",
                        data : {"id_ficha":value}
                         }).done(function(data2){
                                doc = new jsPDF();
                            doc.myText(data2,{align: "center"},0,15);               
                            doc.fromHTML($('#contFichaDownload').html(), 15, 15, {
                                'width': 170,
                                'elementHandlers': specialElementHandlers
                            });
                            doc.save(data2+'.pdf');                       
                    });
            });
    }
    
    function calificarFicha(componente){      
       var value = $(componente).attr("id");
       $.ajax({
            url: "../../calificarFicha",
            method: "POST",
            data : {"id_estudiante":value}
            }).done(function(data){ 
                $("#contFichas").html(data);
            });
    }
    
       (function(API){
        API.myText = function(txt, options, x, y) {
        options = options ||{};
        /* Use the options align property to specify desired text alignment
         * Param x will be ignored if desired text alignment is 'center'.
         * Usage of options can easily extend the function to apply different text 
         * styles and sizes 
        */
        if( options.align == "center" ){
            // Get current font size
            var fontSize = this.internal.getFontSize();

            // Get page width
            var pageWidth = this.internal.pageSize.width;

            // Get the actual text's width
            /* You multiply the unit width of your string by your font size and divide
             * by the internal scale factor. The division is necessary
             * for the case where you use units other than 'pt' in the constructor
             * of jsPDF.
            */
            txtWidth = this.getStringUnitWidth(txt)*fontSize/this.internal.scaleFactor;

            // Calculate text's x coordinate
            x = ( pageWidth - txtWidth ) / 2;
        }

        // Draw text at x,y
        this.text(txt,x,y);
    }
})(jsPDF.API);

    function downloadFichaEstu(componente){
       var value = $(componente).attr("id");
       $.ajax({
            url: "../../DownloadFichaAlmuno",
            method: "POST",
            data : {"id_estudiante":value}
            }).done(function(data){ 
                $("#contFichaDownload").html(data);
                $.ajax({
                        url: "../../GetNameFicha",
                        method: "POST",
                         }).done(function(data2){
                                doc = new jsPDF();
                            doc.myText(data2,{align: "center"},0,15);               
                            doc.fromHTML($('#contFichaDownload').html(), 15, 15, {
                                'width': 170,
                                'elementHandlers': specialElementHandlers
                            });
                            doc.save(data2+'.pdf');                       
                    });
               
                
               
            });
    }
    

    function getAlumnos(componente){

        var value = $(componente).attr("id");
             $.ajax({
            url: "../../GetParticipantes",
            method: "POST",
            data : {"id_ficha":value}
            }).done(function(data){
                $("#contFichas").html(data);
                //EXPORT EXCEL
                $("#btnExportExcel").click(function(e) {
                   window.open('data:application/vnd.ms-excel,' +
                   encodeURIComponent($('#contFichas').html()));
                   e.preventDefault();
                });

                //EXPORT PDF

               
                $('#btnExportPDF').click(function () {
                     doc = new jsPDF();
                    doc.fromHTML($('#contFichas').html(), 15, 15, {
                        'width': 170,
                            'elementHandlers': specialElementHandlers
                    });
                    doc.save('sample-file.pdf');
                });
        });
    }

    function mostrarAlerta(txt,type){
         new PNotify({
                title: 'Operacion realizada',
                text: txt,
                type: type
            });
    }

    function closeFicha(componente){
     var id_ficha = $(componente).attr("id");
     var txt ="";
     $.ajax({
            url: "../../ActivarDesactivar",
            method: "POST",
            data : {"id_ficha":id_ficha}
            }).done(function(data){
                if(data == "error"){
                    mostrarAlerta("Hubo un error interno, comuniquese con el Administrador!","error")
                }else if (data == "1"){
                    txt = "Activo"
                    $(componente).val(txt);
                    console.log("here");
                    mostrarAlerta("Ficha Activada con exito!","info")
                }else if (data == "0"){
                    txt = "Inactivo"
                    $(componente).val(txt);
                    mostrarAlerta("Ficha Desactivada con exito!","info")
                }
            });
    }

    function saveEvaluacion(){
        $("#btnSave").click(function(){
             var formulario = $("#formPreguntas").serialize();

            $.ajax({
                url: "../../SaveEvalu",
                method: "POST",
                data : formulario
            }).done(function(){
               $.ajax({
                    url: "../../GetFichas",
                    method: "POST"
                }).done(function(data){
                    $("#contFichas").html(data);
                });

        });

     });
    }

  function saveCalificacion(componente){
      
             var formulario = $("#formPreguntas").serialize();

            $.ajax({
                url: "../../calcularNota",
                method: "POST",
                data : formulario
            }).done(function(){
             getAlumnos(componente);
        });

   
    }