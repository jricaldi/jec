(function(){
    
    "use strict";
    
    $(document).ready(function(){
        
//        $( "#frmRegistro" ).submit(function( event ) {
//          cancelarRegistro();
//          event.preventDefault();
//        });
        
        $(".botones button").on("click",function(){
            var tipoBoton = $(this).attr("data-opcion");
            var idClaseRecurso = $(this).parents(".fila").attr("data-recurso");
            $.post("comun/AbrirRecurso?idClaseRecurso=" + idClaseRecurso + "&tipoBoton=" + tipoBoton);
        });
        
        $("#btnAgregarNuevo").on("click",function(){
            $("#btnAgregarNuevo").parent().hide();
            $("#divRecursos").addClass("hide");
            $("#divAgregarRecurso").removeClass("hide");
        });
        
        $("#btnCancelar").on("click",function(){
            $("#btnAgregarNuevo").parent().show();
            $("#divRecursos").removeClass("hide");
            $("#divAgregarRecurso").addClass("hide");
            cancelarRegistro();
        });
        
        function cancelarRegistro(){
            $("#txtTitulo").val("");
            $("#txtDescripcion").val("");
            $("#txtRuta").val("");
            $("input.file-path").val("");
        };
        
        $(".nombreAplicacion").each(function (){
            var html = $(this).html();
            $(this).html(html.toLowerCase());
        });  
        
    });
})();
  