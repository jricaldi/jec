(function(){
    
    "use strict";

    $(document).on("ready",function(){
        
        resize();
         
        function resize(){
            window.parent.$("#miFrame").css("height",$("body").outerHeight(true))
        };
        
        $("#cargando").hide();
        
        $("#btnExecute").on("click",function(){
            var pass = $("#txtPassAct").val();
            var newPass = $("#txtNewPass").val();
            var reNewPass = $("#txtReNewPass").val();
            
            if(pass == "" || newPass == "" || reNewPass == "")
                return;
            
            $("#msgError").html("");
            $("#msgOk").html("");
            $("#cargando").show();
            cambiarPassword();
        });
        
        
        
        function cambiarPassword() {
            var param = $("#idForm").serialize();
            var newPass = $("#txtNewPass").val();
            var reNewPass = $("#txtReNewPass").val();

            if (newPass != reNewPass) {
                $("#msgError").html("<i class='small material-icons vertical'>error_outline</i>La nueva contraseña no coincide con el re ingreso de contraseña.");
                $("#cargando").hide();
                return;
            } else {
                $.post("CambioPassword", param)
                .done(function (msg) {
                    $("#cargando").hide();
                    if(msg == "OK"){
                        $("#msgOk").html("<i class='small material-icons vertical'>info_outline</i>" + "La contraseña ha sido actualizada correctamente");
                        limpiarCampos();
                    }
                    else{
                        $("#msgError").html("<i class='small material-icons vertical'>error_outline</i>" + msg);
                    }

                });
            }
        };

        function limpiarCampos() {
            $("#txtPassAct").val("");
            $("#txtNewPass").val("");
            $("#txtReNewPass").val("");
        };
        
    });
    
})();


