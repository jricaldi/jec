$(document).on("ready",function (){
    
    resize();

    function resize(){
         window.parent.$("#miFrame").css("height",$("body").outerHeight(true));
    };
    
    $("#btnExecute").prop('disabled', 'disabled');
    $("#btnExecute").addClass("disabled");
    
    $("#btnBuscar").on("click",function (e){
        e.preventDefault();
        var formulario = $("#form").serialize();
        $.post("GetUsuario",formulario).done(function (data) {
            if(data === "ERROR"){
                $("#lblNombre").html("<i class='red-text small material-icons vertical'>error_outline</i>No se encontro el DNI en el sistema o hubo un error al buscarlo");
            }
            else{
                var json = $.parseJSON(data);
                $("#btnExecute").removeAttr('disabled');
                $("#btnExecute").removeClass("disabled");
                $("#dataDni").val(json.dni);
                $("#dataUsuario").val(json.usuario);
                $("#lblNombre").html( "( " + json.nombre + " )");
            }
        });
        
    });
    
    $("#btnExecute").on("click",function (){
        var dni = $("#dataDni").val();
        var usuario = $("#dataUsuario").val();
        $.post("ResetPassword",{dataDni : dni , dataUsuario : usuario}).done(function (data) {
            if(data === "ERROR"){
                $("#lblPassword").html("<i class='red-text small material-icons vertical'>error_outline</i>Hubo un error al reiniciar el password");
            }
            else{
                $("#btnExecute").prop('disabled', 'disabled');
                $("#btnExecute").addClass("disabled");
                $("#lblPassword").html("El nuevo password es: <b>" + dni + "</b>");
            }
        });
    });
  
});
