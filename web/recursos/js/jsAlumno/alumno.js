(function() {

  "use strict";
      
$(document).on("ready",function(){
    
    try {
        $("a#bienvenida").simulate("click");
        if(global_idClase!=0){
            $.get("BloquearUno?hfIpAlumno=" + global_ipCliente);
        }
    }
    catch (err){
        console.log("no se ha iniciado la clase");
    }

    setInterval(function(){
        $.get("GetEstadoBloqueo").done(function(data){
            if(data === "true" ){
                $("#divOverlay").addClass("masterOverlay");
            }
            else{
                $("#divOverlay").removeClass("masterOverlay");
            }
        });

    },2000);

    $("#secMenu ul li a").on("click", function(){
        var id = $(this).parent().attr("id");
        var classActive = id + "Active";
        $("#secMenu ul li a").removeAttr("class");
        $(this).addClass("bMenu " + classActive);
    });

    $(window).on('unload', function(){
        $.get("DesbloquearUno?hfIpAlumno=" + global_ipCliente);
    });

    $("#lnkCerrar").on("click",function(){
        $.get("DesbloquearUno?hfIpAlumno=" + global_ipCliente);
    });
    
    
});
  
  
})();