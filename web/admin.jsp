<%@include file="include-session.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="recursos/img/favicon.ico">
    <link rel="stylesheet" href="recursos/css/materialize/font.css">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/cssAdmin/admin.css">
  </head> 
<body class="white">
    <header>
        <nav>
          <div class="nav-wrapper">
              <img src="recursos/img/foto_admin.jpg" class="imgDocente circle">
              <a href="#!" class="brand-logo titulo">Administrador</a>
            <ul class="right hide-on-med-and-down">
              <li style="margin-right: 10px"><a class="dropdown-button" data-activates="config"><i class="material-icons options">settings</i></a></li>
            </ul>
              
            <ul class="side-nav fixed menu">
              <li><a href="admin/mainAdministrador.jsp" target="miFrame">Cargas</a></li>
              <li><a href="admin/reseteos.jsp" target="miFrame">Passwords</a></li>
            </ul>
              
            <ul id="slide-out" class="side-nav menu">
              <li><a href="admin/mainAdministrador.jsp" target="miFrame">Cargas</a></li>
              <li><a href="admin/reseteos.jsp" target="miFrame">Passwords</a></li>
              <li class="divider"></li>
              <li id="lnkCerrar"><a href="comun/UsuarioLogoutServlet">( Cerrar Sesión )</a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            
            <ul id="config" class="dropdown-content">
                <li id="lnkCerrar"><a href="comun/UsuarioLogoutServlet">( Cerrar Sesión )</a></li>
            </ul>
            
            
          </div>
        </nav>
    </header>
    <main>
        <iframe name="miFrame" id="miFrame" class="contenido"></iframe>
    </main>
    <%@include file="include-footer.jsp" %>
    
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/materialize/materialize.js"></script>
    <script type="text/javascript" src="recursos/js/jsDocente/docente.js"></script>

</body>
</html>
