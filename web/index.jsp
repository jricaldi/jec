<%@page import="pe.minedu.util.Constante"%>
<%@page import="pe.minedu.bean.UsuarioBean"%>
<%
UsuarioBean usuario=session.getAttribute("usuario")==null?null:(UsuarioBean)session.getAttribute("usuario");
if(usuario==null){
    request.getRequestDispatcher("login.jsp").forward(request, response);
}else{
    if(usuario.getPerfil().equals(Constante.PERFIL_ADMINISTRADOR)){
        request.getRequestDispatcher("admin.jsp").forward(request, response);
    }else if(usuario.getPerfil().equals(Constante.PERFIL_DOCENTE)){
        request.getRequestDispatcher("docente.jsp").forward(request, response);
    }else if(usuario.getPerfil().equals(Constante.PERFIL_ESTUDIANTE)){
        request.getRequestDispatcher("alumno.jsp").forward(request, response);
    }else{
        request.getRequestDispatcher("default.jsp").forward(request, response);
    }
}
%>