<%@include file="include-session.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="recursos/img/favicon.ico">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="recursos/js/jquery-modal/modal.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/cssDocente/inicioDocente/docente.css">
  </head> 
<body>
    <%@include file="include-modals.jsp" %>
    <div id="background"></div>
    <header class="headerLineaMorado">
        <div id="secLogoMinedu">
            <img id="logoJec" src="recursos/img/icons/jec_logo.png">
            <img id="logoMinedu" src="recursos/img/icons/minedu_logo.png">
        </div>
    </header>
    <main class="pt45">
        <div id="panelCentral">
            <div id="ladoA">
                <div id="secDatos">
                    <div class="pt20 pl25">
                        <c:if test="${usuarioInfo.genero=='M'}">
                             <img src="recursos/img/foto_profesor.png" >
                        </c:if>
                        <c:if test="${usuarioInfo.genero=='F'}">
                            <img src="recursos/img/foto_profesora.png">
                        </c:if>
                    </div>
                    <div id="secNombre" class="pt10">
                        <span class="mt10 bold">
                            ${usuarioInfo.nombre} ${usuarioInfo.apellido}
                        </span>
                        <span class="mayuscula">docente</span>
                    </div>
                </div>
                <div id="secMenu">
                    <ul>
                        <c:if test="${idClase!='0'}">
                            <li id="liBiblioteca"><a href="comun_IrARecursos" target="miFrame"><span class="mayuscula">biblioteca</span></a></li>
                            <li id="liEstudiantes"><a href="docente_IrAControlEstudiante" target="miFrame"><span class="mayuscula">estudiantes</span></a></li>
                            <li id="liAplicaciones"><a href="IrAAplicaciones" target="miFrame"><span class="mayuscula">aplicaciones</span></a></li>
                            <li id="liCuestionario"><a href="comun/cuestionario/cuestionario.jsp" target="miFrame"><span class="mayuscula">cuestionario</span></a></li>
                            <li id="liChat"><a href="http://${CONST_JEC_IP_CHAT}/?idClase=${usuarioClase.idClase}&idUsuario=${usuario.idUsuario}&nombre=${usuarioInfo.nombre}&genero=${usuarioInfo.genero}&perfil=${usuario.perfil}" target="miFrame"><span class="mayuscula">chat</span></a></li>
                            <li id="liMonitoreo"><a href="MonitoreoServlet" target="miFrame"><span class="mayuscula">monitoreo</span></a></li>
                        </c:if>
                    </ul>
                </div>
            </div>
            <div id="ladoB">
                <div id="secOpciones">
                    <c:if test="${idClase!='0'}">
                        <span id="detalle" class="mayuscula cprimario iblock">grado "<b>${claseInfo.grado}</b>"/ seccion "<b>${claseInfo.seccion}</b>"/ curso "<b>${claseInfo.nombreCurso}</b>" - <b id="year">2016</b></span>
                    </c:if>
                    <ul class="opciones iblock">
                        <li>
                            <a href="#"><img src="recursos/img/icons/opcion_ico.png"/></a>
                            <ul class="child">
                                <li><a href="comun/seguridad/cambiarPassword.jsp" target="miFrame" >Cambiar contraseña</a></li>
                                <c:if test="${idClase=='0'}">
                                    <li><a href="docente/ClaseIniciarServlet" target="miFrame">Iniciar clase</a></li>
                                </c:if>
                                <c:if test="${idClase!='0'}">
                                    <li><a href="#modalCerrarClase" rel="modal:open" class="modal-trigger">Finalizar clase</a></li>
                                </c:if>
                            </ul>
                        </li>
                        <li>
                            <a href="comun/UsuarioLogoutServlet" style="border-top-right-radius: 10px;"><img src="recursos/img/icons/cerrar_ico.png"/></a>
                        </li>
                    </ul>    
                </div>
                <div id="secIframe">
                    <c:if test="${idClase!='0'}">
                        <a id="bienvenida" class="hide" href="comun/seguridad/bienvenida.jsp" target="miFrame"></a>
                    </c:if>
                    <c:if test="${idClase=='0' && usuario.perfil==PERFIL_DOCENTE}">
                        <a id="bienvenida" class="hide" href="docente/ClaseIniciarServlet" target="miFrame"></a>
                    </c:if>
                    <iframe name="miFrame" id="miFrame"></iframe>
                </div>
            </div>
        </div>
    </main>
  
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-modal/modal.js"></script>
    <script type="text/javascript" src="recursos/js/jsDocente/docente.js"></script>

</body>
</html>
