<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="include-session.jsp" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="recursos/img/favicon.ico">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/cssLogin/login.css">
  </head>
<body>
    <header class="headerLineaMorado">
        <div id="secLogoMinedu">
            <img id="logoMinedu" src="recursos/img/icons/minedu_logo.png" class="mt20">
        </div>
    </header>
    <main>
        <div id="background">
            <div class="pt65">
                <div id="loginPanel">
                    <div class="pb35 borderLogo">
                        <img id="logoJec" src="recursos/img/icons/login_jec_logo_ico.png">
                    </div>
                    <div id="secForm">
                        <form id="loginForm" action="comun/UsuarioLoginServlet" method="POST">
                            <input class="txtJec imgUsuario grayscale" placeholder="Usuario" id="txtUsuario" name="txtUsuario"/>
                            <p id="msgUsuario" class="morado errorMsg noVisible">Por favor ingrese su <span class='bold'>usuario</span></p>
                            <input class="txtJec imgPassword grayscale" placeholder="Contraseña" id="txtPassword" name="txtPassword" type="password"/>
                            <p id="msgPassword" class="morado errorMsg noVisible">Por favor ingrese su <span class='bold'>contraseña</span></p>
                            <button class="btnJec mayuscula" id="btnIngresar">ingresar &nbsp;&nbsp;></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

  <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="recursos/js/jsLogin/jsLogin.js"></script>

</body>
</html>
