<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Aprendizaje virtual JEC</title>
        <meta name="description" content="Jornada Escolar Completa">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/fonts.css"/>
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/estudiantes/estudiantes.css"> 
        <link href="../../recursos/css/cssUtils/cssNotify/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />      
        <link href="../../recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
          <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_docente.css">
           
        </c:if>
        <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
             <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_alunmo.css">
        </c:if>
    </head>
    <body >
        <header>
        <h1 class="titulo cprimarioc">
            <img src=""/> cuestionario
        </h1>
    </header>
               <main style="height: 295px; border-top: 15px solid #fff; border-bottom: 15px solid #fff; box-sizing: border-box;">
            <div  id="contFichas" >
            </div>
         <div id="editor"></div>
         <div id="contFichaDownload" hidden="true"></div>
         
               </main>
       
        <script src="../../recursos/js/jquery/jquery-1.11.3.min.js"></script>
       <!--  <script src="../../recursos/js/bootstrap.min.js"></script>
        <script src="../../recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="../../recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>   -->
        <script src="../../recursos/js/materialize/materialize.js"></script>       
        <script type="text/javascript" src="../../recursos/js/jsUtils/jsNotify/pnotify.custom.min.js"></script>
        <script type="text/javascript" src="../../recursos/js/jsUtils/jsPDF/jspdf.js"></script>
        <script src="../../recursos/js/jsUtils/jsPDF/jspdf.debug.js"></script>               
        <script type="text/javascript" src="../../recursos/js/jsCuestionario/jsCuestionario.js"></script>
    </body>
</html>
