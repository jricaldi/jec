<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
        <script src="../../recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script src="../../recursos/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
        <link href="../../recursos/css/cssUtils/cssNotify/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../../recursos/css/cssUtils/cssDiv/cssDiv.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../../recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../../recursos/css/bootstrap.min.css">
        <script src="../../recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="../../recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>          
        <script type="text/javascript" src="../../recursos/js/jsUtils/jsNotify/pnotify.custom.min.js"></script>
        <script type="text/javascript" src="../../recursos/js/jsUtils/jsPDF/jspdf.js"></script>
        <script src="../../recursos/js/jsUtils/jsPDF/jspdf.debug.js"></script>               
        <script type="text/javascript" src="../../recursos/js/jsCuestionario/jsCuestionario.js"></script>
    </head>
    <body class="contFrame">
        
         <div id="contFichas"     style="margin-top: 15px;"></div>
         <div id="editor"></div>
         <div id="contFichaDownload" hidden="true"></div>
       
    </body>
</html>
