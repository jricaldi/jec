<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    
     <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    </c:if>
    
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
          <link rel="stylesheet" href="recursos/css/materialize/materialize_1.css">
    </c:if>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/biblioteca/biblioteca.css">
  </head> 
<body>
    <section class="centradoYColorSection">
        <div class="row primerDivCentrado">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                     <c:if test="${usuario.perfil==PERFIL_DOCENTE}">                    
                        <h5 class="colorMoradoTexto">Biblioteca</h5>
                    </c:if>
                    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                        <h5 class="colorCelesteTexto">Biblioteca</h5>
                    </c:if>                   
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            
            <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                  <hr class="colorMoradoHR" style="margin-bottom: 50px;">
            </c:if>
             
            <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                 <hr class="colorCelesteHR" style="margin-bottom: 50px;">
            </c:if>
            
            <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                <div class="col s12 m4 marBot40">
                    <button id="btnAgregarNuevo" class="btn waves-effect colorMorado">Agregar&nbsp;Recurso</button>
                </div>
                <form class="col s12 sinPadLat hide" id="divAgregarRecurso" action="comun/RegistrarNuevoRecurso" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s12 m5 l3">
                            <input id="txtTitulo" name="txtTitulo" type="text" required> <label for="txtTitulo" class="center-align">Título</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m5 l3">
                            <input id="txtDescripcion" name="txtDescripcion" type="text" required> <label for="txtDescripcion" class="center-align">Descripción</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m5 l3">
                            <label>Tipo Aplicación</label>
                            <select id="cbTipoAplicacion" name="cbTipoAplicacion" class="browser-default" required>
                                   <c:if test="${not empty listaAplicaciones}" >
                                       <c:forEach var="aplicacion" items="${listaAplicaciones}">
                                           <option value="${aplicacion.idAplicacion}">${aplicacion.nombreAplicacion}</option>
                                       </c:forEach>
                                   </c:if>          
                           </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field col s12 m8 l6">
                         <div class="btn colorMorado">
                           <span style="text-transform: capitalize">Ruta Archivo</span>
                           <input id="txtRuta" name="txtRuta" type="file" required>
                         </div>
                         <div class="file-path-wrapper">
                           <input class="file-path validate" type="text">
                         </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col s12 center">
                            <button id="btnExecute" type="submit" class="btn waves-effect colorMorado">Agregar</button>
                            <button id="btnCancelar" class="btn waves-effect waves-light waves-red btn red" style="background-color:#d32f2f !important;">Cancelar</button>
                        </div>
                    </div>
                </form>
            </c:if>
            <div class="col s12 sinPadLat" id="divRecursos">
                <div class="col s12 sinPadLat">
                    <ul class="collection">
                        <c:if test="${not empty listRecursosClase}">
                            <c:forEach items="${listRecursosClase}" var="recurso">
                                <li class="collection-item avatar" data-recurso="${recurso.idClaseRecurso}" >
                                  <!--<img src="images/yuna.jpg" alt="" class="circle">-->
                                  <div class="row">
                                      <div class="col s6">
                                          <!--<i class="material-icons circle green">play_arrow</i>-->
                                          <img src="recursos/img/aplicaciones/${recurso.nombreAplicacion}.png" alt="Sin imagen" class="circle" style="border:1px solid #F2F2F2;"
                                           onerror="this.onerror=null;this.src='recursos/img/aplicaciones/noImagen.png';">
                                          <span class="title mayus">${recurso.titulo}</span>
                                          <p class="nombreAplicacion capitalize">${recurso.nombreAplicacion}</p>
                                      </div>
                                      <div class="col s6 opciones">
                                          <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                                            <button data-opcion="local" class="btn waves-effect colorMorado">Ver Local</button>
                                            <button data-opcion="todos" class="btn waves-effect colorMorado">Ver Todos</button>
                                          </c:if>
                                          <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                                              <button data-opcion="local" class="btn waves-effect colorMorado">Ver</button>
                                          </c:if>
                                      </div>
                                  </div>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </section>
  <script src="recursos/js/jquery/jquery-2.1.4.js"></script>
  <script src="recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="recursos/js/jsComun/mainRecursoJs.js"></script>

</body>
</html>
