<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_alunmo.css">
    </c:if>
    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_docente.css">
    </c:if>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/biblioteca/biblioteca.css">
  </head> 
<body>
    <header>
        <h1 class="titulo cprimariob">
            <img id="" src=""/> biblioteca
        </h1>
    </header>
    <main style="height: 295px; border-top: 15px solid #fff; border-bottom: 15px solid #fff; box-sizing: border-box;">
        <c:if test="${not empty listRecursosClase}">
            <p>
                <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                     - Seleccione “ver” para revisar el documento solo en su computadora.
                </c:if>
                <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                    - Seleccione “ver local” para revisar el documento solo en su computadora.<br/> 
                    - Seleccione “ver todos” para compartir la vista del documento en todas las computadoras de la clase.
                </c:if>
            </p>
            <div id="listaRecursos">
                    <c:forEach items="${listRecursosClase}" var="recurso">
                        <div class="fila" data-recurso="${recurso.idClaseRecurso}">
                            <div class="recurso bbd">
                                <img class="iblock va" src="recursos/img/aplicaciones/${recurso.nombreAplicacion}.png"
                                onerror="this.onerror=null;this.src='recursos/img/aplicaciones/noImagen.png';"/>
                                <div class="iblock va">
                                    <span class="mayuscula block hideOver titulo">${recurso.titulo}</span>
                                    <span class="block capitalize nombreAplicacion">${recurso.nombreAplicacion}</span>
                                </div>
                            </div>
                            <div class="botones center">
                                <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                                    <button data-opcion="local" class="btnJec bprimario hoverb">ver local</button>
                                    <button data-opcion="todos" class="btnJec bprimario ml15 hoverb">ver todos</button>
                                </c:if>
                                <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                                    <button data-opcion="local" class="btnJec bprimario">ver</button>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach>

            </div>
        </c:if>
        <c:if test="${empty listRecursosClase}">
            <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                <p class="cprimario bold">
                    No existe documentos para su visualzación, su perfil de estudiante no se lo permite.
                </p>
            </c:if>
        </c:if>
    </main>

    <script src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/jsComun/mainRecursoJs.js"></script>

</body>
</html>
