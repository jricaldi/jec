<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
        <script src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script src="recursos/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="recursos/js/jquery-ui/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
        <link href="recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="recursos/css/bootstrap.min.css">
        <script src="recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>   
        <script type="text/javascript" src="recursos/js/jquery-ui/jquery-ui.js"></script>
        <script type="text/javascript" src="recursos/js/jsComun/mainRecursoJs.js"></script>
    </head>
    <body class="contFrame">
        <div id="divTabla" style="text-align: left;">
            <table border="0" style="margin: 0 auto;">
                <thead>
                    <tr>
                        <th class="cabeceraRecursos"><input class="cabeceraRecursosButton" type="button" value="Recurso" /></th>
                        <th class="cabeceraRecursos"><input class="cabeceraRecursosButton" type="button" value="Descripcion" /></th>
                    </tr>
                </thead>
                <c:if test="${not empty listRecursosClase}">
                    <c:forEach items="${listRecursosClase}" var="recurso">
                        <tbody>
                            <tr>
                                <td>
                                    <a class="blancoNegrita" href="#" onclick="abrirRecurso('${recurso.idClaseRecurso}')">${recurso.titulo}</a>
                                </td>
                                <td>
                                    <label class="blancoNegrita"> ${recurso.descripcion}</label>
                                </td>
                            </tr>
                        </tbody>
                    </c:forEach>
                </c:if>
            </table>


        </div>
        <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
            <div id="divBotonNuevoReg" style="text-align: center; padding: 15px">
                <input class="styleBotonCuesti" type="button" value="Nuevo Recurso" onclick="abrirRegistroRecurso()" />
            </div>
            <div id="divNuevoRecurso" style="display: none">
                <form id="frmRegistro" method="POST" 
                      action="comun/RegistrarNuevoRecurso" 
                      onsubmit="return validarFormulario();" 
                      enctype="multipart/form-data"
                      >
                    <table border="0" style="margin: 0 auto;">
                        <tr>
                            <td><label style="margin-left: 15px;" class="blancoNegrita">Titulo</label></td>
                            <td style="padding: 5px">
                                <input class="form-control" type="text" maxlength="200" name="txtTitulo" id="txtTitulo" required />
                            </td>
                        </tr>
                        <tr>
                            <td ><label style="margin-left: 15px;" class="blancoNegrita">Descripcion</label></td>
                            <td style="padding: 5px">
                                <textarea class="form-control" maxlength="400" name="txtDescripcion" id="txtDescripcion" required ></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><label style="margin-left: 15px;" class="blancoNegrita">Tipo Aplicacion</label></td>
                            <td style="padding: 5px">
                                <select class="selectpicker2" name="cbTipoAplicacion" id="cbTipoAplicacion">
                                    <c:if test="${not empty listaAplicaciones}" >
                                        <c:forEach var="aplicacion" items="${listaAplicaciones}">
                                            <option value="${aplicacion.idAplicacion}">${aplicacion.nombreAplicacion}</option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td ><label style="margin-left: 15px;" class="blancoNegrita">Ruta Archivo</label></td>
                            <td style="padding: 5px">
                                <input class="form-control" type="file" name="file" name="txtRuta" id="txtRuta" required="true"/>
                                <!--<input class="form-control" type="text" maxlength="250" name="txtRuta" id="txtRuta" required />-->
                            </td>
                        </tr>
                        <tr >
                            <td style="padding: 15px">
                                <input class ="styleBotonRecur" type="submit" value="Registrar" />
                            </td>
                            <td>
                                <input class ="styleBotonRecur" type="button" onclick="cancelarRegistro()" value="Cancelar" />
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
        </c:if>

    </body>
</html>
