<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/fonts.css"/>
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_alunmo.css">
    </c:if>
    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_docente.css">
    </c:if>
    <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/bienvenida.css">
  </head> 
<body>
    <c:if test="${idClase!='0'}">
        <header>
            <h1 class="titulo cprimariod biconod"><!--<img src="../../recursos/img/icons/bienvenido_ico.png"/>-->
                <c:if test="${usuarioInfo.genero=='M'}"> bienvenido </c:if>
                <c:if test="${usuarioInfo.genero=='F'}"> bienvenida </c:if>
                a clase
            </h1>
        </header>
        <main>
            <p>
                Ahora que ya ha seleccionado la información de la clase, puede revisar el módulo “Biblioteca”
                para acceder al material disponible,  o seleccione el módulo  “Aplicaciones” para ingresar a los 
                programas disponibles en la sesión.  
            </p>
            <div id="datosClase">
               <div class="bbd">
                   <span class="cprimario bold mr30">grado:</span><span>${claseInfo.grado}</span>
                </div>
                <div class="bbd">
                    <span class="cprimario bold mr30">curso:</span><span>${claseInfo.nombreCurso}</span>
                </div>
                <div class="bbd">
                    <span class="cprimario bold mr30">sección:</span><span>${claseInfo.seccion}</span>
                </div>
                <div class="bbd">
                    <span class="cprimario bold mr30">sesión:</span><span>${idSesion}</span>
                </div> 
            </div>
            
        </main>
    </c:if>
    <c:if test="${idClase=='0'}">
        <header>
            <h1 class="titulo">inicie clase</h1>
        </header>
        <main>
            <p>
                Lo primero que tiene que realizar es iniciar una clase, dirigese al ícono con forma
                de engranaje y dale clic a <i class="bold cprimario">iniciar clase</i>.
            </p>
        </main>
    </c:if>
  <script type="text/javascript" src="../../recursos/js/jquery/jquery-2.1.4.js"></script>
</body>
</html>
