<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="../../recursos/css/materialize/font.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="../../recursos/css/materialize/materialize.css">-->
    <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun.css">
    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/materialize/materiax_docente.css">
    </c:if>
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/materialize/materiax_alumno.css">
    </c:if>

    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_docente.css">
    </c:if>
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
        <link rel="stylesheet" href="../../recursos/css/cssUtils/comun/comun_alunmo.css">
    </c:if>
  </head> 
<body style="background-color: #EEECF2; overflow-y:hidden; margin-left:0; margin-right:0; margin-top:0; padding-top: 0; padding-bottom: 0;">
   <header>
        <h1 class="titulo cprimariocontrasena" style="padding-left: 0;">
            <img src=""style="margin-right:2px;"> cambiar contraseña
        </h1>
    </header>


    <section>
        <div class="row">
            <!--<div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5 class="titulo cprimario">Cambiar Contraseñaaa</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>-->
            <!--<hr style="margin-bottom: 20px; width: 300px color: yellow">-->
            <!--<div class="col s12">-->
              <form class="login-form" id="idForm">
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <!--<i class="material-icons prefix">lock_open</i>--> <input class="textContra imgContra" id="txtPassAct" placeholder="Contraseña actual" name="txtPassAct" type="password" required autocomplete="off"> <!--<label for="txtPassAct" class="center-align">Contraseña actual</label>-->
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <!--<i class="material-icons prefix">lock_outline</i>--> <input class="textContra imgContra" id="txtNewPass" name="txtNewPass" placeholder="Nueva contraseña" type="password" required autocomplete="off"><!-- <label for="txtNewPass" class="center-align">Nueva contraseña</label>-->
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <!--<i class="material-icons prefix">lock</i>--> <input class="textContra imgContra" id="txtReNewPass" name="txtReNewPass" placeholder="Repita contraseña" type="password" required autocomplete="off"> <!--<label for="txtReNewPass"  class="center-align">Repita contraseña</label>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col m4 l5">&nbsp;</div>
                    <div class="col s12 m4 l2 center" style="width: 250px">
                        <button id="btnExecute" class="btn waves-effect waves-light" >Cambiar&nbsp;Contraseña &nbsp;&nbsp;&nbsp;<img src="flecha_ico.png"></button>
                    </div>
                </div>

                <div class="row center">
                    <div class="col s12">
                        <p class="center red-text" id="msgError"></p>
                        <p class="center teal-text" id="msgOk"></p>
                    </div>
                    <div id="cargando" class="preloader-wrapper small active">
                        <div class="spinner-layer spinner-blue">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-green">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-yellow">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <!--</div>-->
        </div>
    </section>
    <!-- <section>
        <div class="row">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5>Cambiar Contraseña</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            <hr style="margin-bottom: 50px;">
            <div class="col s12">
              <form class="login-form" id="idForm">
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <i class="material-icons prefix">lock_open</i> <input id="txtPassAct" name="txtPassAct" type="password" required autocomplete="off"> <label for="txtPassAct" class="center-align">Contraseña actual</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <i class="material-icons prefix">lock_outline</i> <input id="txtNewPass" name="txtNewPass" type="password" required autocomplete="off"> <label for="txtNewPass" class="center-align">Nueva contraseña</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <i class="material-icons prefix">lock</i> <input id="txtReNewPass" name="txtReNewPass" type="password" required autocomplete="off"> <label for="txtReNewPass" class="center-align">Repita contraseña</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col m4 l5">&nbsp;</div>
                    <div class="col s12 m4 l2 center">
                        <button id="btnExecute" class="btn waves-effect waves-light">Cambiar&nbsp;Contraseña</button>
                    </div>
                </div>
                <div class="row center">
                    <div class="col s12">
                        <p class="center red-text" id="msgError"></p>
                        <p class="center teal-text" id="msgOk"></p>
                    </div>
                    <div id="cargando" class="preloader-wrapper small active">
                        <div class="spinner-layer spinner-blue">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-green">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-yellow">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            </div>
        </div>
    </section>-->
  <script src="../../recursos/js/jquery/jquery-2.1.4.js"></script>
  <script src="../../recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="../../recursos/js/jsComun/password.js" ></script>

</body>
</html>
