<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../../recursos/js/jquery-ui/jquery-ui.css" />
         <link rel="stylesheet" type="text/css" href="../../recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
       <script src="../../recursos/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../../recursos/css/bootstrap.min.css">
       <script type="text/javascript" src="../../recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="../../recursos/js/jquery-ui/jquery-ui.js"></script>
        <script type="text/javascript" src="../../recursos/js/jsComun/password.js" ></script>
    </head>
    <body class="contFrame">
        <form id="idForm">
            <div style="text-align: center"><h2 class="blancoNegrita">Cambiar Contraseña</h2>
                <table border="0" style="margin: 0 auto;">
                 
                    <tbody>
                        <tr>
                            <td style="padding: 10px;"><label class="blancoNegrita">
                                    Contraseña actual</label>
                            </td>
                            <td>
                                <input class="form-control" id="txtPassAct" name="txtPassAct" type="password" maxlength="20" required />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px;"><label class="blancoNegrita">
                                Nueva contraseña</label>
                            </td>
                            <td>
                                <input class="form-control" id="txtNewPass" name="txtNewPass" type="password" maxlength="20" required />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px;"><label class="blancoNegrita">
                                Repita contraseña</label>
                            </td>
                            <td>
                                <input class="form-control" id="txtReNewPass" name="txtReNewPass" type="password" maxlength="20" required />
                            </td>
                        </tr>
                         <tr>
                             <td colspan="2" style="padding: 10px;">
                                 <input  class="styleBotonRecur" type="button" value="Cambiar" onclick="cambiarPassword()" />
                            </td>
                        </tr>
                    </tbody>
                   
                       
                 </table>
            </div>
        </form>
    </body>
</html>
