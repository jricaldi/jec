<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../recursos/css/materialize/font.css">
    <link rel="stylesheet" href="../recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/comun.css">
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/biblioteca/biblioteca.css">
  </head> 
  <body style="height: 500px;">
    <section>
        <div class="row">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5 class="bold">Administrar</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            <div class="col s12"><p class="vertical"><i class="material-icons yellow-text text-accent-4 vertical">info</i> Si es la primera vez que va ha realizar cargas masivas al sistema
                    deber&aacute; hacerlo siguiendo el siquiente orden:<br>
                <ol>
                    <li>Docente</li>
                    <li>Cursos</li>
                    <li>Secciones</li>
                    <li>Sesiones</li>
                </ol></p></div>
            <div class="col s12" style="margin-bottom: 20px;">
                <ul class="tabs">
                    <li class="tab col s3"><a class="active" href="#divCargarDocentes">Docentes</a></li>
                    <li class="tab col s3"><a class="" href="#divCargarCursos">Cursos</a></li>
                    <li class="tab col s3"><a class="" href="#divCargarSecciones">Secciones</a></li>
                    <li class="tab col s3"><a class="" href="#divCargarSesiones">Sesiones</a></li>
                    <li class="tab col s3"><a class="" href="#divCargarCuestionarios">Cuestionarios</a></li>
                </ul>
            </div>
<!--            <div class="col s12 m5 l3">
                <label>Tipo de Carga</label>
                <select id="cbCargarDatos" name="cbCargarDatos" class="browser-default" onchange="abrirSeleccionCargaDatos()">
                    <option value="0">Seleccionar</option>
                    <option value="1">CARGAR DATOS GRADOS</option>
                    <option value="4">CARGAR DATOS DOCENTES</option>
                    <option value="2">CARGAR DATOS CURSOS</option>
                    <option value="3">CARGAR DATOS SECCIONES</option>
                    <option value="5">CARGAR DATOS SESIONES</option>
                    <option value="6">CARGAR DATOS DOCENTE CURSO</option>          
               </select>
            </div>-->
<!--                <div id="divCargarGrados" class="col s12">
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Grados</span>
                          <input id="datosGrados" name="datosGrados" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>-->
                <div id="divCargarSecciones" class="col s12">
                    <div class="col s12"><p class="vertical"><i class="material-icons teal-text text-accent-4 vertical">live_help</i> Ponga mucha enfasis en la columna "id_grado", ya que la secci&oacute;n estar&aacute; vinculada a este</p></div>
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Secciones</span>
                          <input id="datosSecciones" name="datosSecciones" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div id="divCargarCursos" class="col s12">
                    <div class="col s12"><p class="vertical"><i class="material-icons teal-text text-accent-4 vertical">live_help</i> Ponga mucha enfasis en la columna "codigo_curso", esta sera usada en la excel de <i>Sesiones</i></p></div>
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Cursos</span>
                          <input id="datosCursos" name="datosCursos" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div id="divCargarDocentes" class="col s12">
                    <div class="col s12"><p class="vertical"><i class="material-icons teal-text text-accent-4 vertical">live_help</i> Ponga mucha enfasis en la columna "dni", ya que seran estos el usuario y password de cada docente</p></div>
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Docentes</span>
                          <input id="datosDocente" name="datosDocente" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div id="divCargarSesiones" class="col s12">
                    <div class="col s12"><p class="vertical"><i class="material-icons teal-text text-accent-4 vertical">live_help</i> Ponga mucha enfasis en las columnas "codio_curso" y "ver_estudiante", el primero esta vinculada al codigo escrito en el excel de <i>Cursos</i>, la segunda nos dir&aacute; que recursos puede ser visto por el alumno</p></div>
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Sesiones</span>
                          <input id="datosSesiones" name="datosSesiones" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
                <div id="divCargarCuestionarios" class="col s12">
                    <div class="col s12"><p class="vertical"><i class="material-icons teal-text text-accent-4 vertical">live_help</i> Ponga mucha enfasis en las columnas "codio_curso" y "ver_estudiante", el primero esta vinculada al codigo escrito en el excel de <i>Cursos</i>, la segunda nos dir&aacute; que recursos puede ser visto por el alumno</p></div>
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Cuestionarios</span>
                          <input id="datosCuestionarios" name="datosCuestionarios" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
<!--                <div id="divCargarDocenteCurso" class="col s12">
                    <div class="file-field input-field col s12 m8 l6">
                        <div class="btn">
                          <span style="text-transform: capitalize">XLS Docente - Curso</span>
                          <input id="datosDocenteCurso" name="datosDocenteCurso" type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>-->
                <div id="upload" style="display: none;">Cargando..</div>
                <div id="message"></div>
        </div>

    </section>
  <script type="text/javascript" src="../recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="../recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="../recursos/js/fileupload/jquery.ajaxfileupload.js"></script>
  <script type="text/javascript" src="../recursos/js/jsAdministrador/mainAdministradorJs.js"></script>

</body>
</html>
