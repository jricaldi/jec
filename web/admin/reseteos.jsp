<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../recursos/css/materialize/font.css">
    <link rel="stylesheet" href="../recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/comun.css">
  </head> 
  <body style="height: 500px;">
    <section>
        <div class="row">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5 class="bold">Passwords</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>

            <div class="col s12 marBot20">
                <p class="vertical"><i class="material-icons yellow-text text-accent-4 vertical">info</i>
                    Busque el DNI del docente o estudiante, una vez detectado la persona haga clic en resetear password.<br>
                    El nuevo password sera el mismo DNI.
                </p>
            </div>
            <form class="col s12 sinPadLat" id="form">
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <label for="txtDni" class="active">DNI</label>
                        <input id="txtDni" name="txtDni" type="text" required placeholder="Ingrese aqui el DNI" maxlength="8">
                        <button id="btnBuscar" class="btn waves-effect waves-light">Buscar persona</button>
                    </div>
                </div>     
            </form>
            <div class="row">
                    <div class="col s12 center">
                        <input type="hidden" value="" id="dataDni"/>
                        <input type="hidden" value="" id="dataUsuario"/>
                        <span id="lblNombre">&nbsp;</span><br>
                        <button id="btnExecute" class="btn waves-effect waves-light" style="margin: 10px 0;">Reiniciar&nbsp;Password</button><br>
                        <span id="lblPassword">&nbsp;</span>
                    </div>
            </div>
        </div>

    </section>
  <script type="text/javascript" src="../recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="../recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="../recursos/js/jsAdministrador/reseteos.js"></script>

</body>
</html>
