<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="../recursos/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="../recursos/js/bootstrap.min.js"></script>
        <script src="../recursos/js/fileupload/jquery.ajaxfileupload.js" type="text/javascript"></script>
        <script src="../recursos/js/jsAdministrador/mainAdministradorJs.js" type="text/javascript"></script>        
        <link href="../recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../recursos/css/bootstrap.min.css">
        <script src="../recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="../recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>  
    </head>
    <body style="background: transparent">
        <form id="form" method="POST">
            <div id="divOpciones" style="text-align: center">
                <select class="selectpicker2" id="cbCargarDatos" name="cbCargarDatos" onchange="abrirSeleccionCargaDatos()">
                    <option value="0">--SELECCIONE--</option>
                    <option value="1">CARGAR DATOS GRADOS</option>
                    <option value="2">CARGAR DATOS CURSOS</option>
                    <option value="3">CARGAR DATOS SECCIONES</option>
                    <option value="4">CARGAR DATOS DOCENTES</option>
                    <option value="5">CARGAR DATOS SESIONES</option>
                    <option value="6">CARGAR DATOS DOCENTE CURSO</option>
                </select>
            </div>
            <br>
            <div id="divCargarGrados" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Grados</label>
                <input type="file" name="datosGrados" id="datosGrados" /><br />
            </div>
            <div id="divCargarSecciones" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Secciones</label>
                <input type="file" name="datosSecciones" id="datosSecciones" /><br />
            </div>
            <div id="divCargarCursos" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Cursos</label>
                <input type="file" name="datosCursos" id="datosCursos" /><br />
            </div>
            <div id="divCargarDocentes" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Docentes</label>
                <input type="file" name="datosDocente" id="datosDocente" /><br />
            </div>
            <div id="divCargarSesiones" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Sesiones</label>
                <input type="file" name="datosSesiones" id="datosSesiones" /><br />
            </div>
            <div id="divCargarDocenteCurso" style="display: none">
                <label style=" color: #FF6347;font-weight: bold;">Carga archivo XLS de Docente - Curso</label>
                <input type="file" name="datosDocenteCurso" id="datosDocenteCurso" /><br />
            </div>
            <div id="upload" style="display: none;">Cargando..</div>
            <div id="message"></div>
        </form>
    </body>
</html>
