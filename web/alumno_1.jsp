<%@include file="include-session.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="recursos/img/favicon.ico">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/cssAlumno/inicioEstudiante/estudiante.css">
  </head> 
<body>
    <div id="divOverlay"></div>
    <div class="backgroundEstudiante"></div>
    <header class="white btCelesteHeader">
        <div class="container">
            <div class="container">
                <div class="row">
                    <img src="recursos/img/icons/jec_logo.png" class="mr60 mt10">
                    <img src="recursos/img/icons/minedu_logo.png">
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container mt60">
            <div class="container white">
                <div class="row mb0">
                    <div class="col m3 bCeleste">
                        <div class="col m4 p20_0">
                            <c:if test="${usuarioInfo.genero=='M'}">
                             <img src="recursos/img/foto_alumno.png" >
                            </c:if>
                            <c:if test="${usuarioInfo.genero=='F'}">
                                <img src="recursos/img/foto_alumna.png">
                            </c:if>
                        </div>
                        <div class="col m8 white-text p20_0">
                            <span class="mayuscula bold">${usuarioInfo.nombre} ${usuarioInfo.apellido}</span>
                            <span class="mayuscula">estudiante</span>
                        </div>
                        
                    </div>
                    <div class="col m9 p20_0 white" style="height: 92px">
                        <ul class="opciones">
                            <li>
                                <a href="#"><i class="material-icons">settings</i></a>
                                <ul class="child">
                                    <li><a href="comun/seguridad/cambiarPassword.jsp" target="miFrame" >Cambiar contraseña</a></li>
                                    <c:if test="${idClase=='0'}">
                                        <li><a href="docente/ClaseIniciarServlet" target="miFrame">Iniciar clase</a></li>
                                    </c:if>
                                    <c:if test="${idClase!='0'}">
                                        <li><a id="lnkCerrarClase" class="modal-trigger">Finalizar clase</a></li>
                                    </c:if>
                                </ul>
                            </li>
                            <li>
                                <a href="comun/UsuarioLogoutServlet"><i class="material-icons bold">close</i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row mb0">
                        <div class="col m3 bCelesteMenu p0">
                            <div class="col m12 p0">
                                <ul class="menuLateral grey-text m0">
                                    <li><a href="comun_IrARecursos" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Lista de recursos de la sesion"><img src="recursos/img/icons/biblioteca_ico.png" /><span>biblioteca</span></a></li>
                                    <li><a href="IrAAplicaciones" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Agregar y ver todas las aplicaciones"><img src="recursos/img/icons/aplicaciones_ico.png" /><span>aplicaciones</span></a></li>
                                    <li><a href="comun/cuestionario/cuestionario.jsp" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Ver y crear cuestionarios, con notas"><img src="recursos/img/icons/cuestionario_ico.png" /><span>cuestionario</span></a></li>
                                    <li><a href="http://192.168.2.1:2015/?idClase=${usuarioClase.idClase}&idUsuario=${usuario.idUsuario}&nombre=${usuarioInfo.nombre}&genero=${usuarioInfo.genero}" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Enviar mensajes a estudiantes"><img src="recursos/img/icons/chat_ico.png" /><span>chat</span></a></li>
                                </ul>
                            </div>
                        </div>
                    <div class="col m9 bCelesteIframe" style="height: 450px">
                        <a id="bienvenida" class="hide" href="comun/seguridad/bienvenida.jsp" target="miFrame"></a>
                        <iframe name="miFrame" id="miFrame" style="border: none; width: 100%"></iframe>
                    </div>
                </div>            
            </div>
        </div>
    </main>
    
    <script type="text/javascript">
        //variables globales para ser usadas en los javascript externos;
        var global_idClase = ${idClase};
        var global_ipCliente = "${ipCliente}";
    </script>
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/materialize/materialize.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js" type="text/javascript"></script>
    <script type="text/javascript" src="recursos/js/jsAlumno/alumno.js"></script>

</body>
</html>
