<%@page contentType="text/html" pageEncoding="UTF-8"%>

<section id="modalIniciarClase" style="display:none; border-radius: unset;">
    <div>
        <h3 class="cprimario"><img src="recursos/img/icons/advertencia_morado_ico.png" style="margin-right:2px; vertical-align: middle"/>¿ESTÁ SEGURO DE <span style="color:#2C164B; font-size: 16px; font-weight: 900">INICIAR LA CLASE?</span></h3>
        <div style="background-color: #2C164B; height: 2px">
        </div>
        <p style="padding-left: 20px">Una vez creada la clase podrá cerrarla haciendo clic en el botón <span style="color:#2C164B; font-weight: bold">"Finalizar Clase"</span></p>
    </div>
    <div class="center">
        <a href="#" rel="modal:close" class="mr30" style="font-size: 14px; color:#2C164B; border: 1px solid #2C164B; border-radius: 4px; padding: 8px 22px 4px 22px; text-transform: uppercase">Cancelar</a>
        <a href="#" id="btnConfirmar" class="btnJec bprimario">Confirmar</a>
    </div>
</section>

<section id="modalCerrarClase" style="display:none; border-radius: unset;">
    <div>
        <h3 class="cprimario"><img src="recursos/img/icons/advertencia_morado_ico.png" style="margin-right:2px; vertical-align: middle"/>¿Está seguro de <span style="color:#2C164B; font-size: 16px; font-weight: 900">finalizar la clase?</span></h3>
        <div style="background-color: #2C164B; height: 2px">
        </div>
        <p style="padding-left: 20px">Una vez cerrada la clase podrá crear una nueva haciendo clic en el botón <span style="color:#2C164B; font-size: 16px; font-weight: 900">"Iniciar Clase"</span></p>
    </div>
    <div class="center">
        <a href="#" rel="modal:close" class="mr30" style="font-size: 14px; color:#2C164B; border: 1px solid #2C164B; border-radius: 4px; padding: 8px 22px 4px 22px; text-transform: uppercase">Cancelar</a>
        <a href="docente/ClaseFinalizarServlet" class="btnJec bprimario">Confirmar</a>
    </div>
</section>

<section id="modalEnviarMensaje" style="display:none; border-radius: unset;">
    <div>
        <h3 class="cprimario imensaje" id="titulo" style="text-transform: uppercase;">
        </h3>
        <div style="background-color: #2D174C; height: 2px; margin-top:-5px"></div>
        <p>
            <input type="text" placeholder="Bienvenido" name="mensaje" id="mensaje" value="" autofocus="" style="border:none; padding-left: 30px; margin-bottom: 10px;
    margin-top: 5px;">
        </p>
    </div>
    <div class="center botones">
        <a href="#" rel="modal:close" class="mr30" style="font-size: 14px; color:#2C164B; border: 1px solid #2C164B; border-radius: 4px; padding: 8px 15px 4px 15px; text-transform: uppercase">Cancelar</a>
        <a href="#" rel="modal:close" id="lnkEnviarMensaje" class="btnJec bprimario">enviar</a>
    </div>
</section>