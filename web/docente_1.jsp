<%@include file="include-session.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="recursos/img/favicon.ico">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/cssDocente/inicioDocente/docente.css">
  </head> 
<body style="background-color: #E4E6EA">
    <header>
        <nav>
            <a style="margin-left: 300px; margin-right: 40px;"><img src="recursos/img/logo_JEC_1.png" alt="" style="height: 40px;"></a>  
            <a><img src="recursos/img/logo_minedu.png" style="margin-top: 8px; height: 40px;"></a>
          <div class="nav-wrapper" >
              <a href="#!" class="brand-logo titulo hide-on-med-and-up">Docente</a>
            <ul class="right hide-on-med-and-down">
                <c:if test="${idClase=='0'}">
                    <li class="parpadea"><a href="docente/ClaseIniciarServlet" target="miFrame"><i class="material-icons left">input</i>Iniciar Clase</a></li>
                </c:if>
                <c:if test="${idClase!='0'}">
                    <li><a id="lnkCerrarClase" class="modal-trigger"><i class="material-icons left">close</i>Finalizar Clase</a></li>
                </c:if>
              <li style="margin-right: 10px"><a class="dropdown-button" data-activates="config"><i class="material-icons options">settings</i></a></li>
            </ul>
            
             
            <ul class="side-nav fixed menu">
                <li style="background-color: #2E0838 !important; padding-top: 10px;" class="hide-on-small-only">
                    <div class="row">
                        <div style="display: inline-block;">
                            <c:if test="${usuarioInfo.genero=='M'}">
                             <img src="recursos/img/foto_profesor.png" >
                            </c:if>
                            <c:if test="${usuarioInfo.genero=='F'}">
                                <img src="recursos/img/foto_profesora.png">
                            </c:if>
                        </div>
                        <div style="display: inline-block;">
                            <span style="display: block; line-height: 0; text-transform:uppercase;">${usuarioInfo.nombre} ${usuarioInfo.apellido}</span>
                            <span style="display: block; line-height: 3;color: gray;">DOCENTE</span>
                        </div>
                    </div>

                    
                    </li>
                <c:if test="${idClase!='0'}"> 
                    <li><a href="comun_IrARecursos" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Lista de recursos de la sesion">Biblioteca</a></li>
                    <li><a href="docente_IrAControlEstudiante" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Agregar y ver estudiantes de la sesion">Estudiantes</a></li>
                    <li><a href="IrAAplicaciones" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Agregar y ver todas las aplicaciones">Aplicaciones</a></li>
                    <li><a href="comun/cuestionario/cuestionario.jsp" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Ver y crear cuestionarios, con notas">Cuestionario</a></li>
                    <li><a href="http://192.168.2.1:2015/?idClase=${usuarioClase.idClase}&idUsuario=${usuario.idUsuario}&nombre=${usuarioInfo.nombre}&genero=${usuarioInfo.genero}" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Enviar mensajes a estudiantes">Chat</a></li>
                    <li><a href="MonitoreoServlet" target="miFrame" class="tooltipped mayus" data-position="right" data-delay="300" data-tooltip="Vigilar el estado de las maquinas conectadas a la sesion">Monitoreo</a></li>
                </c:if>
            </ul>
            
              
            <ul id="slide-out" class="side-nav menu">
              <c:if test="${idClase=='0'}">
                    <li><a href="docente/ClaseIniciarServlet" target="miFrame"><i class="material-icons left">input</i>Iniciar Clase</a></li>
              </c:if>
              <c:if test="${idClase!='0'}">
                    <li><a id="lnkCerrarClase" href="docente/ClaseFinalizarServlet"><i class="material-icons left">close</i>Finalizar Clase</a></li>
                    <li class="divider"></li>
                    <li><a href="comun_IrARecursos" target="miFrame">Biblioteca</a></li>
                    <li><a href="docente_IrAControlEstudiante" target="miFrame">Estudiantes</a></li>
                    <li><a href="IrAAplicaciones" target="miFrame">Aplicaciones</a></li>
                    <li><a href="comun/cuestionario/cuestionario.jsp" target="miFrame">Cuestionario</a></li>
                    <li><a href="http://192.168.2.1:2015/?idClase=${usuarioClase.idClase}&idUsuario=${usuario.idUsuario}&nombre=${usuarioInfo.nombre}&genero=${usuarioInfo.genero}" target="miFrame">Chat</a></li>
                    <li><a href="MonitoreoServlet" target="miFrame">Monitoreo</a></li>
                    <li id="lnkCerrar"><a href="comun/UsuarioLogoutServlet">( Cerrar Sesión )</a></li>
              </c:if>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            
            <ul id="config" class="dropdown-content">
                <li><a href="comun/seguridad/cambiarPassword.jsp" target="miFrame" >Cambiar Contraseña</a></li>
                <li id="lnkCerrar"><a href="comun/UsuarioLogoutServlet">( Cerrar Sesión )</a></li>
            </ul>
            
            
          </div>
        </nav>
    </header>
    <main>
        <a id="bienvenida" class="hide" href="comun/seguridad/bienvenida.jsp" target="miFrame"></a>
        <iframe name="miFrame" id="miFrame" style="border: none; width: 100%"></iframe>
        <%@include file="include-modals.jsp" %>
    </main>
    <%@include file="include-footer.jsp" %>
  
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/materialize/materialize.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
    <script type="text/javascript" src="recursos/js/jsDocente/docente.js"></script>

</body>
</html>
