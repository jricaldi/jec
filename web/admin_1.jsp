<%@include file="include-session.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Aprendizaje virtual JEC</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="recursos/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
  <script src="recursos/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>        
    <script type="text/javascript" src="recursos/js/jsUtils/jsNotify/pnotify.custom.min.js"></script>
</head>
<body>
<header>
              <img src="recursos/iconos/jec.gif" class="img-responsive pull-left " width="100" height="80">
          
              <img src="recursos/iconos/MINEDU.jpg" class="img-responsive pull-right " width="200" height="80">
</header>
<div class="container-fluid">
  <div class="row">
    <div class="menuprincipalAdmin  col-xs-4 col-sm-2 col-md-2">
           
           <ul class="nav nav-pills nav-stacked">
             <li class="item8"><a data-toogle="pills"  href="admin/mainAdministrador.jsp" target="miFrame">ADMINISTRADOR</a></li>
            <!-- <li class="item9"><a data-toogle="pills"  href="comun/seguridad/cambiarPassword.jsp" target="miFrame">CAMBIO CONTRASEÑA</a></li>-->
           </ul>
    </div>
    <div class="menucontenidoAdmin col-xs-8 col-sm-10 col-md-10">
          
                   <form class="form-inline" role="form">
                       <div class="form-group pull-right">
                              <button class="btn btn-inicio">Inicio</button>
                              <input  class="btn btn-cerrarsesion" onclick="location.href='comun/UsuarioLogoutServlet'" type="button" value="Cerrar Sesion"/>
                        </div>                   
                   </form>

                   <div class="barradocente">
                     
                     <img src="recursos/iconos/ADMINISTRADOR_full.png" class="img-responsive pull-right " width="270" height="150">
                     <!--<img src="recursos/iconos/sesiondocente-icono-biblio.png" class="img-responsive pull-right " width="100" height="150">-->
                   </div>

         

          <!--   Contenido del menÃº-->
          <div id="content" class="borderFrame content-seccion">
                <!-- contenido cargado con ajax -->
                <iframe name="miFrame" id="miFrame" width="100%" height="550" border="0" style="border: 0;"></iframe>
                <!-- fin contenido cargado con ajax -->
            </div>
                   
          <!--***************************** Fin del MenÃº *******************************--> 
          
    </div>

      <div class="clearfix visible-sm-block"></div>
      <div class="clearfix visible-xs-block"></div>
  </div>
  


</div>


    <script src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
    <script src="recursos/js/bootstrap.min.js"></script>
</body>
</html>