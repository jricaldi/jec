<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Jornada Escolar Completa</title>
        <meta name="description" content="Jornada Escolar Completa">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="recursos/css/materialize/font.css" rel="stylesheet">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
        <link rel="stylesheet" href="recursos/js/jquery-modal/modal.css">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_docente.css">
        <link rel="stylesheet" href="recursos/css/cssUtils/cssDocente/monitoreo/monitoreo.css">
    </head> 
    <body>
        <header>
            <h1 class="titulo cprimario">
                <img src="recursos/img/icons/monitoreo_morado_ico.png"/> monitoreo
            </h1>
        </header>
        <main style="height: 295px; border-top: 15px solid #fff; border-bottom: 15px solid #fff; box-sizing: border-box;">
            <p>
                - La opción “Bloquear a todos” inhabilita a los estudiantes en el sistema. 
                - Para enviar un mensaje directo a todas los estudiantes, puede utilizar la opción de “Mensaje a todos”.
            </p>
            <div class="block center">
                <button class="btnJec bprimario iblock hide" id="blo-todo-btn" onclick="bloquearEquipoTodos()">bloquear a todos</button>
                <button class="btnJec bprimario iblock hide" id="des-todo-btn" onclick="desbloquearEquipoTodos()">desbloquear a todos</button>
                <button class="btnJec bprimario iblock btnEnviarMensaje" onclick="enviarMensajeTodos()">mensaje a todos</button>
            </div>
            <div class="block" id="filas">
                <table>
                    <thead>
                        <tr>
                            <th>cod. alumno</th>
                            <th>nombres</th>
                            <th>fecha sesión</th>
                            <th>ip</th>
                            <th>monitoreo</th>
                            <th>msj</th>
                            <th>bloqueo</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="u" items="${estudiantes}">
                        <c:if test="${ not empty u.ip}">
                            <tr>
                                <td>${u.codigoEstudiante}</td>
                                <td>${u.nombres}</td>
                                <td>${u.fechaLogeo}</td>
                                <td>${u.ip}</td>
                                <td><button class="btnJec bprimario" onclick="tomarPantallazo('${u.ip}')">ver</button></td>
                                <td><button class="btnJec bprimario btnEnviarMensaje" onclick="enviarMensajeUno('${u.ip}')">enviar</button></td>
                                <td>
                                    <button class="btnJec bprimario" id="bloq-${u.idEstudiante}" onclick="bloquearEquipoUno('${u.idEstudiante}')">bloquear</button>
                                    <button class="btnJec bprimario hide" id="des-${u.idEstudiante}" onclick="desbloquearEquipoUno('${u.idEstudiante}')">desbloquear</button>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            
            <a id="btnPantallas" class="hide" href="ListarPantallas" target="pantallas"></a>
            <iframe name="pantallas" id="pantallas" style="border: none; width: 100%"></iframe>
            
            <input type="hidden" name="hfIpAlumno" id="hfIpAlumno" value="" />
            <input type="hidden" name="idClase" id="idClase" value="${idClase}">
        </main>

        <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
        <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
        <script type="text/javascript" src="recursos/js/jquery-modal/modal.js"></script>
        <script type="text/javascript" src="recursos/js/store/store.js"></script>
        <script type="text/javascript" src="recursos/js/jsDocente/mainMonitoreoJs.js"></script>
    </body>
</html>
