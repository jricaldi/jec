<%@page import="pe.minedu.bean.MonitoreoClaseEstudianteBean"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/materialize/font.css">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun.css">
  </head>
  <body>
    <div class="row" style="margin-top: 30px;">
        <c:if test="${not empty estudiantes}">
            <div class="col s12" style="margin-top: 10px;">
                <c:forEach var="estudiante" items="${estudiantes}">
                    <div class="col s4" style="height: 200px; border: 1px solid #f2f2f2">
                        <div class="col s12 black-text center" style="margin-bottom: 5px;">${estudiante.nombres}</div>
                        <div class="col s12"><img class="pantallaEstudiante" id="${estudiante.ip}" src="TomarPantallazoUno?hfIpAlumno=${estudiante.ip}" onclick="window.parent.ejecutarEfecto(this)" style="cursor:pointer; width: 100%"></div>
                    </div>
                </c:forEach>
            </div>
        </c:if>
    </div>
      
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
</body>
</html>
