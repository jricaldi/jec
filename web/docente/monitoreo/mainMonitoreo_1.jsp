<%-- 
    Document   : mainMonitoreo
    Created on : 08-dic-2015, 12:37:24
    Author     : GustavoSedano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="recursos/js/jquery-ui/jquery-ui.css" />
         <link rel="stylesheet" type="text/css" href="recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
        <script type="text/javascript" src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="recursos/js/jquery-ui/jquery-ui.js"></script>
        <script type="text/javascript" src="recursos/js/jsDocente/mainMonitoreoJs.js"></script>
    </head>
    <body class="contFrame">
        <c:if test="${empty usuarioClase}">
            <span>No Hay una Clase activa</span>
        </c:if>
        <c:if test="${not empty usuarioClase}">
            <!-- Div para los comandos generales -->
            <div >
                <table border="0" style="margin: 0 auto;">
                    <tr>
                        <td>
                            <input class="styleBotonRecur" onclick="bloquearEquipoTodos()"type="button" value="Bloquear Todos" />
                         <!--   <a class="blancoNegrita"  onclick="bloquearEquipoTodos() href="#" > Bloquear Equipo Estudiante</a>-->
                        </td>
                        <td>
                             <input class="styleBotonRecur" onclick="desbloquearEquipoTodos()"type="button" value="Desbloquear Todos" />
                           <!-- <a class="blancoNegrita" onclick="desbloquearEquipoTodos()" href="#" >Desbloquear Equipo Estudiante</a>-->
                        </td>
                    
                    
                        <td >
                            <input class="styleBotonRecur" onclick="enviarMensajeTodos()"type="button" value="Mensaje Todos" />
                           <!-- <a onclick="enviarMensajeTodos()" href="#" >Enviar Mensaje a la Clase</a>-->
                        </td>
                    
                   
                        <td >
                            <input class="styleBotonRecur" onclick="abrirBrowserTodos()"type="button" value="Abrir Browser Todos" />
                           <!-- <a onclick="abrirBrowserTodos()" href="#" >Abrir Browser</a>-->
                        </td>
                 </tr>
                </table>
            </div>

            <!-- Div para los comandos por usuario logeado -->
            <div>
                <table border="0" style="width: 100%; padding: 20px">
                    <thead >
                        <tr >
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Cod Alumno</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Nombres</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Fecha Sesion</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">IP</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Monitoreo</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Mensaje</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Bloqueo</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Desbloqueo</label></th>
                            <th style="background: #023158;text-align: center;"><label style="margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Browser</label></th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="u" items="${estudiantes}">
                        <c:if test="${ not empty u.ip}">
                            <tr>
                                <td><label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">${u.codigoEstudiante}</label></td>
                                <td><label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">${u.nombres}</label></td>
                                <td><label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">${u.fechaLogeo}</label></td>
                                <td><label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">${u.ip}</label></td>
                                <td style="text-align: center"><a class="blancoNegrita" onclick="tomarPantallazo('${u.ip}')" href="#" >Ver Pantalla</a></td>
                                <td style="text-align: center"><a class="blancoNegrita" onclick="enviarMensajeUno('${u.ip}')" href="#" >Enviar Mensaje</a></td>
                                <td style="text-align: center"><a class="blancoNegrita" onclick="bloquearEquipoUno('${u.ip}')" href="#" >Bloquear Equipo</a></td>
                                <td style="text-align: center"><a class="blancoNegrita" onclick="desbloquearEquipoUno('${u.ip}')" href="#" >Desbloquear Equipo</a></td>
                                <td style="text-align: center"><a class="blancoNegrita" onclick="abrirBrowserUno('${u.ip}')" href="#" >Abrir Browser</a></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            
            <div>
                <c:if test="${not empty estudiantes}">
                    <c:set var="cont" value="0" scope="page"></c:set>
                    <table border="1" style="width: 1200px">
                            <c:forEach begin="1" end="${cantFilas}" step="1">
                                <tr>
                                <c:forEach begin="1" end="${cantColumnas}" step="1">
                                    <c:choose>
                                        <c:when test="${cont < estudiantes.size()}">
                                            <td style="width: 300px;height: 300px;text-align: center">
                                                <span style="color: white"><strong>${estudiantes.get(cont).nombres}</strong></span>
                                                <br/>
                                                <img src="TomarPantallazoUno?hfIpAlumno=${estudiantes.get(cont).ip}" height="300" width="300" onclick="ejecutarEfecto(this)" style="cursor:pointer">
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:set var="cont" value="${cont + 1}" scope="page"></c:set>
                                </c:forEach>
                                </tr>
                            </c:forEach>             
                    </table>
                </c:if>
            </div>
            
            <input type="hidden" name="hfIpAlumno" id="hfIpAlumno" value="" />

            <div id="mensaje-form" title="Ingresar Mensaje">
                <form>
                    <fieldset>
                        <input type="text" name="mensaje" id="mensaje" value="" class="text ui-widget-content ui-corner-all">
                        <!-- Allow form submission with keyboard without duplicating the dialog button -->
                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                    </fieldset>
                </form>
            </div>
            
            <div id="dialog">
                <img id="imagenGrande" name="imagenGrande" height="750px" width="765px">
            </div>
        </c:if>
            
    </body>
</html>
