<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun.css">
    <link href="recursos/js/jquery-simpleModal/css/basic.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/cssDocente/monitoreo/monitoreo.css">
    
    <!--<link rel="stylesheet" href="recursos/js/jquery-ui/jquery-ui.css" />-->
  </head> 
  <body style="height: 1000px;">
      <section class="centradoYColorSection">
        <div class="row primerDivCentrado">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5>Monitoreo</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            <hr style="margin-bottom: 50px;">
            <div class="col s12 center options">
                <button class="btn waves-effect colorMorado capitalize" onclick="bloquearEquipoTodos()" >Bloquear Todos</button>
                <button class="btn waves-effect colorMorado capitalize" onclick="desbloquearEquipoTodos()">Desbloquear Todos</button>
                <button class="btn waves-effect colorMorado capitalize btnEnviarMensaje" onclick="enviarMensajeTodos()">Mensaje Todos</button>
                <!--<button class="btn waves-effect waves-light capitalize" onclick="abrirBrowserTodos()">Abrir Browser Todos</button>-->
            </div>
            <div class="col s12" style="margin-top: 20px;">
                <table class="striped centered">
                    <thead>
                        <tr>
                            <th data-field="codAlumno">Cod. Alumno</th>
                            <th data-field="nombres">Nombres</th>
                            <th data-field="fecSesion">Fecha Sesion</th>
                            <th data-field="ip">IP</th>
                            <th data-field="monitoreo">Monitoreo</th>
                            <th data-field="mensaje">Mensaje</th>
                            <th data-field="bloqueo">Bloqueo</th>
                            <th data-field="desbloqueo">Desbloqueo</th>
                            <!--<th data-field="browser">Browser</th>-->
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="u" items="${estudiantes}">
                        <c:if test="${ not empty u.ip}">
                            <tr>
                                <td>${u.codigoEstudiante}</td>
                                <td>${u.nombres}</td>
                                <td>${u.fechaLogeo}</td>
                                <td>${u.ip}</td>
                                <td><button class="btn waves-effect colorMorado capitalize green" onclick="tomarPantallazo('${u.ip}')">click</button></td>
                                <td><button class="btnEnviarMensaje btn waves-effect colorMorado capitalize green modal-trigger" onclick="enviarMensajeUno('${u.ip}')">click</button></td>
                                <td><button class="btn waves-effect colorMorado capitalize green" onclick="bloquearEquipoUno('${u.idEstudiante}')">click</button></td>
                                <td><button class="btn waves-effect colorMorado capitalize green" onclick="desbloquearEquipoUno('${u.idEstudiante}')">click</button></td>
                                <!--<td><button class="btn waves-effect waves-light capitalize green" onclick="abrirBrowserUno('${u.ip}')">click</button></td>-->
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            
            <a id="btnPantallas" class="hide" href="ListarPantallas" target="pantallas"></a>
            <iframe name="pantallas" id="pantallas" style="border: none; width: 100%"></iframe>
            
            <input type="hidden" name="hfIpAlumno" id="hfIpAlumno" value="" />
            
            <div id="dialog" class="hide">
                <img id="imagenGrande" name="imagenGrande" class="responsive-img">
            </div>
        </div>
    </section>
    <div id="modalEnviarMensaje" class="modal">
        <div class="modal-content">
          <h4>Enviar Mensaje Directo</h4>
          <input type="text" name="mensaje" id="mensaje" value="" autofocus="">
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-red btn red" style="margin-left:10px; background-color:#d32f2f !important;">Cancelar</a>
          <a href="#!" id="lnkEnviarMensaje"class=" modal-action modal-close waves-effect colorMorado btn">Confirmar</a>
        </div>
    </div>
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <!--<script type="text/javascript" src="recursos/js/jquery-ui/jquery-ui.js"></script>-->
    <script type="text/javascript" src="recursos/js/materialize/materialize.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
    <script type="text/javascript" src="recursos/js/jquery-simpleModal/js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="recursos/js/jsDocente/mainMonitoreoJs.js"></script>
</body>
</html>
