<%-- 
    Document   : instructivoDocente
    Created on : 05-ene-2016, 1:31:24
    Author     : GustavoSedano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table style="width: 900px;" border="1">
            <tr>
                <th>
                    Instructivo Docente
                </th>
            </tr>
            <tr>
                <td>
                    <label>Instructivo 1</label>
                    <br/>
                    <video width="850" height="600" controls>
                        <source src="../../recursos/instructivo/Anexo 1.mp4" type="video/mp4">
                        Tu explorador no soporta la etigueta html de video.
                    </video>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Instructivo 2</label>
                    <br/>
                    <audio controls>
                        <source src="../../recursos/instructivo/02080003.mp3" type="audio/mpeg">
                        Tu explorador no soporta la etigueta html de audio.
                    </audio>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Instructivo 3</label>
                    <br/>
                    <a href="../../recursos/instructivo/Anexo 1.doc">Anexo1.doc</a>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Instructivo 4</label>
                    <br/>
                    <a href="../../recursos/instructivo/Anexo 1.pptx">Anexo1.doc</a>
                </td>
            </tr>
        </table>
    </body>
</html>
