<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${select=='curso'}">
    <option value="0">Seleccionar</option>
    <c:forEach var="curso" items="${Cursos}">   
        <option value="${curso.idCurso}"> ${curso.nombreCurso}</option> 
    </c:forEach>
</c:if>
<c:if test="${select=='seccion'}">
    <option value="0">Seleccionar</option>
    <c:forEach var="seccion" items="${Secciones}">   
        <option value="${seccion.idSeccion}"> ${seccion.nombreSeccion}</option> 
    </c:forEach>
</c:if>
<c:if test="${select=='sesion'}">
    <option value="0">Seleccionar</option>
    <c:forEach var="sesion" items="${Sesiones}">   
        <option value="${sesion.idSesion}"> ${sesion.codigoSesion}</option> 
    </c:forEach>
</c:if>