<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="../recursos/css/materialize/font.css" rel="stylesheet">
    <link rel="stylesheet" href="../recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/comun.css">
  </head> 
<body>
    <section>
        <div class="row">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <h5>Iniciar sesión de clase</h5>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            <div class="col s12">
              <form class="login-form" id="form_sesion" method="POST" action="ClasePublicarServlet" target="_parent">
                <div class="row">
                    <div class="col s12 m5 l3">
                          <label>Grado</label>
                            <select id="idGrado" name="idGrado" class="browser-default">
                                <option value="0">Seleccionar</option>
                                <c:forEach var="grado" items="${GradosByDocente}">   
                                    <option value="${grado.codigoGrado}"> ${grado.idGrado} DE ${grado.nivelPrograma}</option> 
                                </c:forEach>
                            </select>
                    </div>    
                      
                </div>
               <div class="row">
                    <div class="col s12 m5 l3">
                     <label>Curso</label>
                        <select id="selectCurso" name="selectCurso" class="browser-default">
                            <option value="0">Seleccionar</option>            
                        </select>
                    </div> 
                        
                      
                </div>
                <div class="row">
                    <div class="col s12 m5 l3">
                     <label>Sección</label>
                        <select id="selectSeccion" name="selectSeccion" class="browser-default">
                            <option value="0">Seleccionar</option>            
                        </select>
                    </div> 
                        
                      
                </div>
                <div class="row">
                    <div class="col s12 m5 l3">
                      <label>Sesión</label>
                          <select id="selectSesion" name="selectSesion" class="browser-default">
                            <option value="0">Seleccionar</option>         
                        </select>
                    </div> 
                        
                      
                </div>
                <div class="row">
                    <div class="col m4 l5">&nbsp;</div>
                    <div class="col s12 m4 l2 center">
                        <button id="btnExecute" class="btn modal-trigger waves-effect colorMorado disabled">Iniciar&nbsp;Clase</button>
                    </div>
                </div>
                <div class="row center">
                    <div class="col s12">
                        <p class="center red-text" id="msgError"></p>
                        <p class="center teal-text" id="msgOk"></p>
                    </div>
                    <div id="cargando" class="preloader-wrapper small active">
                        <div class="spinner-layer spinner-blue">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-green">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-yellow">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>

                        <div class="spinner-layer spinner-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                            </div><div class="circle-clipper right">
                            <div class="circle"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            </div>
        </div>
    </section>
  <script type="text/javascript" src="../recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="../recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="../recursos/js/jsDocente/mainIniciaClase.js"></script>

</body>
</html>
