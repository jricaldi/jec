<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="../recursos/css/cssUtils/comun/comun_docente.css">
    <link rel="stylesheet" href="../recursos/css/cssUtils/cssDocente/inicioClase/inicioClase.css" type="text/css"/>
  </head> 
<body>
    <header>
        <h1 class="titulo cprimario"><img src="../recursos/img/icons/iniciarsesion_morado_ico.png"/>
            iniciar sesión de clase
        </h1>
    </header>
    <main>
        <p>
            Por favor seleccione la siguiente información para iniciar la sesión de clase.
        </p>
        <form id="form_sesion" method="POST" action="ClasePublicarServlet" target="_parent">
            <div>
                <label for="idGrado" class="cprimario">grado:</label>
                <select id="idGrado" name="idGrado" class="bsecundario">
                    <option value="0">Seleccionar</option>
                    <c:forEach var="grado" items="${GradosByDocente}">   
                        <option value="${grado.codigoGrado}"> ${grado.idGrado} DE ${grado.nivelPrograma}</option> 
                    </c:forEach>
                </select>
            </div>
            <div>
                <label for="selectCurso" class="cprimario">curso:</label>
                <select id="selectCurso" name="selectCurso" class="bsecundario">
                    <option value="0">Seleccionar</option>            
                </select>
            </div>
            <div>
                <label for="selectSeccion" class="cprimario">sección:</label>
                <select id="selectSeccion" name="selectSeccion" class="bsecundario">
                    <option value="0">Seleccionar</option>            
                </select>
            </div>
            <div>
                <label for="selectSesion" class="cprimario">sesión:</label>
                <select id="selectSesion" name="selectSesion" class="bsecundario">
                  <option value="0">Seleccionar</option>         
                </select>
            </div>
        </form>
        <div class="center mt30">
            <!--<span>¡Explore los recursos disponibles aquí!</span>-->
        </div>
        <div class="center" style="width: 290px; text-align: right;">
            <button id="btnExecute" class="btnJec bprimario" style="font-weight:100">iniciar <span style="font-weight:900">clase</span></button>
        </div>
    </main>
  <script type="text/javascript" src="../recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="../recursos/js/jsDocente/mainIniciaClase.js"></script>

</body>
</html>
