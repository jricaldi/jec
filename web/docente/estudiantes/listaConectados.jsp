<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/estudiantes/estudiantes.css">
  </head> 
<body>
    <c:if test="${not empty estudiantes}">
        <table id="listaConectados">
            <thead>
              <tr>
                  <th>CÓDIGO</th>
                  <th>NOMBRES</th>
                  <th>APELLIDO PATERNO</th>
                  <th>APELLIDO MATERNO</th>
                  <th>ESTADO</th>
              </tr>
            </thead>
            <tbody>
            <c:forEach var="estudiante" items="${estudiantes}">
                <tr>
                    <td>${estudiante.codEstudiante}</td>
                    <td>${estudiante.nombre}</td>
                    <td>${estudiante.apellidoPaterno}</td>
                    <td>${estudiante.apellidoMaterno}</td>
                    <td>${estudiante.estado=="0"?"<span class='bold red-text'>INACTIVO</span>":"<span class='bold green-text'>ACTIVO</span>"}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</body>
</html>
