<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script src="recursos/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="recursos/js/jquery-ui/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
       
        <link rel="stylesheet" href="recursos/css/bootstrap.min.css">
       
        <script type="text/javascript" src="recursos/js/jquery-ui/jquery-ui.js"></script>
        <script src="recursos/js/fileupload/jquery.ajaxfileupload.js" type="text/javascript"></script>
        <script src="recursos/js/jsDocente/mainListaAlumnosJs.js" type="text/javascript" ></script>
        <link rel="stylesheet" href="recursos/css/style.css">
        <style type="text/css">
            .centered {
                width: 100%;
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }
        </style>
    </head>
    <body class="contFrame">      
        <form id="form">
            <c:if test="${empty usuarioClase}">
                <span>No Hay una Clase activa</span>
            </c:if>
            <c:if test="${not empty usuarioClase}">
                
                <div id="listaAlumnos" style="margin-left: 10px;margin-right: 10px;margin-top: 15px;">
                    <c:if test="${not empty estudiantes}">
                        <table border="1" style="width: 100%;    border-color: #023158;    border-style: solid;    border-width: 6px;">
                            <thead>
                                <tr>
                                    <th style="background: #023158;text-align: center;">
                                        <label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">C&oacute;digo</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                         <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">Nombres</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                         <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Apellido Paterno</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                        <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita"> Apellido Materno</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                         <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita">Usuario</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                        <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita"> Constraseña</label>
                                    </th>
                                    <th style="background: #023158;text-align: left;">
                                        <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px"  class="blancoNegrita"> Estado</label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="estudiante" items="${estudiantes}">
                                    <tr>
                                        <td style="text-align: center">
                                            <label style="margin-top: 15px; margin-bottom: 15px" class="blancoNegrita"> ${estudiante.codEstudiante}</label>
                                        </td>
                                        <td >
                                           <label style="margin-left: 10px;margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">  ${estudiante.nombre}</label>
                                        </td>
                                        <td>
                                            <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">${estudiante.apellidoPaterno}</label>
                                        </td>
                                        <td>
                                           <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">  ${estudiante.apellidoMaterno}</label>
                                        </td>
                                        <td>
                                           <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">  ${estudiante.usuario}</label>
                                        </td>
                                        <td>
                                           <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">  ${estudiante.password}</label>
                                        </td>
                                        <td>
                                           <label style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px" class="blancoNegrita">  ${estudiante.estado=="0"?"Inact.":"Activo"}</label>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                </div>
            </c:if>
                <table border="0" style="margin: 0 auto;">
                    <tr>
                        <td style="padding: 10px">
                            <input class ="styleBotonRecur" type="button" value="Nuevo Alumno" onclick="nuevoAlumno()" class="boton-default"/>
                        </td>
                        <!--<td>
                            <input class ="styleBotonRecur" type="button" value="Listar Alumnos" onclick="listarAlumnos()" class="boton-default"/>
                        </td>-->                        
                    </tr>
                </table>

                <div id="nuevoAlumno" style="display: none">

                </div>
                <br><b style="margin-left: 25px" class="blancoNegrita">Carga archivo XLS de alumnos</b>
                <div class="centered">
                                
                                <input style="margin-left: 25px" type="file" name="file" /><br />
                                <div style="margin-left: 25px; display: none; " id="upload" ><label class="blancoNegrita">Cargando..</label></div>
                                <div style="color: white; margin-left: 25px" id="message"></div>
                            </div>
        </form>
    </body>
</html>
