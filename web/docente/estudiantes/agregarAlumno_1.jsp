<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
         <link href="recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <script src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script src="recursos/js/fileupload/jquery.ajaxfileupload.js" type="text/javascript"></script>
         <script src="recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>   
        <script type="text/javascript" src="recursos/js/jsDocente/mainListaAlumnosJs.js"></script>
        <script type="text/javascript" src="recursos/js/jsDocente/nuevoALumno.js"></script>
 
    </head>
    <body>
        <table border="0" style="margin: 0 auto;">
    <tr>
        <td>
            <label class="blancoNegrita">Nombres</label>     
        </td>
        <td style="padding: 5px">
            <input class="form-control" id="txtNombres" name="txtNombres" />
        </td>
    </tr>
    <tr>
        <td>
            <label class="blancoNegrita">Apellido Paterno</label>
        </td>
        <td style="padding: 5px">
            <input class="form-control" id="txtAp" name="txtAp" />
        </td>
    </tr>
    <tr>
        <td >
            <label class="blancoNegrita">Apellido Materno</label>
        </td>
        <td style="padding: 5px">
            <input class="form-control" id="txtAm" name="txtAm" />
        </td>
    </tr>
    <tr>
        <td>
            <label class="blancoNegrita"> Codigo Estudiante</label>
        </td>
        <td style="padding: 5px">
            <input class="form-control" id="txtCodigoEstudiante" name="txtCodigoEstudiante" />
        </td>
    </tr>
    <tr>
        <td>
            <label class="blancoNegrita">Genero</label>
        </td>
        <td style="padding: 5px">
            <select class="selectpicker5" id="genero" name="genero">
                <option value="M">Masculino</option>
                <option value="F">Femenino</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <label class="blancoNegrita">Dni</label>
        </td>
        <td style="padding: 5px">
            <input class="form-control" id="txtDni" name="txtDni" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input class="styleBotonRecur" type="button" name="btnAgregarAlumno" value="Agregar Alumno" onclick="agregarAlumno()"/>
            <input class="styleBotonRecur" type="button" name="btnAgregarCancelar" value="Cancelar" onclick="cancelarAgregarAlumno()"/>
        </td>
    </tr>
    
</table>
        
    </body>
</html>
