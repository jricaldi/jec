<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_docente.css">
  </head> 
<body>
    <c:if test="${not empty estudiantes}">
        <table class="responsive-table striped centered">
            <thead>
              <tr>
                  <th data-field="id">Código</th>
                  <th data-field="name">Nombres</th>
                  <th data-field="paterno">Apellido Paterno</th>
                  <th data-field="materno">Apellido Materno</th>
<!--                          <th data-field="usuario">Usuario</th>
                  <th data-field="password">Constraseña</th>-->
                  <th data-field="estado">Estado</th>
              </tr>
            </thead>
            <tbody>
            <c:forEach var="estudiante" items="${estudiantes}">
                <tr>
                    <td>${estudiante.codEstudiante}</td>
                    <td>${estudiante.nombre}</td>
                    <td>${estudiante.apellidoPaterno}</td>
                    <td>${estudiante.apellidoMaterno}</td>
<!--                            <td>${estudiante.usuario}</td>
                    <td>${estudiante.password}</td>-->
                    <td>${estudiante.estado=="0"?"<span class='bold red-text'>Inactivo</span>":"<span class='bold green-text'>Activo</span>"}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    
    <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript">
        resize();

        function resize(){
             window.parent.$("#listaConectados").css("height",$("body").outerHeight(true));
        };
    </script>
</body>
</html>
