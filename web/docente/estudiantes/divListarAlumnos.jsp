<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty estudiantes}">
    <table class="tabla-simple" style="width: 100%">
        <thead>
            <tr>
                <th>
                    <!--<input type="button" value="Cambiar Password" onclick=""  />-->
                </th>
                <th>
                    C&oacute;digo
                </th>
                <th>
                    Nombres
                </th>
                <th>
                    Apellido Paterno
                </th>
                <th>
                    Apellido Materno
                </th>
                <th>
                    Usuario
                </th>
                <th>
                    Constraseña
                </th>
                <th>
                    Estado
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="estudiante" items="${estudiantes}">
                <tr>
                    <td>

                    </td>
                    <td>
                        ${estudiante.codEstudiante}
                    </td>
                    <td>
                        ${estudiante.nombre}
                    </td>
                    <td>
                        ${estudiante.apellidoPaterno}
                    </td>
                    <td>
                        ${estudiante.apellidoMaterno}
                    </td>
                    <td>
                        ${estudiante.usuario}
                    </td>
                    <td>
                        ${estudiante.password}
                    </td>
                    <td>
                        ${estudiante.estado=="0"?"Inact.":"Activo"}
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</c:if>
