<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-sca le=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_docente.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/estudiantes/estudiantes.css">
  </head> 
<body>
    <header>
        <h1 class="titulo cprimario">
            <img src="recursos/img/icons/estudiantes_morado_ico.png"/> estudiantes
        </h1>
    </header>
    <main style="height: 295px; border-top: 15px solid #fff; border-bottom: 15px solid #fff; box-sizing: border-box;">
        <div id="divOpciones">
            <p id="msgTexto"></p>
            <p>
                -La columna “estado”” indica qué alumnos están conectados a la sesión.            
            </p>
            <button class="btnJec bprimario" onclick="nuevoAlumno()">agregar estudiante&nbsp;&nbsp;&nbsp;+</button>
            <button class="btnJec bprimario" onclick="variosAlumnos()">agregar varios&nbsp;&nbsp;&nbsp;+ </button>
        </div>
        <div class="hide" id="divAgregar">
            <div id="nuevoAlumno">

            </div>
        </div>
        <div class="hide" id="divVarios">
            <p class="cprimario tituloSecundario" style="text-transform: uppercase; color: #2D174C; font-weight: bolder; font-size: 18px;">
                <span style="font-size:25px;">+</span> agregar varios
            </p>
            <div class="block">
                <input id="txtFilename" class="caja iblock">
                <input type="file" name="file" id="file">
                <label id="lblFile" for="file" class="btnJec bprimario iblock">ruta archivo</label>
            </div>
            <div class="block" style="width: 484px;">
                <button id="btnCancelar" class="block btnJecSecundario borPrimario cprimario" onclick="cancelar()" style="margin-left:0px; float:right;">Cancelar</button>
            </div>
        </div>
        <a id="btnListarConectados" class="hide" href="docente_listarConectados" target="listaConectados"></a>
        <div id="divLista">
            <iframe name="listaConectados" id="listaConectados"></iframe>   
        </div>
    </main>
    
  <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="recursos/js/fileupload/jquery.ajaxfileupload.js"></script>
  <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
  <script type="text/javascript" src="recursos/js/jsDocente/mainListaAlumnosJs.js"></script>
</body>
</html>
