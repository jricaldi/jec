<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head> 
<body>
    <section>
        <div class="row primerDivCentrado" >
            <form class="col s12 sinPadLat" id="form">
                <label for="txtNombres" class="" style="margin: 10px 0 15px 5px; font-size: 16px; color: #2D174B; font-weight:900">+  Agregar Estudiante</label>
                <div class="row">
                    <!--<div class="input-field col s12 m5 l3">
                        <input id="txtNombres" name="txtNombres" type="text" required> <label for="txtNombres" class="center-align">Nombres</label>
                    </div>-->
                    <div class="">
                        <input id="txtNombres" name="txtNombres" placeholder="Nombres" type="text" required style="background-color:#EFEDF3; border:none; margin-bottom: 15px; width:300px; padding:10px; padding-left:10px; margin-left:5px"> <!--<label for="txtNombres" class="center-align">Nombres</label>-->
                    </div>
                </div>
                <div class="row">
                    <!--<div class="input-field col s12 m5 l3">
                        <input id="txtAp" name="txtAp" type="text" required> <label for="txtAp" class="center-align">Apellido Paterno</label>
                    </div>-->
                    <div class="">
                        <input id="txtAp" name="txtAp" placeholder="Apellido Paterno" type="text" required style="background-color:#EFEDF3; border:none; margin-bottom: 15px; width:300px; padding:10px; padding-left:10px; margin-left:5px"> <!--<label for="txtAp" class="center-align">Apellido Paterno</label>-->
                    </div>
                </div>
                <div class="row">
                    <!--<div class="input-field col s12 m5 l3">
                        <input id="txtAm" name="txtAm" type="text" required> <label for="txtAm" class="center-align">Apellido Materno</label>
                    </div>-->
                    <div class="">
                        <input id="txtAm" name="txtAm" placeholder="Apellido Materno" type="text" required style="background-color:#EFEDF3; border:none; margin-bottom: 15px; width:300px; padding:10px; padding-left:10px; margin-left:5px"> <!--<label for="txtAm" class="center-align">Apellido Materno</label>-->
                    </div>
                </div>
                <div class="row">
                    <!--<div class="input-field col s12 m5 l3">
                        <input id="txtCodigoEstudiante" name="txtCodigoEstudiante" type="text" required> <label for="txtCodigoEstudiante" class="center-align">Código Estudiante</label>
                    </div>-->
                    <div class="">
                        <input id="txtCodigoEstudiante" name="txtCodigoEstudiante" placeholder="Código Estudiante" type="text" required style="background-color:#EFEDF3; border:none; margin-bottom: 15px; width:300px; padding:10px; padding-left:10px; margin-left:5px"> <!--<label for="txtCodigoEstudiante" class="center-align">Código Estudiante</label>-->
                    </div>
                </div>
                 <div class="row">
                    <div class="col s12 m5 l3">
                     <!--<label>Género</label>-->
                     <select id="genero" name="genero" class="browser-default" required style="color:#A3A2A6; background-color:#EFEDF3; border:none; margin-bottom: 15px; width:320px; padding:10px; padding-left:10px; margin-left:5px">
                        <option value="M">Masculino</option>
                        <option value="F">Femenino</option>      
                     </select>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="input-field col s12 m5 l3">
                        <input id="txtDni" name="txtDni" type="text" required> <label for="txtDni" class="center-align">DNI</label>
                    </div>-->
                    <div class="">
                        <input id="txtDni" name="txtDni" placeholder="DNI" type="text" required style="background-color:#EFEDF3; border:none; margin-bottom: 15px; width:300px; padding:10px; padding-left:10px; margin-left:5px"> <!--<label for="txtDni" class="center-align">DNI</label>-->
                    </div>
                </div>
                <div class="">
                    <div class="" style="width: 320px; background-color: red; margin-left: 5px;">
                        <button class="btnAgregar" style="float:right; color: white;border:none;  cursor: pointer; padding: 10px 40px 5px 40px; text-transform:uppercase; border-radius: 6px;background-color: #2D174B; font-weight: 400" onclick="agregarAlumno()">Agregar  +</button>
                        <button class="btnCancelar" style="float:left;border: 1px solid #2C164B; cursor: pointer;color: #2D174B; background-color: #fff;padding: 9px 35px 4px 35px; text-transform:uppercase; border-radius: 6px; margin-left: 6px" onclick="cancelar()">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</body>
</html>
