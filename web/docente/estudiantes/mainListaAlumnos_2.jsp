<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun.css">
  </head> 
<body style="padding-bottom: 550px">
    <section class="centradoYColorSection">
        <c:if test="${empty usuarioClase}">
                <span>No Hay una Clase activa</span>
        </c:if>
        <c:if test="${not empty usuarioClase}">
            <div class="row primerDivCentrado" style="margin-bottom: 0">
                <div class="col s12">
                    <div class="col s12 m4 titulo">
                        <h5>Estudiantes</h5>
                    </div>
                    <div class="col m8">&nbsp;</div>
                </div>

                <hr style="margin-bottom: 50px;">

                <div class="col s12 marBot40" id="divOpciones">
                    <button class="btn waves-effect colorMorado marTop5" onclick="nuevoAlumno()">Agregar&nbsp;Estudiante</button>
                    <button class="btn waves-effect colorMorado marTop5" onclick="variosAlumnos()">Agregar&nbsp;Varios</button>
                </div>
            </div>
            <div class="row hide" id="divAgregar">
                <div class="col s12" id="nuevoAlumno">
                    
                </div>
            </div>
            
            <div class="row hide primerDivCentrado" id="divVarios">
                <div class="col s12"><p class="vertical"><i class="material-icons yellow-text text-accent-4 vertical">info</i> Una vez que escoga el archivo XLS este será subido al sistema automáticamente</p></div>
                <div class="file-field input-field col s12 m8 l6" >
                    <div class="btn colorMorado">
                      <span style="text-transform: capitalize">Ruta Archivo</span>
                      <input id="txtRuta" name="txtRuta" type="file" name="file" required>
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text">
                    </div>  
                </div>
                <div class="col s12 center marTop40">
                    <button id="btnCancelar" class="btn waves-effect waves-light waves-red btn red" style="background-color:#d32f2f !important;" onclick="cancelar()">Cancelar</button>
                </div>  
            </div>
            
            <div class="row center">
                <div class="col s12">
                    <p class="center red-text hide" id="msgError"></p>
                    <p class="center teal-text hide" id="msgOk"></p>
                </div>
                <div id="cargando" class="preloader-wrapper small active hide">
                    <div class="spinner-layer spinner-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                        </div>
                    </div>

                    <div class="spinner-layer spinner-green">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                        </div>
                    </div>

                    <div class="spinner-layer spinner-yellow">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                        </div>
                    </div>

                    <div class="spinner-layer spinner-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                        </div><div class="circle-clipper right">
                        <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </c:if>  
        <a id="btnListarConectados" class="hide" href="docente_listarConectados" target="listaConectados"></a>
        <div id="divLista">
            <iframe name="listaConectados" id="listaConectados" style="border: none; width: 100%"></iframe>   
        </div>
        
        
    </section>
  <script type="text/javascript" src="recursos/js/jquery/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="recursos/js/fileupload/jquery.ajaxfileupload.js"></script>
  <script type="text/javascript" src="recursos/js/jquery-simulate/simulate.js"></script>
  <script type="text/javascript" src="recursos/js/jsDocente/mainListaAlumnosJs.js"></script>
</body>
</html>
