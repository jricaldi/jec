<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="recursos/js/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="recursos/js/jquery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="recursos/js/jquery-ui/jquery-ui.js"></script>
        <script src="recursos/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="recursos/css/cssUtils/cssDocente/inicioDocente/inicioDocente.css">
        <link href="recursos/b_select/css/bootstrap-select.min.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="recursos/css/bootstrap.min.css">
       <script src="recursos/b_select/js/bootstrap-select.js" type="text/javascript"></script>    
        <script src="recursos/b_select/js/bootstrap-select.min.js" type="text/javascript"></script>  
        
        <script type="text/javascript" src="recursos/js/jsDocente/mainAplicacionesJs.js"></script>
    </head>
    <body class="contFrame">
        <div style="margin-top: 35px;    margin-left: 80px;" class="container-fluid">           
  <div class="row">
     
           <!-- <table>
              <thead>
                    <tr>
                        <th>
                            Aplicaciones
                        </th>
                    </tr>
                </thead>-->
                <c:if test="${not empty listaAplicaciones}">
                    <!--<tbody>-->
                        <c:forEach items="${listaAplicaciones}" var="aplicacion">
                             <div class="col-xs-6">
                           <!-- <tr>
                                <td style="height: 40px;">-->
                                    <a href="#" onclick="abrirAplicacion(${aplicacion.idAplicacion})" >
                                        <img src="recursos/img/aplicaciones/${aplicacion.nombreAplicacion}.png" style="border: 0; width: 45px; margin: 8px;">
                                        <label class="blancoNegrita"> ${aplicacion.nombreAplicacion}</label>
                                    </a>
                            <!--    </td>
                            </tr>-->
                            </div>                      
                        </c:forEach>
                  <!--  </tbody>-->
                </c:if>
          <!--  </table>-->
     
  </div>
</div>
        <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <div style="text-align: center;margin-bottom: 20px" >
            <input class="styleBotonRecur" type="button" value="Registrar Aplicación" onclick="abrirRegistroApp()" />
        </div>
        <div style="display: none" id="divNuevoRegistro" >
            <form name="frmRegistro" id="frmRegistro" method="POST" 
                  action="RegistrarNuevaAplicacion" 
                  onsubmit="return validarFormulario();"
                  enctype="multipart/form-data">
                <table border="0" style="margin: 0 auto;">
                    <tr>
                        <td style="padding: 10px">
                            <label style="margin-left: 15px;" class="blancoNegrita">Tipo de Aplicación</label>
                        </td>
                        <td>
                            <select class="selectpicker4" id="tipoAplicacion" name="tipoAplicacion">
                                <option value="1">Programa</option>
                                <option value="2">Multimedia</option>
                                <option value="3">Html</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px">
                            <label style="margin-left: 15px;" class="blancoNegrita">Nombre de Aplicacion</label>
                        </td>
                        <td>
                            <input class="form-control" id="nombreAplicacion" name="nombreAplicacion" type="text" value="" maxlength="45" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px">
                            <label style="margin-left: 15px;" class="blancoNegrita"> Extensiones </label>
                        </td>
                        <td>
                            <input class="form-control" id="extensiones" name="extensiones" type="text" value="" maxlength="250" />
                        </td>
                    </tr>
                    <tr>
                        <td ><label style="margin-left: 15px;" class="blancoNegrita">Ruta Imagen</label></td>
                        <td style="padding: 5px">
                            <input class="form-control" type="file" name="file" name="txtRuta" id="txtRuta" required="true" accept="image/png"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px">
                            <label style="margin-left: 15px;" class="blancoNegrita"> Estado</label>
                        </td>
                        <td>
                            <select  class="selectpicker3" id="idEstado" name="idEstado">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px; text-align: center">
                            <input class="styleBoton" type="submit" value="Registrar"/>
                        </td>
                        <td style="padding: 15px; text-align: center">
                            <input class="styleBoton" type="button" value="Cancelar" onclick="cancelarRegistro()"/>
                        </td>
                    </tr>                    
                </table>
            </form>
        </div>
        </c:if>
    </body>
</html>
