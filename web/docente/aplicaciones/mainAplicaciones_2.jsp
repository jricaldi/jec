<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jornada Escolar Completa</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="recursos/css/materialize/font.css" rel="stylesheet">
    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/materialize/materialize.css">
    </c:if>
    
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
          <link rel="stylesheet" href="recursos/css/materialize/materialize_1.css">
    </c:if>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun.css">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/aplicaciones/aplicaciones.css">
  </head> 
<body>
    <section class="centradoYColorSection">
        <div class="row primerDivCentrado" style="margin-bottom: 0px">
            <div class="col s12">
                <div class="col s12 m4 titulo">
                    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">                    
                        <h5 class="colorMoradoTexto">Aplicaciones</h5>
                    </c:if>
                    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                        <h5 class="colorCelesteTexto">Aplicaciones</h5>
                    </c:if>
                </div>
                <div class="col m8">&nbsp;</div>
            </div>
            
            <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                  <hr class="colorMoradoHR" style="margin-bottom: 50px;">
            </c:if>
             
            <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                 <hr class="colorCelesteHR" style="margin-bottom: 50px;">
            </c:if>
           
        <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
          
            <div class="col s12 m4 marBot40" id="divBtnAgregar">
                <button id="btnAgregarNuevo" class="btn waves-effect colorMorado" onclick="abrirRegistroApp()">Agregar&nbsp;Aplicación</button>
            </div>
            <form class="col s12 sinPadLat hide" id="divNuevo" action="RegistrarNuevaAplicacion" enctype="multipart/form-data">
                <div class="row">
                    <div class="col s12 m5 l3">
                        <label>Tipo de Aplicación</label>
                        <select id="tipoAplicacion" name="tipoAplicacion" class="browser-default" required>
                            <option value="1">Programa</option>
                            <option value="2">Multimedia</option>
                            <option value="3">Html</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <input id="nombreAplicacion" name="nombreAplicacion" type="text" required> <label for="nombreAplicacion" class="center-align">Nombre de Aplicación</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m5 l3">
                        <input id="extensiones" name="extensiones" type="text" placeholder="(Opcional)"> <label for="extensiones" class="center-align" >Extenciones</label>
                    </div>
                </div>
                <div class="row">
                    <div class="file-field input-field col s12 m8 l6">
                     <div class="btn colorMorado">
                       <span style="text-transform: capitalize">Ruta Imagen</span>
                       <input id="txtRuta" name="txtRuta" type="file" accept="image/png" required> 
                     </div>
                     <div class="file-path-wrapper">
                         <input class="file-path validate" type="text" placeholder="La imagen debe estar en formato png">
                     </div>
                   </div>
                </div>
                <div class="row">
                    <div class="col s12 m5 l3">
                        <label>Estado</label>
                        <select id="idEstado" name="idEstado" class="browser-default" required>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 center">
                        <button id="btnExecute" type="submit" class="btn waves-effect colorMorado">Agregar</button>
                        <button id="btnCancelar" class="btn waves-effect waves-light waves-red btn red" style="background-color:#d32f2f !important;" onclick="cancelarRegistro()">Cancelar</button>
                    </div>
                </div>
            </form>
        </c:if>
        <c:if test="${not empty listaAplicaciones}">
            
            <div class="col s12 sinPadLat" id="divRecursos">
                <div class="col s12 sinPadLat">
                    <ul class="collection">
                        <c:forEach items="${listaAplicaciones}" var="aplicacion">
                            <li class="collection-item avatar">
                              <!--<img src="images/yuna.jpg" alt="" class="circle">-->
                              <div class="row">
                                  <div class="col s5">
                                      <img src="recursos/img/aplicaciones/${aplicacion.nombreAplicacion}.png" alt="Sin imagen" class="circle" style="border:1px solid #F2F2F2;"
                                           onerror="this.onerror=null;this.src='recursos/img/aplicaciones/noImagen.png';">
                                      <span class="title mayus">${aplicacion.nombreAplicacion}</span>
                                      <p>
                                          <c:if test="${aplicacion.tipoAplicacion=='1'}">Programa</c:if>
                                          <c:if test="${aplicacion.tipoAplicacion=='2'}">Multimedia</c:if>
                                          <c:if test="${aplicacion.tipoAplicacion=='3'}">Html</c:if>
                                      </p>
                                  </div>
                                  <div class="col s7 opciones">
                                    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                                        <button class="btn waves-effect colorMorado" onclick="abrirAplicacion(${aplicacion.idAplicacion},'local')">Abrir&nbsp;Local</button>
                                        <button class="btn waves-effect colorMorado" onclick="abrirAplicacion(${aplicacion.idAplicacion},'todos')">Abrir&nbsp;Todos</button>
                                    </c:if>
                                    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                                        <button class="btn waves-effect colorCeleste" onclick="abrirAplicacion(${aplicacion.idAplicacion},'local')">Abrir</button>
                                    </c:if>
                                  </div>
                              </div>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </c:if>
        </div>
    </section>
  <script src="recursos/js/jquery/jquery-2.1.4.js"></script>
  <script src="recursos/js/materialize/materialize.js"></script>
  <script type="text/javascript" src="recursos/js/jsDocente/aplicaciones.js"></script>

</body>
</html>
