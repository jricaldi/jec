<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendizaje virtual JEC</title>
    <meta name="description" content="Jornada Escolar Completa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/normalize.css" type="text/css"/>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/fonts.css"/>
    <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_alunmo.css">
    </c:if>
    <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
        <link rel="stylesheet" href="recursos/css/cssUtils/comun/comun_docente.css">
    </c:if>
    <link rel="stylesheet" href="recursos/css/cssUtils/comun/aplicaciones/aplicaciones.css">
  </head> 
<body>
    <header>
        <h1 class="titulo cprimarioa">
            <img src=""/> aplicaciones
        </h1>
    </header>
    <main style="height: 295px; border-top: 15px solid #fff; border-bottom: 15px solid #fff; box-sizing: border-box;">
        <p>
            <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                 - Seleccione “abrir” para iniciar el aplicativo solo en su computadora.
            </c:if>
            <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                - Seleccione “abrir local” para iniciar el aplicativo solo en su computadora.<br/>
                - Seleccione “abrir todos” para iniciar el aplicativo en todas las computadoras del salón.
            </c:if>
        </p>
        <div id="listaAplicaciones">
            <c:if test="${not empty listaAplicaciones}">
                <c:forEach items="${listaAplicaciones}" var="aplicacion">
                    <div class="fila">
                        <div class="aplicacion bbd">
                            <img class="iblock va" src="recursos/img/aplicaciones/${aplicacion.nombreAplicacion}.png"
                            onerror="this.onerror=null;this.src='recursos/img/aplicaciones/noImagen.png';"/>
                            <div class="iblock va">
                                <span class="mayuscula block hideOver titulo">${aplicacion.nombreAplicacion}</span>
                                <span class="block capitalize nombreAplicacion">
                                    <c:if test="${aplicacion.tipoAplicacion=='1'}">Programa</c:if>
                                    <c:if test="${aplicacion.tipoAplicacion=='2'}">Multimedia</c:if>
                                    <c:if test="${aplicacion.tipoAplicacion=='3'}">Html</c:if>
                                </span>
                            </div>
                        </div>
                        <div class="botones center">
                            <c:if test="${usuario.perfil==PERFIL_DOCENTE}">
                                <button class="btnJec bprimario hoverb" onclick="abrirAplicacion(${aplicacion.idAplicacion},'local')">abrir local</button>
                                <button class="btnJec bprimario ml15 hoverb" onclick="abrirAplicacion(${aplicacion.idAplicacion},'todos')">abrir todos</button>
                            </c:if>
                            <c:if test="${usuario.perfil!=PERFIL_DOCENTE}">
                                <button class="btnJec bprimario hoverb" nclick="abrirAplicacion(${aplicacion.idAplicacion},'local')">abrir</button>
                            </c:if>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </main>
    <script src="recursos/js/jquery/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="recursos/js/jsDocente/aplicaciones.js"></script>

</body>
</html>
